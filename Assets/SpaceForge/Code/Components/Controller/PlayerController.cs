﻿using UnityEngine;
using System.Collections;

namespace SpaceForge {

	/// <summary>
	/// Player Controller takes input from the player and applies it to that players Unit; A Sapien, or a Vessel that Sapien is controlling.
	/// </summary>
	[DisallowMultipleComponent]
	public class PlayerController : SpaceComponent {

	}

}