using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceForge {

	public interface IUnitSerializedEvent {

		void CreateEvents();

	}

}