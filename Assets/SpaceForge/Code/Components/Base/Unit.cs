﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceForge.Utilities;

namespace SpaceForge {


	/// <summary>
	/// A 'unit' is any entity that either affects or is affected by gameplay. Runners, Obstacles, Missiles, Finishlines, ecetera. 
	/// 
	/// Unit's act as the 'Entity' in an Entity-Component-System, where properties act as the Components.
	/// </summary>
	[DisallowMultipleComponent, SelectionBase]
	public abstract class Unit : SpaceComponent {

		#region Unity Methods

		protected virtual void Awake()
		{
			if (Application.isPlaying)
				colliders = UnitComponents<Collider2D> ();
		}

		#endregion

		#region Colliders

		Collider2D[] colliders;

		public void SetCollidersEnabled(bool value)
		{
			foreach (var coll in colliders)
				coll.enabled = value;
		}

		public bool IsColliding(Collider2D with)
		{
			bool colliding = false;
			foreach (var coll in colliders) {
				if (coll.IsColliding(with)) {
					colliding = true;
					break;
				}
			}

			return colliding;
		}

		public void SetCollisionsEnabled(Collider2D with, bool value)
		{
			foreach (var coll in colliders) {
				if (value)
					coll.EnableCollisions(with);
				else
					coll.DisableCollisions(with);
			}
		}

		#endregion

		#region Convienience

		/// <summary>
		/// Returns all Components on this units gameObject, or it's sub gameObjects
		/// </summary>
		/// <returns>The components.</returns>
		/// <param name="each">An action to be performed on each component.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public T[] UnitComponents<T> (Action<T> each = null) where T : Component {

			var list = new List<T> ();
			list.AddRange (GetComponents<T> ());

			foreach (Transform child in transform) 
				AddChildUnitComponentsToList<T> (child, list);

			if (each != null) {
				foreach (T component in list)
					each(component);
			}

			return list.ToArray ();
		}

		void AddChildUnitComponentsToList<T>(Transform child, List<T> results) where T : Component {

			//If this child has a unit component, we dont want any components from it, because it's a seperate unit.
			if (child.GetComponent<Unit> ())
				return;

			results.AddRange (child.GetComponents<T> ());

			foreach (Transform subChild in child)
				AddChildUnitComponentsToList<T> (subChild, results);

		}

		[ContextMenu("Refresh Serialized Events")]
		internal void RefreshSerializedEvents()
		{
			foreach (var property in GetComponents<IUnitSerializedEvent>())
				property.CreateEvents ();
		}

		#endregion


	}

}