using UnityEngine;

namespace SpaceForge {

	[ExecuteInEditMode,
	DisallowMultipleComponent,
	RequireComponent(typeof(MeshRenderer)),
	RequireComponent(typeof(MeshFilter))]
	public class Level : SpaceComponent {

	}

}