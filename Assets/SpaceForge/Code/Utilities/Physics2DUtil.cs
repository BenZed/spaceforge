using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceForge.Utilities {

	static class Physics2DUtil {
		
		public static bool IsColliding(this Collider2D collider2D, Collider2D other) {
			return !Physics2D.GetIgnoreCollision (collider2D, other);
		}

		public static void EnableCollisions(this Collider2D collider2D, Collider2D other) {
			if (!collider2D.IsColliding (other))
				Physics2D.IgnoreCollision (collider2D, other, false);

		}

		public static void DisableCollisions(this Collider2D collider2D, Collider2D other) {
			if (collider2D.IsColliding (other))
				Physics2D.IgnoreCollision (collider2D, other, true);

		}
	}

}