﻿using UnityEngine;
using UnityEngine.Events;

namespace BossMedia.Components {

	/// <summary>
	/// The Timer component executes a custom event every set interval.
	/// Timer can loop.
	/// </summary>
	public class Timer : MonoBehaviour {
		
		#region Inspector
		
		//[SerializeField] is what makes fields editable in the inspector. Look up Unity serialization for more information.
		//Any public field that CAN be serialized is serialized by default. We don't want the interval property to be public,
		//because if other scripts or code are going to change the interval property at runtime, we want to be able to validate it.
		//See the OnValidate method and Interval property below.
		[SerializeField] float interval = 1f;
		
		public bool removeOnExpire = false;
		
		//A unity event is something that can be set in the inspector or programatically.
		//You can tell an event to run one function, many, or none. 
		public UnityEvent onExpire;
		
		#endregion
		
		#region Runtime Data
		
		float remaining;
		
		#endregion
		
		#region Unity Callers
		
		//OnValidate is called when changes are made in the inspector.
		//We don't want the interval to be below zero, so if a user
		//sets it as such, we fix it here.
		void OnValidate()
		{
			if (interval < 0f)
				interval = 0f;
		}
		
		void Start()
		{
			remaining = interval;
		}
		
		void Update()
		{
			remaining -= Time.deltaTime;
			
			if (remaining > 0f)
				return;
			
			onExpire.Invoke ();
			
			//Maybe a timer is only intended to execute once. If so, we'll disable it to ensure the update function
			//wont run again next frame, and we'll Destroy the component.
			if (removeOnExpire) {
				enabled = false;
				Destroy (this);
				
			} else     //Otherwise we reset the amount remaining, and continue as normal
				remaining = interval;
		}
		
		#endregion
		
		#region API
		
		public float Interval {
			get {
				return interval;
			}
			set {
				interval = value;
				
				//Call OnValidate manually during runtime.
				OnValidate();
			}
		}
		
		public float Remaining {
			get {
				return remaining < 0f ? 0f : remaining;
			}
		}
		
		//We want to be able to pause the timer by disabling it. If a component is 'enabled' it will run
		//all of it's Unity-nested callers, like Update, Start, LateUpdate, etcetera. If you disable a timer, it
		//will be paused weather or not you call the pause function, disable the component,
		//disable the gameObject or disable the gameObjects parent, ect. As such, the following are just convenience
		//properties and methods. They're the same as changing the 'enabled' member.
		public bool Paused {
			get {
				return !enabled;
			}
		}
		
		public void Pause()
		{
			if (Paused)
				Debug.LogWarning ("Timer on " + name + " is already paused.");
			
			enabled = false;
		}
		
		public void Unpause()
		{
			if (!Paused)
				Debug.LogWarning ("Timer on " + name + " is already running.");
			
			enabled = true;
		}
		
		//This is a convienience method that we can use to create timers quickly. A UnityAction can either be a method
		//or a lambda expression. Add Lambdas to your list of things to learn about, they're very handy.
		public static Timer Create(GameObject gameObject, float interval, UnityAction onExpire, bool destroyOnExpire = false)
		{
			var timer = gameObject.AddComponent<Timer> ();
			timer.interval = interval;
			timer.removeOnExpire = destroyOnExpire;
			
			timer.onExpire = new UnityEvent ();
			timer.onExpire.AddListener (onExpire);
			
			return timer;
		}
		
		#endregion
		
	}

}