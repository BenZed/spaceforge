using UnityEngine;
using System.Collections;

namespace BossMedia.Extensions {
	
	public static class Geometry2D {

		/// <summary>
		/// Gets the angle between the two points, regardless of orientation.
		/// </summary>
		/// <returns>The angle.</returns>
		/// <param name="vector2">Vector2.</param>
		/// <param name="other">Other.</param>
		public static float PointAngle(this Vector2 from, Vector2 to) 
		{
			return Mathf.Atan2 (to.y - from.y, to.x - from.x) * Mathf.Rad2Deg;
		}

		/// <summary>
		/// Clamps a Vector to a given magnitude.
		/// </summary>
		/// <returns>The vector.</returns>
		/// <param name="value">Value.</param>
		/// <param name="magnitude">Magnitude.</param>
		public static Vector2 Clamp(this Vector2 vector, float magnitude = 1f, bool above = true)
		{
			float sqrMag = magnitude * magnitude;
			bool required = above ? vector.sqrMagnitude > sqrMag : vector.sqrMagnitude < sqrMag;

			return required ? vector.normalized * magnitude : vector;
		}

		/// <summary>
		/// Rotates a Vector a given number of degrees
		/// </summary>
		/// <returns>The vector.</returns>
		/// <param name="value">Value.</param>
		/// <param name="magnitude">Magnitude.</param>
		public static Vector2 Rotate(this Vector2 vector, float degrees) {

			float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
			float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);
			
			float tx = vector.x;
			float ty = vector.y;

			vector.x = (cos * tx) - (sin * ty);
			vector.y = (sin * tx) + (cos * ty);

			return vector;
		}

		/// <summary>
		/// If this edge and another edge were both infinite lines, where would they intersect?
		/// </summary>
		/// <returns>The line intersection.</returns>
		/// <param name="otherEdge">Other edge.</param>
		public static Vector2 Intersection(Vector2 aStart, Vector2 aEnd, Vector2 bStart, Vector2 bEnd)
		{
			// Get A,B,C of this edge as a line
			float a1 = aEnd.y - aStart.y;
			float b1 = aStart.x - aEnd.x;
			float c1 = a1 * aStart.x + b1 * aStart.y;
			
			// Get A,B,C of other edge as a line
			float a2 = bEnd.y - bStart.y;
			float b2 = bStart.x - bEnd.x;
			float c2 = a2 *bStart.x + b2 * bStart.y;
			
			// Get delta and check if the lines are parallel
			float delta = a1 * b2 - a2 * b1;
			if (delta == 0f)
				throw new UnityException("Supplied Vector2s describe paralell lines.");
			
			// now return the Vector2 intersection point
			return new Vector2( (b2 * c1 - b1 * c2) / delta, (a1 * c2 - a2 * c1) / delta );
		}
			
		/// <summary>
		/// If there was a circle of radius surrounding this point, this returns a point offset by the given angle, on that imaginary circle.
		/// </summary>
		/// <returns>The on circle.</returns>
		/// <param name="origin">Origin.</param>
		/// <param name="radius">Radius.</param>
		/// <param name="angle">Angle.</param>
		public static Vector2 PointOnCircle(Vector2 origin, float radius, float angle) 
		{
			float x = radius * Mathf.Cos (angle * Mathf.Deg2Rad) + origin.x;
			float y = radius * Mathf.Sin (angle * Mathf.Deg2Rad) + origin.y;

			return new Vector2(x,y);
		}

		/// <summary>
		/// Returns the angular size of a circle of diameter at toPoint, compared to this point
		/// </summary>
		/// <returns>The size.</returns>
		/// <param name="fromPoint">From point.</param>
		/// <param name="toPoint">To point.</param>
		/// <param name="diameter">Diameter.</param>
		public static float AngularSize (Vector2 fromPoint, Vector2 toPoint, float diameter) 
		{
			float distance = Vector2.Distance(fromPoint, toPoint);
			return (diameter / distance) * Mathf.Rad2Deg;
		}
	}
}