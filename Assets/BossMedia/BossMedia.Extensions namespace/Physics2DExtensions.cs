﻿using UnityEngine;
using System.Collections;

namespace BossMedia.Extensions {

	static class Physics2DExtensions {

		/// <summary>
		/// Adds an amount of force that will get the rigidbody to speed, but no more.
		/// </summary>
		/// <param name="rigidbody">Rigidbody.</param>
		/// <param name="speed">Desired speed.</param>
		public static void AddForceToSpeed(this Rigidbody2D rigidbody, float speed) {
			Vector2 forwardSpeed = (-rigidbody.transform.right * speed);

			Vector2 force = (forwardSpeed.normalized * rigidbody.mass * speed);
			force = (rigidbody.drag * force) / (1f - 0.02f * rigidbody.drag);

			rigidbody.AddForce(force);

		}

		public static float AddTorqueToAngularVelocity(this Rigidbody2D rigidbody2D, float torque, ForceMode2D forceMode = ForceMode2D.Force) {
			return rigidbody2D.AddForceAtPositionToAngularVelocity (new Vector2 (-torque, 0f), new Vector2 (1f, 1f), forceMode);
		}
		
		public  static float AddForceAtPositionToAngularVelocity(this Rigidbody2D rigidbody2D, Vector2 force, Vector2 position, ForceMode2D forceMode = ForceMode2D.Force) {
			// Vector from the force position to the CoM
			Vector2 p = rigidbody2D.worldCenterOfMass - position;
			
			// Get the angle between the force and the vector from position to CoM
			float angle = Mathf.Atan2(p.y, p.x) - Mathf.Atan2(force.y, force.x);
			
			// This is basically like Vector3.Cross, but in 2D, hence giving just a scalar value instead of a Vector3
			float t = p.magnitude * force.magnitude * Mathf.Sin(angle) * Mathf.Rad2Deg;
			
			// Continuous force
			if (forceMode == ForceMode2D.Force) t *= Time.fixedDeltaTime;
			
			// Apply inertia
			return t / rigidbody2D.inertia;
		}

		public static void AddForceToTargetVelocity(this Rigidbody2D rigidbody, Vector2 targetVelocity) {

			Vector2 force = (targetVelocity.normalized * rigidbody.mass * targetVelocity.magnitude);
			force = (rigidbody.drag * force) / (1f - 0.02f * rigidbody.drag);
			
			rigidbody.AddForce(force);
		}

		
		public static Vector2 GetRelativeVelocity(this Rigidbody2D body, Rigidbody2D other)
		{
			return (body.velocity - other.velocity);
		}
		
		public static Vector2 GetRelativeVelocityAtPoint(this Rigidbody2D body, Rigidbody2D other, Vector3 point)
		{
			return (body.GetPointVelocity(point) - other.GetPointVelocity(point));
		}

		public static bool IsColliding(this Collider2D collider2D, Collider2D other) 
		{
			return !Physics2D.GetIgnoreCollision (collider2D, other);
		}
		
		public static void EnableCollisions(this Collider2D collider2D, Collider2D other) 
		{
			Physics2D.IgnoreCollision (collider2D, other, false);
		}
		
		public static void DisableCollisions(this Collider2D collider2D, Collider2D other) 
		{
			Physics2D.IgnoreCollision (collider2D, other, true);
		}
	}
}