using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace BossMedia {

	[Serializable]
	public class Grid<T> : ISerializationCallbackReceiver {

		#region Data
		
		int?[,] cells;
		
		[SerializeField]
		List<T> values = new List<T>();
		
		[HideInInspector][SerializeField]
		List<int> indexes = new List<int>();
		
		[HideInInspector][SerializeField]
		List<Coord> coords = new List<Coord>();
		
		#endregion
		
		#region API

		[HideInInspector][SerializeField] Coord _min;
		public Coord min { 
			get { 
				return (cells == null) ? Coord.zero : _min; 
			} 
		} 

		[HideInInspector][SerializeField] Coord _max;
		public Coord max { 
			get { 
				return (cells == null) ? Coord.zero : _max; 
			} 
		} 
		
		public int columns { get { return (cells == null) ? 0 : cells.GetLength (0); } }
		public int rows    { get { return (cells == null) ? 0 : cells.GetLength (1); } }
		
		public int Count {
			get {
				return values.Count;
			}
		}
		
		public T[] ToArray()
		{
			return values.ToArray();
		}
		
		public void ForEach(Action<T, Coord> action)
		{
			if (action == null)
				throw new ArgumentNullException ("action");
			
			for (int i = 0; i < indexes.Count; i++) {
				var index = indexes[i];
				var value = values [index];
				var coord = coords [index];
				action (value, coord);
			}
		}
		
		public void ForEach(Action<T> action)
		{
			if (action == null)
				throw new ArgumentNullException ("action");
			
			foreach (var value in values)
				action (value);
		}
		
		public void Add(T value, Coord coord)
		{
			//Create a tile if one doesn't exist there already
			if (Occupied(coord))
				throw new UnityException(coord + " is already taken.");
			
			int index = indexes.Count;
			
			coords.Add (coord);
			values.Add (value);
			indexes.Add (index);
			
			if (AddValueBoundsAltered (coord))
				CreateCellsFromBounds();
			
			SetCell(coord, index);
		}
		
		public void Move(Coord from, Coord to)
		{
			if (!Occupied(from))
				throw new UnityException("Cannot move from "+from+", coordiantes empty.");
			
			if (Occupied(to))
				throw new UnityException("Cannot move from "+from+" to "+to+", coordinates already taken.");
			
			MoveCell(GetCell(from), to);
		}
		
		public T[] Find(Predicate<T> match) 
		{
			var found = new List<T> ();
			
			if (match == null)
				return found.ToArray();
			
			for (int i = 0; i < values.Count; i++) {
				var value = values [i];
				if (match(value))
					found.Add (value);
			}
			
			return found.ToArray ();
		}
		
		public T[] Find(Func<T, Coord, bool> match) {
			var found = new List<T> ();
			
			if (match == null)
				return found.ToArray();
			
			for (int i = 0; i < indexes.Count; i++) {
				var index = indexes [i];
				var value = values [index];
				var coord = coords [index];
				
				if (match(value, coord))
					found.Add (value);
			}
			
			return found.ToArray ();
		}
		
		
		public bool Occupied(Coord coord)
		{
			return GetCell (coord).HasValue;
		}
		
		public void Remove(Coord coord) 
		{
			var cell = GetCell (coord);
			if (!cell.HasValue)
				throw new UnityException("There is nothing at " + coord);
			
			coords.RemoveAt(cell.Value);
			values.RemoveAt(cell.Value);
			indexes.RemoveAt(cell.Value);
			
			if (values.Count == 0)
				cells = null;
			
			else {
				SetCell (coord, null);
				for(var i = cell.Value; i< indexes.Count; i++) {
					indexes[i]--;
					SetCell(coords[indexes[i]], indexes[i]);
				}
			}
			
			if (RemoveValueAltersBounds(coord))
				CreateCellsFromBounds();
		}
		
		public T Get(Coord coord)
		{
			var cell = GetCell(coord);
			if (!cell.HasValue)
				throw new UnityException ("Nothing at " + coord);
			
			return values [cell.Value];
			
		}

		public void Clear()
		{
			cells = null;
			values.Clear ();
			indexes.Clear ();
			coords.Clear ();
		}

		public T GetRandom()
		{
			if (values.Count == 0)
				throw new UnityException ("Nothing in Grid.");

			var index = UnityEngine.Random.Range (0, values.Count);
			return values [index];
		}
		
		#endregion
		
		#region Cell Helper 
		
		void MoveCell(int? cell, Coord to)
		{	
			Coord from = coords [cell.Value];
			coords[cell.Value] = to;
			
			SetCell(from, null);
			if (RemoveValueAltersBounds(from) | AddValueBoundsAltered(to))
				CreateCellsFromBounds();
			
			SetCell(to, cell);
		}
		
		Coord GetCellCoord(Coord coord)
		{
			return coord - min;
		}
		
		Coord GetCellCoord(Coord coord, out bool withinBounds)
		{	
			var cellCoord = GetCellCoord (coord);
			
			withinBounds = 
				!(cells == null || 
				  cellCoord.column < 0 || cellCoord.column >= columns ||
				  cellCoord.row < 0 || cellCoord.row >= rows);
			
			return cellCoord;
		}
		
		int? GetCell(Coord coord)
		{	
			bool withinBounds;
			Coord cellCoord = GetCellCoord (coord, out withinBounds);
			
			if (withinBounds)
				return cells[cellCoord.column, cellCoord.row];
			else
				return null;
		}
		
		void SetCell(Coord coord, int? cell)
		{
			Coord cellCoord = GetCellCoord (coord);
			cells[cellCoord.column, cellCoord.row] = cell;
		}
		
		#endregion
		
		#region Cells Analyzation
		
		bool RequiresAxisShift(Coord coord, bool checkingRows)
		{
			if (checkingRows && coord.row != _min.row && coord.row != _max.row)
				return false;
			
			else if (!checkingRows && coord.column != _min.column && coord.column != _max.column)
				return false;
			
			int shiftLow = 0;
			int shiftHi = 0;
			
			bool minFound = false, maxFound = false;
			while (!minFound || !maxFound) {
				
				var max = checkingRows ? columns : rows;
				for (var i = 0; i < max; i++) {
					
					int lowX = checkingRows ? i : shiftLow;
					int lowY = checkingRows ? shiftLow : i;
					
					int hiX = checkingRows ? i : shiftHi + (columns - 1);
					int hiY = checkingRows ? shiftHi + (rows - 1) : i;
					
					if (cells[lowX, lowY].HasValue && !minFound)
						minFound = true;
					
					if (cells[hiX, hiY].HasValue && !maxFound)
						maxFound = true;
					
				}
				
				if (!minFound)
					shiftLow++;
				
				if (!maxFound)
					shiftHi--;
				
			}
			
			if (checkingRows) {
				_min.row += shiftLow;
				_max.row += shiftHi;
				
			} else {
				_min.column += shiftLow;
				_max.column += shiftHi;
				
			}
			
			return shiftLow != 0 || shiftHi != 0;
		}
		
		bool RemoveValueAltersBounds(Coord coord)
		{
			//If there isn't a grid yet, we don't have to do anything
			if (cells == null)
				return false;
			
			//Check to see if any rows or columns are empty. 
			var needed = (RequiresAxisShift(coord, checkingRows: false) | RequiresAxisShift(coord, checkingRows: true));
			
			return needed;
		}
		
		bool AddValueBoundsAltered(Coord coord)
		{	
			//Create a new map if it doesn't exist yet, then leave
			if (cells == null) {
				_min = coord;
				_max = coord;
				
				cells = new int?[1, 1];
				
				return false;
			}
			
			//The map doesn't need rebuilding
			if (coord.column >= _min.column && coord.row >= _min.row && 
			    coord.column <= _max.column && coord.row <= _max.row)
				return false;
			
			if (coord.column > _max.column)
				_max.column = coord.column;
			
			if (coord.row > _max.row)
				_max.row = coord.row;
			
			if (coord.column < _min.column)
				_min.column = coord.column;
			
			if (coord.row < _min.row)
				_min.row = coord.row;
			
			return true;
		}
		
		#endregion
		
		#region Cells Creation
		
		void CreateCellsFromBounds() 
		{
			var newCells = new int?[(_max.column - _min.column) + 1, (_max.row - _min.row) + 1];
			for (int i = 0; i < coords.Count; i++) {
				var newCellCoords = coords [i] - min;
				newCells [newCellCoords.x, newCellCoords.y] = indexes[i];
			}
			
			cells = newCells;
		}

		void CreateCellsFromSerializedList()
		{
			bool requiresSerialization = (cells == null && indexes != null && indexes.Count > 0);
			if (!requiresSerialization)
				return;
			
			int[] existingIndexes = new int[indexes.Count];
			T[] existingValues = new T[values.Count];
			Coord[] existingCoords = new Coord[indexes.Count];
			
			indexes.CopyTo(existingIndexes);
			values.CopyTo(existingValues);
			coords.CopyTo(existingCoords);
			
			indexes = new List<int>();
			values = new List<T>();
			coords = new List<Coord>();
			
			foreach (var index in existingIndexes)
				Add (existingValues[index], existingCoords[index]);
			
		}

		#endregion

		#region ISerializationCallbackReceiver implementation

		void EnsureMultiCellArrayFromLists()
		{
			//If the value is a non-serializable type or cells has already been created, then we don't need to serialize.
			if (values == null || cells != null)
				return;
			
			int[] existingIndexes = new int[indexes.Count];
			T[] existingValues = new T[values.Count];
			Coord[] existingCoords = new Coord[indexes.Count];
			
			indexes.CopyTo(existingIndexes);
			values.CopyTo(existingValues);
			coords.CopyTo(existingCoords);
			
			indexes = new List<int>();
			values = new List<T>();
			coords = new List<Coord>();
			
			foreach (var index in existingIndexes)
				Add (existingValues[index], existingCoords[index]);
		}

		public void OnBeforeSerialize ()
		{
			EnsureMultiCellArrayFromLists ();
		}

		public void OnAfterDeserialize ()
		{
			EnsureMultiCellArrayFromLists ();
		}

		#endregion
		
	}
}