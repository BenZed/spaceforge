﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BossMedia.Editor {

	public static class Validate {

		public static ComponentValidator Component(UnityEngine.Component component)
		{
			return new ComponentValidator (component);
		}

		public class ComponentValidator {

			Component component;
			string componentName;

			string Name {
				get {
					return component.gameObject.name + "-" + componentName;
				}
			}

			public ComponentValidator(Component component)
			{
				if (!component)
					throw new ArgumentNullException ("component");

				componentName = component.GetType().Name;

				this.component = component;
			}

			List<string> errors;

			#region Member Cant Be Null

			string MemberIsNull(string memberName = null)
			{
				return Name + (memberName == null ? " has a null member." : " member '" + memberName + "' is null.");
			}

			public ComponentValidator MemberAssigned(Object member,Action<string> handler = null)
			{
				CheckForFail (member != null, handler, MemberIsNull());

				return this;
			}

			#endregion

			#region Requires Component

			string MissingComponent<T>() where T : Component
			{
				return Name + " is missing a required component: " + (typeof(T)).Name;
			}

			public ComponentValidator RequiresComponent<T>(Action<string> handler = null) where T : Component
			{

				CheckForFail (component.GetComponent<T>() != null, handler ?? (str => {

					component.gameObject.AddComponent<T>();

				}) , MissingComponent<T>());

				
				return this;
			}
			#endregion

			#region Component Conflict

			string ConflictedComponent<T>() where T : Component
			{
				return Name + " has a component which conflicts: " + (typeof(T)).Name;
			}
			
			public ComponentValidator ComponentConflict<T>(Action<string> handler = null) where T : Component
			{
				T conflict = component.GetComponent<T> ();
				CheckForFail (conflict == null, handler, ConflictedComponent<T>());

				return this;
			}
			#endregion

			void CheckForFail(bool passed, Action<string> handler, string message)
			{
				if (passed)
					return;

				if (handler == null) {
					handler = (msg) => {
						var behaviour = component as Behaviour;
						if (behaviour && behaviour.isActiveAndEnabled)
							behaviour.enabled = false;

						throw new UnityException (msg);
					};
				}

				handler(message);
			}

			public static implicit operator bool (ComponentValidator validator)
			{
				return validator != null;
			}
		}
	}
}