﻿using UnityEngine;
using UnityEngine.Events;
using BossMedia.Extensions;

namespace BossMedia.Editor {

	[ExecuteInEditMode, DisallowMultipleComponent]
	public sealed class EditorEvents : MonoBehaviour {

		#if UNITY_EDITOR

		[SerializeField, HideInInspector] UnityEvent onAwake;
		[SerializeField, HideInInspector] UnityEvent onStart;
		[SerializeField, HideInInspector] UnityEvent onUpdate;
		[SerializeField, HideInInspector] UnityEvent onGUI;
		[SerializeField, HideInInspector] UnityEvent onDisable;
		[SerializeField, HideInInspector] UnityEvent onEnable;
		[SerializeField, HideInInspector] UnityEvent onDestroy;
		[SerializeField, HideInInspector] UnityEvent onTransformChildrenChanged;
		[SerializeField, HideInInspector] UnityEvent onTransformParentChanged;

		[SerializeField, HideInInspector] int numMonos;

		void RefreshEditorEvents()
		{
			onAwake = new UnityEvent ();
			onStart = new UnityEvent ();
			onUpdate = new UnityEvent ();
			onGUI = new UnityEvent ();
			onDisable = new UnityEvent ();
			onEnable = new UnityEvent ();
			onDestroy = new UnityEvent ();
			onTransformChildrenChanged = new UnityEvent ();
			onTransformParentChanged = new UnityEvent ();

			var monos = GetComponents<MonoBehaviour> ();
			int lastAddedIndex = 0;

			foreach (var mono in monos)  {
				if (mono == this)
					continue;

				lastAddedIndex = onAwake.AddPersistentListener(mono, "EditorAwake");
				if (lastAddedIndex >= 0)
					onAwake.SetPersistentListenerState(lastAddedIndex, UnityEventCallState.EditorAndRuntime);

				lastAddedIndex = onStart.AddPersistentListener(mono, "EditorStart");
				if (lastAddedIndex >= 0)
					onStart.SetPersistentListenerState(lastAddedIndex, UnityEventCallState.EditorAndRuntime);

				lastAddedIndex = onUpdate.AddPersistentListener(mono, "EditorUpdate");
				if (lastAddedIndex >= 0)
					onUpdate.SetPersistentListenerState(lastAddedIndex, UnityEventCallState.EditorAndRuntime);

				lastAddedIndex = onGUI.AddPersistentListener(mono, "EditorGUI");
				if (lastAddedIndex >= 0)
					onGUI.SetPersistentListenerState(lastAddedIndex, UnityEventCallState.EditorAndRuntime);
				
				lastAddedIndex = onDisable.AddPersistentListener(mono, "EditorGameObjectDisabled");
				if (lastAddedIndex >= 0)
					onDisable.SetPersistentListenerState(lastAddedIndex, UnityEventCallState.EditorAndRuntime);

				lastAddedIndex = onEnable.AddPersistentListener(mono, "EditorGameObjectEnabled");
				if (lastAddedIndex >= 0)
					onEnable.SetPersistentListenerState(lastAddedIndex, UnityEventCallState.EditorAndRuntime);
				
				lastAddedIndex = onDestroy.AddPersistentListener(mono, "EditorDestroy");
				if (lastAddedIndex >= 0)
					onDestroy.SetPersistentListenerState(lastAddedIndex, UnityEventCallState.EditorAndRuntime);

				lastAddedIndex = onTransformChildrenChanged.AddPersistentListener(mono, "EditorTransformChildrenChanged");
				if (lastAddedIndex >= 0)
					onTransformChildrenChanged.SetPersistentListenerState(lastAddedIndex, UnityEventCallState.EditorAndRuntime);
				
				lastAddedIndex = onTransformParentChanged.AddPersistentListener(mono, "EditorTransformParentChanged");
				if (lastAddedIndex >= 0)
					onTransformParentChanged.SetPersistentListenerState(lastAddedIndex, UnityEventCallState.EditorAndRuntime);

			}

			numMonos = monos.Length - 1;
		}

		void Reset()
		{
			RefreshEditorEvents ();
		}

		void Awake()
		{
			RefreshEditorEvents ();
			if (!Application.isPlaying)
				onAwake.Invoke ();
		}

		void Start()
		{
			if (!Application.isPlaying)
				onStart.Invoke ();
		}

		void Update()
		{
			if (Application.isPlaying)
				return;

			if (numMonos != GetComponents<MonoBehaviour> ().Length - 1)
				RefreshEditorEvents ();

			onUpdate.Invoke ();
		}

		void OnGui()
		{
			if (!Application.isPlaying)
				onGUI.Invoke ();
		}

		void OnTransformChildrenChanged()
		{
			if (!Application.isPlaying)
				onTransformChildrenChanged.Invoke ();
		}

		void OnTransformParentChanged()
		{
			if (!Application.isPlaying)
				onTransformParentChanged.Invoke ();
		}

		void OnDisable()
		{
			if (Application.isPlaying || gameObject.activeInHierarchy)
				return;
			
			onDisable.Invoke ();
		}

		void OnEnable()
		{
			if (Application.isPlaying || !gameObject.activeInHierarchy)
				return;

			onEnable.Invoke ();
		}

		void OnDestroy()
		{
			if (!Application.isPlaying)
				onDestroy.Invoke ();
		}

		#else
		void Awake()
		{
			Destroy(this);
		}
		#endif
	}

}