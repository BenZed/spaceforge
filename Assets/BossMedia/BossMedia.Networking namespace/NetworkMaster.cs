using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections.Generic;

namespace BossMedia {

	[RequireComponent(typeof(NetworkIdentity))]
	public abstract class NetworkMaster : NetworkBehaviour {

		NetworkIdentity _identity;
		public NetworkIdentity identity {
			get {
				return _identity ?? (_identity = GetComponent<NetworkIdentity>());
			}
		}

		NetworkSlave[] subscribers;

		#region Abstract

		protected abstract NetworkSlave[] GetSubscribers();

		protected abstract void RegisterDirtyBits (Func<uint> nextDirtyBit);

		#endregion

		#region Dirty Bits

		void RefreshDirtyBitsAndSubscribers()
		{
			var subscribers = GetSubscribers ();
			this.subscribers = (subscribers != null) ? subscribers : new NetworkSlave[0];

			nextDirtyBit = 1u;

			foreach (var slave in subscribers) {
				slave.master = this;
				slave.RegisterDirtyBits (NextDirtyBit);
			}

			RegisterDirtyBits (NextDirtyBit);
		}

		internal bool DirtyBitIsSet(uint bit)
		{
			return (syncVarDirtyBits & bit) != 0u;
		}
		
		uint receivedDirtyBits;
		internal bool ReceivedDirtyBitIsSet(uint bit)
		{
			return (receivedDirtyBits & bit) != 0u;
		}
		
		uint nextDirtyBit = 1u;
		uint NextDirtyBit()
		{
			bool atMax = nextDirtyBit == 0u;
			if (atMax)
				throw new UnityException ("Cannot Register Dirty bit for " + name + ", maximum number reached. (32)");
			
			uint bit = nextDirtyBit;
			
			nextDirtyBit *= 2u;

			return bit;
		}

		#endregion

		#region NetworkBehaviour overrides

		public override void OnStartClient ()
		{
			base.OnStartClient ();

			RefreshDirtyBitsAndSubscribers ();
			foreach (var slave in subscribers)
				slave.OnStartClient ();
		}

		public sealed override bool OnSerialize (NetworkWriter writer, bool initialState)
		{
			if (initialState) {
				foreach (var slave in subscribers)
					slave.OnInitialSerialize (writer);
				OnInitialSerialize(writer);

				return true;
			}
			
			writer.WritePackedUInt32 (syncVarDirtyBits);
			if (syncVarDirtyBits == 0u)
				return false;

			foreach (var slave in subscribers)
				slave.OnUpdateSerialize (writer);
			OnUpdateSerialize (writer);

			return true;
		}
		
		public sealed override void OnDeserialize (NetworkReader reader, bool initialState)
		{
			if (initialState) {
				RefreshDirtyBitsAndSubscribers ();

				foreach (var slave in subscribers)
					slave.OnInitialDeserialize (reader);
				OnInitialDeserialize (reader);

				return;
			}

			receivedDirtyBits = reader.ReadPackedUInt32 ();
			if (receivedDirtyBits == 0u)
				return;

			foreach (var slave in subscribers)
				slave.OnUpdateDeserialize (reader);
			OnUpdateDeserialize (reader);

		}

		internal abstract void OnInitialSerialize (NetworkWriter writer);
		
		internal abstract void OnInitialDeserialize (NetworkReader reader);
		
		internal abstract void OnUpdateSerialize (NetworkWriter writer);
		
		internal abstract void OnUpdateDeserialize (NetworkReader reader);

		#endregion
	}
}