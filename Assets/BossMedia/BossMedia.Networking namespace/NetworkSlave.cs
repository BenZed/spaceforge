﻿using System;
using UnityEngine;
using UnityEngine.Networking;

namespace BossMedia {

	public abstract class NetworkSlave : NetworkDummy {

		NetworkMaster _master;
		public NetworkMaster master {
			get {
				return _master;
		    }
			internal set {
				_master = value;
			}
		}

		public override bool isServer {
			get {
				return master && master.isServer;
			}
		}

		protected bool DirtyBitIsSet(uint bit)
		{
			return master && master.DirtyBitIsSet (bit);
		}
		
		protected bool ReceivedDirtyBitIsSet(uint bit)
		{
			return master && master.ReceivedDirtyBitIsSet (bit);
		}
		
		protected void SetDirtyBit(uint bit)
		{
			if (isServer)
				master.SetDirtyBit (bit);
		}

		internal virtual void OnStartClient() {}
		
		internal virtual void RegisterDirtyBits (Func<uint> nextDirtyBit) {}

		internal virtual void OnInitialSerialize (NetworkWriter writer) {}
		
		internal virtual void OnInitialDeserialize (NetworkReader reader) {}
		
		internal virtual void OnUpdateSerialize (NetworkWriter writer) {}
		
		internal virtual void OnUpdateDeserialize (NetworkReader reader) {}
		
	}

}
