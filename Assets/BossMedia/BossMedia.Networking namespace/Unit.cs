﻿using UnityEngine;

namespace BossMedia.Networking {

	public abstract class Unit : NetworkMaster {

		#region implemented abstract members of NetworkMaster

		protected override NetworkSlave[] GetSubscribers ()
		{
			throw new System.NotImplementedException ();
		}

		protected override void RegisterDirtyBits (System.Func<uint> nextDirtyBit)
		{
			throw new System.NotImplementedException ();
		}

		internal override void OnInitialSerialize (UnityEngine.Networking.NetworkWriter writer)
		{
			throw new System.NotImplementedException ();
		}

		internal override void OnInitialDeserialize (UnityEngine.Networking.NetworkReader reader)
		{
			throw new System.NotImplementedException ();
		}

		internal override void OnUpdateSerialize (UnityEngine.Networking.NetworkWriter writer)
		{
			throw new System.NotImplementedException ();
		}

		internal override void OnUpdateDeserialize (UnityEngine.Networking.NetworkReader reader)
		{
			throw new System.NotImplementedException ();
		}

		#endregion


	}

}