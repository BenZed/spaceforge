﻿using UnityEngine;
using UnityEngine.Networking;

namespace BossMedia {

	/// <summary>
	/// A NetworkDummy is something that can be Instantiated clientside on all clients, but has
	/// an isServer property that should be set when instantiated as host. The most obvious example
	/// is for projectiles, which arn't network synced, nut are instantiated for every client.
	/// They only make a difference, however, when they impact only matters serverside.
	/// </summary>
	public abstract class NetworkDummy : MonoBehaviour {

		bool _isServer;
		public virtual bool isServer {
			get {
				return _isServer;
			}
		}

		#region Static Creator

		public static T Create<T>(GameObject prefab, Transform at, bool isServer) where T : NetworkDummy
		{
			bool valid = prefab.GetComponent<T> ();

			if (!valid)
				throw new UnityException ("Prefab does not have a " + typeof(T).GetType ().Name + " Component.");

			var instance = (GameObject)Instantiate (prefab, at.position, at.rotation);
			var dummy = instance.GetComponent<T> ();
			dummy._isServer = isServer;

			return dummy;
		}

		public static T Add<T>(GameObject gameObject, bool isServer) where T : NetworkDummy 
		{
			T dummy = gameObject.AddComponent<T> ();
			dummy._isServer = isServer;

			return dummy;
		}
		#endregion
	}

}
