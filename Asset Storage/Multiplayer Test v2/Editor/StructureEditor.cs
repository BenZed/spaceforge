using UnityEngine;
using UnityEditor;
using UnityExtensions.Grid;
using System;
using System.Collections;
using System.Collections.Generic;

namespace SpaceForge.Structural {
	
	[CustomEditor(typeof(Structure))]
	public class StructureEditor : Editor {

		Structure structure;
		Cursor cursor;

		string error;

		#region Unity Callers

		void OnEnable()
		{
			structure = target as Structure;
			mouseDown = false;
		}

		bool mouseDown = false;

		void OnSceneGUI()
		{
			if (EnsureTool ()) {
				EnsureCursor ();
				MoveCursor ();
				UseCursor ();
			} 

			if ((Tools.current != Tool.None) && cursor)
				cursor.Hide();

		}

		void OnDisable()
		{
			if (cursor)
				cursor.Hide();
		}

		#endregion

		#region Helper


		void UseCursor()
		{
			var e = Event.current;
			int controlID = GUIUtility.GetControlID (FocusType.Passive);

			bool mouseEvent = false;

			if (e.type == EventType.mouseDown && e.button == 1) {
				cursor.ToggleTileType ();
				mouseEvent = true;

			} else if (e.type == EventType.mouseDown && e.button == 2) {
				cursor.TogglePaintType ();
				mouseEvent = true;

			} else if (e.type == EventType.mouseDown && e.button == 0) {
				mouseDown = true;
				mouseEvent = true;

			} else if (e.type == EventType.mouseUp && e.button == 0) {
				mouseDown = false;
				mouseEvent = true;
			} 

			if (mouseEvent) {
				GUIUtility.hotControl = mouseDown ? 0 : controlID;
				e.Use ();
			}

			cursor.Apply (mouseDown, e.shift);

		}

		bool EnsureTool() 
		{
			if (Tools.current == Tool.Rect || Tools.current == Tool.Scale)
				Tools.current = Tool.None;
									
			return Tools.current == Tool.None;
		}

		void EnsureCursor()
		{
			if (cursor == null)
				cursor = structure.transform.GetComponentInChildren<Cursor> ();

			if (cursor)
				cursor.Show ();

			else {
				cursor = Cursor.Create (structure);
			//	cursor.gameObject.hideFlags = HideFlags.HideInHierarchy;
			}

		}

		void MoveCursor()
		{
			Vector2 mousePos = Event.current.mousePosition;
			mousePos.y = Camera.current.pixelHeight - mousePos.y;
			Vector3 position = Camera.current.ScreenPointToRay(mousePos).origin;

			cursor.Move (position);
		}

		#endregion

	}
}