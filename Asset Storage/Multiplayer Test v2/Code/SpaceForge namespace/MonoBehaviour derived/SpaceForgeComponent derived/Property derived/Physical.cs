using UnityEngine;
using UnityExtensions;
using System.Collections;

namespace SpaceForge {

	[ExecuteInEditMode]
	[DisallowMultipleComponent]
	[AddComponentMenu("SpaceForge/Properties/Physical")]
	public class Physical : Property {

		#region Component Links

		Rigidbody2D _body;
		public Rigidbody2D body {
			get {
				return _body;
			}
		}

		#endregion

		[SerializeField]
		float mass;

		#if UNITY_EDITOR
		[SerializeField][ReadOnly]
		string measure = "";
		#endif

		[SerializeField]
		Vector3 startingVelocity;

		#region Unity Callers

		void OnEnable()
		{
			if (!body)
				_body = GetComponent<Rigidbody2D>();
			
			if (!body)
				_body = gameObject.AddComponent<Rigidbody2D> ();

			body.gravityScale =  		  0f;
			body.drag =				 	  0f;
			body.angularDrag = 			  0f;
			body.interpolation =		  RigidbodyInterpolation2D.None;
			body.sleepMode	 = 	 		  RigidbodySleepMode2D.StartAwake;
			body.collisionDetectionMode = CollisionDetectionMode2D.None;
			body.constraints = 			  RigidbodyConstraints2D.None;
		//	body.hideFlags = 			  HideFlags.HideInInspector;
			body.simulated = 			  true;
		}

		void OnDisable()
		{
			if (unit.isServer)
				body.simulated = false;
		}

		protected override void Start()
		{
			SetInitialVelocity ();
			base.Start ();
		}

		void FixedUpdate() 
		{
			if (mass != body.mass)
				mass = body.mass;

			HandleTranslationalDrag ();
			HandleAngularDrag ();
		}

		void OnCollisionEnter2D(Collision2D collision)
		{
			//Collision Point
			Vector2 point = Vector2.zero;
			foreach (var contact in collision.contacts)
				point += contact.point;
			point /= collision.contacts.Length;
			
			//Unit Event
			unit.HostOnCollision (point, collision.transform);

			//Damagable
			var damagable = collision.gameObject.GetComponent<Damagable> ();
			if (!damagable)
				return;

			float damage = (((collision.rigidbody.mass + body.mass) * 0.025f) * collision.relativeVelocity).sqrMagnitude / (float)collision.contacts.Length;
			if (damage < 1f)
				return;

			foreach(var contact in collision.contacts)
				damagable.Damage(damage, contact.point);

		}

		#endregion

		#region Editor Callers

		void OnValidate()
		{
			if (!Application.isPlaying && body)
				body.mass = mass;
		}

		#endregion

		#region Helper

		void SetInitialVelocity()
		{
			body.velocity = startingVelocity;
			body.angularVelocity = startingVelocity.z;
		}

		void HandleTranslationalDrag()
		{
			if (!body.simulated || body.isKinematic || !Application.isPlaying)
				return;
			
			#if UNITY_EDITOR
			float displaySpeed = Mathf.Round (body.velocity.magnitude * 100f) / 100f;
			measure = displaySpeed+"/mps ";
			#endif

			float thresh = constants.physical.MinSpeedDragThreshold;
			float sqrThresh = thresh * thresh;
			
			float sqrSpeed = body.velocity.sqrMagnitude;
			if (sqrSpeed <= sqrThresh) {
				body.drag = 0f;
				return;
			}
			
			float max = constants.physical.MaxSpeed;
			float minDrag = constants.physical.DragAtMinSpeed;
			float sqrMax = max * max;
			
			float maxDrag = constants.physical.DragAtMaxSpeed;
			body.drag = (((sqrSpeed - sqrThresh) / (sqrMax - sqrThresh)) * (maxDrag - minDrag)) + minDrag;

		}

		void HandleAngularDrag()
		{
			if (!body.simulated || body.isKinematic || !Application.isPlaying)
				return;

			#if UNITY_EDITOR
			float displaySpin = Mathf.Round (body.angularVelocity * 100f) / 100f;
			measure += displaySpin+"°";
			#endif

			float thresh = constants.physical.MinSpinDragThreshold;
			float spin = Mathf.Abs (body.angularVelocity);

			if (spin <= thresh) {
				body.angularDrag = 0f;
				return;
			}
			
			float max = constants.physical.MaxSpin;
			float maxDrag = constants.physical.DragAtMaxSpin;

			body.angularDrag = ((spin - thresh) / (max - thresh)) * maxDrag;			
		}

		#endregion

	}

}