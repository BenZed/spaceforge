﻿using UnityEngine;
using System.Collections;

namespace SpaceForge {

	public class Containable : Property {

		#region Nested Types

		public enum Size {
			Small,
			Medium,
			Large,
			Gigantic
		}

		#endregion

		[SerializeField]
		Size _size;

		public Size size {
			get {
				return _size;
			}
		}

		IContainer _container;
		public IContainer container {
			get {
				if (transform.parent == null)
					_container = null;

				else if (_container == null && transform.parent != null)
					_container = transform.parent.GetComponent<IContainer>();

				return _container;
			}
		}

		Physical _physical;
		public Physical physical {
			get {
				return _physical ?? ( _physical = GetComponent<Physical>());
			}
		}

		ShipContainableInterior _interior;
		internal ShipContainableInterior interior {
			get {
				return _interior ?? ( _interior = ShipContainableInterior.Create (this));
			}
		}

		#region Unity Callers
	
		void OnTriggerEnter2D(Collider2D trigger) 
		{
			if (container != null)
				return;

			Transform transform = trigger.transform;
			IContainer tContainer = null;
		
			while (transform != null) {
				tContainer = transform.GetComponent<IContainer>();
				if (tContainer != null)
					break;
				
				transform = transform.parent;
			}

			if (tContainer != null && (int)tContainer.MaximumSize >= (int)size)
				triggerContainer = tContainer;
		}

		void OnTriggerStay2D(Collider2D trigger)
		{
			if (triggerContainer == null || container != null)
				return;
		
			triggerContainer.Load (this);

		}

		void OnTriggerExit2D(Collider2D trigger)
		{
			triggerContainer = null;
		}

		void Update()
		{
			if (container == null)
				return;

			container.UnloadCheck (this);
		}

		protected override void Start()
		{
			if (!unit.isServer) {
				base.Start ();
				return;
			}

			if (!transform.parent)
				return;

			var startContainer = transform.parent.GetComponent<IContainer> ();
			if (startContainer != null)
				startContainer.LoadOnStart (this);
		}

		#endregion

		IContainer triggerContainer = null;

	}

}