using UnityEngine;
using UnityEngine.Events;
using UnityExtensions.Grid;
using UnityExtensions;
using System.Collections.Generic;
using System.Reflection;
using SpaceForge.Structural;

namespace SpaceForge {

	[AddComponentMenu("SpaceForge/Properties/Killable")]
	public class Mortal : Destructible {

		[SerializeField]
		Vital _health;

		public float health {
			get {
				return _health.Current;
			}
		}

		public float healthMax {
			get {
				return _health.Max;
			}
		}

		public bool isDead {
			get {
				return _health.isEmpty;
			}
		}

		internal override void Damage(float damageAmount, Vector2 origin)
		{
			if (!isDead)
				Hurt (damageAmount);

			base.Damage (damageAmount, origin);
		}

		/// <summary>
		/// Hurt is different than damage in the sense that it doesn't effect the units integrity. Only it's health.
		/// </summary>
		/// <param name="hurtAmount">Hurt amount.</param>
		internal void Hurt(float hurtAmount)
		{
			float overKill =  _health.Deplete (hurtAmount);
			if (isDead)
				unit.HostOnDeath (overKill);
		}

		internal void Heal(float healAmount)
		{
			if (isDead)
				return;

			_health.Replenish (healAmount);
		}

		#region Editor callers

		protected override void OnValidate()
		{
			base.OnValidate ();
			if (_health == null || _health.Max >= integrityMax - 5f || _health.Max <= 0f)
				_health = new Vital(integrityMax - 5f);

		}

		#endregion
	}

}