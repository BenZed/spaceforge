using UnityEngine;
using UnityEngine.Events;
using UnityExtensions.Grid;
using UnityExtensions;
using System.Collections.Generic;
using System.Reflection;
using SpaceForge.Structural;

namespace SpaceForge {

	[AddComponentMenu("SpaceForge/Properties/Destructible")]
	public class Destructible : Damagable {

		[SerializeField]
		Vital _integrity;

		public float integrity {
			get {
				return _integrity.Current;
			}
		}

		public float integrityMax {
			get {
				return _integrity.Max;
			}
		}

		public bool isDestroyed {
			get {
				return _integrity.isEmpty;
			}
		}

		internal override void Damage(float damageAmount, Vector2 origin)
		{
			if (isDestroyed)
				return;

			float overDestroy = _integrity.Deplete (damageAmount);
			if (isDestroyed)
				unit.HostOnDestruct (overDestroy);
			else
				base.Damage (damageAmount, origin);
		}

		#region Editor callers
		
		protected virtual void OnValidate()
		{
			if (_integrity == null || _integrity.Max <= 10f)
				_integrity = new Vital(10f);
		}
		
		#endregion
	}

}