using UnityEngine;
using UnityEngine.Events;
using UnityExtensions.Grid;
using UnityExtensions;
using System.Collections.Generic;
using System.Reflection;
using SpaceForge.Structural;

namespace SpaceForge {

	[DisallowMultipleComponent]
	[AddComponentMenu("SpaceForge/Properties/Damagable")]
	public class Damagable: Property {

		Structure structure;

		#region Unity Callers

		void Awake()
		{
			structure = GetComponent<Structure> ();
		}

		#endregion

		#region Main

		internal virtual void Damage(float damageAmount, Vector2 origin)
		{
			if (structure)
				structure.Damage (damageAmount, origin);
			else
				unit.HostOnDamage (damageAmount, origin);
		}

		#endregion
	
	}
}