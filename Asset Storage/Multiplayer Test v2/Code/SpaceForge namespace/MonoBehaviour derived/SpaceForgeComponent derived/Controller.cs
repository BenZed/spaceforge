using UnityEngine;
using System.Collections;

namespace SpaceForge {

	[DisallowMultipleComponent]
	public abstract class Controller : SpaceForgeComponent { }

}