using UnityEngine;
using UnityExtensions;
using System.Collections;

namespace SpaceForge {

	[DisallowMultipleComponent]
	[RequireComponent(typeof(Rigidbody2D))]
	internal sealed class ShipContainableInterior : SpaceForgeComponent {

		Rigidbody2D _body;
		public Rigidbody2D body {
			get {
				return _body ?? (_body = GetComponent<Rigidbody2D>());
			}
		}

		public static ShipContainableInterior Create(Containable containable) {
			
			GameObject newGameObject;

			var colliders = containable.transform.GetComponentsInChildren<Collider2D> ();
			containable.transform.gameObject.SetActive(false);

			newGameObject = new GameObject (containable.name+" (Interior)");
			newGameObject.SetActive(false);
			newGameObject.layer = Game.Constants.InteriorLayer;
			
			newGameObject.transform.parent = containable.transform;
			newGameObject.AddComponent<Rigidbody2D> ();

			foreach (Collider2D collider2D in colliders) {
				var boxCollider = collider2D as BoxCollider2D;
				if (boxCollider) {
					var newBoxCollider = newGameObject.AddComponent<BoxCollider2D>();
					newBoxCollider.isTrigger = boxCollider.isTrigger;
					newBoxCollider.size = boxCollider.size;
					newBoxCollider.offset = boxCollider.offset;

					continue;
				}

				var circleCollider = collider2D as CircleCollider2D;
				if (circleCollider) {
					var newCircleCollider = newGameObject.AddComponent<CircleCollider2D>();
					newCircleCollider.isTrigger = circleCollider.isTrigger;
					newCircleCollider.offset = circleCollider.offset;
					newCircleCollider.radius = circleCollider.radius;
					continue;
				}

				var polygonCollider = collider2D as PolygonCollider2D;
				if (polygonCollider) {
					var newPolygonCollider = newGameObject.AddComponent<PolygonCollider2D>();
					newPolygonCollider.isTrigger = polygonCollider.isTrigger;
					newPolygonCollider.pathCount = polygonCollider.pathCount;
					for(var i = 0; i < newPolygonCollider.pathCount; i++)
						newPolygonCollider.SetPath(i, newPolygonCollider.GetPath(i));

					continue;
				}
			}
						
			var interior = newGameObject.AddComponent<ShipContainableInterior>();

			interior.containable = containable;
			interior.body.drag = 8f;//Game.manager.constants.InteriorDrag;
			interior.body.angularDrag = 8f;//Game.manager.constants.InteriorAngularDrag;
			interior.body.gravityScale = 0f;

			containable.gameObject.SetActive(true);
			
			return interior;
			
		}
		
		Containable _containable;
		public Containable containable {
			get {
				return _containable; 
			}
			private set {
				_containable = value;
			}
		}

		public bool Active {
			get {
				return transform.parent != _containable.transform;
			}
		}
		
		public void Enable(Ship ship) 
		{
			gameObject.SetActive(true);

			transform.parent = ship.interior.transform;
			transform.localPosition = ship.transform.InverseTransformPoint(containable.transform.position);
			transform.localRotation = containable.transform.localRotation;

		 	ship.interior.gameObject.SetActive(true);
		}
		
		public void Disable() 
		{
			transform.parent = containable.transform;
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;

			var shipBody = containable.container.physical.body;
			containable.physical.body.velocity = body.velocity.Rotate (shipBody.rotation) + shipBody.GetPointVelocity (containable.physical.body.position);
			containable.physical.body.angularVelocity = body.angularVelocity;
			
			gameObject.SetActive(false);
		}

		#region Unity Callers
		
		void OnCollisionEnter2D(Collision2D collision) {

			Debug.Log (name+" Collision!");

		}
		
		void Update() {

			//Update position of unit to match this representation
			if (_containable != null) {

				_containable.transform.localPosition = transform.localPosition;
				_containable.transform.localRotation = transform.localRotation;

			} else
				Destroy (gameObject);

		}


		#endregion

	}

}