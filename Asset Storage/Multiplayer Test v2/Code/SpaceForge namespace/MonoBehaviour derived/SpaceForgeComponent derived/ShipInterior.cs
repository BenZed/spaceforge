using UnityEngine;
using UnityExtensions;
using System.Collections;
using System.Collections.Generic;
using SpaceForge.Structural;

namespace SpaceForge {

	internal sealed class ShipInterior : SpaceForgeComponent {

		internal struct SectionInterior {

			public PolygonCollider2D collider;
			public Transform transform;

			public SectionInterior(ShipInterior parent, Section section) {
				collider = new GameObject(section.transform.name+" (Interior)").AddComponent<PolygonCollider2D>();
				transform = collider.transform;
				transform.parent = parent.transform;
				transform.localPosition = Vector3.zero;
				transform.localRotation = Quaternion.identity;
				transform.gameObject.layer = Game.Constants.InteriorLayer;
			}

		}

		Ship ship;

		public static ShipInterior Create(Ship ship) {

			var shipInterior = new GameObject (ship.transform.name+" (Interior)").AddComponent<ShipInterior> ();
			shipInterior.ship = ship;
			shipInterior.gameObject.layer = Game.Constants.InteriorLayer;
			shipInterior.transform.gameObject.SetActive(false);
			shipInterior.transform.SetSiblingIndex (ship.transform.GetSiblingIndex ());

			return shipInterior;

		}

		#region Unity Callers

		void Start() 
		{
			if (ship == null) {
				Destroy (this.gameObject);

				throw new UnityException ("Structure Interiors should only be added by the StructuralInterior.Create method.");
			}

			ship.structure.ForEachSection(section => UpdateWallColliders (section));
		}

		#endregion

		Dictionary<Section, SectionInterior> interiors = new Dictionary<Section, SectionInterior>();

		internal void UpdateWallColliders(Section section)
		{
			if (!interiors.ContainsKey(section))
				interiors.Add(section, new SectionInterior(this, section));

			var interior = interiors[section];
			var pathCount = section.walls.poly.pathCount;

			interior.transform.localPosition = section.transform.localPosition;
			interior.collider.pathCount = pathCount;
			for(var i = 0; i < pathCount; i++)
				interior.collider.SetPath(i, section.walls.poly.GetPath(i));
		}

	}
	
}