﻿using UnityEngine;
using System.Collections;

namespace SpaceForge {

	[RequireComponent(typeof(Unit))]
	public abstract class Property : SpaceForgeComponent {

		#region Game Constants Link

		internal static Game.Constants constants {
			get {
				return Game.manager.constants;
			}
		}

		#endregion

		Unit _unit;
		public Unit unit {
			get {
				return _unit ?? (_unit = GetComponent<Unit>());
			}
		}

		protected virtual void Start()
		{
			if (!unit.isServer && Application.isPlaying)
				Destroy (this);
		}

	}
}