using UnityEngine;
using UnityExtensions;
using UnityEngine.Networking;
using System.Collections;
using SpaceForge.Structural;

namespace SpaceForge {

	[AddComponentMenu("SpaceForge/Human")]
	public class Human : Unit {

		Containable _containable;
		public Containable containable {
			get {
				return _containable ?? (_containable = GetComponent<Containable>());
			}
		}

		Physical _physical;
		public Physical physical {
			get {
				return _physical ?? (_physical = GetComponent<Physical>());
			}
		}

		Mortal _mortal;
		public Mortal mortal {
			get {
				return _mortal ?? (_mortal = GetComponent<Mortal>());
			}
		}

		SpriteRenderer _spriter;
		public SpriteRenderer spriter {
			get {
				return _spriter ?? (_spriter = GetComponentInChildren<SpriteRenderer>());
			}
		}
		#region Property Overrides

		protected override void OnDeath (float overage)
		{
			base.OnDeath (overage);
			spriter.color = Color.gray;
		}

		#endregion

		#region Commands

		public void Move(Vector2 moveAxis)
		{
			if (!containable.interior.Active || (mortal && mortal.isDead))
				return;

			var body = containable.interior.body;
			body.AddForce((body.transform.right * -moveAxis.y + body.transform.up * moveAxis.x) * 10f);
		}

		public void Look(Vector2 lookPoint)
		{
			if (mortal && mortal.isDead)
				return;

			var worldPoint = lookPoint + (Vector2)transform.position;
			var lookAtPoint = transform.parent ? (Vector2)transform.parent.InverseTransformPoint (worldPoint) : worldPoint;
			var rb2 = (containable.interior.Active) ? containable.interior.body : physical.body;

			rb2.rotation = rb2.position.AbsoluteAngle (lookAtPoint);
			rb2.angularVelocity = 0f;
		}

		#endregion


	}

}