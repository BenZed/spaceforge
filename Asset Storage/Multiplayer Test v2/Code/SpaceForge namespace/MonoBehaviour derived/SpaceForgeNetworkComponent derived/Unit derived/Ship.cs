﻿using UnityEngine;
using UnityExtensions;
using UnityExtensions.Grid;
using System.Collections;
using SpaceForge.Structural;

namespace SpaceForge {

	[RequireComponent(typeof(Structure))]
	[AddComponentMenu("SpaceForge/Ship")]
	public class Ship : Unit , IContainer {

		Physical _physical;
		public Physical physical {
			get {
				return _physical ?? (_physical = GetComponent<Physical>());
			}
		}

		Structure _structure;
		public Structure structure {
			get {
				return _structure ?? (_structure = GetComponent<Structure>());
			}
		}

		ShipInterior _interior;
		internal ShipInterior interior {
			get {
				return _interior ?? (_interior = ShipInterior.Create(this));
			}
		}

		#region Unit Overrides

		protected override void OnGeometryUpdate (Section section)
		{
			interior.UpdateWallColliders (section);
		}

		#endregion

		#region Unity Callers

		void Awake() 
		{
			collidableTimer = new Timer (constants.ship.CollidableRefreshInterval);
		}

		protected override void Update()
		{
			base.Update ();

			if (!isServer)
				return;

			UpdateBoundsAndRadius ();
			UpdateCollidables ();
		}

		#endregion

		#region IContainer implementation

		//TODO this is basically just Load without the velocity lerping and checking
		public void LoadOnStart(Containable containable)
		{
			if (containable == null)
				return;
			
			if ((int)containable.size > (int)MaximumSize)
				return;

			containable.interior.Enable (this);
			containable.physical.enabled = false;
			containable.unit.HostOnContainerLoaded (this);
			containable.transform.parent = transform;
			containable.gameObject.layer = Game.Constants.StructureObstacleLayer;
		}

		public void Load (Containable containable)
		{
			if (containable == null)
				return;

			if ((int)containable.size > (int)MaximumSize)
				return;

			Rigidbody2D otherBody = containable.physical.body;
			Rigidbody2D thisBody = physical.body;

			otherBody.velocity = Vector2.Lerp (otherBody.velocity, physical.body.velocity, Time.deltaTime);

			float matchSpeed = constants.ship.MatchingSpeed;
			float matchSpeedSqr = matchSpeed * matchSpeed;

			if ((otherBody.velocity - thisBody.velocity).sqrMagnitude <= matchSpeedSqr) {
				containable.interior.Enable (this);
				containable.physical.enabled = false;
				containable.unit.HostOnContainerLoaded(this);
				containable.transform.parent = transform;
				containable.gameObject.layer = Game.Constants.StructureObstacleLayer;
			}
		}

		public void UnloadCheck (Containable containable)
		{
			if (containable == null || containable.container.physical != physical) 
				throw new UnityException("Can only unload containables that are contained within itself");
			
			if (structure.GetTileAt (containable.transform.position) == null) {
				containable.physical.enabled = true;
				containable.interior.Disable ();
				containable.unit.HostOnContainerUnloaded();
				containable.transform.parent = null;
				containable.gameObject.layer = Game.Constants.ExteriorLayer;
			} 
		}

		public Containable.Size MaximumSize {
			get {
				return Containable.Size.Large;
			}
		}

		public Atmosphere GetAtmosphere(Containable containable)
		{
			if (containable == null || (containable.container as Ship) != this)
				return null;

			Vector2 position = containable.transform.localPosition;

			int col = Mathf.FloorToInt (position.x + structure.center.x);
			int row = Mathf.FloorToInt (position.y + structure.center.y);

			Coord coord = new Coord (col, row);
			Tile tile = structure.GetTile (coord);

			return tile ? tile.atmosphere : null;
		}

		#endregion

		#region Collidables

		Collider2D[] collidables = new Collider2D[32];
		float radius = 0f;
		Bounds bounds;

		void UpdateBoundsAndRadius()
		{
			if (bounds.size == structure.bounds.size && structure.bounds.size != Vector3.zero)
				return;

			bounds = structure.bounds;
			radius = bounds.size.magnitude;

		}

		Timer collidableTimer;
		void UpdateCollidables()
		{
			if (!collidableTimer.ExpireSet (constants.ship.CollidableRefreshInterval))
				return;

			int numCollidables =  Physics2D.OverlapCircleNonAlloc (physical.body.worldCenterOfMass, radius, collidables, 1 << Game.Constants.ExteriorLayer);
			Rigidbody2D thisBody = physical.body;

			float boardSpeed = constants.ship.BoardingSpeed;
			float boardSpeedSqr = boardSpeed * boardSpeed;

			for (var i = 0; i < numCollidables; i++) {
				Collider2D coll = collidables[i];
				Rigidbody2D otherBody = coll.attachedRigidbody;

				float diffSqr = (otherBody.velocity - thisBody.velocity).sqrMagnitude;
				bool inBoardingRange = diffSqr <= boardSpeedSqr;

				structure.ForEachSection(section => {
					if (inBoardingRange && coll.IsColliding(section.roof.poly)) {
						coll.DisableCollisions(section.roof.poly);
						coll.EnableCollisions(section.floor.poly);
					}
					if (!inBoardingRange && coll.IsColliding(section.floor.poly)) {
						coll.EnableCollisions(section.roof.poly);
						coll.DisableCollisions(section.floor.poly);
					}
				});
			}
		}

		#endregion

	}

}