using UnityEngine;
using System.Collections;

namespace SpaceForge {

	[AddComponentMenu("SpaceForge/Projectile")]
	public class Projectile : Unit {

		[SerializeField][Range(0f,100f)] float damage;

		void OnCollisionEnter2D(Collision2D collision) 
		{
			var damagable = collision.transform.GetComponent<Damagable> ();
			if (damagable) {

				Vector2 point = Vector2.zero;
				foreach(var contact in collision.contacts)
					point += contact.point;

				point /= collision.contacts.Length;

				damagable.Damage (damage, point);

			}

			Destroy (gameObject, Time.fixedDeltaTime);
		}
	}
}