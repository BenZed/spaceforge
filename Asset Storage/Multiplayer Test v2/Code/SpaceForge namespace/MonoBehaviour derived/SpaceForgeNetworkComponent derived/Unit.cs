using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Reflection;
using UnityExtensions;
//using SpaceForge.Structural;
//using UnityExtensions.Grid;
using SpaceForge.Structural;

namespace SpaceForge {

	public abstract class Unit : SpaceForgeNetworkComponent {

		#region Message

		class SyncBodyMessage : MessageBase {

			public Vector3 position;
			public Vector3 velocity;

		}

		#endregion

		#region Shortcuts

		internal static Game.Constants constants {
			get {
				return Game.manager.constants;
			}
		}

		public Vector2 position {
			get {
				return transform.localPosition;
			}
			protected set {
				transform.localPosition = new Vector3(value.x, value.y, transform.localPosition.z);
			}
		}

		public float rotation {
			get {
				return transform.eulerAngles.z;
			}
			protected set {
				transform.rotation = Quaternion.Euler(0f, 0f, value);
			}
		}

		#endregion

		#region Rigibody2D link

		protected Rigidbody2D body;
		Containable containable;

		#endregion

		#region Property Client Callers 

		[Client]
		protected virtual void OnDamage(float amount, Vector2 location)
		{
			Debug.Log (name + " Damaged! " + amount);
		}

		[Client]
		protected virtual void OnDestruct(float overage)
		{
			Debug.Log (name + " Destroyed!"); 
		}

		[Client]
		protected virtual void OnDeath(float overage)
		{
			Debug.Log (name + " Killed!");
		}

		[Client]
		protected virtual void OnCollision(Vector2 point) 
		{}

		[Client]
		protected virtual void OnContainerLoaded() 
		{}

		[Client]
		protected virtual void OnContainerUnloaded() 
		{}


		#endregion

		#region Structure Server Callers 

		[Server]
		protected virtual void OnGeometryUpdate(Section section) {}

		#endregion

		#region Inspector

		[Range(1 , 40)] public int 	 syncInterval = 15;
		[Range(0f, 1f)] public float syncPositionThreshold;
		[Range(0f, 5f)] public float syncRotationThreshold;

		#endregion

		#region Data

		bool forceSyncBody = false;

		Timer syncTimer;

		Vector2 lastSyncPosition = Vector2.zero;
		float lastSyncRotation = 0f;

		Vector2 lastSyncVelocity = Vector2.zero;
		float lastSyncAngularVelocity = 0f;

		float lastSyncTime = 0f;
		List<float> syncIntervals = new List<float> ();

		#endregion

		#region Unity Callers

		protected virtual void Start()
		{
			SetToExterior (transform);
			CheckForComponents ();
			SetSyncTimer();
		}

		protected virtual void Update()
		{
			if (isServer) {
				SyncBody ();
				return;
			}

			if (syncIntervals.Count == 0)
				return;

			float avg = 0f;
			foreach (var interval in syncIntervals)
				avg += interval;

			avg /= (float)syncIntervals.Count;

			if (avg == 0f)
				return;

			float t = (Time.time - lastSyncTime) / avg;

			position = Vector3.Lerp (lastSyncPosition, lastSyncPosition + lastSyncVelocity * avg, t);
			rotation = Mathf.LerpAngle (lastSyncRotation, lastSyncRotation + lastSyncAngularVelocity * avg, t);
		}

		internal void ManuallyUpdateBody()
		{
			forceSyncBody = true;
		}

		#endregion

		#region NetworkBehaviour Override

		SyncBodyMessage syncMsg;

		public override bool OnSerialize (NetworkWriter writer, bool initialState)
		{
			if (!initialState && syncVarDirtyBits == 0u) {
				writer.WritePackedUInt32(0u);
				return false;
			}

			if (!initialState)
				writer.WritePackedUInt32 (1u);

			Vector2 pos = position;
			Rigidbody2D rb2 = transform.parent && containable ? containable.interior.body : body;

			syncMsg.position = new Vector3 (pos.x, pos.y, rotation);
			syncMsg.velocity = rb2 ? new Vector3 (rb2.velocity.x, rb2.velocity.y, rb2.angularVelocity) : Vector3.zero;

			lastSyncPosition = position;
			lastSyncRotation = rotation;

			writer.Write (syncMsg);

			return true;
		}

		public override void OnDeserialize (NetworkReader reader, bool initialState)
		{
			if (isServer && NetworkServer.localClientActive)
				return;

			if (!initialState && reader.ReadPackedUInt32 () == 0u)
				return;	

			var read = reader.ReadMessage<SyncBodyMessage> ();

			lastSyncPosition = read.position;
			lastSyncRotation = read.position.z;

			lastSyncVelocity = read.velocity;
			lastSyncAngularVelocity = read.velocity.z;
		
			syncIntervals.Add (Time.time - lastSyncTime);
			while (syncIntervals.Count > 10)
				syncIntervals.RemoveAt (0);

			lastSyncTime = Time.time;
		}

		public override void OnStartClient ()
		{
			client.RegisterHandler (Messages.PlaceInContainer, HandlePlaceContainer);

			var msg = new Messages.CheckData ();
			msg.unitGameObject = gameObject;
			msg.clientConnectionId = client.connection.connectionId;

			client.Send (Messages.CheckForContainer, msg);
		}

		public override void OnStartServer ()
		{
			NetworkServer.RegisterHandler (Messages.CheckForContainer, HandleCheckContainer);
			syncMsg = new SyncBodyMessage();
		}

		#endregion

		#region Messages

		static class Messages {

			public const short CheckForContainer = 1010;
			public const short PlaceInContainer = 1011;

			public class CheckData : MessageBase {
				public GameObject unitGameObject;
				public int clientConnectionId;
			}

			public class PlaceData : MessageBase {
				public GameObject unitGO;
				public GameObject containerGO;
			}
		}

		[Server]
		static void HandleCheckContainer(NetworkMessage msg)
		{
			var read = msg.ReadMessage<Messages.CheckData> ();

			var unitGO = read.unitGameObject;
			var connID = read.clientConnectionId;
		
			if (!unitGO.transform.parent)
				return;

			var unit = unitGO.GetComponent<Unit> ();
			unit.forceSyncBody = true;

			var send = new Messages.PlaceData ();
			send.containerGO = unitGO.transform.parent.gameObject;
			send.unitGO = unitGO;

			NetworkServer.SendToClient(connID, Messages.PlaceInContainer, send);
		}

		[Client]
		static void HandlePlaceContainer(NetworkMessage msg)
		{
			var read = msg.ReadMessage<Messages.PlaceData> ();

			read.unitGO.transform.parent = read.containerGO.transform;
			read.unitGO.layer = Game.Constants.StructureObstacleLayer;

			var body = read.unitGO.GetComponent<Rigidbody2D> ();
			if (body)
				body.isKinematic = true;
		}

		#endregion

		#region Property Host Callers && Client Recievers

		//Called by Damagable
		[Server]
		internal void HostOnDamage(float damage, Vector2 point)
		{
			RpcOnDamage (damage, point);
		}
		[ClientRpc]
		void RpcOnDamage(float damage, Vector2 point)
		{
			OnDamage (damage, point);
		}

		//Called by Destructible
		[Server]
		internal void HostOnDestruct(float overage)
		{
			OnDestruct (overage);
		}
		[ClientRpc]
		void RpcOnDestruct(float overage)
		{
			OnDestruct (overage);
		}

		//Called by Killable
		[Server]
		internal void HostOnDeath(float overage)
		{
			RpcOnDeath (overage);
		}
		[ClientRpc]
		void RpcOnDeath(float overage)
		{
			OnDeath (overage);
		}

		//Called by Physical
		[Server]
		internal void HostOnCollision(Vector2 point, Transform other)
		{
			RpcOnCollision (point);
		}
		[ClientRpc]
		void RpcOnCollision(Vector2 point)
		{
			OnCollision (point);
		}

		//Called by IContainable
		[Server]
		internal void HostOnContainerLoaded(IContainer container)
		{
			forceSyncBody = true;
			RpcOnContainerLoaded (container.transform.gameObject);
		}
		[ClientRpc]
		void RpcOnContainerLoaded(GameObject containerGameObject)
		{
			transform.parent = containerGameObject.transform;
			lastSyncPosition = transform.parent.InverseTransformPoint (lastSyncPosition);

			gameObject.layer = Game.Constants.StructureObstacleLayer;
			body.isKinematic = true;

			OnContainerLoaded ();
		}

		[Server]
		internal void HostOnContainerUnloaded()
		{
			forceSyncBody = true;
			RpcOnContainerUnloaded ();
		}
		[ClientRpc]
		void RpcOnContainerUnloaded()
		{
			if (transform.parent)
				lastSyncPosition = transform.parent.TransformPoint (lastSyncPosition);
			transform.parent = null;

			gameObject.layer = Game.Constants.ExteriorLayer;
			body.isKinematic = false;

			OnContainerUnloaded ();
		}

		//Called by Structure
		[Server]
		internal void HostOnGeometryUpdate(Section section)
		{
			OnGeometryUpdate (section);
		}

		#endregion	

		#region Helper

		void SetSyncTimer()
		{
			float time = syncInterval > 0 ? 1f / (float)syncInterval : 0f;

			if (syncTimer == null)
				syncTimer = new Timer (time);
			else
				syncTimer.Set(time);
		}

		void SyncBody()
		{
			bool doSync = forceSyncBody || (syncInterval > 0 && syncTimer.Expired && RequiresSync());
			if (!doSync)
				return;

			forceSyncBody = false;
			SetSyncTimer();

			//This is the command that forces the network to sync. The legwork is all done in the serialization overrides
			SetDirtyBit (1u);
		}

		bool RequiresSync()
		{
			Vector2 deltaPos = position - lastSyncPosition;
			bool requiresPositionSync = deltaPos.sqrMagnitude > syncPositionThreshold * syncPositionThreshold;

			float deltaRot = rotation - lastSyncRotation;
			bool requiresRotationSync = Mathf.Abs(deltaRot) > syncRotationThreshold;

			return requiresPositionSync || requiresRotationSync;
		}

		void CheckForComponents()
		{
			body = GetComponent<Rigidbody2D> ();
			containable = GetComponent<Containable> ();
		}

		void SetToExterior(Transform transform)
		{
			var go = transform.gameObject;
			if (go.layer == 0)
				go.layer = Game.Constants.ExteriorLayer;

			foreach (Transform child in transform)
				SetToExterior(child);
		}

		#endregion
	}

}