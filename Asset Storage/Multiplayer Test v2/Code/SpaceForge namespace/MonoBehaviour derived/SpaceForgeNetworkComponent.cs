﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using System.Collections;

namespace SpaceForge {

	[RequireComponent(typeof(NetworkIdentity))]
	public abstract class SpaceForgeNetworkComponent : NetworkBehaviour {

		NetworkIdentity _identity;
		public NetworkIdentity identity {
			get {
				return _identity ?? (_identity = GetComponent<NetworkIdentity>());
			}
		}

		protected NetworkClient client {
			get {
				return Game.manager.client;
			}
		}
	}

}