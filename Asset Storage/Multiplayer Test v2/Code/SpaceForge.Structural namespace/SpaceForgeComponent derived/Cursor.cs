using UnityEngine;
using UnityExtensions;
using UnityExtensions.Grid;
using System.Collections;
using System;

namespace SpaceForge.Structural {

	[ExecuteInEditMode]
	[DisallowMultipleComponent]
	public class Cursor : SpaceForgeComponent {

		public enum PaintType
		{
			Floor,
			Wall, 
			//	Attachment,
		}

		[SerializeField] Structure structure;

		[SerializeField] Color placeableColor;
		[SerializeField] Color removeableColor;
		[SerializeField] Color unalterableColor;

		[SerializeField] PaintType _paintType = PaintType.Floor;
		[SerializeField] Tile.Type _tileType = Tile.Type.Regular;

		public PaintType paintType {
			get {
				return _paintType;
			}
			set {
				_paintType = value;
				node.enabled = _paintType == PaintType.Wall;
				tile.enabled = _paintType == PaintType.Floor;
			}
		}

		public Tile.Type tileType {
			get {
				return _tileType;
			}
			set {
				_tileType = value;
			}
		}

		[SerializeField] Sprite tileRegular;
		[SerializeField] Sprite tileRegularPartial;
		[SerializeField] Sprite tileArmoured;
		[SerializeField] Sprite tileArmouredPartial;
		[SerializeField] Sprite tileLight;
		[SerializeField] Sprite tileLightPartial;
		[SerializeField] Sprite wall;

		[SerializeField] SpriteRenderer tile;
		[SerializeField] SpriteRenderer node;
		[SerializeField] SpriteRenderer outline;

		#region Constructor

		static GameObject cursorPrefab;
		public static Cursor Create(Structure structure)
		{
			if (!structure)
				throw new UnityException ("StructureCursor must be created for a Structure.");

			if (!cursorPrefab)
				cursorPrefab = Resources.Load<GameObject> ("Prefabs/StructureCursor");

			var cursorGO = MonoBehaviour.Instantiate (cursorPrefab);
			var cursor = cursorGO.GetComponent<Cursor>();
			cursor.structure = structure;
			cursor.name = structure.name + " (Cursor)";
			cursor.transform.parent = structure.transform;

			return cursor;
		}

		#endregion

		#region API

		Coord currentTileCoord = Coord.zero;
		Coord currentNodeCoord = Coord.zero;
		Coord currentNodeOffset = Coord.zero;

		static readonly Vector3 CursorOffset = new Vector3 (0.5f, 0.5f, -5f);

		public void Move(Vector2 worldPosition)
		{
			if (structure.NumTiles == 0)
				return;

			Vector2 localMouse = structure.transform.InverseTransformPoint (worldPosition);
			Vector2 sectionOffset = -structure.center;

			if (SetNodePosition (localMouse, sectionOffset))
				SetTilePosition (localMouse, sectionOffset);

		}

		public void TogglePaintType() 
		{
			if (paintType == PaintType.Floor)
				paintType = PaintType.Wall;
			
			//		else if (paintType == PaintType.Wall)
			//			paintType = PaintType.Attachment;
			
			else 
				paintType = PaintType.Floor;
		}

		public void ToggleTileType() 
		{
			if (_tileType == Tile.Type.Regular)
				_tileType = Tile.Type.Armoured;

			else if (_tileType == Tile.Type.Armoured)
				_tileType = Tile.Type.Light;

			else
				_tileType = Tile.Type.Regular;
		}


		public void Apply(bool press, bool remove)
		{
			if (!structure.style)
				return;

			if (paintType == PaintType.Floor && isOnCorner)
				ApplyPartialTilePaint (press, remove);

			else if (paintType == PaintType.Floor && isOnCenter)
				ApplyTilePaint (press, remove);

			else if (paintType == PaintType.Wall)
				ApplyWallPaint (press, remove);

		}

		public void Hide()
		{
			tile.gameObject.SetActive (false);
			node.gameObject.SetActive (false);
			outline.enabled = false;
		}

		public void Show()
		{
			tile.gameObject.SetActive (true);
			node.gameObject.SetActive (true);
			outline.enabled = true;
		}

		#endregion

		#region New

		void ApplyPartialTilePaint(bool press, bool remove)
		{
			var tile = structure.GetTile (currentTileCoord);
			string error;

			bool can_place_corner = !tile && Tile.PlacementValid (currentTileCoord, structure, out error);
			bool can_fill_corner = tile && tile.IsPartial && tile.MissingCorner == currentNodeOffset;
			bool can_alter_corner = tile && tile.IsPartial && tile.MissingCorner != currentNodeCoord && tile.type != _tileType;

			bool can_remove_corner = tile && (!tile.IsPartial || (tile.IsPartial && tile.MissingCorner == -currentNodeCoord));

			Tile.Type sourceType = remove && can_remove_corner ? tile.type : _tileType;

			Sprite sprite = sourceType == Tile.Type.Regular ? tileRegularPartial :
				  			sourceType == Tile.Type.Armoured ? tileArmouredPartial :
				            tileLightPartial;

			Color actionColor;
			
			if ((can_place_corner || can_fill_corner || can_alter_corner) && !remove)
				actionColor = placeableColor;
			else if (can_remove_corner && remove)
				actionColor = removeableColor;
			else
				actionColor = unalterableColor;

			SetSprite (sprite, this.tile, currentNodeOffset, actionColor, -45f);

			if (!press)
				return;

			if (can_place_corner && !remove)
				structure.CreateTile (currentTileCoord, true, _tileType, -currentNodeOffset);

			if (can_fill_corner && !remove)
				structure.SetTilePartial (currentTileCoord, null);

			if (can_alter_corner && !remove)
				structure.SetTileType (currentTileCoord, true, _tileType);

			if (can_remove_corner && tile.IsPartial && remove)
				structure.RemoveTile (currentTileCoord);

			else if (can_remove_corner && !tile.IsPartial && remove)
				structure.SetTilePartial (currentTileCoord, currentNodeOffset);

		}

		void ApplyTilePaint(bool press, bool remove) 
		{
			var tile = structure.GetTile (currentTileCoord);
			string error;

			bool can_place_tile = Tile.PlacementValid (currentTileCoord, structure, out error);
			bool can_fill_tile = tile && tile.IsPartial;
			bool can_alter_tile = tile && tile.type != _tileType;

			bool can_remove_tile = tile;

			Tile.Type sourceType = remove && can_remove_tile ? tile.type : _tileType;

			Sprite sprite = sourceType == Tile.Type.Regular ? tileRegular :
							sourceType == Tile.Type.Armoured ? tileArmoured :
							tileLight;

			Color actionColor;

			if ((can_place_tile || can_fill_tile || can_alter_tile) && !remove)
				actionColor = placeableColor;
			else if (can_remove_tile && remove)
				actionColor = removeableColor;
			else
				actionColor = unalterableColor;

			SetSprite (sprite, this.tile, currentNodeOffset, actionColor);

			if (!press)
				return;

			if (can_place_tile && !remove)
				structure.CreateTile (currentTileCoord, true, _tileType, Coord.zero);

			if (can_fill_tile && !remove)
				structure.SetTilePartial (currentTileCoord, null);

			if (can_alter_tile && !remove)
				structure.SetTileType (currentTileCoord, true, _tileType);

			if (can_remove_tile && remove)
				structure.RemoveTile (currentTileCoord);

		}

		Coord lastDirection = Coord.zero;

		void ApplyWallPaint(bool press, bool remove)
		{
			Node wall = structure.GetNode (currentNodeCoord);
			Tile[] tiles = structure.GetTilesSharedByNodePosition (currentNodeCoord);
			Tile tile = structure.GetTile (currentTileCoord) ?? (tiles.Length > 0 ? tiles [0] : null);

			string error;

			var fixedNodeOffset = (tile != null) ? currentNodeCoord - Tile.CoordInNodeSpace(tile.coords) : currentNodeOffset;


			bool can_place_wall = Node.PlacementValid (tile, fixedNodeOffset, out error);
			bool can_remove_wall = wall != null;

			Coord direction = isOnWall ? currentNodeOffset :
							  isOnCenter && tile && tile.IsPartial ? tile.MissingCorner : Coord.zero;

			if (direction == Coord.zero && (!tile || !tile.IsPartial)) {
				direction = lastDirection;
			} else
				lastDirection = direction;

			Color actionColor;
			if (can_place_wall && !remove)
				actionColor = placeableColor;
			else if (can_remove_wall && remove)
				actionColor = removeableColor;
			else 
				actionColor = unalterableColor;

			SetSprite (this.wall, this.node, direction, actionColor);

			if (!press)
				return;
		
			if (can_place_wall && !remove)
				structure.AddWall (currentNodeCoord);//tile.CreateWall (fixedNodeOffset);

			if (can_remove_wall && remove)
				structure.RemoveWall (currentNodeCoord);//tile.RemoveWall (fixedNodeOffset);

		}

		void SetSprite(Sprite sprite, SpriteRenderer render, Coord nodeCoord, Color color, float offset = 0f) {
			render.sprite = sprite;
			render.color = color;

			float angle = nodeCoord != Coord.zero ? Vector2.zero.AbsoluteAngle (new Vector2 (nodeCoord.x, nodeCoord.y)) + offset : 0f;
			var rotation = Quaternion.Euler (0f, 0f, angle);
			render.transform.localRotation = rotation;

		}

		#endregion

		#region Helper

		void SetTilePosition(Vector2 localMouse, Vector2 offset) 
		{
			Vector2 tilePos = FloorPosition (localMouse - offset, 1f);
			
			currentTileCoord = Vector2ToCoord (tilePos);
			var cursorPosition = (Vector3)tilePos + CursorOffset + (Vector3)offset;
			transform.localPosition = cursorPosition;
			transform.localRotation = Quaternion.identity;
		}


		bool SetNodePosition(Vector2 local, Vector2 offset) 
		{
			Vector2 nodePos = RoundPosition (local - offset, 0.5f);

			Coord tileCoordInNodeSpace = Tile.CoordInNodeSpace (currentTileCoord);
			currentNodeOffset = Vector2ToCoord (nodePos, 2f) - tileCoordInNodeSpace;

			if (paintType == PaintType.Floor && isOnWall)
				currentNodeOffset = Coord.zero;

			else if (paintType == PaintType.Wall && isOnCorner)
				return false;

			currentNodeCoord = currentNodeOffset + tileCoordInNodeSpace;
			node.transform.localPosition = new Vector3 ((float)currentNodeOffset.x * 0.5f, (float)currentNodeOffset.y * 0.5f);
			node.transform.localRotation = Quaternion.identity;

			return true;

		}

		bool isOnCorner {
			get {
				return Math.Abs(currentNodeOffset.x) == 1 && Math.Abs(currentNodeOffset.y) == 1;
			}
		}
		
		bool isOnWall {
			get {
				return Math.Abs (currentNodeOffset.x) + Math.Abs(currentNodeOffset.y) == 1;
			}
		}
		
		bool isOnCenter {
			get {
				return currentNodeOffset == Coord.zero;
			}
		}

		Vector2 CentrifyPosition(Vector2 position, float final_round = 0.5f, float bound = 0.05f)
		{
			var floored = FloorPosition (position, 1f);
			var normaled = position - floored;

			var bottom = bound * 0.5f;
			var top = 1f - bottom;

			float x = Mathf.Abs (normaled.x);
			float y = Mathf.Abs (normaled.y);

			if (x < bottom || x > top && y < bottom || y > top)
				return floored + Vector2.one * 0.5f;
			else
				return RoundPosition (position, final_round);

		}

		Vector2 RoundPosition(Vector2 position, float increment = 0.5f)
		{
			return new Vector2 (
				Mathf.Round(position.x / increment) * increment,
				Mathf.Round(position.y / increment) * increment
			);
		}
		
		Vector2 FloorPosition(Vector2 position, float increment = 0.5f)
		{
			return new Vector2 (
				Mathf.Floor(position.x / increment) * increment,
				Mathf.Floor(position.y / increment) * increment
			);
		}
		
		Coord Vector2ToCoord(Vector2 position, float multiplier = 1f)
		{
			int x = Mathf.FloorToInt (position.x * multiplier);
			int y = Mathf.FloorToInt (position.y * multiplier);
			return new Coord (x, y);
		}
		
		#endregion
	}

}