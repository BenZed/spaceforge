﻿using UnityEngine;
using UnityExtensions.Grid;
using System.Collections;
using UnityExtensions;

namespace SpaceForge.Structural {

	public class Attachable : Property {

		#region Data

		[SerializeField] bool _permanent = false;
		public bool permanent {
			get {
				return _permanent;
			}
		}

		public bool attached {
			get {
				return permanent || structure;
			}
		}

		[HideInInspector, SerializeField] Coord _coord;
		public Coord coord {
			get {
				return _coord;
			}
		}
		
		[SerializeField] Coord _direction;
		public Coord direction {
			get {
				return _direction;
			}
		}

		#endregion

		Structure _structure;
		internal Structure structure {
			get {
				return _structure;
			}
		}

		public Tile tile {
			get {
				if (!attached)
					return null;

				return structure.GetTile(_coord);
			}
		}

		protected override void Start ()
		{
			if (permanent && !transform.parent)
				throw new UnityException("A permanent attachable needs to have a structure to attach to.");

			if (permanent && !attached)
				Attach (transform.parent.GetComponent<Structure>(), direction);

			base.Start ();
		}

		#region API

		public void Attach(Structure structure, Coord direction)
		{
			Tile tile = null;
			if (structure)
				tile = structure.GetTileAt(transform.position);

			Attach (structure, tile, direction);
		}

		public void Attach(Structure structure, Coord tileCoord, Coord direction)
		{
			Tile tile = null;
			if (structure)
				tile = structure.GetTile(tileCoord);
			
			Attach (structure, tile, direction);
		}

		public void Attach(Structure structure, Tile tile, Coord direction)
		{
			if (attached)
				throw new UnityException("Already attached.");

			if (!structure)
				throw new UnityException("Must attach to a structure.");

			if (!tile)
				throw new UnityException("Must attach to a tile in a structure.");

			_structure = structure;
			_coord = tile.coords;
			_direction = direction;

			Vector2 dirVec2 = new Vector2 (direction.x, direction.y);
			float angle = dirVec2.AbsoluteAngle(Vector2.zero);

			transform.parent = structure.transform;
			transform.localPosition = tile.position + dirVec2 * 0.5f + Vector2.one * 0.5f;
			transform.rotation = Quaternion.Euler(0f, 0f, angle);
		}

		public void Detach()
		{
			if (permanent)
				throw new UnityException("Cannot Detach a permanent attachment.");

			if (!attached)
				throw new UnityException("Not attached.");

			_structure = null;
		}

		#endregion
	}
}