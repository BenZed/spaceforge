using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityExtensions;
using UnityExtensions.Grid;

namespace SpaceForge.Structural {

	[DisallowMultipleComponent]
	[ExecuteInEditMode]
	public sealed class Section : SpaceForgeComponent {

		#region Constructor 
		
		internal static Section Create(Structure structure, Coord sectionCoord)
		{
			var offset = structure.center;
			var section = new GameObject("Section "+sectionCoord.x+","+sectionCoord.y, typeof(Section)).GetComponent<Section>();
			section.coord = sectionCoord;
			
			//	section.gameObject.layer = Game.Constants.ShipLayer;
			section.transform.parent = structure.transform;
			section.transform.localPosition = offset;
			section.transform.localRotation = Quaternion.identity;
			
			return section;
		}
		
		#endregion

		#region Nested

		[System.Serializable]
		internal struct Flags {
			
			public Flag geometry;
			public Flag damageUV;

		}

		#endregion

		[SerializeField]
		internal Flags flags = new Flags {
			
			geometry = true, 
			damageUV = false,

		};

		internal const int Dimensions = 10;

		/// <summary>
		/// Given a row or column of a tile, returns the row or column that tiles section would be in, in their respective parent structure.
		/// </summary>
		/// <returns>The row or column of a hypothetical section.</returns>
		/// <param name="rowOrColumn">Row or column.</param>
		internal static int AxisInStructure(int axis)
		{
			return axis >= 0
				? axis / Section.Dimensions 
				: ((axis + 1) / Section.Dimensions) - 1;
		}

		[SerializeField]Coord coord;
		public Coord coords{
			get {
				return coord;
			}
		}

		Structure _structure;
		public Structure structure {
			get {
				if (!_structure && (!transform.parent || !(this.structure = transform.parent.GetComponent<Structure>())))
					throw new UnityException("Sections should be the children of GameObjects with a structure component.");
				
				return _structure;
			}
			
			private set {
				_structure = value;
			}
		}

		[ReadOnly][SerializeField] internal Level roof;
		[ReadOnly][SerializeField] internal Level floor;
		[ReadOnly][SerializeField] internal Level walls;
		[ReadOnly][SerializeField] internal Level scaffolding;

		Timer atmosphereTimer;
		float atmosphereTimeStamp = 0f;

		#region Unity Callers

		void Awake()
		{
			CreateLevels ();
			atmosphereTimer = new Timer (0.25f);
		}

		void Start()
		{
			roof.render.sharedMaterial = structure.style.material;
			floor.render.sharedMaterial = structure.style.material;
			walls.render.sharedMaterial = structure.style.material;
			scaffolding.render.sharedMaterial = structure.style.material;

			roof.render.enabled = false;
		}

		#endregion

		internal void CheckFlags()
		{
			if (flags.geometry) {
				flags.damageUV = true;
				structure.style.UpdateGeometryAndUV (this);
			}	

			if (flags.geometry.SwitchOff () && structure.unit && structure.unit.isServer && Application.isPlaying)
				structure.unit.HostOnGeometryUpdate (this);

			#if UNITY_EDITOR
			if (Application.isEditor && !Application.isPlaying) {
				roof.render.enabled = false;

				roof.poly.enabled = false;
				walls.poly.enabled = false;
				floor.poly.enabled = false;
			}
			#endif

			if (flags.damageUV.SwitchOff ()) {
				structure.style.UpdateDamageUV (this);
				structure.flags.mass = true;
			}

			if (Application.isPlaying && atmosphereTimer.ExpireReset()) {
				float timeDelta = Time.time - atmosphereTimeStamp;
				atmosphereTimeStamp = Time.time;
				tiles.ForEach (tile => tile.DiffuseAtmosphere (timeDelta));
			}

		}

		#region Level Management

		void CreateLevels() 
		{
			roof		= CreateLevel (Level.Type.Roof, -0.5f, Game.Constants.StructureLayer, true);
			walls 		= CreateLevel (Level.Type.Wall, -0.1f, Game.Constants.StructureObstacleLayer, true);
			floor 		= CreateLevel (Level.Type.Floor, 0.1f, Game.Constants.StructureLayer, true);
			scaffolding = CreateLevel (Level.Type.Scaffolding, 0.5f, Game.Constants.StructureLayer, false);

			flags.geometry = true;
		}

		Level CreateLevel(Level.Type type, float zOffset, int layer, bool hasPolygonCollider = false)
		{
			var name = type.ToString ();
			Transform existingLevelTansform = transform.Find (name);
			Level level = null;

			if (existingLevelTansform)
				level = existingLevelTansform.GetComponent<Level> ();

			if (!level)
				level = (new GameObject (name, typeof(Level))).GetComponent<Level> ();

			level.transform.parent = transform;
			level.transform.localRotation = Quaternion.identity;
			level.transform.localPosition = Vector3.zero + Vector3.forward * zOffset;

			if (Application.isPlaying)
				level.gameObject.layer = layer;
			level.type = type;
			level.EnsureCollider(hasPolygonCollider);

			return level;
		}

		#endregion

		#region Tile Management

		[Serializable]
		class GridOfTiles : Grid<Tile> {};

		[SerializeField] GridOfTiles tiles;

		public void AddTile(Tile tile) 
		{	
			if (tiles == null)
				tiles = new GridOfTiles();

			if (tile == null)
				throw new UnityException ("Tile attempting to add is null.");
			
			tile.structure = this.structure;
			tiles.Add(tile, Tile.CoordInSection(tile.coords));
			flags.geometry = true;
			structure.flags.center = true;
			structure.flags.mass = true;

		}

		public void RemoveTile(Tile tile) 
		{
			if (tiles == null)
				throw new UnityException ("Cannot remove tile. No tiles have been added to this section.");
			
			var coordInSection = Tile.CoordInSection(tile.coords);
			
			if (tiles.Occupied(coordInSection))
				tiles.Remove (coordInSection);
			
			flags.geometry = true;
			structure.flags.center = true;
			structure.flags.mass = true;
		}

		public Tile GetTile(Coord coordInSection) 
		{
			if (tiles == null)
				tiles = new GridOfTiles();
			
			if (!tiles.Occupied(coordInSection))
				return null;
			else
				return tiles.Get(coordInSection);
		}

		public void ForEachTile(Action<Tile> action)
		{
			if (tiles != null)
				tiles.ForEach(tile => action (tile));
		}

		public int NumTiles {
			get {
				return tiles.Count;
			}
		}

		public float mass {
			get {
				float mass = 0f;

				tiles.ForEach(tile => mass += tile.mass);

				return mass;
			}
		}

		#endregion

		#if UNITY_EDITOR
		internal void EnsureLevelsInEditor()
		{
			if (!roof || !floor || !walls || !scaffolding)
				CreateLevels();

		}
		#endif
	}
}