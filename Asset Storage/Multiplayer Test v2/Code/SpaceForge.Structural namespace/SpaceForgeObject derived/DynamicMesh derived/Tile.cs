using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityExtensions;
using UnityExtensions.Grid;

namespace SpaceForge.Structural {

	[System.Serializable]
	public class Tile : DynamicMesh {

		#region Constructor

		internal Tile (Coord coordsInStructure, Structure structure, bool sealable, Type type, Coord? missingCorner = null)
		{
			this._coordInStructure = coordsInStructure;
			this.position = new Vector2((float)_coordInStructure.x, (float)_coordInStructure.y);
			this.structure = structure;

			SetProperties (sealable, type);
			CreateMesh ();

			//Reversed so we can se
			bool createAsPartial = missingCorner.HasValue && missingCorner.Value != Coord.zero;
			isPartial = !createAsPartial;

			if (createAsPartial) {
				SetPartialTile (missingCorner.Value);
			} else 
				SetWholeTile ();
		}

		#endregion

		#region Constants

		const float MinimumReverbDamage = 0.1f;
		const float BreachThresholdIntegrity = 0.35f;

		#endregion

		#region Nested Types
		
		public enum Type 
		{
			Light,
			Regular,
			Armoured,
		}

		[System.Serializable]
		internal class QuadrantData {
			
			[SerializeField] public Vital integrity;
			[SerializeField] public bool adjacentTypesMatch;
			[SerializeField] public bool adjacentExists;
			[SerializeField] public Door door;

			public QuadrantData(float max)
			{
				this.integrity = new Vital(max);
			}
		}

		[System.Serializable]
		internal class Quadrants : SimpleGrid<QuadrantData> {

			[SerializeField] public float startingHealth;

			public Quadrants(float health) {
				startingHealth = health;
				EnsureAll();
			}

			public void EnsureAll()
			{
				if (!Has (Coord.n))
					Add (Coord.n, new QuadrantData (startingHealth));

				if (!Has (Coord.e))
					Add (Coord.e, new QuadrantData (startingHealth));

				if (!Has (Coord.s))
					Add (Coord.s, new QuadrantData (startingHealth));

				if (!Has (Coord.w))
					Add (Coord.w, new QuadrantData (startingHealth));
			}

			public void RemoveIfHas(Coord coord)
			{
				if (Has (coord))
					Remove (coord);
			}

			internal override bool CoordValid (Coord coord)
			{
				return coord == Coord.n || coord == Coord.e || coord == Coord.s || coord == Coord.w;
			}

		}

		[System.Serializable]
		internal class Nodes : SimpleGrid<Node> { }

		[System.Serializable]
		internal struct Flags {
			public Flag splitCheck;

		}

		#endregion

		#region Mesh

		internal Vertex nw {
			get {
				return GetVertex(new Vector2(0f, 1f));
			}
		}

		internal Vertex ne {
			get {
				return GetVertex(new Vector2(1f, 1f));
			}
		}

		internal Vertex c {
			get {
				return GetVertex(new Vector2(0.5f, 0.5f));
			}
		}

		internal Vertex sw {
			get {
				return GetVertex(new Vector2(0f, 0f));
			}
		}

		internal Vertex se {
			get {
				return GetVertex(new Vector2(1f, 0f));
			}
		}

		void CreateMesh() 
		{
			AddVertex (new Vector2 (0f, 1f));
			AddVertex (new Vector2 (1f, 1f));
			AddVertex (new Vector2 (0.5f, 0.5f));
			AddVertex (new Vector2 (1f, 0f));
			AddVertex (new Vector2 (0f, 0f));

			SetTriOrder (c,nw,ne,
			             c,ne,se,
			             c,se,sw,
			             c,sw,nw);
		}

		#endregion

		#region Component Links
		
		[SerializeField]
		Structure _structure;
		public Structure structure {
			get {
				if (!_structure)
					throw new UnityException("Tiles should always have a structure.");
				return _structure;
			}
			
			internal set {
				_structure = value;
			}
		}
		
		internal Section section {
			get {
				return structure.GetSection(Tile.CoordOfSection(coords), ensure: true);
			}
		}
		
		#endregion


		#region Network Helpers

		internal void SyncIntegrity(float n_integrity, float e_intergity, float s_integrity, float w_integrity)
		{
			if (quadrants.Has(Coord.n))
				quadrants.Get (Coord.n).integrity.Set (n_integrity);
			
			if (quadrants.Has(Coord.e))
				quadrants.Get (Coord.e).integrity.Set (e_intergity);
			
			if (quadrants.Has(Coord.s))
				quadrants.Get (Coord.s).integrity.Set (s_integrity);
			
			if (quadrants.Has(Coord.w))
				quadrants.Get (Coord.w).integrity.Set (w_integrity);
			
			if (integrity <= 0f && !isDestroyed)
				Destroy ();
			else
				section.flags.geometry = true;
			
		}
		
		internal void SyncWall(Coord coord, bool shouldHave) {
			var wall = GetNode(coord);
			var owns = wall && wall.tile == this;
			if (wall && ((shouldHave && !owns) || (!shouldHave && owns)))
				RemoveWall(coord);
			
			if (shouldHave && !owns)
				CreateWall(coord);
		}

		#endregion

		#region Quadrants

		[SerializeField] internal Quadrants quadrants; 

		internal void SetProperties(bool sealable, Type type)
		{
			this.sealable = sealable;
			if (this.sealable && this.atmosphere == null)
				this._atmosphere = new Atmosphere ();

			//Create Nodes
			if (this.nodes == null)
				this.nodes = new Nodes ();

			//Set Type Data
			var typeData = structure.style.GetTileTypeData (type);
			this.mass = typeData.mass;

			//Create Quadrants
			if (this.quadrants == null)
				this.quadrants = new Quadrants (typeData.health * 0.25f);

			RefreshQuadrantData (true);
		}

		internal void RefreshQuadrantData(bool onSurrounding) 
		{
			quadrants.ForEach ((direction, quad) => {
				var adjacent = GetRelativeTile(direction);
				quad.adjacentExists = Tile.QuadrantExists(adjacent, -direction);
				quad.adjacentTypesMatch = quad.adjacentExists ? adjacent.type == type : true;

			});

			section.flags.geometry = true;

			if (!onSurrounding) 
				return;

			ForEachAdjacent ((adjacent, coord) => {
				adjacent.RefreshQuadrantData(false);
			});

		}

		#endregion

		#region Properties

		[SerializeField] internal string uvCode;
		[SerializeField] Type _type;
		
		public Type type {
			get {
				return _type;
			}
			private set {
				_type = value;
			}
		}

		[SerializeField] float _mass = 0f;
		public float mass {
			get {
				float baseMass = isPartial ? _mass * 0.5f : _mass;
				float integrityMass = (baseMass * 0.25f) + (baseMass * 0.75f) * (integrity/maxIntegrity);
				
				return integrityMass;
			}
			internal set {
				_mass = value;
			}
		}

		public float integrity {
			get {
				float _integrity = 0f;
				quadrants.ForEach((coord, quad) => _integrity += quad.integrity.Current);
				
				return _integrity;
			}
		}
		
		public float maxIntegrity {
			get {
				float _max = 0f;
				quadrants.ForEach((coord, quad) => _max += quad.integrity.Max);

				
				return _max;
			}
		}

		[SerializeField][ReadOnly] bool _sealable;
		public bool sealable {
			get {
				return _sealable;
			}
			private set {
				_sealable = value;
			}
		}

		[SerializeField] Atmosphere _atmosphere;
		public Atmosphere atmosphere {
			get {
				if (!sealable)
					return null;

				return _atmosphere; 
			}
		}

		const float TileQuadrantVolume = 25f;
		const float VacuumExchange = 1000f;

		private float volume {
			get {
				return quadrants.Count * TileQuadrantVolume;
			}
		}


		internal void DiffuseAtmosphere(float timeDelta)
		{
			if (!sealable)
				return;

			timeDelta = Mathf.Clamp01(timeDelta);
			float permeability = isPartial && !nodes.Has(Coord.zero) ? 0.4f : 0f;

			quadrants.ForEach((dir, quad) => {

				permeability += Mathf.Lerp (0.05f, 0f, (quad.integrity.Percent * 0.35f) / 0.35f);

				Node wall = GetNode(dir);
				if (wall)
					return;

				Door door = GetDoor(dir);
				if (door && door.isAirTight)
					return;

				Tile adj = GetRelativeTile(dir);
				if (adj && adj.sealable)
					MixAtmosphere(adj);
				else
					permeability += 0.2f;

			});

			float loss = (VacuumExchange * permeability) * timeDelta;
			if (loss > 0f)
				atmosphere.Exchange(null, loss);
		}

		void MixAtmosphere(Tile tile)
		{
			float o2Push = MixGas(atmosphere.o2, tile.atmosphere.o2, tile.volume);
			float co2Push = MixGas(atmosphere.co2, tile.atmosphere.co2, tile.volume);

			atmosphere.o2 -= o2Push;
			tile.atmosphere.o2 += o2Push;

			atmosphere.co2 -= co2Push;
			tile.atmosphere.co2 += co2Push;
		}

		float MixGas(float gas, float otherGas, float otherVolume)
		{
			float totalVolume = otherVolume + volume;
			float totalGas = otherGas + gas;
			
			float target = totalGas / (totalVolume / volume);
			float otherTarget = totalGas - target;

			return  otherTarget - otherGas;
		}

		#endregion

		#region Tile Management

		internal Flags flags = new Flags();

		public Tile GetRelativeTile(Coord coord)
		{
			var absoluteCoord = coords + coord;
			return structure.GetTile (absoluteCoord);
		}

		public void ForEachAdjacent(System.Action<Tile, Coord> action) 
		{
			foreach (var coord in Coord.Around) {
				var adj = GetRelativeTile(coord);
				if (adj)
					action(adj, coord);
			}
		}

		#endregion
		
		internal void Destroy() 
		{
			isDestroyed = true;

			ne.enableCollider = false;
			nw.enableCollider = false;
			se.enableCollider = false;
			sw.enableCollider = false;
			c.enableCollider = false;

			nodes.ForEach ((coord, node) => node.DisableColliders());

			section.flags.geometry = true;
		}
		
		bool _isDestroyed = false;
		public bool isDestroyed {
			get {
				return _isDestroyed;
			}
			private set {
				_isDestroyed = value;
			}
		}


		#region Partial Tile

		[SerializeField] Coord missingCorner = Coord.zero;
		[SerializeField] bool isPartial = true;

		public bool IsPartial {
			get {
				return isPartial;
			}
		}

		public Coord MissingCorner {
			get {
				if (!IsPartial)
					throw new UnityException("Cannot get the missing corner of a non-partial tile.");

				return missingCorner;
			}
		}

		public void SetPartialTile(Coord relativeCornerCoord)
		{
			if (isPartial && relativeCornerCoord != missingCorner)
				throw new UnityException ("Already a partial tile.");

			nw.enableMesh = true;
			ne.enableMesh = true;
			se.enableMesh = true;
			sw.enableMesh = true;

			if (relativeCornerCoord == Coord.nw) {
				nw.enableMesh = false;
				SetColliderOrder(sw,ne, ne,se, se,sw);

			} else if (relativeCornerCoord == Coord.ne) {
				ne.enableMesh = false;
				SetColliderOrder(sw,nw, nw,se, se,sw);

			} else if (relativeCornerCoord == Coord.se) {
				se.enableMesh = false;
				SetColliderOrder(sw,nw, nw,ne, ne,sw);

			} else if (relativeCornerCoord == Coord.sw) {
				sw.enableMesh = false;
				SetColliderOrder(se,nw, nw,ne, ne,se);

			} else
				throw new UnityException ("Cannot set a partial tile with coordinates " + relativeCornerCoord);

			isPartial = true;

			missingCorner = relativeCornerCoord;

			var right = Coord.NextAround (missingCorner);
			var left = Coord.PrevAround (missingCorner);

			quadrants.RemoveIfHas (right);
			quadrants.RemoveIfHas (left);

			if (nodes.Has (right)) {
				var rightWall = nodes.Get (right);
				if (rightWall && !TransferNode (rightWall))
					RemoveWall (rightWall.place);
			}

			if (nodes.Has (left)) {
				var leftWall = nodes.Get (left);
				if (leftWall && !TransferNode (leftWall))
					RemoveWall (leftWall.place);
			}

			RefreshQuadrantData (true);
		}

		public void SetWholeTile()
		{
			if (!isPartial)
				throw new UnityException ("Already a whole tile.");

			//Removes diagonal wall if one exists here.
			if (GetNode (Coord.zero) != null)
				RemoveWall (Coord.zero);
			
			nw.enableMesh = true;
			ne.enableMesh = true;
			se.enableMesh = true;
			sw.enableMesh = true;

			quadrants.EnsureAll ();
									
			SetColliderOrder (nw,ne, 
			                  ne,se, 
			                  se,sw, 
			                  sw,nw);

			isPartial = false;

			missingCorner = Coord.zero;
			RefreshQuadrantData (true);
		}

		#endregion

		#region Door Management

		internal Door CreateDoor(Coord doorCoord)
		{
			Door door = Door.Create (structure, this, doorCoord);
			quadrants.Get (doorCoord).door = door;

			return door;
		}

		internal Door GetDoor(Coord doorCoord)
		{
			if (Node.TypeFromPlacement (doorCoord) != Node.Type.Wall)
				throw new UnityException("Invalid door offset: " + doorCoord);
			
			QuadrantData quadrant = quadrants.Has (doorCoord) ? quadrants.Get (doorCoord) : null;
			Door door =  quadrant == null ? null : quadrant.door;

			if (door)
				return door;

			Tile adj = GetRelativeTile(doorCoord);
			if (adj) 
				quadrant = adj.quadrants.Has (-doorCoord) ? adj.quadrants.Get (-doorCoord) : null;
			if (quadrant != null)
				door = quadrant.door;

			return door;
		}

		internal void RemoveDoor(Coord doorCoord)
		{
			Door door = GetDoor (doorCoord);
			if (!door)
				throw new UnityException ("Cannot remove door, none exists.");

			quadrants.Get (doorCoord).door = null;
			Structure.Destroy (door.gameObject);		
		}

		#endregion Door

		#region Node Management

		[SerializeField] Nodes nodes;

		internal static bool WallCoordValid(Coord wallCoord) 
		{
			var x = System.Math.Abs (wallCoord.x);
			var y = System.Math.Abs (wallCoord.y);

			return x + y < 2;
		}

		public Node CreateWall(Coord wallCoord)
		{
			if (!WallCoordValid (wallCoord))
				throw new UnityException ("Cannot add wall at "+wallCoord);

			var node = CreateNode (wallCoord);

			DetermineCorners (wallCoord);

			return node;
		}

		public void RemoveWall(Coord wallCoord)
		{
			if (!WallCoordValid (wallCoord))
				throw new UnityException ("Cannot remove wall, one cannot exist at "+wallCoord);

			var wall = GetNode (wallCoord);
			if (!wall)
				throw new UnityException ("Cannot remove wall, one doesn't exist at "+wallCoord);

			var owner = wall.tile;
			var place = wall.place;

			owner.RemoveNode (wall.place);
			owner.DetermineCorners (place);
		}

		void DetermineCorners(Coord wallCoord)
		{
			int offsetDistance = 1;
			Coord around = wallCoord;

			if (Node.TypeFromPlacement (wallCoord) == Node.Type.Diagonal) {
				around = MissingCorner;
				offsetDistance = 2;
			}

			var left = Coord.AroundResolve (around, -offsetDistance);
			var right = Coord.AroundResolve (around, offsetDistance);

			DetermineCorner (left);
			DetermineCorner (right);
			
			RefreshNodeGeometry (wallCoord);
		}

		void DetermineCorner(Coord cornerCoord)
		{
			bool should = RequiresCorner (cornerCoord);
			bool changed = false;
			Node corner = GetNode (cornerCoord);

			if (corner && !should) {
				changed = true;
				corner.tile.RemoveNode(corner.place);
				
			} else if (!corner && should) {
				changed = true;
				CreateNode(cornerCoord);
			}

			if (changed)
				RefreshNodeGeometry (cornerCoord);
		}

		bool RequiresCorner(Coord cornerCoord)
		{
			var intersecting = GetWallsIntersectingCorner (cornerCoord).Length;
			return intersecting != 0 && intersecting != 2;
		}

		internal Node[] GetWallsIntersectingCorner(Coord cornerCoord)
		{
			var walls = new List<Node> ();
			foreach (var coord in Coord.Around) {
				var wall = GetNode (coord + cornerCoord);
				if (!wall)
					continue;

				if (wall.type == Node.Type.Corner)
					throw new UnityException("Something fucked up, somewhere. It shouldn't be possible to get a corner here.");

				if (wall.type != Node.Type.Diagonal || (wall.tile.MissingCorner != coord && wall.tile.MissingCorner != -coord))
					walls.Add (wall);

			}

			return walls.ToArray ();
		}

		Node CreateNode(Coord placeCoord) 
		{
			string error;
			if (!Node.PlacementValid (this, placeCoord, out error)) 
				throw new UnityException (error);

			var node = new Node (this, placeCoord);
			nodes.Add (placeCoord, node);

			return node;
		}

		internal Node GetNode(Coord placeCoord)
		{
			return structure.GetNode (placeCoord + Tile.CoordInNodeSpace (coords));
		}

		void RemoveNode(Coord placeCoord)
		{
			nodes.Remove (placeCoord);
		}

		internal void TransferNodes(bool doRefresh = true)
		{
			ForEachNode ((place, node) => {
				if (place == Coord.zero)
					return;

				TransferNode (node, doRefresh);
			});
		}

		internal bool TransferNode(Node node, bool doRefresh = true) {
		
			var adjTiles = node.tile.structure.GetTilesSharedByNodePosition(node.coords);
			foreach(var adj in adjTiles) {
				if (adj == node.tile)
					continue;
				
				var new_place = node.coords - Tile.CoordInNodeSpace(adj.coords);

				string err;
				if (!Node.PlacementValid(adj, new_place, out err) && err == Node.AlreadyExistsError) {

					nodes.Remove(node.place);
					adj.nodes.Add(new_place, node);
					node.SetTile(adj,new_place);
					if (doRefresh)
						adj.RefreshNodeGeometry();

					return true;
				}
			}

			return false;
		}

		internal void ForEachNode(System.Action<Coord,Node> action) 
		{
			nodes.ForEach (action);
		}

		internal void RefreshNodeGeometry() 
		{
			nodes.ForEach ((coord, node) => {
				if (node.type != Node.Type.Corner) 
					DetermineCorners(coord);
			});
		}

		void RefreshNodeGeometry(Coord placeCoord) {
						
			foreach (var coord in Coord.Around) {
				var adjacentNode = GetNode (placeCoord + coord);
				if (adjacentNode)
					adjacentNode.RefreshGeometry();
			}

			var node = GetNode (placeCoord);
			if (node)
				node.RefreshGeometry ();

			section.flags.geometry = true;

		}

		#endregion

		#region Coordinates 
				
		/// <summary>
		/// Given a row or column of a tile, returns what that tiles row or column would be inside of it's section
		/// </summary>
		/// <returns>The row or column of a hypothetical section.</returns>
		/// <param name="rowOrColumn">Row or column.</param>
		static int AxisInSection(int axis)
		{
			return axis >= 0
				? axis % Section.Dimensions 
				: ((axis + 1) % Section.Dimensions) + Section.Dimensions - 1;
		}
		
		internal static Coord CoordInSection(Coord structureCoords)
		{
			return new Coord(AxisInSection(structureCoords.x), AxisInSection(structureCoords.y));
		}
		
		internal static Coord CoordOfSection(Coord structureCoords)
		{
			return new Coord(Section.AxisInStructure(structureCoords.x), Section.AxisInStructure(structureCoords.y));
		}

		internal static Coord CoordInNodeSpace(Coord tileCoords)
		{
			return new Coord ((tileCoords.x * Node.Dimensions) + 1, (tileCoords.y * Node.Dimensions) + 1);
		}

		internal static Coord CoordFromNodeSpace(Coord nodeCoords)
		{
			if (System.Math.Abs(nodeCoords.x) % 2 != 1 || System.Math.Abs(nodeCoords.y) % 2 != 1)
				throw new UnityException ("A tile could not exist at Node Coordinates " + nodeCoords);

			return new Coord ((nodeCoords.x  - 1) / Node.Dimensions, (nodeCoords.y - 1) / Node.Dimensions );
		}

		[ReadOnly][SerializeField]Coord _coordInStructure;
		public Coord coords {
			get { 
				return _coordInStructure;
			}
		}

		#endregion

		#region Static Validators
		
		public static bool QuadrantExists(Tile tile, Coord coord)
		{
			if (tile == null)
				return false;

			return tile.quadrants.Has (coord);
		}

		public static bool QuadrantValid(Tile tile, Coord coord) {

			if (!QuadrantExists(tile,coord))
				return false;

			var quadrant = tile.quadrants.Get (coord);
			return !quadrant.integrity.isEmpty;
		}

		public static implicit operator bool(Tile tile)
		{
			return tile != null;
		}
				
		public static bool ExistsInSection(Tile tile, Tile tileb)
		{
			return tile && tileb && tile.section.transform == tileb.section.transform;
		}
		
		internal static bool PlacementValid(Coord structureCoords, Structure structure, out string error)
		{
			error = "";
			
			Tile maybeTile = structure.GetTile(structureCoords);
			if (maybeTile) {
				error = "Tile already exists at that location.";
				return false;
			}

			return true;
		}
		
		#endregion
		
		#region Operators and Overloads
		
		public override string ToString ()
		{ 
			return string.Format ("[{0}-Tile {1},{2}]", type.ToString(), _coordInStructure.column,  _coordInStructure.row); 
		}
		
		public override bool Equals (object obj)
		{
			Tile tile = obj as Tile;
			if (tile == null)
				return false;
			else
				return _coordInStructure == tile._coordInStructure && structure == tile.structure; 
			
		}
				
		public override int GetHashCode ()
		{
			return _coordInStructure.column ^ _coordInStructure.row + structure.GetInstanceID();
		}
		
		#endregion

	}

}