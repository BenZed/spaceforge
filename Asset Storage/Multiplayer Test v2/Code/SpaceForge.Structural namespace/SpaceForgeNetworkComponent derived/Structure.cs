using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityExtensions;
using UnityExtensions.Grid;
using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SpaceForge.Structural {

	[DisallowMultipleComponent]
	[ExecuteInEditMode]
	[AddComponentMenu("SpaceForge/Properties/Structure")]
	public sealed class Structure : SpaceForgeNetworkComponent {

		#region Nested

		[System.Serializable]
		internal struct Flags {
			
			public Flag mass;
			public Flag split;
			public Flag disinegrating;
			public Flag center;

		}

		#endregion

		#region Messages 

		static class Messages {
			
			public const short CreateTile = 1001;
			public const short RemoveTile = 1002;
			public const short SetTilePartial = 1003;
			public const short SetTileType = 1004;
			public const short AddWall = 1005;
			public const short RemoveWall = 1006;
			public const short FullSync = 1007;
			public const short SyncTile = 1008;

			public class Connection : MessageBase {
				
				public GameObject structureGO;
				public int clientConnectionID;
			}

			public class Coord : MessageBase {

				public GameObject structureGO;

				public int col;
				public int row;
			}
			
			public class TileType : MessageBase {

				public GameObject structureGO;

				public int col;
				public int row;
				public bool sealable;
				public Tile.Type type;
				public string uvCode;
			}
			
			public class TilePartial : MessageBase {

				public GameObject structureGO;

				public int col;
				public int row;
				public bool isPartial;
				public sbyte missingX;
				public sbyte missingY;
			}

			public class TileFull : MessageBase {
			
				public GameObject structureGO;
							
				public int col;
				public int row;

				public bool sealable;
				public Structural.Tile.Type type;
				public string uvCode;

				public bool isPartial;
				public sbyte missingX;
				public sbyte missingY;

				public float nIntegrity;
				public bool nWall;
				
				public float eIntegrity;
				public bool eWall;
				
				public float sIntegrity;
				public bool sWall;
				
				public float wIntegrity;
				public bool wWall;

				public bool cWall;

			}
		}

		#endregion

		#region Space Components

		Physical physical;

		Unit _unit;

		internal Unit unit {
			get {
				return _unit;
			}
		}

		#endregion

		/// <summary>
		/// The mass of all of the units and attachments on this structure,
		/// </summary>
		float loadedMass = 0f;

		Grid<Section> sections;
		
		[SerializeField] internal Style style;
		
		[SerializeField] internal Flags flags;

			#if UNITY_EDITOR

			[MenuItem("SpaceForge/Create Structure")]
			public static Structure Create() 
			{
				var structure = (new GameObject ("New Structure", typeof(Structure))).GetComponent<Structure> ();
				structure.style = Resources.Load<Style> ("Structure Styles/Prototype2D");

				return structure;
			}

			#endif

		#region Unity Callers

		void Awake()
		{
			CreateSectionsFromAttached ();
			GetSpaceComponents ();
		}

		void Start()
		{
			if (Application.isPlaying)
				flags.center = true;

			emptySectionCoords = new List<Coord>();

			if (Application.isPlaying && !isServer && isClient)
				SendFullSync ();
		}

		void Update()
		{
			#if UNITY_EDITOR
			if (Application.isEditor && !Application.isPlaying)
				EditorUpdate();
			#endif

			if (sections == null)
				return;

			sections.ForEach (section => {
				section.CheckFlags ();

				if (section.NumTiles == 0)
					emptySectionCoords.Add (section.coords);
			});

			if (flags.center.SwitchOff ()) {
				CenterSections ();
				RecalcBounds();
			}

			//SelfMass Flag
			if (flags.mass.SwitchOff() && physical) {
				float tileMass = 0;
				sections.ForEach(section => tileMass += section.mass);
				physical.body.mass = tileMass + loadedMass;
			}

			RemoveEmptySections ();

		}

			#if UNITY_EDITOR

			void EditorUpdate()
			{
				bool invalid = false;
				
				if (!style)
					return;
				
				//Check for invalid sections. In case they were deleted in the editor.
				if (sections != null)
					sections.ForEach(section => {
						if (!section)
							invalid = true;
					});
				
				if (sections == null || invalid)
					CreateSectionsFromAttached();
				
				sections.ForEach (section => section.EnsureLevelsInEditor ());
				
				if (NumTiles == 0) {
					CreateTile(new Coord(4,4), true, Tile.Type.Regular);
					CreateTile(new Coord(4,5), true, Tile.Type.Regular);
					CreateTile(new Coord(5,5), true, Tile.Type.Regular);
					CreateTile(new Coord(5,4), true, Tile.Type.Regular);
				}
			}

			#endif

		#endregion

		#region Network Behaviour Overrides

		public override void OnStartServer()
		{
			NetworkServer.RegisterHandler (Messages.CreateTile, 	  HandleSyncTileCreate);
			NetworkServer.RegisterHandler (Messages.RemoveTile, 	  HandleSyncTileRemove);
			NetworkServer.RegisterHandler (Messages.SetTilePartial,   HandleSyncTilePartial);
			NetworkServer.RegisterHandler (Messages.SetTileType,	  HandleSyncTileType);
			NetworkServer.RegisterHandler (Messages.AddWall, 		  HandleSyncAddWall);
			NetworkServer.RegisterHandler (Messages.RemoveWall,	      HandleSyncRemoveWall);
			NetworkServer.RegisterHandler (Messages.FullSync,	  	  HandleFullSync);
		}
		
		public override void OnStartClient ()
		{
			client.RegisterHandler (Messages.SyncTile, HandleFullSyncTile);
		}

		#endregion

		#region Helper

		void GetSpaceComponents()
		{
			physical = GetComponent<Physical> ();
			_unit = GetComponent<Unit> ();
		}
	
		[Client]
		void SendFullSync()
		{
			sections.ForEach (section => {
				section.gameObject.SetActive(false);
				Destroy(section.gameObject);
			});
			sections.Clear ();

			var msg = new Messages.Connection ();
			msg.clientConnectionID = client.connection.connectionId;
			msg.structureGO = gameObject;

			Game.manager.client.Send (Messages.FullSync, msg);
		}

		[Server]
		static void HandleFullSync(NetworkMessage net)
		{
			var read = net.ReadMessage<Messages.Connection> ();

			var structure = read.structureGO.GetComponent<Structure> ();
			var connectionId = read.clientConnectionID;

			structure.ForEachTile (tile => {
				var msg = new Messages.TileFull();

				msg.structureGO = read.structureGO;

				msg.col = tile.coords.column;
				msg.row = tile.coords.row;
				
				msg.isPartial = tile.IsPartial;
				msg.missingX = (sbyte)(tile.IsPartial ? tile.MissingCorner.column : 0);
				msg.missingY = (sbyte)(tile.IsPartial ? tile.MissingCorner.row : 0);
				
				msg.sealable = tile.sealable;
				msg.type = tile.type;
				msg.uvCode = tile.uvCode;
				
				msg.nIntegrity = tile.quadrants.Has(Coord.n) ? tile.quadrants.Get(Coord.n).integrity.Current : 0f;
				msg.eIntegrity = tile.quadrants.Has(Coord.e) ? tile.quadrants.Get(Coord.e).integrity.Current : 0f;
				msg.sIntegrity = tile.quadrants.Has(Coord.s) ? tile.quadrants.Get(Coord.s).integrity.Current : 0f;
				msg.wIntegrity = tile.quadrants.Has(Coord.w) ? tile.quadrants.Get(Coord.w).integrity.Current : 0f;
				
				var nNode = tile.GetNode(Coord.n);
				msg.nWall = nNode && nNode.tile == tile;
				
				var eNode = tile.GetNode(Coord.e);
				msg.eWall = eNode && eNode.tile == tile;
				
				var sNode = tile.GetNode(Coord.s);
				msg.sWall = sNode && sNode.tile == tile;
				
				var wNode = tile.GetNode(Coord.w);
				msg.wWall = wNode && wNode.tile == tile;

				msg.cWall = tile.GetNode(Coord.zero);

				NetworkServer.SendToClient (connectionId, Messages.SyncTile, msg);
			});
		}

		#endregion

		#region Section Management

		[SerializeField][ReadOnly]Vector3 _center = Vector3.zero;

		internal Vector3 center
		{
			get {
				return _center;
			}
		}

		void CalculateCenter()
		{
			_center = Vector3.zero;

			ForEachTile (tile => _center += (Vector3)tile.position);
			
			_center /= NumTiles == 0 ? 1 : NumTiles;
			_center += new Vector3(0.5f,0.5f,0f); //add the offset of the center of a tile

		}

		/// <summary>
		/// Offsets the parent so that it is in the center of it's structure.
		/// </summary>
		void CenterSections()
		{
			Vector3? newWorldPosition = null;

			CalculateCenter();

			sections.ForEach(section => {
				if (section == null)
					return;

				Vector3 oldLocal = section.transform.localPosition;
				
				section.transform.localPosition = -center;
				
				if (!newWorldPosition.HasValue)
					newWorldPosition = section.transform.parent.TransformPoint (oldLocal + center);

			});
			
			//Move the structure by the opposite amount we just moved the tile parent,
			//so it doesn't appear to jump in space
			//Vector3 worldDifference = newWorldPosition.Value - structure.transform.position;
			if (newWorldPosition.HasValue && isServer)
				transform.position = newWorldPosition.Value;
			
		}

		void CreateSectionsFromAttached()
		{
			sections = new Grid<Section>();
			var existingSections = GetComponentsInChildren<Section>();
			
			foreach(var section in existingSections) 
				sections.Add(section, section.coords);
		}

		/// <summary>
		/// Gets a section matching the section coordinates in a strucutre.
		/// If no section exists, creates it.
		/// </summary>
		/// <returns>The found or created section.</returns>
		/// <param name="column">Section Column.</param>
		/// <param name="row">Section Row.</param>
		internal Section GetSection(Coord sectionCoords, bool ensure)
		{
			Section section = null;
			
			if (sections.Occupied(sectionCoords))
				section = sections.Get(sectionCoords);
			
			else if (ensure) {
				section = Section.Create(this, sectionCoords);
				sections.Add (section, sectionCoords);
			} 
			
			return section;
		}

		internal void ForEachSection(Action<Section> action) {
			sections.ForEach (action);
		}

		List<Coord> emptySectionCoords = new List<Coord>();
		void RemoveEmptySections()
		{
			if (emptySectionCoords.Count == 0)
				return;
			
			while (emptySectionCoords.Count > 0) {

				var coord = emptySectionCoords[0];

				var section = GetSection(coord, false);
				if (!section)
					continue;
				
				sections.Remove(coord);
				emptySectionCoords.RemoveAt(0);

				#if UNITY_EDITOR
				if (Application.isEditor && !Application.isPlaying) {
					DestroyImmediate(section.gameObject);
					continue;                                                     
				}
				#endif

				Destroy (section.gameObject);
			}
		}

		#endregion

		#region Tile Management

		List<Coord> tilesAddedBySync;

		[Client]
		static void HandleFullSyncTile(NetworkMessage msg)
		{
			var read = msg.ReadMessage<Messages.TileFull> ();
			var structure = read.structureGO.GetComponent<Structure> ();

			var coords = new Coord (read.col, read.row);
			Coord? corner = read.isPartial ? new Coord (read.missingX, read.missingY) : default(Coord?);

			var tile = structure.CreateTile(coords, read.sealable, read.type, corner, read.uvCode, false);
			tile.SyncIntegrity (read.nIntegrity, read.eIntegrity, read.sIntegrity, read.wIntegrity);

			tile.SyncWall (Coord.n, read.nWall);
			tile.SyncWall (Coord.e, read.eWall);
			tile.SyncWall (Coord.s, read.sWall);
			tile.SyncWall (Coord.w, read.wWall);
			tile.SyncWall (Coord.zero, read.cWall);
	
		}

		[Client]
		void SendSyncTileCreated(Tile tile)
		{
			var msg = new Messages.TileType ();
			msg.structureGO = gameObject;
			msg.col = tile.coords.column;
			msg.row = tile.coords.row;
			msg.type = tile.type;
			msg.sealable = tile.sealable;
			msg.uvCode = tile.uvCode;
			Game.manager.client.Send (Messages.CreateTile, msg);
		}
		
		[Server]
		static void HandleSyncTileCreate(NetworkMessage msg)
		{
			var read = msg.ReadMessage<Messages.TileType> ();
			var structure = read.structureGO.GetComponent<Structure> ();

			structure.RpcSyncTileCreated (read.col, read.row, read.sealable, read.type, read.uvCode);
		}
		
		[ClientRpc]
		void RpcSyncTileCreated(int col, int row, bool sealable, Tile.Type type, string uvCode)
		{
			var coords = new Coord (col, row);
			var tile = GetTile (coords);
			if (!tile)
				tile = CreateTile (coords, sealable, type, null, uvCode, false);
		}

		public Tile CreateTile(Coord structureCoords, bool sealable, Tile.Type type, Coord? missingCorner = null)
		{
			return CreateTile (structureCoords, sealable, type, missingCorner, null, true);
		}

		internal Tile CreateTile(Coord structureCoords, bool sealable, Tile.Type type, Coord? missingCorner, string uvCode, bool doNetwork)
		{
			string error;
			if (!Tile.PlacementValid(structureCoords, this, out error))
				throw new UnityException("Cannot Create Tile:\n\t"+error);
			
			Tile tile = new Tile (structureCoords, this, sealable, type, missingCorner);

			tile.uvCode = uvCode == null ? style.CreateUVCode (tile) : uvCode;
			GetSection(Tile.CoordOfSection(tile.coords), ensure: true).AddTile (tile);

			tile.RefreshQuadrantData (true);

			if (isServer && doNetwork)
				RpcSyncTileCreated (tile.coords.x, tile.coords.y, sealable, type, tile.uvCode);

			else if (isClient && doNetwork)
				SendSyncTileCreated (tile);

			if (isServer && tile.IsPartial && doNetwork)
				RpcSyncTilePartial (tile.coords.x, tile.coords.y, tile.IsPartial, (sbyte)tile.MissingCorner.x, (sbyte)tile.MissingCorner.y);

			else if (isClient && tile.IsPartial && doNetwork)
				SendSyncTilePartial (tile);

			return tile;
		}

		[ClientRpc]
		void RpcSyncTileIntegrity(int col, int row, float n_integrity, float e_intergity, float s_integrity, float w_integrity)
		{
			var tileCoords = new Coord (col, row);
			var tile = GetTile (tileCoords);
			tile.SyncIntegrity (n_integrity, e_intergity, w_integrity, s_integrity);

		}
		
		[Client]
		void SendSyncTileType(Tile tile)
		{
			var msg = new Messages.TileType ();
			msg.structureGO = gameObject;
			msg.col = tile.coords.column;
			msg.row = tile.coords.row;
			msg.type = tile.type;
			msg.sealable = tile.sealable;
			msg.uvCode = tile.uvCode;
			Game.manager.client.Send (Messages.SetTileType, msg);
		}
		
		[Server]
		static void HandleSyncTileType(NetworkMessage msg)
		{
			var read = msg.ReadMessage<Messages.TileType> ();
			var structure = read.structureGO.GetComponent<Structure> ();

			structure.RpcSyncTileType (read.col, read.row, read.sealable, read.type, read.uvCode);
		}
		
		[ClientRpc]
		void RpcSyncTileType(int col, int row, bool sealable, Tile.Type type, string uvCode)
		{
			var coords = new Coord (col, row);
			var tile = GetTile (coords);
			if (!tile)
				throw new UnityException ("Expecting tile");
			SetTileType (coords, sealable, type, uvCode, false);
		}
		
		public void SetTileType(Coord structureCoords, bool sealable, Tile.Type type)
		{
			SetTileType (structureCoords, sealable, type, null, true);
		}
		
		internal void SetTileType(Coord structureCoords, bool sealable, Tile.Type type, string uvCode, bool doNetwork)
		{
			var tile = GetTile (structureCoords);
			if (!tile)
				return;
			
			if (uvCode != null)
				tile.uvCode = uvCode;
			
			tile.SetProperties (sealable, type);
			tile.RefreshQuadrantData (true);
			
			if (isServer && doNetwork)
				RpcSyncTileType (tile.coords.x, tile.coords.y, sealable, type, tile.uvCode);
			
			else if (isClient && doNetwork)
				SendSyncTileType (tile);

		}
		
		[Client]
		void SendSyncTilePartial(Tile tile)
		{
			var msg = new Messages.TilePartial ();
			msg.structureGO = gameObject;
			msg.col = tile.coords.column;
			msg.row = tile.coords.row;
			msg.isPartial = tile.IsPartial;
			msg.missingX = tile.IsPartial ? (sbyte)tile.MissingCorner.x : (sbyte)0;
			msg.missingY = tile.IsPartial ? (sbyte)tile.MissingCorner.y : (sbyte)0;
			Game.manager.client.Send (Messages.SetTilePartial, msg);
		}
		
		[Server]
		static void HandleSyncTilePartial(NetworkMessage msh)
		{
			var read = msh.ReadMessage<Messages.TilePartial> ();
			var structure = read.structureGO.GetComponent<Structure> ();

			structure.RpcSyncTilePartial (read.col, read.row, read.isPartial, read.missingX, read.missingY);
		}
		
		[ClientRpc]
		void RpcSyncTilePartial(int col, int row, bool partial, sbyte missingX, sbyte missingY)
		{
			var tileCoords = new Coord (col, row);
			Coord? missingCorner = null;
			if (partial)
				missingCorner = new Coord (missingX, missingY);
			
			SetTilePartial (tileCoords, missingCorner, false);
		}

		public void SetTilePartial(Coord structureCoords, Coord? missingCorner = null)
		{
			SetTilePartial (structureCoords, missingCorner, true);
		}
		
		internal void SetTilePartial(Coord structureCoords, Coord? missingCorner, bool doNetwork)
		{
			Tile tile = GetTile (structureCoords);
			if (!tile)
				return;
			
			bool set_partial = missingCorner.HasValue;
			bool can_set_partial = !tile.IsPartial || (missingCorner.HasValue && tile.MissingCorner != missingCorner.Value);
			bool can_set_whole = tile.IsPartial;
			
			if (set_partial && can_set_partial)
				tile.SetPartialTile (missingCorner.Value);
			
			else if (!set_partial && can_set_whole)
				tile.SetWholeTile ();
			
			
			if (isServer && doNetwork && set_partial) 
				RpcSyncTilePartial (tile.coords.column, tile.coords.row, set_partial, (sbyte)missingCorner.Value.x, (sbyte)missingCorner.Value.y);
			
			else if (isServer && doNetwork && !set_partial)
				RpcSyncTilePartial (tile.coords.column, tile.coords.row, false, (sbyte)0, (sbyte)0);
			
			else if (isClient && doNetwork)
				SendSyncTilePartial (tile);

		}

		[Client]
		void SendSyncTileRemove(Coord coords)
		{
			var msg = new Messages.Coord ();
			msg.structureGO = gameObject;
			msg.col = coords.column;
			msg.row = coords.row;
			client.Send (Messages.RemoveTile, msg);
		}
		
		[Server]
		static void HandleSyncTileRemove(NetworkMessage msg)
		{
			var read = msg.ReadMessage<Messages.Coord> ();
			var structure = read.structureGO.GetComponent<Structure> ();

			structure.RpcSyncTileRemoved(read.col, read.row);
		}
		
		[ClientRpc]
		void RpcSyncTileRemoved(int col, int row)
		{
			var coords = new Coord(col, row);
			var tile = GetTile (coords);
			if (tile)
				RemoveTile (coords, false);
		}

		public void RemoveTile(Coord coordInStructure)
		{
			RemoveTile (coordInStructure, true);
		}

		internal void RemoveTile(Coord coordInStructure, bool doNetwork)
		{
			var tile = GetTile (coordInStructure);
			if (!tile)
				throw new UnityException ("Cannot remove Tile, none exist at " + coordInStructure);

			Coord tileCoords = tile.coords;

			tile.TransferNodes ();
			tile.section.RemoveTile (tile);

			foreach (var coord in Coord.Around) {
				var adj = GetTile(tileCoords + coord);
				if (adj) {
					adj.RefreshQuadrantData(false);
					adj.RefreshNodeGeometry();
				}
			}

			if (isServer && doNetwork)
				RpcSyncTileRemoved (tileCoords.x, tileCoords.y);

			else if (isClient && doNetwork)
				SendSyncTileRemove (tileCoords);

		}

		public Tile GetTile(Coord coordInStructure)
		{
			Section section = GetSection(Tile.CoordOfSection(coordInStructure), ensure: false);
			if (section) 
				return section.GetTile(Tile.CoordInSection(coordInStructure));
			
			else
				return null;
		}

		Tile GetTileAt(Vector2 worldPosition, out Coord structureCoords, out Vector2 localPoint)
		{
			localPoint = transform.InverseTransformPoint (worldPosition) + center;
			int column = Mathf.FloorToInt(localPoint.x);
			int row = Mathf.FloorToInt (localPoint.y);
			
			structureCoords = new Coord(column, row);

			return GetTile(structureCoords);
		}
		
		public Tile GetTileAt(Vector2 worldPosition)
		{
			Coord coords;
			Vector2 localPoint;
			return GetTileAt (worldPosition, out coords, out localPoint);
		}
		
		//Not to toot my own horn, but this is a fucking brilliant piece of code.
		public Tile GetTileClosestTo(Vector3 worldPosition)
		{
			if (NumTiles == 0)
				return null;
			
			Coord coord;
			Vector2 localPoint;
			var exactTile = GetTileAt (worldPosition, out coord, out localPoint);
			
			if (exactTile)
				return exactTile;
			
			Tile closestTile = null;
			int numTilesChecked = 0;
			int searchIncrement = 1;
			
			while (closestTile == null) {
				
				var tilesInRing = new List<Tile>();
				bool ringComplete = false;
				int searchCol = -searchIncrement, searchRow = -searchIncrement;
				
				//we grab tiles in expanding rings around the initial row and col
				while (!ringComplete) {
					
					var searchTile = GetTile(coord + new Coord(searchCol, searchRow));
					if (searchTile) {
						tilesInRing.Add(searchTile);
						numTilesChecked++;
					}
					
					//this forces the search pointer to go around the tiles in a ring
					if (searchCol < searchIncrement && searchRow == -searchIncrement)
						searchCol ++;

					else if (searchCol == searchIncrement && searchRow < searchIncrement)
						searchRow ++;

					else if (searchCol > -searchIncrement && searchRow == searchIncrement)
						searchCol --;
					 
					else if (searchCol == -searchIncrement && searchRow > -searchIncrement)
						searchRow --;
					
					//This condition will only ever happen on the last search.
					if (searchCol == -searchIncrement && searchRow == -searchIncrement)
						ringComplete = true;
					
				}
				
				foreach(var ringTile in tilesInRing)
					if (closestTile == null || 
					    ((Vector2)ringTile.position - localPoint).sqrMagnitude < ((Vector2)closestTile.position - localPoint).sqrMagnitude)
						closestTile = ringTile;
				
				searchIncrement ++;
				
			}
			
			return closestTile;
		}
		
		public Tile GetCentermostTile()
		{
			return GetTileClosestTo(transform.position);
		}
		
		internal void FindConnectedTiles(Tile tile, List<Tile> connected)
		{
			connected.Add(tile);
			tile.flags.splitCheck = true;

			tile.ForEachAdjacent ((adj, coord) => {

				if (!adj || adj.flags.splitCheck || (adj.isDestroyed && tile.isDestroyed))
					return;
				
				FindConnectedTiles(adj, connected);

			});
		}
		
		public Tile[] FindConnectedTiles(Tile tile)
		{
			var connected = new List<Tile>();
			FindConnectedTiles(tile, connected);
			
			return connected.ToArray();			                   
		}

		public void ForEachTile(Action<Tile> action)
		{
			if (sections != null)
				sections.ForEach (section => section.ForEachTile (action));
		}

		public int NumTiles {
			get {
				int count = 0;
				if (sections == null)
					return count;

				sections.ForEach(section => count += section.NumTiles);

				return count;
			}
		}

		Bounds _bounds = new Bounds(Vector3.zero, Vector3.zero);
		public Bounds bounds {
			get {
				return _bounds;
			}
		}

		void RecalcBounds()
		{
			if (!physical)
				return;

			var strucCols = GetComponentsInChildren<PolygonCollider2D>();
			_bounds = new Bounds (physical.body.position, Vector3.zero);
			
			foreach (var col in strucCols)
				if (col.gameObject.layer == Game.Constants.StructureLayer)
					_bounds.Encapsulate (col.bounds);
				
		}

		#endregion

		#region Node Management

		static Coord[] cornerOffsets = 	new Coord[]{ Coord.nw, Coord.ne, Coord.se, Coord.sw };
		static Coord[] hWallOffsets = 	new Coord[]{ Coord.w, Coord.e };
		static Coord[] vWallOffsets = 	new Coord[]{ Coord.n, Coord.s };
		static Coord[] dWallOffsets = 	new Coord[]{ Coord.zero };

		Coord[] GetTileNodeOffsets(Coord nodeCoord)
		{
			//If x && y are even, we must be on a corner node.

			bool xEven = nodeCoord.x % 2 == 0;
			bool yEven = nodeCoord.y % 2 == 0;

			if (xEven && yEven) 
				return cornerOffsets;

			else if (xEven && !yEven)
				return hWallOffsets;

			else if (!xEven && yEven)
				return vWallOffsets;

			else 
				return dWallOffsets;

		}

		internal Node GetNode(Coord nodeCoord)
		{
			foreach (var placeCoord in GetTileNodeOffsets (nodeCoord)) {
				
				var tileCoord = Tile.CoordFromNodeSpace (nodeCoord - placeCoord);
				var tile = GetTile (tileCoord);
				
				if (!tile)
					continue;
				
				Node node = null;

				tile.ForEachNode((coord, otherNode) => {
					if (otherNode.place == placeCoord)
						node = otherNode;
				});

				if (node)
					return node;
			}
			
			return null;
		}

		internal Tile[] GetTilesSharedByNodePosition(Coord nodeCoord) 
		{
			var tiles = new List<Tile> ();
			foreach (var placeCoord in GetTileNodeOffsets (nodeCoord)) {
				
				var tileCoord = Tile.CoordFromNodeSpace (nodeCoord - placeCoord);
				var tile = GetTile (tileCoord);
				
				if (tile)
					tiles.Add (tile);
			}
			return tiles.ToArray ();
		}

		
		[Client]
		void SendSyncAddWall(Coord nodeCoord)
		{
			var msg = new Messages.Coord();

			msg.structureGO = gameObject;
			msg.col = nodeCoord.column;
			msg.row = nodeCoord.row;

			Game.manager.client.Send (Messages.AddWall, msg);
		}
		
		[Server]
		static void HandleSyncAddWall(NetworkMessage msg) {
		
			var read = msg.ReadMessage<Messages.Coord> ();
			var structure = read.structureGO.GetComponent<Structure> ();

			structure.RpcSyncWallAdd (read.col, read.row);
		}
		
		[ClientRpc]
		void RpcSyncWallAdd(int nodeCol, int nodeRow)
		{
			var coord = new Coord (nodeCol, nodeRow);
			var wall = GetNode (coord);
			if (!wall)
				AddWall (coord);
		}
		
		public Node AddWall(Coord nodeCoord)
		{
			return AddWall (nodeCoord, true);
		}

		
		internal Node AddWall(Coord nodeCoord, bool doNetwork)
		{
			foreach (var placeCoord in GetTileNodeOffsets (nodeCoord)) 
			{
				var tileCoord = Tile.CoordFromNodeSpace(nodeCoord - placeCoord);
				var tile = GetTile(tileCoord);
				
				if (!tile)
					continue;
				
				var wall = tile.CreateWall(placeCoord);

				if (isServer && doNetwork)
					RpcSyncWallAdd(nodeCoord.column, nodeCoord.row);
				else if (isClient && doNetwork)
					SendSyncAddWall(nodeCoord);

				return wall;
			}
			
			throw new UnityException ("Can't add node to that location, no tiles available.");
		}

		
		[Client]
		void SendSyncRemoveWall(Coord nodeCoord)
		{
			var msg = new Messages.Coord();

			msg.structureGO = gameObject;
			msg.col = nodeCoord.column;
			msg.row = nodeCoord.row;
			
			client.Send (Messages.RemoveWall, msg);
		}
		
		[Server]
		static void HandleSyncRemoveWall(NetworkMessage msg)
		{
			var read = msg.ReadMessage<Messages.Coord> ();
			var structure = read.structureGO.GetComponent<Structure> ();
			structure.RpcSyncRemoveWall (read.col, read.row);
		}
		
		[ClientRpc]
		void RpcSyncRemoveWall(int nodeCol, int nodeRow)
		{
			var coord = new Coord (nodeCol, nodeRow);
			var wall = GetNode (coord);
			if (wall)
				RemoveWall (coord, false);
		}
		
		public void RemoveWall(Coord nodeCoord)
		{
			RemoveWall (nodeCoord, true);			
		}

		internal void RemoveWall(Coord nodeCoord, bool doNetwork)
		{
			var node = GetNode (nodeCoord);
			if (!node || node.type == Node.Type.Corner)
				throw new UnityException ("Can't remove wall from that location, none exists.");

			node.tile.RemoveWall(node.place);

			if (isServer && doNetwork)
				RpcSyncRemoveWall (nodeCoord.column, nodeCoord.row);

			else if (isClient && doNetwork)
				SendSyncRemoveWall (nodeCoord);
		}

		#endregion

		#region Damage

		[Server]
		internal void Damage (Damage dmg)
		{

			var damagedTiles = new List<Tile> ();
			var tile = GetTileClosestTo (point);
			var damagePoint = tile.section.transform.InverseTransformPoint((Vector3)point) - (Vector3)tile.position;
			
			DamageTile (tile, amount, damagedTiles, damagePoint);
			
			foreach (var damagedTile in damagedTiles) {
				var coord = damagedTile.coords;
				var quads = damagedTile.quadrants;
				
				float northIntegrity = quads.Has(Coord.n) ? quads.Get (Coord.n).integrity.Current : 0f;
				float eastIntegrity =  quads.Has(Coord.e) ? quads.Get (Coord.e).integrity.Current : 0f;
				float southIntegrity = quads.Has(Coord.s) ? quads.Get (Coord.s).integrity.Current : 0f;
				float westIntegrity =  quads.Has(Coord.w) ? quads.Get (Coord.w).integrity.Current : 0f;
				
				RpcSyncTileIntegrity (coord.x, coord.y, northIntegrity, eastIntegrity, southIntegrity, westIntegrity);
			}

		}
				
		void DamageTile(Tile tile, float amount, List<Tile> damagedTiles, Vector2? point = null)
		{
			tile.section.flags.damageUV = true;
			damagedTiles.Add (tile);
			
			var typeData = tile.structure.style.GetTileTypeData (tile.type);
			ReverbDamage (tile, damagedTiles, amount * typeData.damageReverbFactor, ref amount);
			
			if (tile.isDestroyed)
				return;
			
			AbsorptionQuadrantDamage (tile, point, ref amount);
			RandomQuadrantDamage (tile, ref amount);
			EvenQuadrantDamage (tile, ref amount);
			
			//If we still have a remaining amount, then the tile must be destroyed.
			if (amount <= 0f)
				return;
			
			var secondaryDamageTiles = new List<Tile> ();
			secondaryDamageTiles.Add (tile);
			ReverbDamage (tile, secondaryDamageTiles, amount, ref amount);
			foreach (var secondaryTile in secondaryDamageTiles)
				if (!damagedTiles.Contains (secondaryTile))
					damagedTiles.Add (secondaryTile);
			
			tile.Destroy();
			
		}
		
		void ReverbDamage(Tile tile, List<Tile> reverberatedTiles, float total, ref float amount)
		{
			var tilesToReverb = new List<Tile> ();
			tile.quadrants.ForEach ((coord, quad) => {
				var adj = tile.GetRelativeTile(coord);
				if (!adj)
					return;
				
				if (!reverberatedTiles.Contains (adj) && adj.integrity >= tile.integrity)
					tilesToReverb.Add (adj);
			});
			
			if (tilesToReverb.Count == 0)
				return;
			
			float totalTiles = (float)tilesToReverb.Count;
			float reverbIndividual = total / totalTiles;
			if (reverbIndividual > amount * 0.5f)
				reverbIndividual = amount * 0.5f;
			
			if (reverbIndividual < Game.manager.constants.damagable.MinimumReverbDamage)
				return;
			
			foreach (var rTile in tilesToReverb)
				DamageTile(rTile, reverbIndividual, reverberatedTiles);
			
			amount -= reverbIndividual * totalTiles;
			
		}
		
		void AbsorptionQuadrantDamage(Tile tile, Vector2? focusPoint, ref float amount)
		{
			if (!focusPoint.HasValue)
				return;
			
			Coord focusedCoord;
			tile.quadrants.GetRandom (out focusedCoord);
			tile.quadrants.ForEach ((place, quad) => {
				if (quad.integrity.isEmpty)
					return;
				
				var quadPosition = new Vector2(place.x, place.y);
				var focusQuadPosition = new Vector2(focusedCoord.x, focusedCoord.y);
				
				if ((quadPosition - focusPoint.Value).sqrMagnitude < (focusQuadPosition - focusPoint.Value).sqrMagnitude)
					focusedCoord = place;
				
			});
			
			var typeData = tile.structure.style.GetTileTypeData (tile.type);
			
			QuadrantDamage (tile.quadrants.Get (focusedCoord), amount * typeData.damageAbsorptionFactor, ref amount);
		}
		
		
		void RandomQuadrantDamage(Tile tile, ref float amount)
		{
			//With remaining amount, damage the quadrants at Random.
			if (amount <= 0f)
				return;
			
			var typeData = tile.structure.style.GetTileTypeData (tile.type);
			
			foreach (var quad in tile.quadrants.ToArray()) {
				float randomDamage = UnityEngine.Random.Range(0f, amount) * typeData.damageAbsorptionFactor;
				QuadrantDamage(quad, randomDamage, ref amount);
			}
			
		}
		
		void EvenQuadrantDamage(Tile tile, ref float amount)
		{
			//With remaining amount, damage the quadrants evenly.
			while (amount > 0f) {
				
				var damagableQuadrants = new List<Tile.QuadrantData>();
				tile.quadrants.ForEach((coord, quad) => {
					if (!quad.integrity.isEmpty)
						damagableQuadrants.Add (quad);
				});
				
				if (damagableQuadrants.Count == 0)
					return;
				
				float damage = amount / (float)damagableQuadrants.Count;
				foreach (var quad in tile.quadrants.ToArray())
					QuadrantDamage(quad, damage, ref amount);
				
			}
		}
		
		void QuadrantDamage(Tile.QuadrantData quadant, float damage, ref float amount)
		{
			float overage = quadant.integrity.Deplete(damage);
			
			amount -= damage;
			amount += overage;
		}
		
		#endregion

	}

}