﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityExtensions;
using UnityExtensions.Grid;
using System.Collections;

namespace SpaceForge.Structural {
	
	[RequireComponent(typeof(BoxCollider2D))]
	public class Door : Unit {
		
		#region Data
		
		[SyncVar, SerializeField] bool shouldBeOpen;
		[HideInInspector, SerializeField] float openState = 0f;
				
		BoxCollider2D box;

		#endregion
		
		#region Links 
		
		[HideInInspector, SerializeField] Structure _structure;
		public Structure structure {
			get {
				return _structure ?? (_structure = GetComponentInParent<Structure>());
			}
		}
		
		#endregion
		
		#region Unity Callers
		
		protected override void Start()
		{
			GetBox ();
			//Validate ();
			base.Start();
		}
		
		protected override void Update()
		{
			openState = Mathf.Lerp (openState, shouldBeOpen ? 1f : 0f, Time.deltaTime * 4f);
			openState = Mathf.Round (openState * 500f) / 500f;
			box.enabled = !isWideOpen;
			TemporaryAnimate ();
		}
		
		#endregion

		#region NetworkBehaviour Overrides

		public override void OnStartServer ()
		{
			base.OnStartServer();
			NetworkServer.RegisterHandler(Messages.OpenDoor, HandleOpenDoor);
			NetworkServer.RegisterHandler(Messages.CloseDoor, HandleCloseDoor);
		}

		#endregion

		#region Messages
		
		static class Messages {
			
			public const short OpenDoor = 1012;
			public const short CloseDoor = 1013;

		}

		[Server]
		static void HandleOpenDoor(NetworkMessage msg)
		{
			var read = msg.ReadMessage<Game.Messages.UnitGameObject>();
			var door = read.unitGameObject.GetComponent<Door>();
			door.Open();
		}

		[Server]
		static void HandleCloseDoor(NetworkMessage msg)
		{
			var read = msg.ReadMessage<Game.Messages.UnitGameObject>();
			var door = read.unitGameObject.GetComponent<Door>();
			door.Close();
		}

		#endregion
		
		#region API
		
		public void Open()
		{
			if (isServer)
				shouldBeOpen = true;
			else {
				var msg = new Game.Messages.UnitGameObject();
				msg.unitGameObject = gameObject;
				client.Send(Messages.OpenDoor, msg);
			}
		}
		
		public void Close()
		{
			if (isServer)
				shouldBeOpen = true;
			else {
				var msg = new Game.Messages.UnitGameObject();
				msg.unitGameObject = gameObject;
				client.Send(Messages.CloseDoor, msg);
			}
		}
		
		public bool isAirTight {
			get {
				return openState < 0.05f;
			}
		}
		
		public bool isWideOpen {
			get {
				return openState > 0.95f;
			}
		}

		#endregion
		
		#region Creation
		
		public static bool PlacementValid(Structure structure, Tile tile, Coord nodeOffset, out string error)
		{
			if (!structure) {
				error = "Doors must be placed on valid structures.";
				return false;
				
			} else if (!structure.style || !structure.style.doorPrefab) {
				error = "Cannot place door, style for structure does not include a door prefab.";
				return false;
				
			} else if (Node.TypeFromPlacement (nodeOffset) != Node.Type.Wall) {
				error = nodeOffset+" is an invalid node offset to place a door.";
				return false;
				
			} else if (!Node.PlacementValid (tile, nodeOffset, out error)) {
				error = (error == Node.AlreadyExistsError) ? "Cannot place door, node exists at that location." : error.Replace ("node", "door");
				return false;
				
			} else if (tile.GetDoor (nodeOffset)) {
				error = "Cannot place door, door already exists in that location.";
				return false;
			}
			
			return true;
		}
		
		public static Door Create(Structure structure, Tile tile, Coord nodeOffset)
		{
			string error;
			if (!PlacementValid(structure, tile, nodeOffset, out error))
				throw new UnityException(error);
			
			var prefab = structure.style.doorPrefab;
			var instance = Instantiate<GameObject> (prefab.gameObject).GetComponent<Door> ();
			var attachable = instance.GetComponent<Attachable>();
			attachable.Attach(structure, tile, nodeOffset);
			
			instance.name = "Door";
			
			return instance;
		}
		
		#endregion
		
		#region Helper
		static Flag logDisclaimer = false;
		void TemporaryAnimate()
		{
			if (logDisclaimer.SwitchOn ())
				Debug.LogWarning ("Doors are going to need an animator component with proper opening and closing animations. Not this scale lerp bullshit.");
			
			var scale = transform.localScale;
			scale.y = 1f - openState;
			transform.localScale = scale;
		}
		/*
		void Validate()
		{
			bool valid = true;

			if (!structure) 
			{
				Debug.LogError("Door existed without a structure. Doors should only be created by structures.");
				valid = false;
			}
			
			if (valid && !tile || tile.coords != coord) 
			{
				Debug.LogError("Door existed with mismatching tile coordinates. Doors should only be created by structures.");
				valid = false;
			}
			
			if (valid && !tile.quadrants.Has(direction) || !tile.quadrants.Get (direction).door == this) 
			{
				Debug.LogError("Door reference broken on tile. Doors should only be created by structures.");
				valid = false;
			}
			
			if (valid && !tile || tile.coords != coord) 
			{
				Debug.LogError("Door existed with mismatching tile coordinates. Doors should only be created by structures.");
				valid = false;
			}
			
			if (!valid)
				Destroy (gameObject);
		}*/
		
		void GetBox()
		{
			box = GetComponent<BoxCollider2D> ();
		}
		#endregion
		
	}
}