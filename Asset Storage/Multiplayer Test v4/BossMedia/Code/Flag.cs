using UnityEngine;
using System.Collections;

namespace BossMedia {

	[System.Serializable]
	public struct Flag {
		
		[SerializeField]bool on;
		
		#region Constructors
		public Flag (bool on = false) {
			this.on = on;
		}
		
		#endregion
		
		#region Helpers
		public bool SwitchOn() {
			if (!on) {
				on = true;
				return true;
			}
			return false;
		}
		
		public bool SwitchOff() {
			if (on) {
				on = false;
				return true;
			}
			return false;
		}
		
		public void Toggle() {
			on = !on;
		}
		
		#endregion
		
		#region Operator Overloading
		
		public override string ToString ()
		{
			return ((on) ? "[On]" : "[Off]");
		}
		
		public override bool Equals (object obj) {
			return base.Equals (obj);
		}
		
		public override int GetHashCode () {
			return base.GetHashCode ();
		}
		
		public static implicit operator Flag(bool value) {
			return new Flag(value);
		}

		public static implicit operator bool(Flag value) {
			return value.on;
		}
		
		public static Flag operator == (Flag flag, bool value) {
			return flag.on == value;
		}
		
		public static Flag operator != (Flag flag, bool value) {
			return flag.on != value;
		}
		
		#endregion
	}


	public class Unfinished {

		Flag described = false;
		readonly string message;

		Unfinished (string message)
		{
			this.message = message;
		}

		public static Unfinished Code (string message) {
			return new Unfinished(message);
		}

		public void Describe()
		{
			if (described.SwitchOn ())
				Debug.LogWarning(message);
		}
	}
}