﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using System;

namespace BossMedia {

	public class MessageHandler {
		
		NetworkMessageDelegate serverHandler;
		NetworkMessageDelegate clientHandler;
		short id;
		
		public MessageHandler(NetworkMessageDelegate  handler, short id) : this(handler, handler, id) {}
		
		public MessageHandler(NetworkMessageDelegate  clientHandler, NetworkMessageDelegate serverHandler, short id)
		{
			this.id = id;
			this.serverHandler = serverHandler;
			this.clientHandler = clientHandler;
			
			if (this.clientHandler == null || this.serverHandler == null)
				throw new UnityException("Missing handler.");
		}

		public void Send(MessageBase msg)
		{
			Send (null, msg);
		}
		
		public void Send(NetworkClient from, MessageBase msg)
		{
			if (from == null)
				NetworkServer.SendToAll(id, msg);
			else
				from.Send (id, msg);
		}
		
		public void Send(MessageBase msg, params int[] clientConnectionIds)
		{
			foreach(var connectionId in clientConnectionIds) 
				NetworkServer.SendToClient(connectionId, id, msg);
		}
		
		public void Send(MessageBase msg, params NetworkClient[] clients)
		{
			foreach(var client in clients) 
				NetworkServer.SendToClient(client.connection.connectionId, id, msg);
		}
		
		public void RegisterForClient(NetworkClient client)
		{
			if (client == null)
				throw new UnityException("client can't be null.");
			client.RegisterHandler(id, clientHandler);
		}
		
		public void RegisterForServer()
		{
			NetworkServer.RegisterHandler(id, serverHandler);
		}
		
		public static implicit operator bool (MessageHandler message)
		{
			return message != null;
		}
	}
}