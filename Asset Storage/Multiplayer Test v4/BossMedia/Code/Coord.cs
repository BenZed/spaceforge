using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace BossMedia {

	[System.Serializable]
	public struct Coord {

		public int x;
		public int y;

		public int column {
			get {
				return x;
			}
			set {
				x = value;
			}
		}

		public int row {
			get {
				return y;
			}
			set {
				y = value;
			}
		}
		
		public Coord(int x, int y) {

			this.x = x;
			this.y = y;

		}

		#region Constructor Shortcuts 

		static Coord _center = new Coord(0,0);
		public static Coord center 		{ get { return _center; } }
		public static Coord zero 		{ get { return _center; } }

		static Coord _north = new Coord(0,1);
		public static Coord n 			{ get { return _north; } }
		public static Coord up 			{ get { return _north; } }
		public static Coord north		{ get { return _north; } }

		static Coord _northEast = new Coord(1,1);
		public static Coord ne 			{ get { return _northEast; } }
		public static Coord one 		{ get { return _northEast; } }
		public static Coord northEast   { get { return _northEast; } }

		static Coord _east = new Coord(1,0);
		public static Coord e 			{ get { return _east; } }
		public static Coord right		{ get { return _east; } }
		public static Coord east		{ get { return _east; } }

		static Coord _southEast = new Coord(1,-1);
		public static Coord se 			{ get { return _southEast; } }
		public static Coord southEast 	{ get { return _southEast; } }

		static Coord _south = new Coord(0,-1);
		public static Coord s 			{ get { return _south; } }
		public static Coord down 		{ get { return _south; } }
		public static Coord south		{ get { return _south; } }

		static Coord _southWest = new Coord(-1,-1);
		public static Coord sw 			{ get { return _southWest; } }
		public static Coord southWest	{ get { return _southWest;} }

		static Coord _west = new Coord(-1,0);
		public static Coord w 			{ get { return _west; } }
		public static Coord left		{ get { return _west; } }
		public static Coord west		{ get { return _west; } }

		static Coord _northWest = new Coord(-1, 1);
		public static Coord nw			{ get { return _northWest; } }
		public static Coord northWest	{ get { return _northWest; } }

		#endregion

		#region Around

		static Coord[] _around = new Coord[] {
			n,
			ne,
			e,
			se,
			s,
			sw,
			w,
			nw,
		};

		static ReadOnlyCollection<Coord> _aroundReadOnly = Array.AsReadOnly(_around);
		public static ReadOnlyCollection<Coord> Around {

			get {
				return _aroundReadOnly;
			}

		}

		public static Coord AroundResolve(Coord coord, int difference) {
			
			if (!Around.Contains(coord))
				throw new UnityException("Input coord must be n, n, ne, e, se, s, sw, w or nw");
			
			int index = 0;
			int count = _around.Length;
			for (int i = 0; i < count; i++) {
				if (coord == _around [i]) {
					index = i + difference; 
					break;
				}
			}

			while (index < 0)
				index += count;

			return _around [index % count];
		}
		
		public static Coord NextAround(Coord coord)
		{
			return AroundResolve (coord, 1);
		}
		
		public static Coord PrevAround(Coord coord)
		{
			return AroundResolve (coord, -1);
		}


		#endregion

		#region Operators and Overrides

		public static Coord operator * (Coord left, float right) {

			float x = (float)left.x;
			float y = (float)left.y;

			return new Coord(Mathf.RoundToInt(x * right), Mathf.RoundToInt(y * right));

		}

		public static Coord operator * (Coord left, int right) {

			return left * (float)right;

		}

		public static Coord operator + (Coord left, Coord right) {

			return new Coord(left.x + right.x, left.y + right.y);

		}

		public static Coord operator + (Coord left, IEnumerable<Coord> right) {

			int x = left.x;
			int y = left.y;

			foreach(var coord in right) {
				x += coord.x;
				y += coord.y;
			}

			return new Coord(x,y);

		}

		public static Coord operator - (Coord left, Coord right) {

			return new Coord(left.x - right.x, left.y - right.y);

		}

		public static Coord operator - (Coord left) {
			
			return new Coord(-left.x , -left.y);
			
		}

		public static bool operator == (Coord left, Coord right) {

			return left.x == right.x && left.y == right.y;

		}

		public static bool operator != (Coord left, Coord right) {

			return left.x != right.x || left.y != right.y;

		}

		public static explicit operator Vector2(Coord coord) {

			return new Vector2((float)coord.x, (float)coord.y);

		}

		public static explicit operator Coord(Vector2 vect) {

			return new Coord((int)vect.x, (int)vect.y);

		}

		public override string ToString () {

			return 
				this == Coord.n ? "[Coord North]" :
				this == Coord.ne ? "[Coord NorthEast]" :
				this == Coord.e ? "[Coord East]" :
				this == Coord.se ? "[Coord SouthEast]" :
				this == Coord.s ? "[Coord South]" :
				this == Coord.sw ? "[Coord SouthWest]" :
				this == Coord.w ? "[Coord West]" :
				this == Coord.nw ? "[Coord NorthWest]" :
				string.Format ("[Coord {0}, {1}]", x, y);

		}

		public override bool Equals (object obj) {

			if (obj is Coord) {

				Coord coord = (Coord) obj;
				return coord.x == x && coord.y == y;

			} else

				return false;

		}

		public override int GetHashCode () {

			return x.GetHashCode() ^ y.GetHashCode();

		}

		#endregion

	}

}