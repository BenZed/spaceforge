using UnityEngine;
using UnityEngine.Events;
using System;
using System.Reflection;

#if UNITY_EDITOR

using UnityEditor;
using UnityEditor.Events;

#endif

namespace BossMedia {

	public static class EventExtensions {

		public static void AddPersistentListener(this UnityEvent unityEvent, Component targetComponent, string methodName, params Type[] arguments)
		{
			#if UNITY_EDITOR
			try {
				if (arguments == null)
					arguments = new Type[0];
				
				MethodInfo targetInfo = UnityEvent.GetValidMethodInfo (targetComponent, methodName, arguments);
				UnityAction methodDelegate = Delegate.CreateDelegate (typeof(UnityAction), targetComponent, targetInfo) as UnityAction;
				UnityEventTools.AddPersistentListener (unityEvent, methodDelegate);
				
			} catch { }
			#endif
		}

		public static void AddPersistentListener<T0>(this UnityEvent<T0> unityEvent, Component targetComponent, string methodName, params Type[] arguments)
		{
			#if UNITY_EDITOR
			try {
				if (arguments == null)
					arguments = new Type[0];
				
				MethodInfo targetInfo = UnityEvent.GetValidMethodInfo (targetComponent, methodName, arguments);
				UnityAction<T0> methodDelegate = Delegate.CreateDelegate (typeof(UnityAction<T0>), targetComponent, targetInfo) as UnityAction<T0>;
				UnityEventTools.AddPersistentListener (unityEvent, methodDelegate);
				
			} catch { }
			#endif
		}

		public static void AddPersistentListener<T0,T1>(this UnityEvent<T0,T1> unityEvent, Component targetComponent, string methodName, params Type[] arguments)
		{
			#if UNITY_EDITOR
			try {
				if (arguments == null)
					arguments = new Type[0];
				
				MethodInfo targetInfo = UnityEvent.GetValidMethodInfo (targetComponent, methodName, arguments);
				UnityAction<T0,T1> methodDelegate = Delegate.CreateDelegate (typeof(UnityAction<T0,T1>), targetComponent, targetInfo) as UnityAction<T0,T1>;
				UnityEventTools.AddPersistentListener (unityEvent, methodDelegate);
				
			} catch { }
			#endif
		}

		public static void AddPersistentListener<T0,T1,T2>(this UnityEvent<T0,T1,T2> unityEvent, Component targetComponent, string methodName, params Type[] arguments)
		{
			#if UNITY_EDITOR
			try {
				if (arguments == null)
					arguments = new Type[0];
				
				MethodInfo targetInfo = UnityEvent.GetValidMethodInfo (targetComponent, methodName, arguments);
				UnityAction<T0,T1,T2> methodDelegate = Delegate.CreateDelegate (typeof(UnityAction<T0,T1,T2>), targetComponent, targetInfo) as UnityAction<T0,T1,T2>;
				UnityEventTools.AddPersistentListener (unityEvent, methodDelegate);
				
			} catch { }
			#endif
		}
		
		public static void AddPersistentListener<T0,T1,T2,T3>(this UnityEvent<T0,T1,T2,T3> unityEvent, Component targetComponent, string methodName, params Type[] arguments)
		{
			#if UNITY_EDITOR
			try {
				if (arguments == null)
					arguments = new Type[0];
				
				MethodInfo targetInfo = UnityEvent.GetValidMethodInfo (targetComponent, methodName, arguments);
				UnityAction<T0,T1,T2,T3> methodDelegate = Delegate.CreateDelegate (typeof(UnityAction<T0,T1,T2,T3>), targetComponent, targetInfo) as UnityAction<T0,T1,T2,T3>;
				UnityEventTools.AddPersistentListener (unityEvent, methodDelegate);
				
			} catch { }
			#endif
		}

		public static bool HasPersistantListeners(this UnityEventBase unityEvent)
		{
			return unityEvent.GetPersistentEventCount () > 0;
		}
	}
}