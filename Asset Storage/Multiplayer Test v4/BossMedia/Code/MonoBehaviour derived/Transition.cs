﻿using UnityEngine;
using UnityEngine.Events;
using System;

namespace BossMedia {

	[ExecuteInEditMode]
	public class Transition : MonoBehaviour {

		UnityAction action;
		Func<bool> isCompleted;
		UnityAction onCompleted;

		string id = null;

		#region Unity Callers

		void Start()
		{
			if (action != null && isCompleted != null)
				return;

			string error_explanation = "Transition requires both an action function and a completion function.\n";

			#if UNITY_EDITOR
			bool in_editor = Application.isEditor && !Application.isPlaying;
			if (in_editor) {
				error_explanation += "Do not add this component in Edit mode. Code should add it with Transition.Create().";
			} else
			#endif
				error_explanation += "Do not use AddComponent to add a Transition. Use Transition.Create()";


			Debug.LogError(error_explanation);
			#if UNITY_EDITOR
			if (in_editor)
				DestroyImmediate(this);
			else
			#endif
				Destroy(this);

		}

		void Update()
		{
			action ();

			if (!isCompleted ())
				return;

			if (onCompleted != null)
				onCompleted ();

			enabled = false;

			Destroy (this);
		}

		#endregion

		public static Transition Create(GameObject target, UnityAction action, Func<bool> isFinished, UnityAction onFinished = null, string id = null) 
		{
			if (target == null)
				throw new UnityException ("Cannot add a transition to null.");

			var transition = target.AddComponent<Transition>();

			transition.action = action;
			transition.isCompleted = isFinished;
			transition.onCompleted = onFinished;

			return transition;
		}

		public static Transition Create(Component target, UnityAction action, Func<bool> isFinished, UnityAction onFinished = null, string id = null) 
		{
			if (target == null)
				throw new UnityException ("Cannot add a transition to null.");
			
			return Create (target.gameObject, action, isFinished, onFinished, id);
		}

		public static Transition Get(GameObject target, string id = null)
		{
			if (target == null)
				throw new UnityException ("Cannot get a transition from null.");

			var transitions = target.GetComponents<Transition> ();
			foreach (var transition in transitions) {
				if (id != null && transition.id != id)
					continue;

				return transition;
			}

			return null;
		}

		public static Transition Get(Component target, string id = null)
		{
			if (target == null)
				throw new UnityException ("Cannot get a transition from null.");

			return Get (target.gameObject, id);
		}
	}
}