﻿using UnityEngine;
using UnityEngine.Events;

namespace BossMedia {

	/// <summary>
	/// Helper class for interpolating a transform from one transform to another. 
	/// Pretty straightforward.
	/// </summary>
	[DisallowMultipleComponent]
	internal class Interpolator : MonoBehaviour {

		const float Threshold = 0.1f;
		const float DefaultSpeed = 4f;

		#region Inspector

		[SerializeField]
		internal bool applyPosition = true;

		[SerializeField]
		internal bool applyRotation = true;

		[SerializeField]
		public bool applyScale = false;

		[Range(1f,100f), SerializeField]
		internal float speed = DefaultSpeed;

		[SerializeField] 
		public Transform target;

		[SerializeField]
		internal UnityEvent onComplete;

		#endregion

		#region API

		internal bool atPosition {
			get  {
				if (!target)
					return false;

				var delta = transform.localPosition - target.transform.localPosition;

				return delta.sqrMagnitude < Threshold * Threshold;
			}
		}

		internal bool atRotation {
			get {
				if (!target)
					return false;

				return Quaternion.Angle(transform.localRotation, target.transform.localRotation) < Threshold;
			}
		}
		
		internal bool atScale {
			get {
				if (!target)
					return false;

				var delta = transform.localScale - target.transform.localScale;
				return delta.sqrMagnitude < Threshold * Threshold;
			}
		}

		#endregion

		#region Unity Callers

		void Update () 
		{

			bool complete = true;

			if (!target) {
				enabled = false;
				return;
			}

			if (applyPosition) {
				transform.localPosition = Vector3.Lerp(transform.localPosition, target.localPosition, Time.deltaTime * speed);
				complete = complete ? atPosition : false;
			}

			if (applyRotation) {
				transform.localRotation = Quaternion.Slerp(transform.localRotation, target.localRotation, Time.deltaTime * speed);
				complete = complete ? atRotation : false;
			}

			if (applyScale) {
				transform.localScale = Vector3.Lerp(transform.localScale, target.localScale, Time.deltaTime * speed);
				complete = complete ? atScale : false;
			}

			if (complete)
				enabled = false;

			if (complete && onComplete != null)
				onComplete.Invoke ();

		}

		#endregion

		public static Interpolator Interpolate(Transform transform, Transform target, UnityAction onComplete = null, float speed = DefaultSpeed, bool applyPos = true, bool applyRot = false, bool applyScale = false)
		{
			var interpolator = transform.GetComponent<Interpolator> () ?? transform.gameObject.AddComponent<Interpolator> ();
			interpolator.target = target;

			bool hasAction = onComplete != null;

			if (hasAction && interpolator.onComplete == null)
				interpolator.onComplete = new UnityEvent ();

			if (hasAction) {
				interpolator.onComplete.RemoveAllListeners();
				interpolator.onComplete.AddListener (onComplete);
			}

			interpolator.speed = speed;
			interpolator.applyPosition = applyPos;
			interpolator.applyRotation = applyRot;
			interpolator.applyScale = applyScale;

			return interpolator;
		}
	}
	
}