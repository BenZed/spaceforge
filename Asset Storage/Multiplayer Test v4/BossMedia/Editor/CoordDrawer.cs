﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace BossMedia {

	[CustomPropertyDrawer(typeof(Coord))]
	public class CoordDrawer : PropertyDrawer {

		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			var xProp = property.FindPropertyRelative ("x");
			var yProp = property.FindPropertyRelative ("y");
						
			//Draw Property;
			EditorGUI.BeginProperty (position, label, property);
			var indent = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;

			//Draw Vital Label
			position = EditorGUI.PrefixLabel (position, GUIUtility.GetControlID (FocusType.Passive), label);

			//Current Label
			EditorGUI.PrefixLabel (position, GUIUtility.GetControlID (FocusType.Passive), new GUIContent("X  "));

			//Current Property
			var xRect = new Rect (position.x + 20, position.y, position.width * 0.2f, position.height);
			EditorGUI.PropertyField (xRect, xProp, GUIContent.none);

			//Max Label
			var yLabel = new Rect (xRect.x + xRect.width + 5, xRect.y, 15, xRect.height);
			EditorGUI.PrefixLabel (yLabel, GUIUtility.GetControlID (FocusType.Passive), new GUIContent("Y  "));

			//Max Property
			var yRect = new Rect (yLabel.x + yLabel.width + 5, position.y, xRect.width, position.height);
			EditorGUI.PropertyField (yRect, yProp, GUIContent.none);

			//EndDraw Property
			EditorGUI.indentLevel = indent;
			EditorGUI.EndProperty ();

		}
	}
}