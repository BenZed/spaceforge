using UnityEngine;
using BossMedia;
using System.Collections.Generic;

namespace SpaceForge {

	public class Resource {

		public enum Type {

			Buildacite,
			Burnacite,
			Plasmacite,

		}
		
		public readonly Type type;
		public readonly Vital stored;
		
		public Resource(Type type, float amount) {

			this.type = type;
			this.stored = new Vital (amount);

		}

		public class Group {
			
			readonly Resource buildacite;
			readonly Resource burnacite;
			readonly Resource plasmacite;
			
			public Group(float buildacite, float burnacite, float plasmacite) {

				this.buildacite = new Resource (Resource.Type.Buildacite, buildacite);
				this.burnacite = new Resource (Resource.Type.Burnacite, burnacite);
				this.plasmacite = new Resource (Resource.Type.Plasmacite, plasmacite);

				this.buildacite.stored.Replenish(100f);
				this.burnacite.stored.Replenish(100f);
				this.plasmacite.stored.Replenish(100f);

			}
		}
	}
}