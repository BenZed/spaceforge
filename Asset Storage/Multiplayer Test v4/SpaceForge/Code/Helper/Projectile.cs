﻿using UnityEngine;

namespace SpaceForge {

	[RequireComponent(typeof(Rigidbody2D))]
	[RequireComponent(typeof(Collider2D))]
	[RequireComponent(typeof(SpriteRenderer))]
	[DisallowMultipleComponent]
	public class Projectile : NetworkDummy {

		#region Links

		Rigidbody2D _body;
		internal Rigidbody2D body {
			get {
				return _body ?? (_body = GetComponent<Rigidbody2D>());
			}
		}

		#endregion

		[SerializeField]
		Damage damage;
		
		[SerializeField]
		float velocity;
		
		[SerializeField]
		float range;
		
		float duration = 0f;
		float maxDuration = 0f;

		internal Vector2 relativeVelocity;

		#region Unity Callers

		 void Awake ()
		{
			maxDuration = range / velocity;
		}
		
		void Start()
		{
			body.velocity = (Vector2)transform.up.normalized * velocity + relativeVelocity;
		}

		void OnCollisionEnter2D(Collision2D collision)
		{
			if (isServer)
				Impact (collision);

			gameObject.SetActive (false);
			Destroy (gameObject);
		}

		void Update()
		{
			duration += Time.deltaTime;
			
			if (duration >= maxDuration)
				Destroy (gameObject);
		}

		#endregion

		void Impact(Collision2D collision)
		{
			//Find Damagable
			var damagable = collision.transform.GetComponent<Damagable> ();
			if (!damagable)
				damagable = collision.transform.GetComponentInParent<Damagable> ();
				
			if (!damagable)
				return;

			//Get Center
			var center = Vector2.zero;

			foreach (var contact in collision.contacts)
				center += contact.point;

			center /= (float)collision.contacts.Length;

			//Create Damage
			var impact = new Damage (damage.amount, center, damage.type);
			damagable.Damage (impact);
		}

	}

}