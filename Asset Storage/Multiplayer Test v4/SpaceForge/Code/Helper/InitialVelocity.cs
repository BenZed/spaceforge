﻿using UnityEngine;

namespace SpaceForge {

	public class InitialVelocity : MonoBehaviour {

		[SerializeField] Vector3 initialVelocity;

		void Start()
		{
			var body = GetComponent<Rigidbody2D> ();
			body.velocity = initialVelocity;
			body.angularVelocity = initialVelocity.z;

			Destroy (this);
		}

		void OnValidate()
		{
			if (!GetComponent<Rigidbody2D> ())
				Debug.Log ("InitialVelocity Component should be placed on a gameObject with a Rigidbody2D");
		}

	}

}