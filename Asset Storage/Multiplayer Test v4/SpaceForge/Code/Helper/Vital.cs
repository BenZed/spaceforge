using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

namespace SpaceForge {
	
	[System.Serializable]
	public class Vital {
		
		[SerializeField]
		float max;
		public float Max { get { return max; } }
		
		[SerializeField, SyncVar]
		float current;
		public float Current { get { return current; } }
		
		public bool isEmpty { get { return current <= 0f; } }
		public bool isFull { get { return current >= max; } }
		public float Percent { get { return (current/max);} }

		#region Constructor 

		public Vital (float maximumAmount, float startingAmount) {
			
			if (maximumAmount <= 0f)
				throw new UnityException("maximumAmount has to be above 0.");
			
			this.max = maximumAmount;
			
			if (startingAmount < 0f || startingAmount > maximumAmount)
				throw new UnityException("startingAmount has to be between 0 and maximumAmount.");
			
			this.current = startingAmount;
		}

		public Vital (float amount) : this(amount, amount){}

		#endregion

		/// <summary>
		/// Replenish by the specified amount, returns the surplus.
		/// </summary>
		/// <param name="amount">Amount.</param>
		public float Replenish(float amount) {
			current += amount;
			
			var surplus = 0f;
			if (current > max) {
				surplus = current - max;
				current = max;
			}
			
			return surplus;
		}	
		
		/// <summary>
		/// Deplete by the specified amount, returns the surplus.
		/// </summary>
		/// <param name="amount">Amount.</param>
		public float Deplete(float amount) {
			current -= amount;
			
			var surplus = 0f;
			
			if (current < 0f) {
				surplus = -current;
				current = 0f;
			}
			
			return surplus;
		}
		
		public void Set(float amount) {
			if (amount < current)
				Deplete(current - amount);
			else
				Replenish (amount - current);
			
		}
		
		/// <summary>
		/// Sets the current 
		/// </summary>
		/// <param name="amount">Amount.</param>
		public void SetPercentage(float amount) {
			amount = Mathf.Clamp01(amount);
			Set((amount * 100f)/max);
		}
		
		public override string ToString ()
		{
			return string.Format ("[Vital: {1} / {2} ]", Current, Max);
		}

	}	
}