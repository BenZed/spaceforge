using UnityEngine;
using System;
using System.Collections;

namespace SpaceForge {

	[Serializable]
	public sealed class Atmosphere {

		[SerializeField]
		private float _o2 = 100f;
		public float o2 {
			get {
				return _o2;

			} set {
				_o2 = value >= 0f ? value : 0f;

			}
		}
		
		[SerializeField]
		private float _co2 = 0f;
		public float co2 {
			get {
				return _co2;

			} set {
				_co2 = value >= 0f ? value : 0f;

			}
		}

		public float total {
			get {
				return co2 + o2;
			}
		}

		public float o2Mix {
			get {
				return o2/total;
			}
		}

		public float co2Mix {
			get {
				return co2/total;
			}
		}
		
		public Color GetColor(float volume) {
				
			Color qualityColor = Color.Lerp (Color.yellow, Color.white, o2Mix);
			qualityColor.a = Mathf.Lerp(0f, 1f, total / volume);

			return qualityColor;

		}
			
		public void ChangePressure(float byFactor){
			if (byFactor < 0f)
				throw new System.Exception("Atmosphere pressure cannot be negative.");

			_o2 *= byFactor;
			_co2 *= byFactor;
		}
		
		public float ConvertO2(float amount){
			if (amount < 0f)
				throw new System.Exception("Cannot convert a negative amount of O2.");

			if (_o2 < amount) {
				float under = amount - _o2;
				_co2 += _o2;
				_o2 = 0;

				return under;
			}

			_co2 += amount;
			_o2 -= amount;

			return 0f;
		}
		
		public float ConvertCO2(float amount){
			if (amount < 0f)
				throw new System.Exception("Cannot convert a negative amount of CO2.");
			
			if (_co2 < amount) {
				float under = amount - _co2;
				_o2 += _co2;
				_co2 = 0;
				
				return under;
			} 
				
			_o2 += amount;
			_co2 -= amount;
			
			return 0f;
		}

		public void Exchange(Atmosphere other, float pressureExchange)
		{
			if (pressureExchange < 0f && other != null)
				other.Exchange (this, -pressureExchange);

			if (pressureExchange <= 0f)
				return;

			if (pressureExchange > total)
				pressureExchange = total;

			float o2amount = o2Mix * pressureExchange;
			float co2amount = co2Mix * pressureExchange;

			o2 -= o2amount;
			co2 -= co2amount;

			if (other != null) {
				other.o2 += o2amount; 
				other.co2 += co2amount;
			}
		}

		public override string ToString ()
		{
			return string.Format ("[Atmosphere: o2={0}, co2={1}]", o2, co2);
		}
	}
}