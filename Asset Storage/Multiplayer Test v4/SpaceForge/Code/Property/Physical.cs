﻿using UnityEngine;
using UnityEngine.Networking;

namespace SpaceForge {

	[ExecuteInEditMode]
	[DisallowMultipleComponent]
	[AddComponentMenu("SpaceForge/Properties/Physical")]
	public class Physical : Property {

		[SerializeField]
		float mass;
	
		#region Links

		Rigidbody2D _body;
		public Rigidbody2D body {
			get {				
				if (!_body)
					_body = GetComponent<Rigidbody2D>();

				if (!_body)
					_body = gameObject.AddComponent<Rigidbody2D>();

				return _body;
			}
		}

		Bounds _bounds;
		public Bounds bounds {
			get {
				if (_bounds == default(Bounds)) {
					var colliders = unit.UnitComponents<Collider2D>();
					foreach(var collider in colliders)
						_bounds.Encapsulate(collider.bounds);
				}

				return _bounds;
			}
		}

		#endregion

		#region Unity Callers

		void OnEnable()
		{			
			body.gravityScale =  		    0f;
			body.drag =				 		0f;
			body.angularDrag = 				0f;
			body.interpolation =		 	RigidbodyInterpolation2D.None;
			body.sleepMode	 = 	 		 	RigidbodySleepMode2D.StartAwake;
			body.collisionDetectionMode =	CollisionDetectionMode2D.Continuous;
			body.constraints =				RigidbodyConstraints2D.None;
			body.hideFlags = 				HideFlags.None;
			body.simulated = 				true;

		}

		void OnDisable()
		{
			if (body)
				body.simulated = false;
		}

		void FixedUpdate() 
		{
			if (mass != body.mass)
				mass = body.mass;
			
			CalculateTransationalDrag ();
			CalculateAngularDrag ();
		}

		static bool physical_damage_disabled = false;

		void OnCollisionEnter2D(Collision2D collision)
		{
			if (!physical_damage_disabled) {
				physical_damage_disabled = true;
				Debug.LogWarning ("Physical Damage Disabled, Algorithm needs further considerations.");
			}

			if (!unit.damagable || physical_damage_disabled)
				return;

			Vector2 point = Vector2.zero;
			foreach (var contact in collision.contacts)
				point += contact.point;
			point /= collision.contacts.Length;

			//If the other collision is not with another rigidbody, in the case of a collision with a structure interior representation, for
			//example, I'm not sure what the best stratgy would be. So for now the colliding mass will just be the same as the current mass
			float otherMass = collision.rigidbody ? collision.rigidbody.mass : body.mass;
			float otherRadius = collision.collider.bounds.size.magnitude;

			float mass = body.mass;
			float radius = bounds.size.magnitude;

			float amount = mass * otherMass * collision.relativeVelocity.sqrMagnitude;
			if (radius > otherRadius)
				amount *= Mathf.Clamp01 ((otherRadius / radius) * 2f);

			amount *= Game.manager.constants.damagable.ImpactDamageFactor;
			if (amount < Game.manager.constants.damagable.MinimumReverbDamage)
				return;
			
			amount /= collision.contacts.Length;

			foreach(var contact in collision.contacts)
				unit.damagable.Damage(amount, contact.point, Damage.Type.Ballistic);
		}

		#endregion

		#region Editor Callers
		
		void OnValidate()
		{
			if (!Application.isPlaying && unit && body)
				body.mass = mass;
		}
		
		#endregion

		#region Drag

		void CalculateTransationalDrag()
		{
			if (!body.simulated || body.isKinematic || !Application.isPlaying)
				return;

			var constants = Game.manager.constants;

			float thresh = constants.physical.MinSpeedDragThreshold;
			float sqrThresh = thresh * thresh;
			
			float sqrSpeed = body.velocity.sqrMagnitude;
			if (sqrSpeed <= sqrThresh) {
				body.drag = 0f;
				return;
			}
			
			float max = constants.physical.MaxSpeed;
			float minDrag = constants.physical.DragAtMinSpeed;
			float sqrMax = max * max;
			
			float maxDrag = constants.physical.DragAtMaxSpeed;
			body.drag = (((sqrSpeed - sqrThresh) / (sqrMax - sqrThresh)) * (maxDrag - minDrag)) + minDrag;
			
		}
		
		void CalculateAngularDrag()
		{
			if (!body.simulated || body.isKinematic || !Application.isPlaying)
				return;
			
			var constants = Game.manager.constants;

			float thresh = constants.physical.MinSpinDragThreshold;
			float spin = Mathf.Abs (body.angularVelocity);
			
			if (spin <= thresh) {
				body.angularDrag = 0f;
				return;
			}
			
			float max = constants.physical.MaxSpin;
			float maxDrag = constants.physical.DragAtMaxSpin;
			
			body.angularDrag = ((spin - thresh) / (max - thresh)) * maxDrag;			
		}
		
		#endregion
	}

}