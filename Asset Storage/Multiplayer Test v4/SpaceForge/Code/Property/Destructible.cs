﻿using UnityEngine;
using UnityEngine.Networking;

namespace SpaceForge {
	
	[AddComponentMenu("SpaceForge/Properties/Destructible")]
	public class Destructible : Damagable {

		uint integrityDirtyBit;

		[SerializeField]
		Vital integrity;

		public bool isDestroyed {
			get {
				return integrity.isEmpty;
			}
		}
	
		#region NetworkBehaviour overrides

		internal override void RegisterDirtyBits (System.Func<uint> nextDirtyBit)
		{
			integrityDirtyBit = nextDirtyBit ();
		}

		internal override void OnInitialSerialize (NetworkWriter writer)
		{
			writer.Write (integrity.Current);
		}

		internal override void OnInitialDeserialize (NetworkReader reader)
		{
			integrity.Set (reader.ReadSingle ());
		}

		internal override void OnUpdateSerialize (NetworkWriter writer)
		{
			if (DirtyBitIsSet (integrityDirtyBit))
				writer.Write (integrity.Current);
		}

		internal override void OnUpdateDeserialize (NetworkReader reader)
		{
			if (ReceivedDirtyBitIsSet (integrityDirtyBit))
				integrity.Set(reader.ReadSingle());
		}

		#endregion

		public override void Damage(Damage damage)
		{
			base.Damage(damage);

			bool wasDestroyed = isDestroyed;
			integrity.Deplete(damage.amount);

			if (!wasDestroyed && unit.isServer)
				SetDirtyBit(integrityDirtyBit);

			if (!wasDestroyed && isDestroyed && unit.isServer)
				NetworkServer.Destroy(gameObject);
		}

	}
}