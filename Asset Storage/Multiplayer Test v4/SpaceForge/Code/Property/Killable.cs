using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using BossMedia;

namespace SpaceForge {
	
	[AddComponentMenu("SpaceForge/Properties/Killable")]
	public class Killable : Destructible {

		uint healthDirtyBit;

		[SerializeField]
		Vital health;

		public bool isDead {
			get {
				return health.isEmpty;
			}
		}

		#region Property Overrides

		internal override void OnStartClient()
		{
			CreateOnKillMessage ();
		}

		public override void CreateEvents ()
		{
			base.CreateEvents ();

			onKill = new DamageEvent();
			onKill.AddPersistentListener<Damage>(unit, "OnKill", typeof(Damage));
		}

		#endregion

		#region Property Network overrides

		internal override void RegisterDirtyBits (System.Func<uint> nextDirtyBit)
		{
			base.RegisterDirtyBits (nextDirtyBit);
			healthDirtyBit = nextDirtyBit ();
		}
		
		internal override void OnInitialSerialize (NetworkWriter writer)
		{
			base.OnInitialSerialize (writer);
			writer.Write (health.Current);
		}
		
		internal override void OnInitialDeserialize (NetworkReader reader)
		{
			base.OnInitialDeserialize (reader);
			var amount = reader.ReadSingle ();
			health.Set (amount);

			if (isDead)
				onKill.Invoke (SpaceForge.Damage.Generic);
		}
		
		internal override void OnUpdateSerialize (NetworkWriter writer)
		{
			base.OnUpdateSerialize (writer);
			if (DirtyBitIsSet (healthDirtyBit))
				writer.Write (health.Current);
		}
		
		internal override void OnUpdateDeserialize (NetworkReader reader)
		{
			base.OnUpdateDeserialize (reader);
			if (ReceivedDirtyBitIsSet (healthDirtyBit))
				health.Set(reader.ReadSingle());
		}

		#endregion

		[SerializeField] DamageEvent onKill;

		public override void Damage(Damage damage)
		{
			base.Damage(damage);
			Hurt (damage);
		}

		public void Hurt(Damage damage)
		{
			bool wasDead = isDead;
			float overage = health.Deplete(damage.amount);

			if (!unit.isServer)
				return;

			if (!wasDead)
				SetDirtyBit(healthDirtyBit);

			if (!wasDead && isDead)
				onKillMessage.Send(new KillMessage(gameObject, overage, damage.center, (byte)damage.type));
		}

		#region Kill Message

		static MessageHandler onKillMessage;

		class KillMessage : MessageBase {

			public KillMessage () { }

			public KillMessage (GameObject gameObject, float overkillAmount, Vector2 overKillOrigin, byte overKillType)
			{
				this.gameObject = gameObject;
				this.overkillAmount = overkillAmount;
				this.overKillOrigin = overKillOrigin;
				this.overKillType = overKillType;
			}

			public GameObject gameObject;
			public float overkillAmount;
			public Vector2 overKillOrigin;
			public byte overKillType;

		}

		void CreateOnKillMessage()
		{
			if (!Application.isPlaying)
				return;

			if (!onKillMessage) {
				onKillMessage = new MessageHandler ((msg) => {

					var read = msg.ReadMessage<KillMessage> ();
					var killable = read.gameObject.GetComponent<Killable> ();
					var overkill = new Damage (read.overkillAmount, read.overKillOrigin, (Damage.Type)read.overKillType);
					killable.onKill.Invoke(overkill);

				}, Game.Messages.Kill);
			}

			onKillMessage.RegisterForClient (NetworkManager.singleton.client);
		}

		#endregion
	}
}