using UnityEngine;
using UnityEngine.Events;
using BossMedia;
using System;
using UnityEngine.Networking;

namespace SpaceForge.Structural.Ships {

	[DisallowMultipleComponent]
	[AddComponentMenu("SpaceForge/Properties/Interactable")]
	public class Interactable : Property , ISapienControl {

		#region Unity Callers

		protected override void Awake ()
		{
			base.Awake ();
			GetUI();
		}

		#endregion

		#region Main

		GameObject ui;

		void GetUI()
		{
			var uiTransform = transform.Find ("UI");
			
			if (uiTransform)
				ui = uiTransform.gameObject;

			if (ui && ui.activeSelf)
				ui.SetActive(false);
		}

		[SerializeField] bool _grabToInteract;
		public bool grabToInteract {
			get {
				return _grabToInteract;
			}
		}

		[SerializeField] bool _requireAttached;
		public bool requireAttached {
			get {
				return _requireAttached;
			}
		}

		[SerializeField] Sapien _user;
		public Sapien user {
			get {
				return _user;
			}
			private set {
				_user = value;
			}
		}
		
		public void BeginInteract(Sapien sapien)
		{
			if (user)
				return;
			
			user = sapien;
			
			if (ui && user.player && user.player.isLocalPlayer)
				ui.SetActive(true);

			if (user.player && isServer)
				unit.identity.AssignClientAuthority (user.player.connectionToClient);

			var mover = unit as ISapienControl;
			if (mover != null)
				user.control = mover;
			else
				user.control = this;
			
			onBeginInteract.Invoke(user);
		}
		
		public void EndInteract()
		{
			if (!user)
				return;

			if (user.player && isServer)
				unit.identity.RemoveClientAuthority (user.player.connectionToClient);

			user.control = null;
			user = null;

			if (ui)
				ui.SetActive(false);

			onEndInteract.Invoke();
		}
		
		#endregion

		#region ISapienControl implementation

		public void MoveFaceAim ()
		{
			if ((user.killable && user.killable.isDead) || user.movement.y < -0.5f) {
				EndInteract(); 
				return;
			}

			if (!user.physical)
				return;

			user.rotationDeterministic = true;

			float facing = Geometry2D.AbsoluteAngle(user.transform.position, transform.position) + 90f;
			var body = user.containable && user.containable.containedBodyActive ? user.containable.containedBody : user.physical.body;

			float local 		 = user.containable && user.containable.container ? user.containable.container.rotation : 0f;
			body.rotation 		 = Mathf.LerpAngle(body.rotation, facing - local, Time.fixedDeltaTime * 3f);
		}

		public void ContextAction (bool modifier, bool isLeft) 
		{ 
		}

		#endregion

		#region Events

		[Serializable]
		public class SapienEvent : UnityEvent<Sapien> {}

		[SerializeField]SapienEvent onBeginInteract;
		[SerializeField]UnityEvent onEndInteract;

		public override void CreateEvents ()
		{
			if (onBeginInteract == null)
				onBeginInteract = new SapienEvent();
			onBeginInteract.AddPersistentListener(unit, "OnBeginInteract", typeof(Sapien));

			if (onEndInteract == null)
				onEndInteract = new UnityEvent();
			onBeginInteract.AddPersistentListener(unit, "OnEndInteract");
		}

		#endregion

	}
	
}