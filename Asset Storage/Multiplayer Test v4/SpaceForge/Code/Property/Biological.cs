using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

namespace SpaceForge {

	[AddComponentMenu("SpaceForge/Properties/Biological")]
	public class Biological: Killable {

		bool inhale = false;
		bool wants_to_hold = false;

		[SerializeField] 
		Atmosphere lungs = new Atmosphere ();

		[SerializeField, Range(0.02f,0.4f), Tooltip("Units of Oxygen Required, per second.")] 
		float o2Required;

		[SerializeField, Range(1f,20f)] 
		float lungCapacity;

		#region Unity Callers

		void Update()
		{
			#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
			#endif

			MetabolizeO2 ();
			Breathe ();
		}

		#endregion

		#region Property Network derived

		uint lungsDirtyBit = 1u;

		internal override void RegisterDirtyBits (System.Func<uint> nextDirtyBit)
		{
			base.RegisterDirtyBits (nextDirtyBit);
			lungsDirtyBit = nextDirtyBit ();
		}

		internal override void OnInitialSerialize (NetworkWriter writer)
		{
			base.OnInitialSerialize (writer);
			writer.Write(lungs.o2);
			writer.Write(lungs.co2);
		}

		internal override void OnInitialDeserialize (NetworkReader reader)
		{
			base.OnInitialDeserialize (reader);
			lungs.o2 = reader.ReadSingle();
			lungs.co2 = reader.ReadSingle();
		}

		internal override void OnUpdateSerialize (NetworkWriter writer)
		{
			base.OnUpdateSerialize (writer);
			if (DirtyBitIsSet (lungsDirtyBit)) {
				writer.Write(lungs.o2);
				writer.Write(lungs.co2);
			}
		}

		internal override void OnUpdateDeserialize (NetworkReader reader)
		{
			base.OnUpdateDeserialize (reader);
			if (ReceivedDirtyBitIsSet (lungsDirtyBit)) {
				lungs.o2 = reader.ReadSingle();
				lungs.co2 = reader.ReadSingle();
			}
		}

		#endregion

		#region Helper

		const float ExhaledCap = 1f;
		const float ExhaledCapThresh = ExhaledCap + 0.5f;

		void MetabolizeO2() 
		{
			if (isDead)
				return;

			float required = o2Required * Time.deltaTime;
			float missing = lungs.ConvertO2 (required);
			float hurt = (missing / required ) * Time.deltaTime;
			var damage = new Damage (hurt, Vector2.zero, SpaceForge.Damage.Type.Biological);

			if (hurt > 0f)
				Hurt (damage);

			if (unit.isServer)
				SetDirtyBit (lungsDirtyBit);

		}

		void Breathe()
		{
			var atmosphere = this.atmosphere;

			//If Dead
			if (isDead && lungs.total > 0f)
				lungs.Exchange (atmosphere, Time.deltaTime);

			else if (isDead)
				return;

			var constants = Game.manager.constants;

			bool wants_to_breathe = lungs.o2Mix < constants.biological.InhaleO2Threshold || lungs.total < lungCapacity;
			float diaphram = lungs.total / lungCapacity;

			float inhaledCap = constants.biological.InhaledLungCapacityFactor;
			float inhaledCapThresh = inhaledCap - 0.05f;

			bool holding = ((wants_to_hold && diaphram > inhaledCapThresh) || atmosphere == null) && lungs.o2Mix > 0f && diaphram == Mathf.Clamp(diaphram, ExhaledCap, inhaledCap);

			if ((!inhale && diaphram < ExhaledCapThresh && wants_to_breathe) || diaphram < ExhaledCap)
				inhale = true;

			if ((inhale && diaphram > inhaledCapThresh) || diaphram > inhaledCap)
				inhale = false;

			if (holding)
				return;

			float delta = diaphram - Mathf.Lerp (diaphram, inhale ? inhaledCap : ExhaledCap, Time.deltaTime * constants.biological.BreathingSpeed);
			float exchange = delta * lungCapacity;

			lungs.Exchange (atmosphere, exchange);
		}

		Atmosphere _atmosphere;
		public Atmosphere atmosphere
		{
			get {
				if (_atmosphere == null && unit.containable && unit.containable.container != null)
					return unit.containable.container.ShareAtmosphere (unit.containable);

				return _atmosphere;
			}
			internal set {
				_atmosphere = value;
			}
		}

		#endregion
	
	}
}