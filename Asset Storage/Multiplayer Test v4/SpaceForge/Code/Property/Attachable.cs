﻿using UnityEngine;
using UnityEngine.Events;
using BossMedia;

namespace SpaceForge.Structural.Ships {

	[AddComponentMenu("SpaceForge/Properties/Attachable")]
	public class Attachable : Containable {

		#region Data 

		[SerializeField] Structure footprint;

		[SerializeField] UnityEvent onAttach;
		[SerializeField] UnityEvent onDetach;

		GameObject uiInstance;

		[SerializeField] Coord _coords;
		public Coord coords {
			get {
				return _coords;
			}
			private set {
				_coords = value;
			}
		}
		
		[SerializeField] Coord _direction;
		public Coord dir {
			get {
				return _direction;
			}
			private set {
				_direction = value;
			}
		}

		Structure _containerStructure;
		public Structure containerStructure {
			get {
				if (!container)
					return null;

				if (_containerStructure && _containerStructure.transform != container.transform)
					_containerStructure = null;

				return _containerStructure ?? (_containerStructure = container.GetComponent<Structure>());
			}
		}

		#endregion

		#region Events

		public override void CreateEvents ()
		{
			base.CreateEvents ();
			onAttach = new UnityEvent ();
			onAttach.AddPersistentListener (unit, "OnAttach");

			onDetach = new UnityEvent ();
			onDetach.AddPersistentListener (unit, "OnDetach");
		}

		#endregion

		#region Placement

		static Unfinished validation = Unfinished.Code("Validate placement does not yet take footprints into account properly. They are simply ignored for now.");

		void ValidatePlacement()
		{
			validation.Describe ();

			RoundCoordinates (transform);

			var pos = transform.localPosition;
			_coords = new Coord (Mathf.FloorToInt(pos.x), Mathf.FloorToInt(pos.y));

			Tile tile = containerStructure.GetTile (coords);
			if (!tile) {
				Destroy(gameObject);
				throw new UnityException ("Attachments need to be placed on a valid tile.");
			}
		}

		internal static void RoundCoordinates(Transform transform)
		{
			//Round pos
			var pos = transform.localPosition;
			pos.x = Mathf.Floor (pos.x) + 0.5f;
			pos.y = Mathf.Floor (pos.y) + 0.5f;
			pos.z = 0f;
			transform.localPosition = pos;
			
			//Round rot
			float angle = transform.localEulerAngles.z;
			angle = Mathf.Round (angle / 90f) * 90f;
			transform.localEulerAngles = Vector3.forward * angle;
		}

		internal static bool CompareFootPrints(Structure footprint, Structure structure, Coord offset)
		{
			bool valid = true;
			
			if (footprint == null)
				return true;
			
			if (structure == null)
				return false;
			
			footprint.ForEachTile(footTile => {
				if (!valid)
					return;
				
				var compareTile = structure.GetTile (footTile.coords + offset);
				if (!compareTile) {
					valid = false;
					return;
				}
				
				for (var i = 0; i < 8; i += 2) {
					Coord dir = Coord.Around[i];
					bool quadMatch = footTile.quadrants.Has(dir) && compareTile.quadrants.Has (dir);
					bool nodeMatch = footTile.GetNode(dir) && compareTile.GetNode(dir);
					
					if (!(quadMatch && nodeMatch)) {
						valid = false;
						return;
					}
				}
			});
			
			return valid;
		}

		#endregion
	}
}