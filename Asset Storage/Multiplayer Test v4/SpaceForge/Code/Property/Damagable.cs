﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using BossMedia;

namespace SpaceForge {

	[Serializable]
	public struct Damage {

		public enum Type {
			Ballistic,
			Energetic,
			Radiative,
			Incindiary,
			Biological,
		}

		public float amount;
		[NonSerialized] public Vector2 center;
		public Type type;

		public Damage(float amount, Vector2 center, Type type)
		{
			this.amount = amount;
			this.center = center;
			this.type = type;
		}

		public static Damage Generic
		{
			get {
				return new Damage(0f, Vector2.zero, Type.Ballistic);
			}
		}
	}

	[DisallowMultipleComponent]
	[AddComponentMenu("SpaceForge/Properties/Damagable")]
	public class Damagable : Property {

		[Serializable]
		protected class DamageEvent : UnityEvent<Damage>{}

		[SerializeField]
		DamageEvent onDamage;
	
		#region Property Overrides

		public override void CreateEvents ()
		{
			onDamage = new DamageEvent ();
			onDamage.AddPersistentListener (unit, "OnDamage", typeof(Damage));
		}

		#endregion

		#region API

		public void Damage(float amount, Vector2 center, Damage.Type type)
		{
			var damage = new Damage(amount, center, type);
			Damage(damage);
		}

		public virtual void Damage(Damage damage)
		{
			onDamage.Invoke (damage);
		}

		#endregion

	}
}