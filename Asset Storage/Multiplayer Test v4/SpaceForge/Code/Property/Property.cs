﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Reflection;
using BossMedia;

namespace SpaceForge {

	/// <summary>
	/// Base class for modifiers that can be applied to a Unit. Is an Obstacle destructible? Is a Runner buffed?
	/// 
	/// Properties must be placed on GameObjects that have a Unit component. 
	/// 
	/// Rather than being NetworkBehaviours themselves, they hijack NetworkSerialization functionality from their attached unit
	/// component in the form of abstract properties. This way, a unit could have several properties without the added overhead
	/// of sending information for several NetworkBehaviours. There are pros and cons to doing it this way, but for a quick mobile
	/// game, this should fit very nicely.
	/// </summary>
	[RequireComponent(typeof(Unit)), ExecuteInEditMode]
	public abstract class Property : NetworkSlave, IUnitSerializedEvent {

		#region Links

		Unit _unit;
		public Unit unit {
			get {
				return _unit ?? (_unit = master as Unit) ?? (_unit = GetComponent<Unit>());
			}
		}

		#endregion


		protected virtual void Awake()
		{
			#if UNITY_EDITOR
			if (eventsInitialized.SwitchOn())
				CreateEvents();
		}

		[SerializeField, HideInInspector] Flag eventsInitialized = false;
			#else
		}
			#endif

		public virtual void CreateEvents() {}
	}

}
