using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using BossMedia;
using SpaceForge.Structural;

namespace SpaceForge {

	[DisallowMultipleComponent]
	[AddComponentMenu("SpaceForge/Properties/Containable")]
	public class Containable : Property {

		#region Inspector
		
		[SerializeField, Range(1,100)] int _size;
		public int size {
			get {
				return _size;
			}
		}
		
		#endregion

		#region Unity Callers

		void OnTransformParentChanged()
		{
			container = transform.parent ? transform.parent.GetComponent<Container> () : null;

			if (unit.isServer) {
				SetDirtyBit (containerDirtyBit);
				unit.ForceSyncBody();
			}
		}

		void OnValidate()
		{
			if (_size < 1)
				_size = 1;
		}

		void LateUpdate()
		{
			if (!containedBodyActive || !transform.parent)
				return;

			var pos = containedBody.transform.localPosition;
			pos.z = unit.transform.localPosition.z;

			unit.transform.localPosition = pos;
			unit.transform.localEulerAngles = containedBody.transform.localEulerAngles;
		}

		protected virtual void Start()
		{
			if (!Application.isPlaying)
				return;

			if (isServer && container)
				container.Load (this);

			else if (!isServer)
				SendEnsureContainerMessage ();

		}

		#endregion

		#region Container

		[SerializeField] Container _container;
		public Container container {
			get {
				return _container;
			}
			private set {
				_container = value;
			}
		}

		#endregion

		#region Property overrides

		internal override void OnStartClient()
		{
			CreateEnsureContainerMessage ();
		}

		uint containerDirtyBit;

		internal override void RegisterDirtyBits (Func<uint> nextDirtyBit)
		{
			containerDirtyBit = nextDirtyBit ();
		}

		internal override void OnInitialDeserialize (NetworkReader reader)
		{
			base.OnInitialDeserialize (reader);
			if (container) 
				container.Load (this);

		}

		internal override void OnUpdateSerialize (NetworkWriter writer)
		{
			if (DirtyBitIsSet (containerDirtyBit)) {
				Transform containerT = container ? container.transform : null;
				writer.Write (containerT);
			}
		}

		internal override void OnUpdateDeserialize (NetworkReader reader)
		{
			if (ReceivedDirtyBitIsSet (containerDirtyBit)) {
				Transform parent = reader.ReadTransform ();

				if (transform.parent == parent)
					return;

				if (parent)
					parent.GetComponent<Container>().Load (this);

				else if (container)
					container.Unload(this);

			}
		}

		#endregion

		#region ContainedBody

		Rigidbody2D _containedBody; 
		public Rigidbody2D containedBody {
			get {
				return _containedBody;
			}
		}
		
		internal bool containedBodyActive {
			get {
				return _containedBody && _containedBody.gameObject.activeSelf;
			}
		}

		internal Rigidbody2D EnableContainedBody(Transform interiorTransform)
		{
			if (!_containedBody)
				_containedBody = CreateContainableBody ();

			_containedBody.gameObject.SetActive (true);
			_containedBody.transform.parent = interiorTransform;

			if (unit.physical)
				unit.physical.enabled = false;

			return _containedBody;
		}

		internal void MatchContainedBody()
		{
			_containedBody.transform.localPosition = unit.transform.parent.InverseTransformPoint(unit.transform.position);
			_containedBody.transform.localRotation = unit.transform.localRotation;
		}

		internal void DisableContainedBody()
		{
			if (!_containedBody)
				_containedBody = CreateContainableBody ();

			if (unit.physical) {
				var containerBody = container.physical.body;
				unit.physical.body.position = unit.transform.position;
				unit.physical.body.velocity = Geometry2D.RotateVector (containedBody.velocity, containerBody.transform.localEulerAngles.z) + containerBody.GetPointVelocity (unit.physical.body.position);
				unit.physical.body.angularVelocity = containedBody.angularVelocity;
			}
			_containedBody.gameObject.SetActive (false);
			_containedBody.transform.parent = transform;
			_containedBody.transform.localPosition = Vector3.zero;

			if (unit.physical)
				unit.physical.enabled = true;
		}

		Rigidbody2D CreateContainableBody() {
			var colliders = unit.GetComponents<Collider2D> ();
			transform.gameObject.SetActive (false);
			
			GameObject newGameObject = new GameObject (name + " (Interior)");
			newGameObject.SetActive (false);
			newGameObject.layer = Game.Constants.Layers.Interior;
			
			newGameObject.transform.parent = transform;
			newGameObject.transform.localScale = transform.localScale;

			foreach (Collider2D collider2D in colliders) {

				if (collider2D.isTrigger)
					continue;

				var boxCollider = collider2D as BoxCollider2D;
				if (boxCollider) {
					var newBoxCollider = newGameObject.AddComponent<BoxCollider2D> ();
					newBoxCollider.size = boxCollider.size;
					newBoxCollider.offset = boxCollider.offset;
					continue;
				}
				
				var circleCollider = collider2D as CircleCollider2D;
				if (circleCollider) {
					var newCircleCollider = newGameObject.AddComponent<CircleCollider2D> ();
					newCircleCollider.offset = circleCollider.offset;
					newCircleCollider.radius = circleCollider.radius;
					continue;
				}
				
				var polygonCollider = collider2D as PolygonCollider2D;
				if (polygonCollider) {
					var newPolygonCollider = newGameObject.AddComponent<PolygonCollider2D> ();
					newPolygonCollider.pathCount = polygonCollider.pathCount;
					for (var i = 0; i < newPolygonCollider.pathCount; i++)
						newPolygonCollider.SetPath (i, newPolygonCollider.GetPath (i));
					continue;
				}
			}

			var body = newGameObject.AddComponent<Rigidbody2D> ();
			if (unit is Sapien)
				body.constraints = RigidbodyConstraints2D.FreezeRotation;

			body.drag = Game.manager.constants.ship.InteriorDrag;
			body.angularDrag = Game.manager.constants.ship.InteriorAngularDrag;
			body.gravityScale = 0f;
			body.isKinematic = !unit.physical;

			gameObject.SetActive(true);

			return body;
		}

		#endregion

		#region Events

		[Serializable]
		public class ContainerEvent : UnityEvent<Container> {}

		[SerializeField] ContainerEvent onEnter;
		[SerializeField] UnityEvent onExit;

		public override void CreateEvents()
		{
			onEnter = new ContainerEvent();
			onEnter.AddPersistentListener<Container>(unit, "OnEnter", typeof(Container));

			onExit = new UnityEvent();
			onExit.AddPersistentListener(unit, "OnExit");
		}

		#endregion

		#region Ensure Container
		
		//What is this ensureParent message? Well, for some reason, transforms cant seem to be reliably sent OnInitialSerialize()
		//Instead, we have the client ask the server to manually set the parentDirtyBit in Start()
		static MessageHandler ensureContainer;
		
		static void CreateEnsureContainerMessage()
		{
			if (ensureContainer)
				return;
			
			ensureContainer = new MessageHandler ((msg) => {
				
				var read = msg.ReadMessage<Game.Messages.GameObjectMessage>();
				var containable = read.gameObject.GetComponent<Containable>();
				//simulate OnTransformChanged on target containable so the correct dirty bit is set

				containable.OnTransformParentChanged ();
				
			}, Game.Messages.EnsureContainer);
			
			ensureContainer.RegisterForServer ();
		}
		
		void SendEnsureContainerMessage()
		{
			if (unit.isServer)
				return;

			ensureContainer.Send (NetworkManager.singleton.client, new Game.Messages.GameObjectMessage(gameObject));
		}
		
		#endregion

	}
}