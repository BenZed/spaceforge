using UnityEngine;
using System.Collections;
using SpaceForge.Structural;
using BossMedia;

namespace SpaceForge {

	[AddComponentMenu("SpaceForge/Craft")]
	[RequireComponent(typeof(Structure))]
	public class Craft : Container , ISapienControl {

		[SerializeField] Transform pilotSeat;
		CircleCollider2D enterTrigger;

		[SerializeField] Atmosphere atmosphere;

		[SerializeField] Sapien pilot;

		Structure _structure;
		public Structure structure {
			get { return _structure ?? (_structure = GetComponent<Structure> ()); }
		}

		#region ISapienMover implementation

		public void MoveFaceAim ()
		{
			Debug.Log (pilot.movement);
		}

		public void ContextAction(bool modifier, bool isLeft)
		{

		}

		#endregion

		#region Unity Callers

		void OnValidate()
		{
			if (pilotSeat && pilotSeat.parent != transform) {
				pilotSeat = null;
				Debug.LogError("Pilot Seat must be parented to the craft.");
			}
		}

		void OnTriggerStay2D(Collider2D other)
		{
			if (!other.IsTouching (enterTrigger))
				return;

			if (pilot)
				return;

			var containable = other.GetComponent<Containable> ();
			if (!CanLoad (containable))
				return;

			var sapien = (Sapien)containable.unit;
			if (sapien.leftHand.grabbedBody == physical.body || sapien.rightHand.grabbedBody == physical.body)
				Load (containable);

		}

		void Start()
		{
			enterTrigger = pilotSeat ? pilotSeat.GetComponent<CircleCollider2D> () : null;
		}

		#endregion

		#region Property Callers

		void OnDamage(Damage dmg)
		{
			structure.Damage (dmg);
		}

		#endregion

		#region Container Overrides

		protected override int InitialLayer ()
		{
			return Game.Constants.Layers.Exterior;
		}

		public override bool CanLoad (Containable containable)
		{
			return base.CanLoad (containable) && containable.unit is Sapien;
		}

		public override void Load (Containable containable)
		{
			containable.transform.parent = transform;
			Interpolator.Interpolate (containable.transform, pilotSeat, null, 4f, true, true);
			pilot = (Sapien)containable.unit;
			pilot.leftHand.Release();
			pilot.rightHand.Release();
			pilot.control = this;
		}

		public override void Unload (Containable containable)
		{
			if (pilot && pilot.containable == containable) {
				TryParentContainerTransfer(pilot.containable);
				pilot.control = null;
			}
		}

		public override Atmosphere ShareAtmosphere (Containable containable)
		{
			return atmosphere;
		}

		#endregion
	}

}