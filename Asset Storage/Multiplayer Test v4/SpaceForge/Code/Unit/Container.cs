﻿using UnityEngine;

namespace SpaceForge.Structural {

	public abstract class Container : Unit {

		#region Unity Callers

		void OnValidate()
		{
			if (maxSize < 1)
				maxSize = 1;
		}

		#endregion

		#region API

		[SerializeField] int _maxSize = 1;
		public int maxSize {
			get {
				return _maxSize;
			}
			private set {
				_maxSize = value;
			}
		}

		public virtual bool CanLoad(Containable containable)
		{
			return containable && containable.size <= maxSize;
		}

		public virtual void Load(Containable containable)
		{
			containable.transform.parent = transform;
		}

		public virtual bool CanUnload(Containable containable)
		{
			//If the containable exists, isn't this unit and is parented to this unit
			return containable != null && containable.transform != transform && containable.transform.IsChildOf (transform);
		}

		public virtual void Unload(Containable containable)
		{
			TryParentContainerTransfer (containable);
		}

		protected void TryParentContainerTransfer(Containable containable)
		{
			Container parentContainer = this.containable ? this.containable.container : null;
			
			if (parentContainer && parentContainer.CanLoad (containable))
				parentContainer.Load (containable);
			else
				containable.transform.parent = null;
		}

		public virtual Atmosphere ShareAtmosphere(Containable containable) 
		{
			return null;
		}

		#endregion

	}

}