using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using BossMedia;
using SpaceForge.Structural;
using SpaceForge.Structural.Ships;

namespace SpaceForge {

	internal class Hand : BodyPart {

		public enum ReachType {
			None,
			Left,
			Right,
			RightAndLeft,
			RightOrLeft,
		}

		Quaternion idleRotation;

		[SerializeField] 
		Sprite defaultOpenSprite;

		[SerializeField] internal Sprite openSprite;
		[SerializeField] internal Sprite closedSprite;

		[SerializeField]
		Vector2 idleCenter;

		[SerializeField]
		Vector2 reachCenter;

		[SerializeField]
		float reachRange;

		[SerializeField]
		Vector2 grabCenter;
		
		[SerializeField]
		float grabRange;

		[SerializeField] bool _isLeft;
		public bool isLeft {
			get {
				return _isLeft;
			}
		}

		bool just_tried_grab = false;

		Hand otherHand;
		DistanceJoint2D grabJoint;
		
		public Rigidbody2D grabbedBody {
			get {
				return grabJoint ? grabJoint.connectedBody : null;
			}
		}

		#region BodyPart Override

		protected override void SetTargets ()
		{
			bool is_aiming = IsAiming ();
			bool grab_sprite = !just_tried_grab && !grabbedBody && (is_aiming && (!equipment || !equipment.enabled));

			just_tried_grab = false;

			if (!is_aiming && grabbedBody)
				Release ();
		
			if (is_aiming || grabbedBody)
				Aim ();

			else if (equipment && equipment.enabled)
				Steady ();
			else
				Idle ();

			VisualizeGrabbing(grab_sprite);
		}

		internal override void SetDefaultSprite ()
		{
			openSprite = defaultOpenSprite;
			closedSprite = defaultSprite;
			base.SetDefaultSprite ();
		}

		#endregion

		#region Unity Callers

		void Awake()
		{
			FindOtherHand ();
			SetupGrabJoint ();
			SetIdleRotation ();
		}

		#endregion

		#region Editor Callers

		void OnDrawGizmosSelected()
		{
			Gizmos.color = _isLeft ? Color.red : Color.green;
			Gizmos.DrawWireSphere (transform.parent.TransformPoint((Vector3)reachCenter), reachRange);
			Gizmos.DrawWireSphere (transform.parent.TransformPoint((Vector3)idleCenter), 0.01f);
			Gizmos.DrawWireSphere (transform.TransformPoint((Vector3)grabCenter), grabRange);
		}

		void OnValidate()
		{
			if (!otherHand)
				FindOtherHand ();

			//the the rest of the function just ensures that changes to one hand are mirro
			otherHand.idleCenter = idleCenter;
			otherHand.idleCenter.x = -otherHand.idleCenter.x;

			otherHand.reachCenter = reachCenter;
			otherHand.reachCenter.x = -otherHand.reachCenter.x;

			otherHand.reachRange = reachRange;

			otherHand.grabCenter = grabCenter;
			otherHand.grabRange = grabRange;

			otherHand._isLeft = !isLeft;
		}

		#endregion

		#region Helper

		void FindOtherHand()
		{
			var hands = sapien.UnitComponents<Hand> ();
			foreach (var hand in hands) {
				if (hand == this)
					continue;
				
				otherHand = hand;
				break;
			}
		}

		void SetupGrabJoint()
		{
			var grabJoints = new List<DistanceJoint2D> ();
			grabJoints.AddRange (sapien.GetComponents<DistanceJoint2D> ());

			while (grabJoints.Count < 2)
				grabJoints.Add (sapien.gameObject.AddComponent<DistanceJoint2D> ());

			int myIndex = isLeft ? 1 : 0;
			grabJoint = grabJoints[myIndex];
			grabJoint.distance = 0.15f;
			grabJoint.maxDistanceOnly = true;
			grabJoint.enableCollision = true;
			grabJoint.enabled = false;

		}

		internal void Grab()
		{
			just_tried_grab = true;

			if (grabbedBody)
				return;

			bool in_ship = sapien.gameObject.layer == Game.Constants.Layers.ShipObstacle;

			Vector2 point = transform.TransformPoint (grabCenter);
			Collider2D[] collidersGrabbed = Physics2D.OverlapCircleAll (point, grabRange, (1 << Game.Constants.Layers.ShipObstacle) | (1 << Game.Constants.Layers.Exterior));

			foreach (Collider2D collider in collidersGrabbed) {
				bool is_door = collider.gameObject.CompareTag("Door");
			
				if (is_door && sapien.isServer) {
					var door = collider.transform.parent.GetComponent<Door>();
					door.ship.SetDoorOpen(door.coord, door.dir, !door.Open);
				}

				if (is_door)
					continue;

				Interactable interactable = in_ship ? collider.GetComponent<Interactable>() : null;
				if (interactable && !interactable.user && interactable.grabToInteract) {
					interactable.BeginInteract(sapien);
					break;
				}

				if (collider.isTrigger || !collider.attachedRigidbody)
					continue;

				if (!collider.attachedRigidbody.GetComponent<NetworkIdentity>())
					continue;

				if (collider.attachedRigidbody == sapien.physical.body)
					continue;

				if (sapien.containable && 
				    sapien.containable.container && 
				    sapien.containable.container.physical && 
				    collider.attachedRigidbody == sapien.containable.container.physical.body)
					continue;

				if (in_ship)
					continue;

				grabJoint.connectedBody = collider.attachedRigidbody;

				grabJoint.anchor = Vector2.zero;

				Vector2 cAnchor = collider.attachedRigidbody.transform.InverseTransformPoint(point);
				grabJoint.connectedAnchor = cAnchor;
				grabJoint.anchor = idleCenter;
				grabJoint.distance = ((Vector2)sapien.transform.position - point).magnitude;
				grabJoint.maxDistanceOnly = true;
				grabJoint.enabled = true;
				sapien.z = collider.transform.position.z - 0.5f;

				break;
			}

		}

		internal void Release()
		{
			if (!grabbedBody)
				return;

			grabJoint.connectedBody = null;
			grabJoint.enabled = false;
		}

		void SetIdleRotation()
		{
			float angle = Geometry2D.AbsoluteAngle (Vector2.down * 0.125f, idleCenter) + 90f;
			idleRotation = Quaternion.Euler (0f, 0f, angle);
		}

		void Aim()
		{
			Vector2 point;

			if (grabbedBody) {
				point = grabJoint.connectedBody.transform.TransformPoint (grabJoint.connectedAnchor);
				point = sapien.transform.InverseTransformPoint(point);
				point = Vector2.MoveTowards(point, idleCenter, grabRange * 1.5f);
				targetSpeed = DefaultLerpSpeed * 5f;

			} else 
				point = (Vector2)sapien.transform.InverseTransformPoint (sapien.aiming);

			float aimAngle = Geometry2D.AbsoluteAngle (idleCenter, point) + 90f;
			targetRotation = Quaternion.Euler (0f, 0f, aimAngle);

			var reach = grabbedBody ? point : Geometry2D.ClampVector (point, reachRange) + reachCenter;

			//Prevents arms from being able to reach all the way around and scratch your back
			if (!grabbedBody && reach.y < 0f && ((!isLeft && reach.x < idleCenter.x) || (isLeft && reach.x > idleCenter.x)))
				reach.x = idleCenter.x;

			targetPosition = reach;
		}

		void Idle()
		{
			targetPosition = idleCenter;
			targetRotation = idleRotation;
		}

		void Steady()
		{
			//targetPosition = idleCenter;
			Vector2 currentPosition = transform.localPosition;

			Vector2 idleOff = currentPosition - idleCenter;

			float steadyRange = reachRange * 0.5f;
			if (idleOff.sqrMagnitude <= steadyRange * steadyRange)
				targetPosition = currentPosition;
			else
				targetPosition = (idleOff.normalized * steadyRange) + idleCenter;

			targetSpeed = DefaultLerpSpeed * 0.5f;
		}

		internal bool IsAiming()
		{
			if (sapien.reach == ReachType.RightAndLeft)
				return true;
			
			else if (sapien.reach == ReachType.Left)
				return isLeft;
			
			else if (sapien.reach == ReachType.Right)
				return !isLeft;
			
			//if (sapien.reach == ReachType.None)
			return false;
			
			//sapien.reach must be ReachType.RightOrLeft, so the computer decides which hand
			/*
			if (!otherHand || (otherHand.equipment && otherHand.equipment.equipped))
				return true;

			Vector2 aim = sapien.transform.InverseTransformPoint (sapien.aiming);

			float sqrDist = 		 (aim - (Vector2)	 	   transform.localPosition).sqrMagnitude;
			float otherHandSqrDist = (aim - (Vector2)otherHand.transform.localPosition).sqrMagnitude;

			bool isCenterBody = (isLeft && aim.x <= otherHand.idleCenter.x) || (!isLeft && aim.x >= otherHand.idleCenter.x);

			return isCenterBody || sqrDist < otherHandSqrDist;
			*/
		}

		void VisualizeGrabbing(bool grabbing)
		{
			Sprite current = spriter.sprite;
			if (grabbing && current != openSprite)
				spriter.sprite = openSprite;
			
			else if (!grabbing && current != closedSprite)
				spriter.sprite = closedSprite;
		}

		#endregion

	}
}