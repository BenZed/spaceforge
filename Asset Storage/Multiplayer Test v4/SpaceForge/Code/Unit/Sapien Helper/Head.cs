using UnityEngine;
using BossMedia;

namespace SpaceForge {

	internal class Head : BodyPart {

		const float MinTurnPointY = 0.05f;

		#region BodyPart Overrides

		void Awake ()
		{
			globalRotation = true;
		}

		protected override void SetTargets()
		{
			//Get point in unit space
			Vector2 turnPoint = sapien.transform.InverseTransformPoint (sapien.aiming);

			//limit turn point to be facing ahead of unit
			if (turnPoint.y < MinTurnPointY)
				turnPoint.y = MinTurnPointY;

			//back to world space
			turnPoint = sapien.transform.TransformPoint (turnPoint);

			//+90 because AbsoluteAngle has a different 0 degree point that unity
			float angle = Geometry2D.AbsoluteAngle (sapien.transform.position, turnPoint) + 90f;

			//determin quat
			targetRotation = Quaternion.Euler (0f, 0f, angle);

		}

		#endregion

	}
	
}