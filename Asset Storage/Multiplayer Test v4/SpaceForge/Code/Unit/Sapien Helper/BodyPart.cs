﻿using UnityEngine;
using UnityEngine.Networking;

namespace SpaceForge {

	[RequireComponent(typeof(SpriteRenderer)), DisallowMultipleComponent]
	internal abstract class BodyPart : MonoBehaviour {

		protected const float DefaultLerpSpeed = 5f;

		[SerializeField] protected Sprite defaultSprite;

		#region Links

		Sapien _sapien;
		protected Sapien sapien {
			get {
				return _sapien ?? (_sapien = transform.parent.GetComponent<Sapien>());
			}
		}

		SpriteRenderer _spriter;
		internal SpriteRenderer spriter {
			get {
				return _spriter ?? (_spriter = transform.GetComponent<SpriteRenderer>());
			}
		}

		#endregion

		protected Quaternion targetRotation;
		protected Vector3 targetPosition;
		protected float targetSpeed;

		Equipment _equipment;
		public Equipment equipment {
			get {
				//Refresh equipment if this transform has children and equipment isn't assigned, or if the assigned equipment isn't parented to this transform
				if ((transform.childCount > 0 && !_equipment) || (_equipment && _equipment.transform.parent != transform))
					_equipment = GetComponentInChildren<Equipment> ();

				return _equipment;
			}
		}
	
		#region 

		protected abstract void SetTargets ();

		protected virtual void MoveEquipment() { }

		internal virtual void SetDefaultSprite()
		{
			spriter.sprite = defaultSprite;
		}

		#endregion

		#region Unity Callers

		void Awake()
		{
			if (!spriter.sprite)
				SetDefaultSprite();
		}

		void LateUpdate()
		{
			if (sapien.killable && sapien.killable.isDead) {
				enabled = false;
				return;
			}

			targetSpeed = DefaultLerpSpeed;
			SetTargets ();
			LerpToTargets ();
		}

		#endregion

		#region Helper

		protected bool globalRotation = false;

		void LerpToTargets()
		{
			targetPosition.z = transform.localPosition.z;
			transform.localPosition = 	  Vector3.Lerp (transform.localPosition, targetPosition, Time.deltaTime * targetSpeed);

			if (globalRotation) 
				transform.rotation = Quaternion.Slerp (transform.rotation, targetRotation, Time.deltaTime * targetSpeed);
			else
				transform.localRotation = Quaternion.Slerp (transform.localRotation, targetRotation, Time.deltaTime * targetSpeed);
		}

		internal void OnInitialSerialize(NetworkWriter writer)
		{
			writer.Write (transform.localPosition);
			writer.Write (transform.localEulerAngles);
		}

		internal void OnInitialDeserialize(NetworkReader reader)
		{
			transform.localPosition = reader.ReadVector3 ();
			transform.localEulerAngles = reader.ReadVector3 ();
		}

		#endregion
	}

}