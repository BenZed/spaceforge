using UnityEngine;

namespace SpaceForge {
	
	public interface ISapienControl {

		void MoveFaceAim();

		void ContextAction(bool modifier, bool isLeft);

	}
}