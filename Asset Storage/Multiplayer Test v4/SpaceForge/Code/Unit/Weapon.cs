﻿using UnityEngine;
using UnityEngine.Networking;
using BossMedia;

namespace SpaceForge.Structural.Ships {

	public class Weapon : Unit, ISapienControl {

		[SerializeField] Transform anchor;
		[SerializeField] Transform muzzle;

		[SerializeField] GameObject projectilePrefab;
		[SerializeField, Range(0.125f, 10f)] float turretSpeed;
		[SerializeField] int ammunition;
		[SerializeField, Range(0.1f, 3f)] float fireDelay;

		float fireTimeStamp;

		#region Unit Overrides

		protected override int InitialLayer ()
		{
			return Game.Constants.Layers.Exterior;
		}

		#endregion

		#region ISapienControl implementation

		public void MoveFaceAim ()
		{
			if (!interactable || !interactable.user)
				return;

			SetTarget (interactable.user.aiming);

			interactable.MoveFaceAim ();
		}

		public void ContextAction (bool modifier, bool isLeft)
		{
			Fire ();
		}

		#endregion

		float target = 0;

		public void SetTarget(Vector2 aim)
		{
			target = Geometry2D.AbsoluteAngle (aim, anchor.position) - 90f;
		}

		public void Fire()
		{
			if (ammunition <= 0)
				return;

			if (fireTimeStamp + fireDelay > Time.time)
				return;
			
			ammunition--;
			fireTimeStamp = Time.time;
			
			var instantiateAt = muzzle ? muzzle : transform;
			
			var projectile = NetworkDummy.Create<Projectile>(projectilePrefab, instantiateAt, isServer);
			projectile.gameObject.layer = Game.Constants.Layers.Exterior;
			
			var relBody = containable.container.physical.body;
			projectile.relativeVelocity = relBody.velocity;
			SetCollisionsEnabled (projectile.GetComponent<Collider2D> (), false);
						
		//	if (isServer)
		//		SetEquipmentDirtyBit();
		}

		#region Property Callers

		#endregion

		#region Unity Callers

		protected override void Awake()
		{
			base.Awake ();

			target = anchor.localEulerAngles.z;
		}

		void OnValidate()
		{
			if (ammunition < 0)
				ammunition = 0;
		}

		void Update()
		{
			float z = anchor.eulerAngles.z;

			z = Mathf.LerpAngle (z, target, Time.deltaTime * turretSpeed);

			anchor.eulerAngles = Vector3.forward * z;
		}

		#endregion



	}

}