using UnityEngine;
using UnityEngine.Networking;
using BossMedia;
using SpaceForge.Structural.Ships;

namespace SpaceForge {

	[AddComponentMenu("SpaceForge/Sapien")]
	public class Sapien : Unit, ISapienControl {
		
		static Color DeathColor = new Color (0.45f, 0.45f, 0.525f, 0.95f);
		
		const float UncontrollableAngularVelocityThreshold = 400f;

		internal const float MaximumAimMagnitude = 10f;

		[SerializeField, Range(0.5f,10f)] float walkSpeed = 2f;

		#region Links

		Player _player;
		public Player player {
			get {
				if (_player && _player.transform.parent != transform)
					_player = null;
				
				return _player ?? (_player = GetComponentInChildren<Player>());					
			}
		}

		Head _head;
		internal Head head {
			get {
				if (!_head)
					ValidateBodyParts();
				return _head;
			}
		}

		Torso _torso;
		internal Torso torso {
			get {
				if (!_torso)
					ValidateBodyParts();
				return _torso;
			}
			
		}

		Hand _leftHand;
		internal Hand leftHand {
			get {
				if (!_leftHand)
					ValidateBodyParts();
				return _leftHand;
			}
		}

		Hand _rightHand;
		internal Hand rightHand {
			get {
				if (!_rightHand)
					ValidateBodyParts();
				return _rightHand;
			}
		}

		#endregion

		#region API
		
		Vector2 _movement;
		public Vector2 movement {
			get {
				return _movement;
			}
			private set {
				_movement = Geometry2D.ClampVector(value);
			}
		}
		
		internal Hand.ReachType reach;
		
		Vector2 _aiming = Vector2.zero;
		public Vector2 aiming {
			get {
				return _aiming;
			}
			private set {
				Vector2 pos = transform.position;
				_aiming = Geometry2D.ClampVector(value - pos, MaximumAimMagnitude) + pos;
			}
		}
		
		float _facing = 0f;
		public float facing {
			get {
				return _facing;
			}
			private set {
				_facing = value;
			}
		}
		
		internal void Move(Vector2 value)
		{
			value = Geometry2D.ClampVector (value);
			
			if (value == movement)
				return;
			
			movement = value;
			if (isServer)
				SetDirtyBit (moveDirtyBit);
		}
		
		internal void Reach(Hand.ReachType value)
		{
			if (reach == value)
				return;
			
			reach = value;
			if (isServer)
				SetDirtyBit (reachDirtyBit);
		}
		
		internal void Aim(Vector2 value)
		{
			Vector2 pos = (Vector2)transform.position;
			value = Geometry2D.ClampVector (value - pos, MaximumAimMagnitude) + pos;
			
			if (aiming == value)
				return;
			
			aiming = value;
			
			if (isServer)
				SetDirtyBit (aimDirtyBit);
		}
		
		internal void Face(float value)
		{
			if (Mathf.Approximately(facing, value))
				return;
			
			facing = value;
			
			if (isServer)
				SetDirtyBit (faceDirtyBit);
		}	
		
		#endregion

		#region Unity Callers

		protected override void FixedUpdate()
		{	
			base.FixedUpdate ();
			control.MoveFaceAim ();
		}

		#endregion

		#region Property Callers

		void OnKill(Damage overkill)
		{
			UnitComponents<SpriteRenderer> (spriter => spriter.color = DeathColor);
		}
		
		#endregion

		#region Mover
		
		ISapienControl _control;
		public ISapienControl control {
			get {
				return _control;
			}
			set {
				_control = value == null ? this : value;
			}
		}
		
		public void MoveFaceAim ()
		{
			bool is_dead = biological && biological.isDead;
			if (is_dead || !physical)
				return;
			
			Rigidbody2D body = physical.body;

			bool in_space = !containable || !containable.container;

			bool rotationControllable = !in_space || Mathf.Abs (physical.body.angularVelocity) < UncontrollableAngularVelocityThreshold;
			
			if (!isServer)
				rotationDeterministic = rotationControllable && !is_dead && player;

			if (in_space && torso.equipment is JetPack)
				torso.equipment.Action ();
			
			else if (!in_space && containable.containedBodyActive) {
				body = containable.containedBody;
				Vector2 move = (body.transform.up * movement.y + body.transform.right * movement.x) * walkSpeed * Game.manager.constants.ship.InteriorDrag * Time.fixedDeltaTime;
				body.AddForce(move, ForceMode2D.Impulse);
			}

			if (rotationControllable) {
				float local 		 = !in_space ? containable.container.rotation : 0f;
				float angle 		 = Mathf.LerpAngle(_facing, _facing + body.angularVelocity - local, 1f);
				body.angularVelocity = Mathf.Lerp(body.angularVelocity, 0f, Time.fixedDeltaTime);
				body.rotation 		 = Mathf.LerpAngle(body.rotation, angle, Time.fixedDeltaTime * 3f);
			}

		}

		public void ContextAction(bool modifier, bool isLeft)
		{
			if (killable && killable.isDead)
				return;
			Hand hand = isLeft ? leftHand : rightHand;

			bool is_aiming = hand.IsAiming ();

			Equipment equipment = hand.equipment;

			if (!equipment && !modifier && is_aiming)
				hand.Grab ();
			if (!equipment)
				return;
			
			if (!equipment.enabled && !modifier && is_aiming)
				hand.Grab();
			else if (equipment.enabled && !modifier)
				equipment.Action ();
			else if (!equipment.enabled && modifier)
				equipment.enabled = true;
			else if (equipment.enabled && modifier)
				equipment.enabled = false;
		}
		
		#endregion

		#region Unit Overrides

		protected override int InitialLayer ()
		{
			return Game.Constants.Layers.Exterior;
		}

		#endregion

		#region Unit Network Overrides

		uint moveDirtyBit;
		uint reachDirtyBit;
		uint aimDirtyBit;
		uint faceDirtyBit;

		uint headEquipDirtyBit;
		uint torsoEquipDirtyBit;
		uint leftHandEquipDirtyBit;
		uint rightHandEquipDirtyBit;

		protected override void RegisterDirtyBits (System.Func<uint> nextDirtyBit)
		{
			base.RegisterDirtyBits (nextDirtyBit);
			moveDirtyBit = nextDirtyBit ();
			reachDirtyBit = nextDirtyBit ();
			aimDirtyBit = nextDirtyBit ();
			faceDirtyBit = nextDirtyBit ();

			headEquipDirtyBit = nextDirtyBit ();
			torsoEquipDirtyBit = nextDirtyBit ();
			leftHandEquipDirtyBit = nextDirtyBit ();
			rightHandEquipDirtyBit = nextDirtyBit ();

		}
		
		internal override void OnInitialSerialize (NetworkWriter writer)
		{
			base.OnInitialSerialize (writer);
			writer.Write (movement);
			writer.Write ((byte)reach);
			writer.Write (aiming);
			writer.Write (facing);

			head.OnInitialSerialize (writer);
			rightHand.OnInitialSerialize (writer);
			leftHand.OnInitialSerialize (writer);
			torso.OnInitialSerialize (writer);

			SetDirtyBit (headEquipDirtyBit);
			SetDirtyBit (torsoEquipDirtyBit);
			SetDirtyBit (leftHandEquipDirtyBit);
			SetDirtyBit (rightHandEquipDirtyBit);
		}

		internal override void OnInitialDeserialize (NetworkReader reader)
		{
			base.OnInitialDeserialize (reader);
			movement = reader.ReadVector2 ();
			reach = (Hand.ReachType)reader.ReadByte ();
			aiming = reader.ReadVector2 ();
			facing = reader.ReadSingle ();

			head.OnInitialDeserialize (reader);
			rightHand.OnInitialDeserialize (reader);
			leftHand.OnInitialDeserialize (reader);
			torso.OnInitialDeserialize (reader);
		}

		internal override void OnUpdateSerialize (NetworkWriter writer)
		{
			base.OnUpdateSerialize (writer);
			if (DirtyBitIsSet(moveDirtyBit))
				writer.Write (movement);

			if (DirtyBitIsSet(reachDirtyBit))
				writer.Write ((byte)reach);

			if (DirtyBitIsSet (aimDirtyBit))
				writer.Write (aiming);

			if (DirtyBitIsSet(faceDirtyBit))
				writer.Write (facing);

			if (head.equipment)
				head.equipment.Serialize (writer);
			
			if (torso.equipment)
				torso.equipment.Serialize (writer);
			
			if (rightHand.equipment)
				rightHand.equipment.Serialize (writer);
			
			if (leftHand.equipment)
				leftHand.equipment.Serialize (writer);
		}

		internal override void OnUpdateDeserialize (NetworkReader reader)
		{
			base.OnUpdateDeserialize (reader);
			if (ReceivedDirtyBitIsSet(moveDirtyBit))
				movement = reader.ReadVector2 ();

			if (ReceivedDirtyBitIsSet(reachDirtyBit))
				reach = (Hand.ReachType)reader.ReadByte ();

			if (ReceivedDirtyBitIsSet (aimDirtyBit))
				aiming = reader.ReadVector2 ();

			if (ReceivedDirtyBitIsSet (faceDirtyBit))
				facing = reader.ReadSingle ();

			if (head.equipment)
				head.equipment.Deserialize (reader);
			
			if (torso.equipment)
				torso.equipment.Deserialize (reader);
			
			if (rightHand.equipment)
				rightHand.equipment.Deserialize (reader);
			
			if (leftHand.equipment)
				leftHand.equipment.Deserialize (reader);
		}

		public override void OnStartClient()
		{
			base.OnStartClient ();
			ValidateBodyParts ();
			
			head.SetDefaultSprite ();
			leftHand.SetDefaultSprite ();
			rightHand.SetDefaultSprite ();
			torso.SetDefaultSprite ();
			
			control = this;

			var found_equipment = GetComponentsInChildren<Equipment> ();
			foreach (var equipment in found_equipment) {
				if (equipment.sapien != this)
					equipment.Attach (this);
				equipment.enabled = true;
			}
		}

		#endregion

		#region Helper

		internal uint GetEquipmentDirtyBit(BodyPart bodyPart)
		{
			if (bodyPart == head)
				return headEquipDirtyBit;

			else if (bodyPart == torso)
				return torsoEquipDirtyBit;

			else if (bodyPart == rightHand)
				return rightHandEquipDirtyBit;

			else if (bodyPart == leftHand)
				return leftHandEquipDirtyBit;

			else
				throw new UnityException ("Equipment must be parented to this Sapien.");
		}

		//Ensure there are no more than one right hand, one left hand and one head.
		void ValidateBodyParts()
		{
			var parts = UnitComponents<BodyPart> ();
			_head = null;
			_torso = null;
			_rightHand = null;
			_leftHand = null;

			foreach (var part in parts) {
				if (part is Hand)
					ValidateHand((Hand)part);

				else if (part is Head) 
					ValidateHead((Head)part);

				else if (part is Torso)
					ValidateTorso((Torso)part);
			}

			if (!head || !torso || !rightHand || !leftHand)
				throw new UnityException (name + " is Missing Body parts!");
		}

		void ValidateHand(Hand hand)
		{
			if (hand.isLeft && !_leftHand)
				_leftHand = hand;

			else if (hand.isLeft && _leftHand)
				throw new UnityException("Can't have more than one left hands.");
			
			if (!hand.isLeft && !_rightHand)
				_rightHand = hand;

			else if (!hand.isLeft && _rightHand)
				throw new UnityException("Can't have more than one right hands.");
		}

		void ValidateHead(Head head)
		{
			if (!_head)
				_head = head;
			else
				throw new UnityException ("Can't more than one head.");
		}

		void ValidateTorso(Torso torso)
		{
			if (!_torso)
				_torso = torso;
			else
				throw new UnityException ("Can't more than one torso.");
		}

		#endregion
	}
}