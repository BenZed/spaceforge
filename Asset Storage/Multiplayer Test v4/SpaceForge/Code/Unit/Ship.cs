﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using BossMedia;

namespace SpaceForge.Structural.Ships {

	[AddComponentMenu("SpaceForge/Ship"),
	 RequireComponent(typeof(Structure))]
	public class Ship : Container {

		Structure _structure;
		public Structure structure {
			get {
				return _structure ?? (_structure = GetComponent<Structure>());
			}
		}

		ShipInterior _interior;
		internal ShipInterior interior {
			get {
				if (!_interior)
					_interior = ShipInterior.Create (this);

				return _interior;
			}
		}

		ShipCollidableTrigger _trigger;
		internal ShipCollidableTrigger trigger {
			get {
				return _trigger;
			}
			set {
				_trigger = value;
			}
		}

		Flag updateTrigger = false;

		#region Unity Callers

		void Update()
		{
			UpdateTriggerSize ();

			if (isServer)
				UnloadCheck ();

			MixAtmosphere ();

		}

		#endregion

		#region Property Callers

		void OnDamage (Damage damage)
		{
			structure.Damage (damage);
		}

		#endregion

		#region Structure Callers

		void OnSectionAdd(Section section)
		{
			NetworkDummy.Add<ShipFloorTrigger> (section.floor.gameObject, isServer);
		}

		void OnSectionRemove(Section section)
		{
			interior.RemoveSectionInterior (section);
		}

		const float TriggerSizeMultiplier = 1.2f;

		void OnSectionUpdate(Section section)
		{
			interior.UpdateWallColliders (section);

			if (!trigger) {
				var triggerGameObject = new GameObject(name + " Collidable Trigger", typeof(BoxCollider2D));
				triggerGameObject.transform.parent = transform;
				triggerGameObject.transform.localPosition = Vector2.zero;
				triggerGameObject.transform.localRotation = Quaternion.identity;
				triggerGameObject.layer = Game.Constants.Layers.ShipTrigger;
				trigger = NetworkDummy.Add<ShipCollidableTrigger>(triggerGameObject, isServer);
				trigger.box.offset = Vector3.up * 1.75f;

			}

			updateTrigger = true;
		}

		void OnTileAdd(Tile tile)
		{
			if (!tile)
				return;

			bool should = tile.sealable;
			bool has = atmospheres.Occupied (tile.coords);

			if (should && has)
				return;

			else if (!should && has)
				atmospheres.Remove (tile.coords);

			else if (should && !has)
				atmospheres.Add (new Atmosphere (), tile.coords);
		}

		void OnTileRemove(Tile tile)
		{
			if (atmospheres.Occupied (tile.coords))
				atmospheres.Remove (tile.coords);
		}

		/*
		[ContextMenu("Set Atmosphere")]
		internal void RefreshSerializedEvents()
		{
			atmospheres.ForEach (a => a.o2 = 100f);
		}
*/
		
		#endregion

		#region Loaded Units

		List<Containable> loaded = new List<Containable> ();

		void UpdateTriggerSize()
		{
			if (updateTrigger.SwitchOff ()) {
				trigger.box.size = structure.bounds.size * TriggerSizeMultiplier;
				trigger.transform.localPosition = Vector2.zero;
			}
		}

		[Server]
		void UnloadCheck()
		{
			for (int i = 0; i < loaded.Count; i++) {
				Containable containable = loaded [i];
				
				if (CanUnload(containable)) {
					Unload (containable);
					loaded.RemoveAt(i);
					i--;
				}
			}
		}

		#endregion

		#region Doors

		class Doorway : SimpleGrid<Door> {
			internal override bool CoordValid (Coord coord)
			{
				return Math.Abs (coord.x) + Math.Abs (coord.y) == 1;
			}
		}

		Grid<Doorway> doorways = new Grid<Doorway> ();


		internal static bool DoorPlacementValid(Ship ship, Coord coord, Coord dir, out string error)
		{
			error = string.Empty;

			if (!ship || !ship.structure) {
				error = "Must place doors on valid Ships";
				return false;
			}

			if (ship.doorways == null)
				ship.doorways = new Grid<Doorway> ();

			Tile tile = ship.structure.GetTile (coord);
			if (!tile) {
				error = "Must place door on a valid tile.";
				return false;
			}

			if (tile.IsPartial && (tile.MissingCorner == Coord.NextAround (dir) || tile.MissingCorner == Coord.PrevAround (dir))) {
				error = "Can't place doors on empty spaces on partial tiles.";
				return false;
			}

			Doorway doorway = ship.doorways.Occupied (coord) ? ship.doorways.Get (coord) : null;
			if (doorway != null && doorway.Has (dir)) {
				error = "Can't place Door where one already exists.";
				return false;
			}

			Tile adjTile = tile.GetRelativeTile (dir);
			Doorway adjDoorWay = adjTile && ship.doorways.Occupied (adjTile.coords) ? ship.doorways.Get (adjTile.coords) : null;
			if (adjDoorWay != null && adjDoorWay.Has(-dir)) {
				error = "Can't place Doors where one already exists.";
				return false;
			}

			return true;
		}

		internal void AddDoor(Door door)
		{
			string error = "Door does not exist.";
			if (!door || !DoorPlacementValid(this, door.coord, door.dir, out error))
				throw new UnityException(error);

			if (!doorways.Occupied (door.coord))
				doorways.Add (new Doorway (), door.coord);

			var doorway = doorways.Get(door.coord);
			doorway.Add (door.dir, door);
		}

		internal Door GetDoor(Coord coord, Coord dir)
		{
			if (!doorways.Occupied(coord))
				return null;

			var doorway = doorways.Get(coord);
			if (!doorway.Has (dir))
				return null;

			return doorway.Get (dir);
		}

		[Server]
		internal void SetDoorOpen(Coord coord, Coord dir, bool value)
		{
			var door = GetDoor (coord, dir);
			if (door && !door.enabled) 
				RpcSetDoorOpen (coord.x, coord.y, dir.x, dir.y, value);
		}

		[ClientRpc]
		void RpcSetDoorOpen(int coordX, int coordY, int dirX, int dirY, bool value)
		{
			var coord = new Coord (coordX, coordY);
			var dir = new Coord (dirX, dirY);
			var door = GetDoor (coord, dir);

			door.Open = value;
		}

		#endregion

		#region Atmosphere

		
		[Serializable]
		class Atmospheres : Grid<Atmosphere> { }
		
		[SerializeField] Atmospheres atmospheres;
		const float MixAtmosphereInterval = 0.125f;
		
		const float QuadrantVolume = 25f;
		const float VacuumExchange = 400f;

		float mixAtmosphereTimer = MixAtmosphereInterval;

		void MixAtmosphere()
		{
			mixAtmosphereTimer -= Time.deltaTime;
			if (mixAtmosphereTimer > 0f)
				return;

			float delta = MixAtmosphereInterval - mixAtmosphereTimer;
			mixAtmosphereTimer = MixAtmosphereInterval;

			atmospheres.ForEach ((atmos, coord) => DiffuseAtmosphere (atmos, coord, delta));
		}

		internal void DiffuseAtmosphere(Atmosphere atmosphere, Coord coord, float timeDelta)
		{
			Tile tile = structure.GetTile(coord);
			timeDelta = Mathf.Clamp01(timeDelta);
			float permeability = tile.IsPartial && !tile.GetNode(Coord.zero) ? 0.4f : 0f;

			tile.ForEachQuadrant((dir, quad) => {

				float quadPer = quad.integrity.Percent;
				if (quadPer <= 0.35f)
					permeability += Mathf.Lerp (0.05f, 0f, quad.integrity.Percent / 0.35f);
				
				Node wall = tile.GetNode(dir);
				if (wall)
					return;
				
				Door door = GetDoor(coord, dir);
				if (door && door.IsAirTight)
					return;

				Coord adjCoord = coord + dir;

				Door adjDoor = GetDoor(adjCoord, -dir);
				if (adjDoor)
					return;

				Atmosphere adjAtmos = atmospheres.Occupied(adjCoord) ? atmospheres.Get (adjCoord) : null;
				Tile adjTile = structure.GetTile(adjCoord);
				if (adjAtmos != null && adjTile) {
					float vol = (float)tile.NumQuadrants * QuadrantVolume;
					float adjVol = (float)adjTile.NumQuadrants * QuadrantVolume;
					MixAtmosphere(atmosphere, adjAtmos, vol, adjVol);
				} else
					permeability += 0.2f;
				
			});
			
			float loss = (VacuumExchange * permeability) * timeDelta;
			if (loss > 0f)
				atmosphere.Exchange (null, loss);

			AtmosphereVisualize (coord, atmosphere);
		}
		
		void MixAtmosphere(Atmosphere thisAt, Atmosphere adjAt, float thisVolume, float adjVol)
		{
			float o2Push = MixGas(thisAt.o2, adjAt.o2, thisVolume, adjVol);
			float co2Push = MixGas(thisAt.co2, adjAt.co2, thisVolume, adjVol);
			
			thisAt.o2 -= o2Push;
			adjAt.o2 += o2Push;
			
			thisAt.co2 -= co2Push;
			adjAt.co2 += co2Push;
		}

		float MixGas(float gas, float otherGas, float thisVol, float adjVol)
		{
			float totalVolume = adjVol + thisVol;
			float totalGas = otherGas + gas;
			
			float target = totalGas / (totalVolume / thisVol);
			float otherTarget = totalGas - target;
			
			return  otherTarget - otherGas;
		}

		static Sprite atmosphereSprite;
		Dictionary<Coord, SpriteRenderer> ars = new Dictionary<Coord, SpriteRenderer>();

		void AtmosphereVisualize(Coord coord, Atmosphere atmosphere)
		{
			if (atmosphere.o2 > 50f && !ars.ContainsKey (coord))
				return;

			if (!atmosphereSprite)
				atmosphereSprite = Resources.Load<Sprite> ("Sprites/Atmosphere");

			if (!ars.ContainsKey (coord)) {
				var go = new GameObject("Atmosphere " + coord, typeof(SpriteRenderer));
				var ar = go.GetComponent<SpriteRenderer>();

				go.transform.parent = transform;
				go.transform.localPosition = new Vector2(coord.x, coord.y) - (Vector2)structure.center + Vector2.one * 0.5f;
				go.transform.localRotation = Quaternion.identity;

				ars.Add(coord, ar);
				ar.sprite = atmosphereSprite;
				ar.color = Color.red;
			}

			var currAr = ars [coord];
			var color = currAr.color;

			color.a = Mathf.Clamp (50f - atmosphere.total, 0, 50f) / 100f;

			currAr.color = color;
		}

		#endregion

		#region Container Overrides

		protected override int InitialLayer ()
		{
			return Game.Constants.Layers.Ship;
		}

		public override Atmosphere ShareAtmosphere (Containable containable)
		{
			if (!containable)
				return null;

			var tile = structure.GetTileAt(containable.transform.position);
			if (tile && atmospheres.Occupied (tile.coords))
				return atmospheres.Get (tile.coords);
			else
				return null;
		}

		public override bool CanLoad (Containable containable)
		{
			return containable && structure.GetTileAt (containable.transform.position) && containable.container == null;
		}

		public override void Load (Containable containable)
		{
			containable.transform.parent = transform;
			containable.gameObject.layer = Game.Constants.Layers.ShipObstacle;

			containable.EnableContainedBody(interior.transform);
			containable.MatchContainedBody ();

			structure.ForEachSection (section => {
				containable.unit.SetCollisionsEnabled (section.roof.poly, false);
				containable.unit.SetCollisionsEnabled (section.floor.poly, true);
			});

			loaded.Add (containable);
		} 

		public override bool CanUnload (Containable containable)
		{
			return containable && !structure.GetTileAt (containable.transform.position) && containable.container == this;
		}

		public override void Unload (Containable containable)
		{
			containable.DisableContainedBody ();

			containable.transform.parent = null;
			containable.gameObject.layer = Game.Constants.Layers.Exterior;

		}

		#endregion

	}

}