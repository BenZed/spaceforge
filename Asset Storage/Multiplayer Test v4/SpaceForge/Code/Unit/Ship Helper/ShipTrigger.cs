using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

namespace SpaceForge.Structural.Ships {

	[DisallowMultipleComponent, RequireComponent(typeof(Collider2D))]
	public abstract class ShipTrigger : ShipComponent {

		#region Data
		
		Dictionary<Rigidbody2D, Containable> floaters = new Dictionary<Rigidbody2D, Containable>();
		List<Rigidbody2D> ignoreList = new List<Rigidbody2D> ();
		
		#endregion

		#region Main

		/// <summary>
		/// Run on containables currently in this gameObjects trigger.
		/// </summary>
		/// <param name="containable">Containable.</param>
		protected abstract void InTrigger(Containable containable);

		/// <summary>
		/// Containable will be ignored until it enters the trigger again.
		/// </summary>
		/// <param name="containable">Containable.</param>
		protected void Ignore(Containable containable)
		{
			if (containable.unit.physical && floaters.ContainsKey(containable.unit.physical.body))
				ignoreList.Add (containable.unit.physical.body);
		}

		float timeStamp = 0f;
		float _interval;
		protected float interval {
			get {
				return _interval;
			}
			set {
				_interval = value < 0f ? 0f : value;
			}
		}

		#endregion

		#region Unity Callers

		void FixedUpdate()
		{
			if (Time.time < timeStamp + interval)
				return;

			timeStamp = Time.time;

			//Do with floaters
			foreach (var kv in floaters) {
				if (kv.Value)
					InTrigger (kv.Value);
			}

			//Remove ignored floaters
			while (ignoreList.Count > 0) {
				floaters.Remove(ignoreList[0]);
				ignoreList.RemoveAt(0);
			}
		}

		void OnTriggerEnter2D(Collider2D enter)
		{
			Rigidbody2D enterBody = enter.attachedRigidbody;
			if (!enterBody)
				return;

			Containable containable = enterBody.GetComponent<Containable> ();
			if (containable && !floaters.ContainsKey(enterBody))
				floaters.Add (enterBody, containable);

		}
	
		void OnTriggerExit2D(Collider2D exit)
		{
			Rigidbody2D exitBody = exit.attachedRigidbody;
			if (exitBody && floaters.ContainsKey (exitBody))
				floaters.Remove (exitBody);
			
		}

		void Start()
		{
			if (!ship)
				ship = GetComponentInParent<Ship> ();

			CheckShip ();
		}

		#endregion
	}
	
}
