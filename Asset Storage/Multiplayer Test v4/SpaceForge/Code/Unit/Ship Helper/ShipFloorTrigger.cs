﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

namespace SpaceForge.Structural.Ships {
	
	public sealed class ShipFloorTrigger : ShipTrigger {

		#region Shiper Trigger Override

		protected override void InTrigger (Containable containable)
		{
			if (!ship.CanLoad(containable) || containable.transform.parent == ship.transform)
				return;
			
			Rigidbody2D otherBody = containable.unit.physical.body;
			Rigidbody2D thisBody = ship.physical.body;
			otherBody.velocity = Vector2.Lerp (otherBody.velocity, thisBody.velocity, Time.fixedDeltaTime);
			
			float matchSpeedSqr = Mathf.Pow (Game.manager.constants.ship.MatchingSpeed, 2f);
			bool matchSpeedMet = (otherBody.velocity - thisBody.velocity).sqrMagnitude <= matchSpeedSqr;

			if (matchSpeedMet)
				Ignore (containable);

			if (matchSpeedMet && isServer) 
				ship.Load (containable);
		}

		#endregion
	}

}
