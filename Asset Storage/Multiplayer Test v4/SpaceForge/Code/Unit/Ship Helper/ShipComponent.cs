﻿using UnityEngine;
using UnityEngine.Networking;

namespace SpaceForge.Structural.Ships {

	public abstract class ShipComponent : NetworkDummy {

		public override bool isServer {
			get {
				return ship && ship.isServer;
			}
		}

		[SerializeField] Ship _ship;
		public Ship ship {
			get {
				return _ship;
		    }
			protected set {
				_ship = value;
			}
		}

		protected void CheckShip()
		{
			if (!ship) {
				Destroy (gameObject);
				throw new UnityException (this.GetType ().Name + " needs to have it's ship field assigned!");
			}
		}

		internal virtual void OnShipSplit()
		{
			throw new System.NotImplementedException();
		}
	}
}
