using UnityEngine;
using UnityEngine.Networking;
using BossMedia;

namespace SpaceForge.Structural.Ships {

	[DisallowMultipleComponent, ExecuteInEditMode]
	public class Door : ShipComponent {

		[SerializeField] Coord _coord;
		public Coord coord {
			get {
				return _coord;
			}
		}

		[SerializeField] Coord _direction;
		public Coord dir {
			get {
				return _direction;
			}
		}

		[SerializeField] bool open;
		float openPhase = 0f;

		const float Speed = 3f;

		#region Slider

		Transform slider;
		BoxCollider2D sliderBox;

		Vector3 sliderClosedScale;
		Vector3 sliderOpenScale;

		static Vector3 sliderClosedPos = Vector3.up * 0.5f;
		Vector3 sliderOpenPos;

		#endregion

		Transform interiorSlider;

		#region API

		public bool Open {
			get {
				return open;
			}
			set {
				open = value;
				enabled = true;
			}
		}

		public bool IsAirTight {
			get {
				return openPhase < 0.05f;
			}
		}

		#endregion

		#region Unity Callers

		const float OpenValue = 1f;
		const float ClosedValue = 0f;

		void Update()
		{
			#if UNITY_EDITOR
			if (!Application.isPlaying) {
				Attachable.RoundCoordinates (transform);
				return;
			}
			#endif

			openPhase 		     = Mathf.MoveTowards (openPhase, open ? OpenValue : ClosedValue, Time.deltaTime * Speed);
			slider.localPosition = Vector3.Lerp (sliderClosedPos, sliderOpenPos, openPhase);
			slider.localScale 	 = Vector3.Lerp (sliderClosedScale,  sliderOpenScale, openPhase);

			interiorSlider.parent.localPosition = transform.localPosition;
			interiorSlider.localPosition = slider.localPosition;
			interiorSlider.localScale = slider.localScale;

			if (open && Mathf.Approximately (openPhase, OpenValue))
				enabled = false;
			else if (!open && Mathf.Approximately (openPhase, ClosedValue))
				enabled = false;
		}

		void Start()
		{
			#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
			#endif

			ship = GetComponentInParent<Ship> ();
			CheckShip ();
			RegisterDoorInShip ();
			
			GetSlider ();
			CreateInterior ();
		}

		#endregion

		void GetSlider()
		{
			slider = transform.Find ("Slider");
			sliderBox = slider.GetComponent<BoxCollider2D> ();

			sliderClosedScale = slider.localScale;
			sliderOpenScale = sliderClosedScale;
			sliderOpenScale.x = 0.0f;
			sliderOpenPos = sliderClosedPos + Vector3.left * (sliderClosedScale.x * 0.5f);
		}

		void CreateInterior()
		{
			var interior = new GameObject (name + " (Interior)").transform;
			interior.parent = ship.interior.transform;
			interior.gameObject.layer = Game.Constants.Layers.Interior;
			interior.localPosition = transform.localPosition;
			interior.localRotation = transform.localRotation;

			interiorSlider = new GameObject ("Slider (Interior)").transform;
			interiorSlider.parent = interior;
			interiorSlider.localPosition = slider.localPosition;
			interiorSlider.localRotation = slider.localRotation;
			interiorSlider.localScale = slider.localScale;
			interiorSlider.gameObject.layer = Game.Constants.Layers.Interior;

			var interiorBox = interiorSlider.gameObject.AddComponent<BoxCollider2D> ();
			interiorBox.offset = sliderBox.offset;
			interiorBox.size = sliderBox.size;

		}

		void RegisterDoorInShip()
		{
			Attachable.RoundCoordinates (transform);

			var tile = ship.structure.GetTileAt (transform.position);
			if (!tile) {
				Destroy(gameObject);
				throw new UnityException("Door placed improperly. Must be placed on tile!");
			}

			_coord = tile.coords;

			float angle = transform.localEulerAngles.z;

			if (Mathf.Approximately (angle, 0f))
				_direction = Coord.n;
			else if (Mathf.Approximately (angle, 90f))
				_direction = Coord.w;
			else if (Mathf.Approximately (angle, 180f))
				_direction = Coord.s;
			else if (Mathf.Approximately (angle, 270f))
				_direction = Coord.e;
			else
				throw new UnityException("Door placed improperly, must be Rotated to 0, 90, 180, or 270 degrees.");

			ship.AddDoor (this);

		}

	}
}
