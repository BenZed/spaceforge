using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

namespace SpaceForge.Structural.Ships {

	public sealed class ShipCollidableTrigger : ShipTrigger {

		BoxCollider2D _box;
		public BoxCollider2D box {
			get {
				return _box ?? (_box = GetComponent<BoxCollider2D>());
			}
		}

		#region Unity Callers

		void Awake()
		{
			interval = Game.manager.constants.ship.CollidableRefreshInterval;
			box.isTrigger = true;
		}

		#endregion

		#region ShipTrigger Implementation

		protected override void InTrigger (Containable containable)
		{
			float diffSqr = (containable.unit.physical.body.velocity - ship.physical.body.velocity).sqrMagnitude;
			float boardSpeedSqr =  Mathf.Pow (Game.manager.constants.ship.BoardingSpeed, 2f);

			bool inBoardingRange = diffSqr <= boardSpeedSqr;

			Unit unit = containable.unit;

			if (containable.container == ship)
				Ignore (containable);

			ship.structure.ForEachSection(section => {
				if (inBoardingRange && unit.IsColliding(section.roof.poly)) {
					unit.SetCollisionsEnabled(section.roof.poly, false);
					unit.SetCollisionsEnabled(section.floor.poly, true);

				} else if (!inBoardingRange && unit.IsColliding(section.floor.poly)) {
					unit.SetCollisionsEnabled(section.roof.poly, true);
					unit.SetCollisionsEnabled(section.floor.poly, false);
				}
			});
		}

		#endregion


	}
}
