﻿using UnityEngine;
using BossMedia;
using System.Collections.Generic;

namespace SpaceForge.Structural.Ships {

	[DisallowMultipleComponent]
	public sealed class ShipInterior : ShipComponent {

		internal struct SectionInterior {
			
			public PolygonCollider2D collider;
			public Transform transform;
			
			public SectionInterior(ShipInterior parent, Section section) {
				collider = new GameObject (section.transform.name + " (Interior)").AddComponent<PolygonCollider2D>();

				transform = collider.transform;
				transform.parent = parent.transform;

				transform.localPosition = Vector3.zero;
				transform.localRotation = Quaternion.identity;

				transform.gameObject.layer = Game.Constants.Layers.Interior;
			}
		}

		static float distance = 0f;
		static Flag distanceWarning = true;

		public static ShipInterior Create(Ship ship) 
		{
			var shipInterior = new GameObject (ship.transform.name + " (Interior)").AddComponent<ShipInterior> ();
			shipInterior.ship = ship;
			shipInterior.gameObject.layer = Game.Constants.Layers.Interior;
			shipInterior.transform.gameObject.SetActive(false);
			shipInterior.transform.SetSiblingIndex (ship.transform.GetSiblingIndex ());
			distance += 200f;
			shipInterior.transform.position = Vector2.one * distance;
			if (distanceWarning.SwitchOff ())
				Debug.LogWarning ("Right now, as ship interiors are created, they're just placed Vector2.one * 200f away from the previous one. Eventually we'll have to come up with some kind of packing algorith, or something.");

			shipInterior.CheckShip ();

			return shipInterior;
		}
		
		#region Unity Callers

		void OnTransformChildrenChanged()
		{
			int numLoaded = transform.childCount - ship.structure.NumSections;
			gameObject.SetActive (numLoaded > 0);
		}
		
		#endregion
		
		Grid<SectionInterior> interiors;

		internal void RemoveSectionInterior(Section section)
		{
			var interior = interiors.Get (section.coords);
			interiors.Remove (section.coords);
			Destroy (interior.transform.gameObject);
		}
		
		internal void UpdateWallColliders(Section section)
		{
			if (interiors == null)
				interiors = new Grid<SectionInterior> ();

			if (!interiors.Occupied(section.coords))
				interiors.Add(new SectionInterior(this, section), section.coords);
			
			SectionInterior interior = interiors.Get (section.coords);
			int pathCount = section.walls.poly.pathCount;
			
			interior.transform.localPosition = section.transform.localPosition;
			interior.collider.pathCount = pathCount;
			for(var i = 0; i < pathCount; i++)
				interior.collider.SetPath(i, section.walls.poly.GetPath(i));
		}
		
	}
	
}
