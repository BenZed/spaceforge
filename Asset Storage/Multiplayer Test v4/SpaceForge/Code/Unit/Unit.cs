﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections.Generic;
using BossMedia;
using SpaceForge.Structural.Ships;

namespace SpaceForge {
	
	/// <summary>
	/// A 'unit' is any entity that either affects or is affected by gameplay. Runners, Obstacles, Missiles, Finishlines, ecetera. 
	/// Things like particle effects and visual embellishment do not need to be synced over the network.
	/// 
	/// Unit's act as the 'Entity' in an Entity-Component-System, where properties act as the Components. (You might notice that
	/// unity itself is an entity-component-system. We're just narrowing it down for our purposes.)
	/// 
	/// The unit contains base functionality for syncing it's position and rotation across the network, as well as handling
	/// the syncing of any properties attached to it.
	/// </summary>
	[DisallowMultipleComponent, SelectionBase]
	public abstract class Unit : NetworkMaster {

		#region Links

		public Containable containable { get; private set; }
			
		public Damagable damagable { get; private set; }

		public Destructible destructible { get; private set; }

		public Killable killable { get; private set; }

		public Biological biological { get; private set; }

		public Physical physical { get; private set; }

		public Attachable attachable { get; private set; }

		public Permanent permanent { get; private set; }

		public Interactable interactable { get; private set; }

		#endregion

		protected abstract int InitialLayer ();

		#region Unity Callers

		protected virtual void Awake()
		{
			if (Application.isPlaying) {
				gameObject.layer = InitialLayer ();
				colliders = UnitComponents<Collider2D> ();
			}
		}
		 
		protected virtual void LateUpdate()
		{
			if (!Application.isPlaying)
				return;

			if (isServer && RequiresSync ())
				SetDirtyBit (syncBodyDirtyBit);

			else if (!isServer && syncBodyType == SyncBodyType.Transform)
				ApplySyncTransform ();

		}

		protected virtual void FixedUpdate()
		{
			if (isServer || !Application.isPlaying)
				return;

			if (syncBodyType != SyncBodyType.Transform)
				ApplySyncBody ();
		}

		protected virtual void OnTransformParentChanged()
		{
			InferSyncBodyType ();
		}

		#endregion

		#region NetworkMaster overrides

		uint syncBodyDirtyBit = 1u;

		protected override NetworkSlave[] GetSubscribers ()
		{
			var properties = GetComponents<Property> ();
			
			foreach (var property in properties) {
				if (property is Containable)
					containable = (Containable)property;
				
				if (property is Damagable)
					damagable = (Damagable)property;
				
				if (property is Destructible)
					destructible = (Destructible)property;
				
				if (property is Killable)
					killable = (Killable)property;
			
				if (property is Biological)
					biological = (Biological)property;
				
				if (property is Physical)
					physical = (Physical)property;

				if (property is Attachable)
					attachable = (Attachable)property;

				if (property is Permanent)
					permanent = (Permanent)property;

				if (property is Interactable)
					interactable = (Interactable)property;
			}

			InferSyncBodyType ();

			return properties;
		}

		protected override void RegisterDirtyBits (Func<uint> nextDirtyBit)
		{
			syncBodyDirtyBit = nextDirtyBit ();
		}

		internal override void OnInitialSerialize (NetworkWriter writer) 
		{
			syncBodyMsg = SyncBodyMessage.Create(this);
			syncBodyMsg.Write (writer);

		}

		internal override void OnInitialDeserialize (NetworkReader reader) 
		{
			syncBodyMsg = SyncBodyMessage.Create(this);
			syncBodyMsg.Read (reader);// TODO Causes crashes for some reason, all of a sudden.
		}

		internal override void OnUpdateSerialize(NetworkWriter writer) 
		{
			if (DirtyBitIsSet (syncBodyDirtyBit))
				syncBodyMsg.Write (writer);
		}
		
		internal override void OnUpdateDeserialize(NetworkReader reader) 
		{
			if (ReceivedDirtyBitIsSet (syncBodyDirtyBit))
				syncBodyMsg.Read (reader);
		}

		#endregion

		#region SyncBody

		internal enum SyncBodyType {
			Body,
			Transform,
			ContainedBody
		}

		class SyncBodyMessage : MessageBase {
			
			public Vector2 position;
			public float rotation;
			
			public Vector2 velocity;
			public float angular;

			public Unit unit;

			public SyncBodyMessage()
			{
				unit = null;
			}

			public static SyncBodyMessage Create(Unit unit)
			{
				var sbm = new SyncBodyMessage ();
				sbm.unit = unit;

				return sbm;
			}

			public void Write(NetworkWriter writer)
			{
				position = unit.position;
				rotation = unit.rotation;
				velocity = unit.velocity;
				angular = unit.angular;
				
				unit.syncBodyTimeStamp = Time.time;
				
				writer.Write(this);
			}

			public void Read(NetworkReader reader)
			{
				var read = reader.ReadMessage<SyncBodyMessage> ();
				position = read.position;
				rotation = read.rotation;
				velocity = read.velocity;
				angular = read.angular;

				unit.syncBodyTimeStamp = Time.time;
			}
		}

		[SerializeField, Range(0, 30)] 
		int syncFrequency = 4;

		[SerializeField, HideInInspector]
		SyncBodyType _syncBodyType;

		internal SyncBodyType syncBodyType {
			get {
				return _syncBodyType;
			}
			private set {
				_syncBodyType = value;
			}
		}

		SyncBodyMessage syncBodyMsg;

		float syncBodyTimeStamp = 0f;

		internal void ForceSyncBody()
		{
			SetDirtyBit (syncBodyDirtyBit);
		}
		
		internal bool rotationDeterministic = false;
		internal bool positionDeterministic = false;

		void InferSyncBodyType()
		{
			//If we've gotten here, we'll do a manual sync just to be safe.
			ForceSyncBody ();

			//Disable the physical property if we're parented to something.
			bool has_body = physical && physical.body;
			bool using_contained_body = has_body && containable && containable.containedBodyActive;
			bool has_parent = transform.parent;

			if (physical)
				physical.enabled = !has_parent;

			if (!has_body) 
				_syncBodyType = SyncBodyType.Transform;
			
			else if (!has_parent && has_body)
				_syncBodyType = SyncBodyType.Body;
			
			else if (has_parent && using_contained_body)
				_syncBodyType = SyncBodyType.ContainedBody;
			
			else if (has_parent && !using_contained_body)
				_syncBodyType = SyncBodyType.Transform;	
		}

		[Server]
		bool RequiresSync() {
			
			if (syncFrequency <= 0 || (positionDeterministic && rotationDeterministic))
				return false;

			if ((position - syncBodyMsg.position).sqrMagnitude > LowPosErrThreshold * LowPosErrThreshold)
				return true;
			
			if (Mathf.Abs(rotation - syncBodyMsg.rotation) > LowRotErrThreshold)
				return true;
			
			return false;
		}

		const float LowPosErrThreshold = 0.075f;
		const float HiPosErrThreshold = 1f;

		const float LowRotErrThreshold = 2f;
		const float HiRotErrThreshold = 60f;

		const float LowErrLerp = 1f;
		const float HiErrLerp = 8f;

		[Client]
		void ApplySyncBody()
		{
			float syncDelta = Time.time - syncBodyTimeStamp;

			if (syncDelta <= Time.fixedDeltaTime || syncBodyMsg == null || (positionDeterministic && rotationDeterministic))
				return;
			
			float syncInterval = syncFrequency == 0 ? syncDelta : 1f / (float)syncFrequency; 
			SyncBodyMessage sbm = syncBodyMsg;
			
			Vector2 expectedPos = sbm.position + (sbm.velocity * syncDelta);
			float posErr = (position - expectedPos).magnitude;

			if (posErr > LowPosErrThreshold && posErr <= HiPosErrThreshold && !positionDeterministic) {

				Vector2 projectedPos = sbm.position + sbm.velocity * syncInterval;
				float lerpDelta = Mathf.Lerp(LowErrLerp, HiErrLerp, posErr / HiPosErrThreshold) * Time.fixedDeltaTime;

				position = Vector2.Lerp(position, projectedPos, lerpDelta);
				velocity = Vector2.Lerp(velocity, sbm.velocity, lerpDelta);

			} else if (posErr > HiPosErrThreshold && !positionDeterministic) {

				position = expectedPos;
				velocity = sbm.velocity;
			}

			float expectedRot = Mathf.LerpAngle(sbm.rotation, sbm.rotation + (sbm.angular * syncDelta), 1f);
			float rotationErr = Mathf.Abs(rotation - expectedRot);

			if (rotationErr > LowRotErrThreshold && rotationErr < HiRotErrThreshold && !rotationDeterministic) {
				
				float projectedRot = Mathf.LerpAngle(sbm.rotation, sbm.rotation + sbm.angular * syncInterval, 1f);
				float lerpDelta = Mathf.Lerp(LowErrLerp, HiErrLerp, rotationErr / HiRotErrThreshold) * Time.fixedDeltaTime;

				rotation = Mathf.LerpAngle(rotation, projectedRot, lerpDelta);
				angular = Mathf.LerpAngle(angular, sbm.angular, lerpDelta);
				
			} else if (rotationErr > HiRotErrThreshold && !rotationDeterministic) {
				
				rotation = expectedRot;
				angular = sbm.angular;
				
			}
		}

		void ApplySyncTransform()
		{
			position = syncBodyMsg.position;
			rotation = syncBodyMsg.rotation;
			//	position += transformVelocity * Time.deltaTime;
			//	rotation += transformAngular * Time.deltaTime;
		}

		#endregion

		#region Coordinates

		internal float z {
			get {
				return transform.position.z;
			}
			set {
				var pos = transform.position;
				pos.z = value;
				transform.position = pos;
			}
		}

		public Vector2 position {
			get {
				return syncBodyType != SyncBodyType.ContainedBody ? transform.localPosition : containable.containedBody.transform.localPosition;
			}
			set {
				if (syncBodyType != SyncBodyType.ContainedBody)
					transform.localPosition = new Vector3(value.x, value.y, z);
				else
					containable.containedBody.transform.localPosition = value;
			}
		}
		
		public float rotation {
			get {
				return syncBodyType != SyncBodyType.ContainedBody ? transform.localEulerAngles.z : containable.containedBody.transform.localEulerAngles.z;
			}
			
			set {
				var angledVector = Vector3.forward * value;
				if (syncBodyType != SyncBodyType.ContainedBody)
					transform.localEulerAngles = angledVector;
				else
					containable.containedBody.transform.localEulerAngles = angledVector;
			}
		}
		
		Vector2 transformVelocity;
		public Vector2 velocity {
			get {
				if (syncBodyType == SyncBodyType.Body)
					return physical.body.velocity;
				
				else if (syncBodyType == SyncBodyType.ContainedBody)
					return containable.containedBody.velocity;
				
				else
					return transformVelocity;
			}
			set {
				if (syncBodyType == SyncBodyType.Body)
					physical.body.velocity = value;
				
				else if (syncBodyType == SyncBodyType.ContainedBody)
					containable.containedBody.velocity = value;
				
				else
					transformVelocity = value;
			}
		}
		
		float transformAngular;
		public float angular {
			get {
				if (syncBodyType == SyncBodyType.Body)
					return physical.body.angularVelocity;
				
				else if (syncBodyType == SyncBodyType.ContainedBody)
					return containable.containedBody.angularVelocity;
				
				else
					return transformAngular;
				
			}
			set {
				if (syncBodyType == SyncBodyType.Body)
					physical.body.angularVelocity = value;
				
				else if (syncBodyType == SyncBodyType.ContainedBody)
					containable.containedBody.angularVelocity = value;
				
				else
					transformAngular = value;
			}
		}

		#endregion

		#region Colliders

		Collider2D[] colliders;

		public void SetCollidersEnabled(bool value)
		{
			foreach (var coll in colliders)
				coll.enabled = value;
		}

		public bool IsColliding(Collider2D with)
		{
			bool colliding = false;
			foreach (var coll in colliders) {
				if (coll.IsColliding(with)) {
					colliding = true;
					break;
				}
			}

			return colliding;
		}

		public void SetCollisionsEnabled(Collider2D with, bool value)
		{
			foreach (var coll in colliders) {
				if (value)
					coll.EnableCollisions(with);
				else
					coll.DisableCollisions(with);
			}
		}

		#endregion

		#region Convienience

		/* TODO Add this when we've got effects and buffs

		Dictionary<Type, Effect> cachedEffects = new Dictionary<Type, Effect>();

		/// <summary>
		/// If a property exists, and an action is supplied, the action will be run on that property.
		/// </summary>
		/// <param name="action">Action.</param>
		/// <typeparam name="T">Property</typeparam>
		public T Effect<T> (Action<T> action = null) where T : Effect {
			
			var type = typeof(T);
			T cached = null;
			bool has_action = action != null;
			
			bool contains = cachedEffects.ContainsKey (type);
			if (contains)
				cached = cachedEffects[type] as T;
			
			//if the dictionary has the component and the component hasn't since been removed, we're all done!
			if (contains && cached && has_action)
				action (cached);
			
			if (contains && cached)
				return cached;
			
			T effect = GetComponent<T>();
			
			if (effect && has_action)
				action(effect);
			
			//If the component exists, we cache it into the dictionary
			if (effect)
				cachedEffects [type] = effect;
			
			//if the component doesn't exist, but is in the dictionary, it must have been previously added and since destroyed
			else if (contains)
				cachedEffects.Remove (type);
			
			return effect;
		}

		/// <summary>
		/// Returns the result of the predicate if the property exists, otherwise it returns false.
		/// </summary>
		/// <param name="action">Action.</param>
		/// <typeparam name="T">Property</typeparam>
		public bool Effect<T>(Predicate<T> predicate) where T : Effect {

			var effect = Effect<T> ();
			if (predicate == null || !effect)
				return effect;

			return predicate (effect);		
		}
		*/

		/// <summary>
		/// Returns all Components related to this unit.
		/// </summary>
		/// <returns>The components.</returns>
		/// <param name="each">An action to be performed on each component.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public T[] UnitComponents<T> (Action<T> each = null) where T : Component {

			var list = new List<T> ();
			list.AddRange (GetComponents<T> ());

			foreach (Transform child in transform) 
				AddChildUnitComponentsToList<T> (child, list);

			if (each != null) {
				foreach (T component in list)
					each(component);
			}

			return list.ToArray ();
		}

		void AddChildUnitComponentsToList<T>(Transform child, List<T> results) where T : Component {

			//If this child has a unit component, we dont want any components from it, because it's a seperate unit.
			if (child.GetComponent<Unit> ())
				return;

			results.AddRange (child.GetComponents<T> ());

			foreach (Transform subChild in child)
				AddChildUnitComponentsToList<T> (subChild, results);

		}

		[ContextMenu("Refresh Serialized Events")]
		internal void RefreshSerializedEvents()
		{
			foreach (var property in GetComponents<IUnitSerializedEvent>())
				property.CreateEvents ();
		}

		#endregion

	}

}