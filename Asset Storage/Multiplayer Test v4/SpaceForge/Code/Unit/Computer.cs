﻿using UnityEngine;
using UnityEngine.Networking;

namespace SpaceForge.Structural.Ships {

	public class Computer : Unit {

		#region implemented abstract members of Unit

		protected override int InitialLayer ()
		{
			return Game.Constants.Layers.Exterior;
		}

		#endregion

		public void DoSomething()
		{
			CmdDoSomething ();
		}

		[Command]
		void CmdDoSomething()
		{
		}

		public void DoSomethingWithSlider(float value)
		{
			CmdDoSomethingWithSlider (value);
		}

		[Command]
		void CmdDoSomethingWithSlider(float value)
		{
		}

	}

}