﻿using UnityEngine;
using UnityEngine.Networking;

namespace SpaceForge {

	[RequireComponent(typeof(SpriteRenderer))]
	public abstract class Equipment : NetworkDummy {

		public enum Fitting {
			Hand,
			Torso,
			Head,
		}

		#region Links

		SpriteRenderer _spriter;
		public SpriteRenderer spriter {
			get {
				return _spriter ?? (_spriter = GetComponent<SpriteRenderer>());
			}
		}

		Sapien _sapien;
		public Sapien sapien {
			get {
				return _sapien;
			}
			set {
				_sapien = value;
			}
		}


		BodyPart _bodyPart;
		internal BodyPart bodyPart {
			get {
				return _bodyPart;
			}
		}

		#endregion

		#region Inspector 

		internal abstract Fitting fitting { get; }

		[SerializeField] Vector3 _offset;
		internal Vector3 offset {
			get {
				return _offset;
			}
		}

		[SerializeField] float _angularOffset;
		internal float angularOffset {
			get {
				return _angularOffset;
			}
		}

		[SerializeField] Sprite enabledSprite;
		[SerializeField] Sprite disabledSprite;

		#endregion

		#region Unity Callers

		void Start() { 
			if (!sapien) {
				Debug.LogError (name + " must be attached to a Sapien! If placed in the editor, ensure it is parented to a sapien.");
				Destroy (gameObject);
			}
		}

		protected virtual void OnEnable()
		{
			spriter.sprite = enabledSprite;

			if (!sapien) {
				enabled = !transform.parent;
				return;
			}

			if (sapien.isServer)
				SetEquipmentDirtyBit ();
		}

		protected virtual void OnDisable()
		{
			if (spriter) //Check for spriter so it doesn't throw an exception when the game exists playmode
				spriter.sprite = disabledSprite;

			if (sapien && sapien.isServer)
				sapien.SetDirtyBit (sapien.GetEquipmentDirtyBit (bodyPart));
		}

		#endregion

		#region Gameplay

		public virtual void Action () {}

		protected virtual void OnAttach() {}

		protected virtual void OnRemove() {}

		public void Attach(Sapien sapien)
		{
			if (!sapien)
				throw new UnityException ("Equipment must be attached to a Sapien.");

			BodyPart bodyPart = GetFittingBodyPart (sapien, fitting);

			if (!bodyPart)
				throw new UnityException ("Sapien already has equipment in that slot! Can't add.");

			this.sapien = sapien;
			this._bodyPart = bodyPart;

			transform.parent = bodyPart.transform;
			transform.localPosition = offset;
			transform.localEulerAngles = Vector3.forward * angularOffset;

			OnAttach ();
		}

		public void Remove()
		{
			OnRemove ();
			Destroy (gameObject);
		}

		#endregion

		#region Network Hijack

		internal void SetEquipmentDirtyBit()
		{
			uint db = sapien.GetEquipmentDirtyBit (bodyPart);
			sapien.SetDirtyBit(db);
		}

		public override bool isServer {
			get {
				return sapien && sapien.isServer;
			}
		}

		internal void Serialize(NetworkWriter writer) 
		{ 
			uint db = sapien.GetEquipmentDirtyBit (bodyPart);
			if (sapien.DirtyBitIsSet (db))
				OnSerialize (writer);
		}

		internal void Deserialize(NetworkReader reader) 
		{
			uint db = sapien.GetEquipmentDirtyBit (bodyPart);
			if (sapien.ReceivedDirtyBitIsSet (db))
				OnDeserialize (reader);
		}

		protected virtual void OnSerialize(NetworkWriter writer)
		{
			writer.Write (enabled);
		}

		protected virtual void OnDeserialize(NetworkReader reader)
		{
			enabled = reader.ReadBoolean ();
		}

		#endregion

		#region Static 

		static BodyPart GetFittingBodyPart(Sapien sapien, Fitting fitting) 
		{
			BodyPart part = null;
		
			switch (fitting) {
			case Fitting.Head:
				part = sapien.head.equipment ? null : sapien.head;
				break;
				
			case Fitting.Hand:
				part = sapien.rightHand.equipment ? sapien.leftHand.equipment ? null : sapien.leftHand : sapien.rightHand;
				break;

			case Fitting.Torso:
				part = sapien.torso.equipment ? null : sapien.torso;
				break;
			}

			return part;
		}

		internal static T Add<T>(Sapien sapien, GameObject prefab) where T : Equipment
		{
			if (sapien == null)
				throw new UnityException("Sapien can't be null.");

			var prefabEquip = prefab.GetComponent<T>();
			if (!prefabEquip)
				throw new UnityException("Prefab must be an Equipment prefab.");

			var targetBodyPart = GetFittingBodyPart (sapien, prefabEquip.fitting);
			if (!targetBodyPart)
			    throw new UnityException ("Sapien already has equipment in that slot! Can't add.");

			var instance = (GameObject)Instantiate (prefab);
			T equip = instance.GetComponent<T> ();

			equip.Attach (sapien);
			equip.enabled = false;

			return equip;
		}

		#endregion
	}
}