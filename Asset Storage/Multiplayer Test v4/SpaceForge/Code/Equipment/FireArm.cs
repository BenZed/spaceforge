using UnityEngine;
using UnityEngine.Networking;
using BossMedia;

namespace SpaceForge {

	public class FireArm : Equipment {

		[SerializeField]
		GameObject projectilePrefab;
		
		[SerializeField]
		byte ammunition = 30;
		
		[SerializeField]
		byte maxAmmunition = 30;
		
		[SerializeField]
		float fireDelay = 0.1f;
		
		[SerializeField]
		Transform muzzle;
		
		float fireTimeStamp = 0;
		
		#region Equipment
		
		internal override Fitting fitting {
			get {
				return Fitting.Hand;
			}
		}
		
		public override void Action ()
		{
			if (ammunition <= 0) {
				enabled = false;
				return;
			}

			if (fireTimeStamp + fireDelay > Time.time)
				return;

			ammunition--;
			fireTimeStamp = Time.time;
			
			var instantiateAt = muzzle ? muzzle : transform;

			var projectile = NetworkDummy.Create<Projectile>(projectilePrefab, instantiateAt, sapien.isServer);
			projectile.gameObject.layer = sapien.gameObject.layer;

			var relBody = sapien.syncBodyType != Unit.SyncBodyType.ContainedBody ? sapien.physical.body : sapien.containable.container.physical.body;
			projectile.relativeVelocity = relBody.velocity;
			sapien.SetCollisionsEnabled (projectile.GetComponent<Collider2D> (), false);

			if (sapien.isServer)
				SetEquipmentDirtyBit();
		}
		
		#endregion

		#region Network
		
		protected override void OnSerialize (NetworkWriter writer)
		{
			base.OnSerialize (writer);
			writer.Write (ammunition);
		}

		protected override void OnDeserialize (NetworkReader reader)
		{
			base.OnDeserialize (reader);
			ammunition = reader.ReadByte ();
		}

		#endregion

		#region Unity Callers

		void OnValidate()
		{
			if (muzzle && muzzle.transform.parent != transform) {
				Debug.LogError("Muzzle must be parented to weapon.");
				muzzle = null;
			}
			
			if (maxAmmunition < 1)
				maxAmmunition = 1;
			
			if (ammunition > maxAmmunition)
				ammunition = maxAmmunition;
			
			if (ammunition < 0)
				ammunition = 0;
		}

		#endregion
	}
}