using UnityEngine;
using UnityEngine.Networking;

namespace SpaceForge {

	public class JetPack : Equipment {

		[SerializeField, Range(0.1f,1f)] float thrust = 0.25f;
		[SerializeField] Vital fuel;

		#region implemented abstract members of Equipment

		internal override Fitting fitting {
			get {
				return Fitting.Torso;
			}
		}

		public override void Action ()
		{
			if (!enabled)
				return;

			Vector2 move = sapien.movement;
			Vector2 force = sapien.transform.up * move.y + sapien.transform.right * move.x;

			if (!fuel.isEmpty && sapien.isServer)
				SetEquipmentDirtyBit ();
			
			if (!fuel.isEmpty) {
				fuel.Deplete (force.magnitude * thrust * Time.fixedDeltaTime);
				sapien.physical.body.AddForce (force * thrust);

			} else
				enabled = false;
		}

		#endregion

		protected override void OnSerialize (NetworkWriter writer)
		{
			base.OnSerialize (writer);
			writer.Write (fuel.Current);
		}

		protected override void OnDeserialize (NetworkReader reader)
		{
			base.OnDeserialize (reader);
			fuel.Set (reader.ReadSingle ());
		}
	}
}