using UnityEngine;
using UnityEngine.Networking;

namespace SpaceForge {

	public class Wearable : Equipment {

		[SerializeField] Sprite leftGloveClosed;
		[SerializeField] Sprite leftGloveOpen;

		[SerializeField] Sprite rightGloveClosed;
		[SerializeField] Sprite rightGloveOpen;

		[SerializeField] Sprite shirt;

		[SerializeField] Atmosphere airTank;

		protected override void OnEnable ()
		{
			base.OnEnable ();
			if (sapien && sapien.biological)
				sapien.biological.atmosphere = airTank;
		}

		protected override void OnDisable ()
		{
			base.OnDisable ();
			if (sapien && sapien.biological) 
				sapien.biological.atmosphere = null;
		}

		protected override void OnAttach ()
		{
			sapien.leftHand.closedSprite = leftGloveClosed;
			sapien.leftHand.openSprite = leftGloveOpen;
			sapien.torso.spriter.sprite = shirt;
			
			sapien.rightHand.closedSprite = rightGloveClosed;
			sapien.rightHand.openSprite = rightGloveOpen;

		}

		protected override void OnRemove ()
		{
			sapien.leftHand.SetDefaultSprite ();
			sapien.rightHand.SetDefaultSprite ();
			sapien.torso.SetDefaultSprite ();
		}

		#region implemented abstract members of Equipment

		internal override Fitting fitting {
			get {
				return Fitting.Head;
			}
		}

		public override void Action ()
		{
			throw new System.NotImplementedException ();
		}

		#endregion

		protected override void OnSerialize (NetworkWriter writer)
		{
			base.OnSerialize (writer);
			writer.Write (airTank.o2);
			writer.Write (airTank.co2);
		}

		protected override void OnDeserialize (NetworkReader reader)
		{
			base.OnDeserialize (reader);
			airTank.o2 = reader.ReadSingle ();
			airTank.co2 = reader.ReadSingle ();
		}
	}
}