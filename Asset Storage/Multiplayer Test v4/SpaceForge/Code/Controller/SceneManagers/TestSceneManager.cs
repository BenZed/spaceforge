using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;

namespace SpaceForge {

	public class TestSceneManager : SceneManager {

		public override void OnSceneLoadedForPlayer(Player player)
		{
			Debug.Log (player.name + " scene loaded.");
		}

		IEnumerator Start()
		{
			yield return new WaitForSeconds (3f);

			var players = FindObjectsOfType<Player> ();
			Player player = null;

			foreach(var other in players)
			{
				if (other.isLocalPlayer) {
					player = other; 
					break;
				}
			}
		
			if (!player || player.transform.parent)
				yield break;
			
			foreach(var sapien in FindObjectsOfType<Sapien>())
			{
				if (sapien.GetComponentInChildren<Player>())
					continue;
				
				player.SetSapien(sapien);
				
				break;
			}

		}

	}
}
