using UnityEngine;

namespace SpaceForge {

	[DisallowMultipleComponent]
	[RequireComponent(typeof(MeshRenderer))]
	[RequireComponent(typeof(MeshFilter))]
	public sealed class Background : MonoBehaviour {

		[Tooltip("How the texture maps to the background.")]
		[RangeAttribute(5f,100f)]
		public float textureScale = 60f;
		
		[Tooltip("How big the background feels. \n0 - Same size as foreground. \n100 - Infinitly huge.")]
		[RangeAttribute(0f,100f)]
		public float environmentScale;

		static Vector3 RelativePosition = new Vector3(0f,0f,100f);

		#region Unity Callers

		void Start() 
		{
			ValidateParent ();
			AdjustMaterialAndRenderer ();
			transform.localPosition = RelativePosition;
		}

		void Update() 
		{
			//Determine Size of background
			float ortho = SpaceCamera.instance.zoom;

			float y = ortho * 2f;
			float x = ((float)Screen.width / (float)Screen.height) * y;
			float scale = Mathf.Max(x,y);
			transform.localScale = new Vector3(scale,scale, 1f);

			//Determine Zoom Scales and Offsets
			float zoom = ortho / textureScale;
			float sizeExponent = (100f - environmentScale) / 100f;
			float zoomScale = Mathf.Pow(zoom, sizeExponent);
			Vector2 zoomOffset = new Vector2(zoomScale, zoomScale);

			//Set Texture Scale
			material.mainTextureScale = zoomOffset;

			//Set Texture Position
			var pos = transform.position;
			zoom *= 0.5f;
			float offsetX = (pos.x * sizeExponent) / (scale / zoom);
			float offsetY = (pos.y * sizeExponent) / (scale / zoom);
			material.mainTextureOffset = new Vector2(offsetX, offsetY) + (zoomOffset * -0.5f);
		}

		#endregion

		#region Helper

		void ValidateParent() 
		{
			if (SpaceCamera.instance.transform != transform.parent)
				throw new UnityException ("The background component should be parented to a gameObject with a camera controller component.");
		}
		
		Material material;
		void AdjustMaterialAndRenderer()
		{
			MeshRenderer renderer = GetComponent<MeshRenderer>();
			material = renderer.sharedMaterial;
			renderer.enabled = true;
			
			//These settings are for performance. They SHOULD match in the editor, anwyay.
			renderer.receiveShadows = false;
			renderer.useLightProbes = false;
			renderer.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;
			renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
			renderer.probeAnchor = null;
			
		}

		#endregion

	}

}