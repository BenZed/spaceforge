using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using BossMedia;

namespace SpaceForge {

	public class Player : NetworkBehaviour {

		#region Unity Callers

		void Awake()
		{
			nameTag = GetComponent<Text> ();
		}

		void Update()
		{
			ControlSapien();
		}

		void LateUpdate()
		{
			NameTagToSapien();
		}

		#endregion

		#region Sapien

		[SyncVar]
		NetworkIdentity _sapienIdentity;

		Sapien _sapien;
		public Sapien sapien {
			get {
				if (!transform.parent || !_sapienIdentity)
					return _sapien = null;

				return _sapien ?? (_sapien = _sapienIdentity.GetComponent<Sapien>()); 
			}
		}

		public void SetSapien(Sapien sapien)
		{
			var identity = sapien ? sapien.identity : null;
			if (identity && identity.GetComponentInChildren<Player>()) {
				Debug.LogWarning("Cannot control a sapien controlled by another player.");
				return;
			}

			if (isServer)
				RpcSetSapien (identity);
			else
				CmdSetSapien(identity);
		}

		[Command]
		void CmdSetSapien(NetworkIdentity identity)
		{
			RpcSetSapien(identity);
		}

		[ClientRpc]
		void RpcSetSapien(NetworkIdentity identity)
		{
			SetSapien(identity);
		}

		void SetSapien(NetworkIdentity identity)
		{
			_sapienIdentity = identity;
			transform.SetParent (_sapienIdentity ? _sapienIdentity.transform : null);

			if (sapien) {
				sapien.name = nameTag.text + "'s Human";
				var cam = SpaceCamera.instance;
				cam.unit = sapien;
				cam.SetTargetZoom (SpaceCamera.NeutralZoom);
			}
		}

		static Vector3 NameTagOffset = Vector3.up * 0.375f + Vector3.back * 0.75f;

		void NameTagToSapien()
		{
			if (!(nameTag.enabled = (sapien)))
				return;

			transform.rotation = Quaternion.identity;
			transform.position = sapien.transform.position + NameTagOffset;;
		}

		void ControlSapien()
		{
			if (!isLocalPlayer || !sapien)
				return;

			bool strafe = Input.GetKey(KeyCode.LeftShift);
			bool modifyAction = Input.GetKey (KeyCode.LeftAlt);

			bool rightReach = Input.GetKey (KeyCode.E);
			bool leftReach = Input.GetKey (KeyCode.Q);

			bool rightAction = Input.GetMouseButtonDown (1);
			bool leftAction = Input.GetMouseButtonDown (0);

			Hand.ReachType reach =  
			rightReach && leftReach  ? Hand.ReachType.RightAndLeft : 
						  leftReach  ? Hand.ReachType.Left : 
						  rightReach ? Hand.ReachType.Right : 
			     					   Hand.ReachType.None;

			Vector2 move = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
			Vector2 mousePos = SpaceCamera.instance.pointer;

			float face = Geometry2D.AbsoluteAngle (sapien.transform.position, mousePos) + 90f;

			/***** AIM, MOVE, REACH  ********/

			sapien.Aim (mousePos);
			sapien.Move (move);
			sapien.Reach (reach);
			if (!isServer)
				CmdAimMoveReach (move, mousePos, (byte)reach);

			/***** HAND ACTIONS  ********/

			if (rightAction)
				sapien.control.ContextAction (modifyAction, false);
			if (rightAction && !isServer)
				CmdContextAction (modifyAction, false);
			else if (rightAction && isServer)
				RpcContextAction (modifyAction, false);

			if (leftAction)
				sapien.control.ContextAction (modifyAction, true);
			if (leftAction && !isServer)
				CmdContextAction (modifyAction, true);
			else if (rightAction && isServer)
				RpcContextAction (modifyAction, true);

			/***** FACING  ********/

			if (!strafe)
				sapien.Face (face);
			if (!strafe && !isServer)
				CmdSetFacing(face);

		}

		[Command]
		void CmdAimMoveReach (Vector2 move, Vector2 aim, byte reach)
		{
			if (!sapien)
				return;

			sapien.Move(move);
			sapien.Aim(aim);
			sapien.Reach((Hand.ReachType)reach);
		}
		
		[Command]
		void CmdSetFacing(float face)
		{
			if (!sapien)
				return;
			
			sapien.Face(face);
		}

		[Command]
		void CmdContextAction(bool modifier, bool isLeft)
		{
			sapien.control.ContextAction (modifier, isLeft);
			RpcContextAction (modifier, isLeft);
		}

		[ClientRpc]
		void RpcContextAction(bool modifier, bool isLeft)
		{
			sapien.control.ContextAction (modifier, isLeft);
		}


		#endregion

		#region Display Name

		Text nameTag;

		public void SetPlayerName(string value)
		{
			if (!nameTag)
				nameTag = GetComponent<Text>();

			if (value == null) {
				nameTag.text = "";
				return;
			}

			nameTag.text = value.ToUpper();
			if (sapien)
				sapien.name = value + "'s Player";
			name = value + "'s Player";
		}

		#endregion

	}

}
