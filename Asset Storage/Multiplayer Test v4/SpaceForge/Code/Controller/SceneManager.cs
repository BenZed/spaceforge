﻿using UnityEngine.Networking;

namespace SpaceForge {

	public class SceneManager : NetworkBehaviour {

		public virtual void OnSceneLoadedForPlayer(Player player) { }

	}
}