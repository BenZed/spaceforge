using UnityEngine;
using UnityEngine.Networking;
using BossMedia;
using System.Collections;
//using SpaceForge.Structural;
using System.Collections.Generic;
using UnityStandardAssets.Network;

namespace SpaceForge {

	public class Game : LobbyManager {

		#region Messages

		internal static class Messages {

			public const short EnsureContainer = 1000;
			public const short Kill = 1001;

			public class GameObjectClientMessage : MessageBase {

				public GameObject gameObject;
				public int connectionId;

				public GameObjectClientMessage(){}

				public GameObjectClientMessage(GameObject gameObject, int connectionId)
				{
					this.gameObject = gameObject;
					this.connectionId = connectionId;
				}
			}

			public class GameObjectMessage : MessageBase {
				
				public GameObject gameObject;

				public GameObjectMessage(){}
				
				public GameObjectMessage(GameObject gameObject)
				{
					this.gameObject = gameObject;
				}
			}
		}

		#endregion

		#region Constants

		[System.Serializable]
		internal class Constants {

			public static class Layers {

				public const int Exterior = 8;
				public const int Interior = 9;
				public const int Ship = 10;
				public const int ShipObstacle = 11;
				public const int ShipTrigger = 12;
			}

			[System.Serializable]
			public class Camera {

				[System.Serializable]
				public class Detail
				{
					[SerializeField] float minZoom;
					[SerializeField] float maxZoom;

					[SerializeField] bool keepUnitInView;
					/*
					[SerializeField] bool motionOverlay;
					[SerializeField] bool thermalOverlay;
					[SerializeField] bool energyOverlay;
					[SerializeField] bool shapeOverlay;*/
					
					public float MinZoom { get { return minZoom; } }
					public float MaxZoom { get { return maxZoom; } }
					
					public bool KeepUnitInView { get { return keepUnitInView; } }
					/*
					public bool MotionOverlay  { get { return motionOverlay; } }
					public bool ThermalOverlay { get { return thermalOverlay; } }
					public bool EnergyOverlay  { get { return energyOverlay; } }
					public bool ShapeOverlay   { get { return shapeOverlay; } }*/
				}

				public Detail unitDetail 		= new Detail ();
				public Detail radarDetail 	= new Detail ();
				public Detail sensorDetail 	= new Detail ();
				public Detail observerDetail 	= new Detail ();

			}

			[System.Serializable]
			public class Ship {

				[SerializeField] float boardingSpeed;
				[SerializeField] float matchingSpeed;
				[SerializeField] float collidableRefreshInterval;

				[SerializeField] float interiorDrag;
				[SerializeField] float interiorAngularDrag;


				public float BoardingSpeed { get { return boardingSpeed; } }
				public float MatchingSpeed { get { return matchingSpeed; } }
				public float CollidableRefreshInterval { get { return collidableRefreshInterval; } }

				public float InteriorDrag { get { return interiorDrag; } }
				public float InteriorAngularDrag { get { return interiorAngularDrag; } }

			}

			[System.Serializable]
			public class Damagable {

				[SerializeField] float minimumReverbDamage;
				public float MinimumReverbDamage { get { return minimumReverbDamage; } }

				[SerializeField] float impactDamageFactor;
				public float ImpactDamageFactor { get { return impactDamageFactor; } }
			}

			[System.Serializable]
			public class Biological {
				
				[SerializeField, Range(0.1f,0.9f)] float inhaleO2Threshold;
				[SerializeField, Range(1.25f,3f)] float inhaledMaxCapacityFactor;
				[SerializeField, Range(1f,5f)] float breathingSpeed;

				public float InhaleO2Threshold { get { return inhaleO2Threshold; } }
				public float InhaledLungCapacityFactor { get { return inhaledMaxCapacityFactor; } }
				public float BreathingSpeed { get { return breathingSpeed; } }

			}

			[System.Serializable]
			public class Physical {
				
				[SerializeField] float minSpeedDragThreshold;
				[SerializeField] float maxSpeed;
				[SerializeField] float dragAtMinSpeed;
				[SerializeField] float dragAtMaxSpeed;
				[SerializeField] float minSpinDragThreshold;
				[SerializeField] float maxSpin;
				[SerializeField] float dragAtMaxSpin;

				public float MinSpeedDragThreshold { get { return minSpeedDragThreshold; } }
				public float MaxSpeed { get { return maxSpeed; } }
				public float DragAtMinSpeed { get { return dragAtMinSpeed; } }
				public float DragAtMaxSpeed { get { return dragAtMaxSpeed; } }
				public float MinSpinDragThreshold { get { return minSpinDragThreshold; } }
				public float MaxSpin { get { return maxSpin; } }
				public float DragAtMaxSpin { get { return dragAtMaxSpin; } }

			}

			public Ship ship = new Ship();
			public Physical physical = new Physical ();
			public Damagable damagable = new Damagable ();
			public Biological biological = new Biological ();

			[SerializeField] List<string> randomNames = new List<string>();
			public string RandomName()
			{
				int max = randomNames.Count;
				if (max == 0)
					return "No Name";

				int i = Random.Range(0, max);

				return randomNames[i];
			}

			public Camera camera = new Camera();

		}
	
		[SerializeField]
		internal Constants constants;

		#endregion 

		#region Unity Callers

		protected override void Start()
		{
			base.Start ();

			Singleton.Ensure<Game>();
			Application.LoadLevel ("Lobby");
		}

		#endregion

		public override void OnServerSceneChanged (string sceneName)
		{
			var sceneManager = FindObjectOfType<SceneManager> ();
			var players = FindObjectsOfType<Player> ();

			if (sceneManager)
				foreach (var player in players)
					sceneManager.OnSceneLoadedForPlayer (player);
			else
				Debug.Log ("Scene Manager Not Found.");
		}

		public override bool OnLobbyServerSceneLoadedForPlayer (GameObject lobbyPlayerGO, GameObject playerGO)
		{

			var lobbyPlayer = lobbyPlayerGO.GetComponent<LobbyPlayer>();
			var player = playerGO.GetComponent<Player> ();

			player.SetPlayerName (lobbyPlayer.playerName);

			return true;
		}

		#region Singleton
		
		public static Game manager {
			get {
				if (!Application.isPlaying)
					throw new UnityException("Do not access Game Manager in Edit mode, please!");

				return Singleton.Get<Game>();
			}
		}

		static Game () 
		{
			Singleton.Register<Game> (null, true);
		}

		#endregion
	}

}