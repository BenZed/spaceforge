using UnityEngine;
using BossMedia;
using System.Collections;
using System.Collections.Generic;

namespace SpaceForge {

	[RequireComponent(typeof(Camera))]
	public class SpaceCamera : MonoBehaviour {

		public enum Limits {
			Unit,
			Radar,
			Sensor,
			Observer,
		}

		Unit _unit;
		public Unit unit {
			get {
				return _unit;
			}
			set {
				_unit = value;

				if (value == null)
					limits = Limits.Observer;

				else if (limits == Limits.Observer)
					limits = Limits.Unit;

			}
		}

		[SerializeField] Limits _limits;
		public Limits limits {
			get {
				return _limits;
			
			}
			set {
				if (_limits == value)
					return;

				_limits = value;

				switch (_limits) {
				case Limits.Unit:
					detail = Game.manager.constants.camera.unitDetail;
					break;

				case Limits.Radar:
					detail = Game.manager.constants.camera.radarDetail;
					break;

				case Limits.Sensor:
					detail = Game.manager.constants.camera.sensorDetail;
					break;

				case Limits.Observer:
					detail = Game.manager.constants.camera.observerDetail;
					break;
				}
			}
		}

		Camera cam;
		float targetZoom = NeutralZoom;
		Game.Constants.Camera.Detail detail;

		public const float NeutralZoom = 5f;
		const float ZPosition = -10f;
		const float Speed = 10f;
		const float ZoomMultFactor = 0.1f;

		public float zoom {
			get {
				return cam.orthographicSize;
			}
			private set {
				cam.orthographicSize = value;
			}
		}

		public void SetTargetZoom(float value)
		{
			targetZoom = value;
		}

		public Vector2 position {
			get {
				return (Vector2)transform.position;
			}
			set {
				transform.position = new Vector3(value.x, value.y, ZPosition);
			}
		}

		public float rotation {
			get {
				return transform.eulerAngles.z;
			}
			set {
				transform.eulerAngles = Vector3.forward * value;
			}
		}

		public Vector3 pointer {
			get {
				return cam.ScreenToWorldPoint(Input.mousePosition + Vector3.forward * -ZPosition);
			}
		}

		#region Unity Callers

		void FixedUpdate() 
		{
			UpdateZoom ();
			UpdateTranslate ();
			UpdateRotation ();
		}

		void Awake() 
		{
			Singleton.Ensure<SpaceCamera> ();

			SetCamera ();

			limits = Limits.Observer;
			if (detail == null)
				detail = Game.manager.constants.camera.observerDetail;

			position = transform.position;

			targetZoom = zoom;
		}

		#endregion

		#region Helper

		void UpdateZoom()
		{
			float zoomInput = Input.GetAxis ("Mouse ScrollWheel");
			float zoomMult = zoom * ZoomMultFactor;
			float zoomDelta = zoomInput * zoomMult;

			targetZoom = Mathf.Clamp (targetZoom + zoomDelta, detail.MinZoom, detail.MaxZoom);
			zoom = Mathf.Lerp (zoom, targetZoom, Time.deltaTime * Speed);
		}

		Vector3 lastPointer = Vector3.zero;
		void UpdateTranslate()
		{
		//	Vector2 pointerTranslate = GetPointerTranslate ();
			if (unit)
				position = Vector2.Lerp (position, unit.transform.position, Time.deltaTime * Speed);


		}

		void UpdateRotation()
		{
			float target = 0f;
			rotation = Mathf.LerpAngle (rotation, target, Time.deltaTime * Speed * 0.25f);
	
		}

		Vector2 GetPointerTranslate()
		{
			if (!Input.GetMouseButton (2))
				return Vector2.zero;
			
			Vector2 pointerDelta = (Vector2)(Input.mousePosition - lastPointer) * Time.deltaTime;
			lastPointer = Input.mousePosition;
			
			return pointerDelta * (zoom / NeutralZoom);
		}

		void SetCamera()
		{
			cam = GetComponent<Camera> ();
			cam.backgroundColor = Color.black;
			cam.orthographic = true;
			cam.clearFlags = CameraClearFlags.SolidColor;
		}

		#endregion

		#region Singleton

		public static SpaceCamera instance {
			get {
				return Singleton.Get<SpaceCamera>();
			}
		}

		static SpaceCamera()
		{
			Singleton.Register<SpaceCamera> (null, true);
		}

		#endregion
	}
}