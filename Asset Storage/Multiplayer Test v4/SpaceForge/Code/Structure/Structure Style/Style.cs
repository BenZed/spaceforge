﻿using UnityEngine;
using BossMedia;

namespace SpaceForge.Structural {

	public abstract class Style : ScriptableObject {

		#region Nested

		[System.Serializable]
		public class TileTypeData {
			[Range(0.125f,3f)] public float mass;
			[Range(0,1f)] public float damageAbsorptionFactor;
			[Range(0,1f)] public float damageReverbFactor;
			[Range(10f,800f)] public float health;
		}

		#endregion
	
		[SerializeField] public Material material;
		[SerializeField] [Range(0.05f, 0.25f)] public float wallWidth;
		[SerializeField] public TileTypeData lightTileData;
		[SerializeField] public TileTypeData regularTileData;
		[SerializeField] public TileTypeData armoredTileData;

		internal TileTypeData GetTileTypeData(Tile.Type type) 
		{
			if (type == Tile.Type.Light)
				return lightTileData;
			else if (type == Tile.Type.Armoured)
				return armoredTileData;
			else
				return regularTileData;
		}

		public abstract void CreateUVCode(Tile tile);
		public abstract void UpdateDamageUV(Section section);
		public abstract void UpdateGeometryAndUV (Section section);


	}

}