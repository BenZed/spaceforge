using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using BossMedia;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SpaceForge.Structural {

	public class Style2D : Style {

		static Style2D () {
			
			BuildQuandrantUVs();
			
		}

		#region implemented abstract members of StructureStyle

		public override void CreateUVCode (Tile tile) 
		{
			var uvCode = string.Empty;
			for (var i = 0; i < 4; i++)
				uvCode += RandomVariation();
			
			uvCode += UnityEngine.Random.value > 0.80f ? 'T' : 'F';
			uvCode += UnityEngine.Random.Range (0, 6);
			
			tile.uvCode = uvCode;
		}

		char RandomVariation()
		{
			float value = UnityEngine.Random.value;
			
			if (value == Mathf.Clamp (value, 0.35f, 1f))
				return 'N';
			
			if (value == Mathf.Clamp (value, 0.175f, 0.35f))
				return 'E';
			
			if (value == Mathf.Clamp (value, 0.0825f, 0.175f))
				return 'S';
			
			return 'W';
		}

		public override void UpdateDamageUV(Section sec)
		{
			var floorUVs = new List<Vector2> ();
			var roofUVs = new List<Vector2> ();
			var scaffoldingUVs = new List<Vector2> ();
			
			sec.ForEachTile (tile => {
				DrawDamageUV(tile, floorUVs, Level.Type.Floor);
				DrawDamageUV(tile, roofUVs, Level.Type.Roof);
				DrawDamageUV(tile, scaffoldingUVs, Level.Type.Scaffolding);
			});

			sec.floor.filter.sharedMesh.uv2 = floorUVs.ToArray ();
			sec.roof.filter.sharedMesh.uv2 =  roofUVs.ToArray ();
			sec.scaffolding.filter.sharedMesh.uv2 = scaffoldingUVs.ToArray ();

		}

		public override void UpdateGeometryAndUV(Section sec)
		{
			var tileVerts = new List<Vector3> ();
			var tileTris = new List<int> ();

			var wallVerts = new List<Vector3> ();
			var wallTris = new List<int> ();

			var floorUVs = new List<Vector2> ();
			var roofUVs = new List<Vector2> ();
			var scaffoldingUVs = new List<Vector2> ();

			var tileMeshes = new List<DynamicMesh> ();
			var wallMeshes = new List<DynamicMesh> ();

			sec.ForEachTile (tile => {
				tileMeshes.Add (tile);
				tile.ForEachNode((coord, node) => {
					wallMeshes.Add(node);
				});
				DrawUV(tile, floorUVs, roofUVs, scaffoldingUVs);
			});
			
			var tilePaths = DynamicMesh.CreateColliderPathsAndGeometry (tileVerts, tileTris, tileMeshes);
			var wallPaths = DynamicMesh.CreateColliderPathsAndGeometry (wallVerts, wallTris, wallMeshes);
			
			var tileVertArr = tileVerts.ToArray ();
			var tileTriArr = tileTris.ToArray ();

			DynamicMesh.ApplyGeometryToFilters (tileVertArr, tileTriArr, sec.floor.filter, sec.roof.filter, sec.scaffolding.filter);
			DynamicMesh.ApplyGeometryToFilters (wallVerts.ToArray(), wallTris.ToArray(), sec.walls.filter);
			DynamicMesh.ApplyPathsToPolygonColliders (tilePaths, sec.floor.poly, sec.roof.poly);
			DynamicMesh.ApplyPathsToPolygonColliders (wallPaths, sec.walls.poly);

			sec.floor.filter.sharedMesh.uv = floorUVs.ToArray ();
			sec.roof.filter.sharedMesh.uv =  roofUVs.ToArray ();
			sec.scaffolding.filter.sharedMesh.uv = scaffoldingUVs.ToArray ();

			//sec.walls.filter.sharedMesh.uv = wallUVArr;
			//sec.walls.filter.sharedMesh.uv = damageUvArr;

			sec.roof.poly.enabled = true;
			sec.floor.poly.enabled = true;
			sec.walls.poly.enabled = true;
		}

		#endregion

		#region Mapping Measurments 
		
		const float UV1 = 0.125f;
		const float UV1_2 = 0.0625f;
		const float UV1_4 = 0.03125f;
		const float UV1_8 = 0.015625f;
		const float UV0 = 0f;
		const float Pixel = 0.002f;
		
		#endregion

		#region QuadrantUV Base
				
		static SimpleGrid<Vector2[]> quadrantUvs;
		
		static void BuildQuandrantUVs()
		{
			var NW = new Vector2(Pixel, UV1 - Pixel);
			var NE = new Vector2(UV1 - Pixel, UV1 - Pixel);
			var Center = new Vector2(UV1_2, UV1_2);
			var SE = new Vector2(UV1 - Pixel, Pixel);
			var SW = new Vector2(Pixel, Pixel);
			
			quadrantUvs = new SimpleGrid<Vector2[]> ();
			quadrantUvs.Add (Coord.n, new Vector2[]{
				Center,
				NW,
				NE,
			});
			
			quadrantUvs.Add (Coord.e, new Vector2[]{
				Center,
				NE,
				SE,
			});
			
			quadrantUvs.Add (Coord.s, new Vector2[]{
				Center,
				SE,
				SW,
			});
			
			quadrantUvs.Add (Coord.w, new Vector2[]{
				Center,
				SW,
				NW,
			});
		}
		
		#endregion

		#region UV Helper


		internal void DrawUV (Tile tile, List<Vector2> floorUVs, List<Vector2> roofUVs, List<Vector2> scaffoldingUVs) 
		{
			for(var i = 0; i < 7; i += 2) {
				Coord direction = Coord.Around[i];
				if (!Tile.QuadrantExists(tile, direction))
					continue;

				DrawQuadrant(tile, Level.Type.Floor, direction, floorUVs);
				DrawQuadrant(tile, Level.Type.Roof, direction, roofUVs);
				DrawQuadrant(tile, Level.Type.Scaffolding, direction, scaffoldingUVs);
			};
		}
		
		internal void DrawDamageUV (Tile tile, List<Vector2> uvs, Level.Type levelType)
		{
			for(var i = 0; i < 7; i += 2) {
				Coord direction = Coord.Around[i];
				if (!Tile.QuadrantExists(tile, direction))
					continue;
				
				//So that all level layers don't have the same damage variation.
				var quadrant = tile.quadrants.Get (direction);

				Vital integrity = quadrant.integrity;
				int uvCol, uvRow;
				
				if (integrity.isEmpty && levelType != Level.Type.Scaffolding) {
					
					uvCol = 7;
					uvRow = 1;
					
				} else {
					
					var iPercent = integrity.Percent;
					float uFactor;
					
					if (iPercent >= 0.7f) 
						uFactor = 0f;
					else 
						uFactor = (0.7f - iPercent) * 10f;
					
					uvCol = Mathf.CeilToInt(uFactor);
					int damageVariation = System.Convert.ToInt32(tile.uvCode[5]);
					int levelVariation = (damageVariation + (int)levelType) % 6;
					uvRow = levelVariation + 2;
				}
				
				DrawQuadrantWithUVCoords(direction, uvs, uvCol, uvRow);				
			}
		}

		Coord CodedDrawDir(string uvCode, Coord quadDir)
		{
			var codeIndex = quadDir == Coord.n ? 0 :
							quadDir == Coord.e ? 1 :
							quadDir == Coord.s ? 2 : 3;

			char letter = uvCode [codeIndex];

			return letter == 'N' ? Coord.n :
				   letter == 'E' ? Coord.e :
				   letter == 'S' ? Coord.s : Coord.w;
		}

		void DrawQuadrant(Tile tile, Level.Type levelType, Coord dir, List<Vector2> uvs)
		{
			int uvCol = UvColFromTileType(tile.type);
			int uvRow = UvRowFromLevelType(tile, levelType);
			Coord drawDir = CodedDrawDir (tile.uvCode, dir);
			
			if (levelType == Level.Type.Roof && !tile.sealable) {
				uvCol = 7;
				uvRow = 1;
				
			} else if (!DrawQuadAdjacent(ref drawDir, ref uvCol, dir, tile, levelType))
				DrawTypeTransition(ref drawDir, ref uvCol, dir, tile, levelType);
			
			DrawQuadrantWithUVCoords(drawDir, uvs, uvCol, uvRow);
		}
		
		bool DrawQuadAdjacent(ref Coord drawDir, ref int uvCol, Coord forwardDir, Tile tile, Level.Type levelType) 
		{
		
			Coord leftDir = Coord.AroundResolve (forwardDir, -2);
			Coord rightDir = Coord.AroundResolve (forwardDir, 2);

			bool forward = Tile.QuadrantExists(tile, forwardDir) && tile.quadrants.Get (forwardDir).adjacentExists;
			bool left =    Tile.QuadrantExists(tile, leftDir   ) && tile.quadrants.Get (leftDir   ).adjacentExists;
			bool right =   Tile.QuadrantExists(tile, rightDir  ) && tile.quadrants.Get (rightDir  ).adjacentExists;

			if (forward)
				return false;
			else
				uvCol++;
			
			if (levelType == Level.Type.Scaffolding)
				return true;
			
			if (!left && !right)
				drawDir = Coord.w;
			
			else if (left && !right)
				drawDir = Coord.s;
			
			else if (!left && right)
				drawDir = Coord.n;
			
			else //if left && right
				drawDir = Coord.e;
			
			return true;
		}
		
		void DrawTypeTransition(ref Coord drawDir, ref int uvCol, Coord forwardDir, Tile tile, Level.Type levelType)
		{
			Coord leftDir = Coord.AroundResolve (forwardDir, -2);
			Coord rightDir = Coord.AroundResolve (forwardDir, 2);
			
			bool forwardMatch = Tile.QuadrantExists(tile, forwardDir) && tile.quadrants.Get (forwardDir).adjacentTypesMatch;
			bool leftMatch =    Tile.QuadrantExists(tile, leftDir   ) && tile.quadrants.Get (leftDir   ).adjacentTypesMatch;
			bool rightMatch =   Tile.QuadrantExists(tile, rightDir  ) && tile.quadrants.Get (rightDir  ).adjacentTypesMatch;
			
			if (forwardMatch || tile.type == Tile.Type.Regular)
				return;
			else
				uvCol--;
			
			if (levelType == Level.Type.Scaffolding)
				return;
			
			if (leftMatch && !rightMatch)
				drawDir = Coord.s;

			else if (!leftMatch && rightMatch)
				drawDir = Coord.n;

			else if (leftMatch && rightMatch)
				drawDir = Coord.e;

			else //if !leftMatch && !rightMatch
				drawDir = Coord.w;
		}
		
		int UvColFromTileType(Tile.Type type)
		{
			return (int)type * 3;
		}
		
		int UvRowFromLevelType(Tile tile, Level.Type type)
		{
			bool quadrantsDetailed = tile.uvCode[4] == 'T';
			switch (type) {
				
			case Level.Type.Floor:
				if (!tile.sealable)
					return 2;
				else
					return quadrantsDetailed ? 6 : 7;
				
			case Level.Type.Roof:
				return quadrantsDetailed ? 4 : 5;
				
			case Level.Type.Scaffolding:
				return 3;
				
			case Level.Type.Wall:
			default:
				return 1;
				
			}
		}
		
		void DrawQuadrantWithUVCoords(Coord drawDir, List<Vector2> uvList, int uvCol, int uvRow)
		{
			Vector2 offset = new Vector2(UV1 * (float)uvCol, UV1 * (float)uvRow);
			var uvs = quadrantUvs.Get (drawDir);

			for(int i = 0; i < uvs.Length; i++)
				uvList.Add (uvs[i] + offset);

		}

		#endregion

	}

}