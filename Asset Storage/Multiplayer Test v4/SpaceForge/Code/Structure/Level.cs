using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SpaceForge.Structural {

	[ExecuteInEditMode]
	[DisallowMultipleComponent]
	[RequireComponent(typeof(MeshRenderer))]
	[RequireComponent(typeof(MeshFilter))]
	public sealed class Level : NetworkDummy {

		#region Nested Types
		
		public enum Type {
			Roof,
			Wall,
			Floor,
			Scaffolding
		}
		
		#endregion

		#region Component Links

		MeshFilter _filter;
		internal MeshFilter filter {
			get {
				return _filter ?? (_filter = GetComponent<MeshFilter>());
			}
		}

		MeshRenderer _render;
		internal MeshRenderer render {
			get {
				return _render ?? (_render = GetComponent<MeshRenderer>());
			}
		}

		PolygonCollider2D _poly;
		internal PolygonCollider2D poly {
			get {
				return _poly ?? (_poly = GetComponent<PolygonCollider2D>());
			}
		}

		#endregion

		#region Unity Callers

		void Awake() {

			SetRendererDefaults ();
			SetFilterDefaults ();
		}

		#endregion

		#region Main

		[SerializeField, HideInInspector] internal Type type;

		internal void EnsureCollider(bool exists)
		{
			if (!exists && poly)
				Destroy (_poly);

			if (!exists)
				return;

			if (!poly)
				_poly = gameObject.AddComponent<PolygonCollider2D> ();

			poly.isTrigger = type == Type.Floor;
			poly.usedByEffector = false;
			poly.offset = Vector2.zero;
			poly.enabled = Application.isPlaying;
			poly.hideFlags = HideFlags.None;
		}

		internal void SetDamageUV(Vector2[] uvs) 
		{
			filter.sharedMesh.uv2 = uvs;
		}
		
		internal void SetMaterial(Material material)
		{
			render.material = material;
		}

		#endregion

		#region Helper

		void SetRendererDefaults()
		{
			var render = this.render;

			render.receiveShadows = true;
			render.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
			render.useLightProbes = false;
			render.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;
			render.probeAnchor = null;
			render.hideFlags = HideFlags.None;

		}

		void SetFilterDefaults()
		{
			var filter = this.filter;
			filter.hideFlags = HideFlags.None;
		}

		#endregion

	}
}