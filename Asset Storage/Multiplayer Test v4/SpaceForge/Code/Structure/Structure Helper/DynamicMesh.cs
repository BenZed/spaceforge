using System;
using System.Collections.Generic;
using UnityEngine;
using BossMedia;

namespace SpaceForge.Structural {

	[Serializable]
	public class DynamicMesh : ISerializationCallbackReceiver {
		
		#region Nested

		public class Edge {
			public readonly Vertex a;
			public readonly Vertex b;
			
			public Edge(Vertex a, Vertex b)
			{
				this.a = a;
				this.b = b;
			}

			Vector2 Rounded(Vector2 input, float amount = 1000f) {

				if (amount == 0f)
					return input;

				return new Vector2(Mathf.Round (input.x * amount) / amount, Mathf.Round(input.y * amount) / 1000f);

			}
			
			public Vector2 start {
				get {
					return Rounded(a.position);
				}
			}

			public Vector2 mid {
				get {
					return Rounded((start - end) * 0.5f + end);
				}
			}

			public Vector2 end {
				get {
					return Rounded(b.position);
				}
			}
						
			#region Static Overrides
			
			public override bool Equals (object obj)
			{
				if (obj is Edge) {
					var edge = (Edge)obj;
					//An edge is equal regardless of which order it's points are in
					return (edge.start == start && edge.end == end) || (edge.end == start && edge.start == end);
				}
				
				return false;
			}
			
			public override int GetHashCode ()
			{
				return start.GetHashCode() ^ end.GetHashCode();
			}
			
			public override string ToString ()
			{
				return string.Format ("["+start.x+","+start.y+"->"+end.x+","+end.y+"]");
			}
			
			#endregion
			
		}

		[Serializable]	
		public class Vertex {
			
			[SerializeField][HideInInspector] Vector2 _parent;

			[SerializeField] public bool enableMesh = true;

			[SerializeField] public bool enableCollider = true;

			/// <summary>
			/// The position of this vertex, not including the origin.
			/// </summary>
			[SerializeField] public Vector2 offset = Vector2.zero;

			[SerializeField][HideInInspector] Vector2 _key;

			/// <summary>
			/// The key position of this vertex inside it's Dynamic Mesh.
			/// </summary>
			/// <value>The local.</value>
			public Vector2 key {
				get {
					return _key;
				}
			}

			/// <summary>
			/// Position not including offset.
			/// </summary>
			/// <value>The global.</value>
			public Vector2 origin {
				get {
					return _parent + key;
				}
			}

			/// <summary>
			/// Position of this vertex.
			/// </summary>
			/// <value>The position.</value>
			public Vector2 position {
				get {
					return origin + offset;
				} set {
					offset = value - origin;
				}
			}
			
			public override bool Equals (object obj)
			{
				var vertex = obj as Vertex;
				if (vertex == null)
					return false;
				
				return vertex.key == key && _parent == vertex._parent;
			}
			
			public override int GetHashCode ()
			{
				return key.GetHashCode () ^ _parent.GetHashCode ();
			}
	
			public void SnapToIntersection(Vector2 start1, Vector2 end1, Vector2 start2, Vector2 end2)
			{
				var intersection = Geometry2D.Intersection(start1, end1, start2, end2);
				offset = intersection - origin;
			}

			public void SnapToIntersection(Vertex start1, Vertex end1, Vertex start2, Vertex end2)
			{
				SnapToIntersection (start1.position, end1.position, start2.position, end2.position);
			}

			public void SnapToIntersection(Edge edge1, Edge edge2)
			{
				SnapToIntersection(edge1.start, edge1.end, edge2.start, edge2.end);
			}

			public static Vertex CreateForDynamicMesh(DynamicMesh mesh, Vector2 coord)
			{
				if (mesh.vertices.ContainsKey(coord))
					throw new UnityException("Cannot place a vertex there, one already exists.");
				
				var vertex = new Vertex(mesh._position, coord);
				mesh.vertices.Add(coord, vertex);
				
				return vertex;
			}
			
			public override string ToString ()
			{
				return string.Format ("[Vertex: morphed={0}, enabled={1}]", _parent, enableMesh);
			}
			
			Vertex(Vector2 parent, Vector2 local) 
			{
				this._parent = parent;
				this._key = local;
			}
			
		}
		
		#endregion
		
		#region Data 
		
		[SerializeField][HideInInspector] internal Vector2[] triOrder;
		[SerializeField][HideInInspector] internal Vector2[] colliderOrder;

		[SerializeField][HideInInspector] List<Vector2> _coords;
		[SerializeField] List<Vertex> _verts;

		Dictionary<Vector2, Vertex> vertices;
		
		[SerializeField] Vector2 _position;

		public Vector2 position {
			get {
				return _position;
			}
			protected set {
				_position = value;
			}
		}

		#endregion
		
		#region API

		public Vector2 center {
			get {

				var center = Vector2.zero;
				int numEnabled = 0;
				foreach(var vertex in vertices.Values) {
					if (!vertex.enableMesh)
						continue;
					numEnabled ++;
					center += vertex.position;
				}

				center /= numEnabled;

				return center;
			}
		}

		public void ResetOffsets()
		{
			foreach (var vertex in vertices.Values) 
				vertex.offset = Vector2.zero;
		}

		public void Rotate(float angle)
		{
			foreach (var vertex in vertices.Values) 
				vertex.position = Geometry2D.RotateVector (vertex.key, angle) + Geometry2D.RotateVector (vertex.offset, angle) + position;
		}
		
		public Vertex AddVertex(Vector2 coord)
		{
			return Vertex.CreateForDynamicMesh(this, coord);
		}
		
		public Vertex GetVertex(Vector2 coord)
		{
			return vertices[coord];
		}
		
		public bool HasVertex (Vector2 coord)
		{
			return vertices.ContainsKey(coord);
		}
		
		public void SetTriOrder(params Vector2[] locations)
		{
			if (locations.Length % 3 != 0)
				throw new UnityException("The number of verticies in a tri order must be in a multiple of 3.");

			EnsureVertInEachLocation (locations);

			this.triOrder = locations;
		}

		public void ClearVertices()
		{
			this.vertices = new Dictionary<Vector2, Vertex>();
			this.colliderOrder = null;
			this.triOrder = null;
		}

		public void ClearTriOrder()
		{
			this.triOrder = null;
		}
		
		public void SetColliderOrder(params Vector2[] locations)
		{
			if (locations.Length % 2 != 0)
				throw new UnityException("The number of verticies in a collider order must be even.");
		
			EnsureVertInEachLocation (locations);
		
			this.colliderOrder = locations;
		}
		
		public void ClearColliderOrder()
		{
			this.colliderOrder = null;
		}
		
		#region Convenience Overloads
		
		public Vertex AddVertex(float x, float y)
		{
			return AddVertex (new Vector2(x, y));
		}
		
		public bool HasVertex(float x, float y) 
		{
			return HasVertex (new Vector2(x, y));
		}
		
		public Vertex GetVertex(float x, float y)
		{
			return GetVertex(new Vector2(x, y));
		}
		
		public void SetTriOrder(params float[] axis)
		{
			SetTriOrder(AxesToVector2s(axis));
		}
		
		public void SetColliderOrder(params float[] axis)
		{
			SetColliderOrder(AxesToVector2s(axis));
		}
		
		public void SetTriOrder(params Vertex[] verts)
		{
			SetTriOrder(VertexToVector2s(verts));
		}
		
		public void SetColliderOrder(params Vertex[] vets)
		{
			SetColliderOrder(VertexToVector2s(vets));
		}
		
		#endregion 
		
		#endregion
		
		#region Static Drawers
		
		public static void ApplyColliderPathsAndGeometry(GameObject gameObject, params DynamicMesh[] meshes)
		{
			var collider = gameObject.GetComponent<PolygonCollider2D>() ?? gameObject.AddComponent<PolygonCollider2D>();
			var filter = gameObject.GetComponent<MeshFilter>() ?? gameObject.AddComponent<MeshFilter>();

			var verts = new List<Vector3>();
			var tris = new List<int>();
			var meshList = new List<DynamicMesh> ();
			meshList.AddRange (meshes);

			var paths = CreateColliderPathsAndGeometry(verts, tris, meshes);
			ApplyGeometryToFilters (verts, tris, filter);
			ApplyPathsToPolygonColliders (paths, collider);
		}

		public static void ApplyGeometry(MeshFilter filter, params DynamicMesh[] meshes)
		{
			var verts = new List<Vector3>();
			var tris = new List<int>();
			var meshList = new List<DynamicMesh> ();
			meshList.AddRange (meshes);
			
			CreateGeometry(verts, tris, meshes);
			ApplyGeometryToFilters(verts, tris, filter);
		}
		
		public static List<Vector2[]> CreateColliderPathsAndGeometry(List<Vector3> verts, List<int> tris, IEnumerable<DynamicMesh> meshes) 
		{
			var edges = new Dictionary<Edge, int>();
			BuildVertsTrisEdges(meshes, verts, tris, edges);
			return BuildColliderPaths(edges);
		}

		public static List<Vector2[]> CreateColliderPaths(IEnumerable<DynamicMesh> meshes)
		{
			var edges = new Dictionary<Edge, int>();
			BuildVertsTrisEdges(meshes, null, null, edges);
			
			return BuildColliderPaths(edges);
		}

		public static void CreateGeometry(List<Vector3> verts, List<int> tris, IEnumerable<DynamicMesh> meshes) 
		{
			BuildVertsTrisEdges(meshes, verts, tris, null);
		}

		
		public static void ApplyGeometryToFilters(List<Vector3> verts, List<int> tris, params MeshFilter[] filters) 
		{
			var vertArr = verts.ToArray ();
			var triArr = tris.ToArray ();
			
			ApplyGeometryToFilters (vertArr, triArr, filters);
		}
		
		public static void ApplyGeometryToFilters(Vector3[] verts, int[] tris, params MeshFilter[] filters) 
		{
			foreach (var filter in filters) {
				Mesh mesh = filter.sharedMesh == null ? new Mesh() : filter.sharedMesh;
				
				mesh.Clear();

				mesh.vertices = verts;
				mesh.triangles = tris;

				mesh.Optimize();
				mesh.RecalculateNormals();

				filter.sharedMesh = mesh;
			}
		}
		
		public static void ApplyPathsToPolygonColliders(List<Vector2[]> paths, params PolygonCollider2D[] colliders) 
		{
			foreach(var collider in colliders) {
				collider.pathCount = paths.Count;
				for (int i = 0; i < paths.Count; i++) {
					var path = paths [i];
					collider.SetPath(i, path);
				}
			}
		}

		#endregion
		
		#region Helper

		void EnsureVertInEachLocation (Vector2[] locations) {
			foreach (var location in locations)
				if (!HasVertex (location))
					throw new UnityException ("No vertex at " + location);
		}

		static Vector2[] AxesToVector2s(float[] axis) 
		{
			if (axis.Length % 2 != 0)
				throw new UnityException("Must be an equal number of axis.");
			
			var vector2s = new List<Vector2>();
			for(var i = 0; i < axis.Length - 1; i += 2) {
				var x = axis[i];
				var y = axis[i+1];
				
				var vector2 = new Vector2(x,y);
				vector2s.Add(vector2);
			}
			
			return vector2s.ToArray();
		}

		static Vector2[] VertexToVector2s(Vertex[] vertexes) 
		{
			var vector2s = new List<Vector2>();
			
			foreach(var vert in vertexes)
				vector2s.Add (vert.key);
			
			return vector2s.ToArray();
		}

		static void AddEdge(Edge edge, Dictionary<Edge, int> edges) 
		{
			if (edges.ContainsKey(edge))
				edges[edge]++;
			else
				edges[edge] = 1;
		}
		
		static void BuildVertsTrisEdges(IEnumerable<DynamicMesh> meshes, List<Vector3> verts = null, List<int> tris = null, Dictionary<Edge, int> edges = null)
		{
			if (meshes == null)
				throw new UnityException("Must supply at least one dynamic mesh.");
			
			foreach(var mesh in meshes) {
				
				bool colliderDefined = mesh.colliderOrder != null && mesh.colliderOrder.Length > 0;
				bool trisDefined = mesh.triOrder != null && mesh.triOrder.Length > 0;

				if (colliderDefined && edges != null) {
					for(int i = 0; i < mesh.colliderOrder.Length -1; i += 2) {
						var vert1 = mesh.GetVertex(mesh.colliderOrder[i]);
						var vert2 = mesh.GetVertex(mesh.colliderOrder[i+1]);

						if (!vert1.enableCollider || !vert2.enableCollider)
							continue;

						var edge = new Edge(vert1, vert2);

						AddEdge(edge, edges);
					}
				}
				
				if (((colliderDefined || edges == null) && verts == null && tris == null) || !trisDefined)
					continue;
				
				//If any of the vertices making a face are disabled, the entire face must be skipped
				for (int i = 0; i < mesh.triOrder.Length - 2; i += 3) {
					var vert1 = mesh.GetVertex(mesh.triOrder[i]);
					var vert2 = mesh.GetVertex(mesh.triOrder[i + 1]);
					var vert3 = mesh.GetVertex(mesh.triOrder[i + 2]);

					if (vert1 == null || vert2 == null || vert3 == null)
						continue;

					bool enableMesh = vert1.enableMesh && vert2.enableMesh && vert3.enableMesh;

					if (verts != null && enableMesh) {
						verts.Add (vert1.position);
						verts.Add (vert2.position);
						verts.Add (vert3.position);
					}
					
					if (tris != null && enableMesh) {
						tris.Add (verts.Count-3);
						tris.Add (verts.Count-2);
						tris.Add (verts.Count-1);
					}

					bool enableCollider = vert1.enableCollider && vert2.enableCollider && vert3.enableCollider;
					if (!colliderDefined && edges != null && enableCollider) {
						Edge[] face;
						face = new Edge[] {
							new Edge(vert1, vert2),
							new Edge(vert2, vert3),
							new Edge(vert3, vert1)
						};
						
						foreach(var edge in face)
							AddEdge (edge, edges);
					}
				}
			}
		}
		
		static List<Edge> GetOuterEdges(Dictionary<Edge, int> edges) 
		{
			var outerEdges = new List<Edge>();
			
			foreach(var edge in edges.Keys) {
			//	Debug.Log (edge+" "+edges[edge]);
				var numSharedFaces = edges[edge];
				if (numSharedFaces == 1)
					outerEdges.Add (edge);
			}
			
			return outerEdges;
		}
		
		static List<Vector2[]> BuildColliderPaths(Dictionary<Edge, int> edges) 
		{
			if (edges == null)
				return null;	
			
			var outerEdges = GetOuterEdges(edges);
			
			var paths = new List<List<Edge>>();
			List<Edge> path = null;
			
			while (outerEdges.Count > 0) {
				
				if (path == null) {
					path = new List<Edge>();
					path.Add (outerEdges[0]);
					paths.Add (path);
					
					outerEdges.RemoveAt(0);
				}
				
				bool foundAtLeastOneEdge = false;
				
				int i = 0;
				while (i < outerEdges.Count) {
					var edge = outerEdges [i];
					bool removeEdgeFromOuter = false;
					
					if (edge.end == path[0].start) {
						path.Insert (0, edge);
						removeEdgeFromOuter = true;
					}
					else if (edge.start == path[path.Count - 1].end) {
						path.Add(edge);
						removeEdgeFromOuter = true;
					}
					
					if (removeEdgeFromOuter) {
						foundAtLeastOneEdge = true;
						outerEdges.RemoveAt(i);
					} else
						i++;
				}
				
				//If we didn't find at least one edge, then the remaining outer edges must belong to a different path
				if (!foundAtLeastOneEdge)
					path = null;
				
			}
			
			var cleanedPaths = new List<Vector2[]>();
			
			foreach(var builtPath in paths) {
				var coords = new List<Vector2>();
				
				foreach(var edge in builtPath)
					coords.Add (edge.start);
				
				cleanedPaths.Add (CoordinatesCleaned(coords));
			}
			
			
			return cleanedPaths;
		}
		
		static bool CoordinatesFormLine(Vector2 a, Vector2 b, Vector2 c)
		{
			//If the area of a triangle created from three points is zero, they must be in a line.
			float area = a.x * ( b.y - c.y ) + 
						 b.x * ( c.y - a.y ) + 
						 c.x * ( a.y - b.y );
			
			return Mathf.Approximately(area, 0f);
		}
		
		static Vector2[] CoordinatesCleaned(List<Vector2> coordinates) 
		{
			List<Vector2> coordinatesCleaned = new List<Vector2> ();
			coordinatesCleaned.Add (coordinates [0]);
			
			var lastAddedIndex = 0;
			
			for (int i = 1; i < coordinates.Count; i++) {
				var coordinate = coordinates [i];
				
				Vector2 lastAddedCoordinate = coordinates [lastAddedIndex];
				Vector2 nextCoordinate = (i + 1 >= coordinates.Count) ? coordinates[0] : coordinates [i + 1];
				
				if (!CoordinatesFormLine(lastAddedCoordinate, coordinate, nextCoordinate)) {
					coordinatesCleaned.Add (coordinate);
					lastAddedIndex = i;			
				}
			}
			
			return coordinatesCleaned.ToArray ();
		}
		
		#endregion 

		#region ISerializationCallbackReceiver implementation
		
		public void OnBeforeSerialize()
		{
			if (vertices == null)
				return;

			if (_coords == null)
				_coords = new List<Vector2> ();

			if (_verts == null)
				_verts = new List<Vertex> ();

			_coords.Clear();
			_verts.Clear();
			foreach(var coordVerts in vertices)
			{
				_coords.Add(coordVerts.Key);
				_verts.Add(coordVerts.Value);
			}
		}
		
		public void OnAfterDeserialize()
		{
			if (_coords == null || _verts == null)
				return;

			vertices = new Dictionary<Vector2, Vertex>();
			for (int i = 0; i != Math.Min(_coords.Count,_verts.Count); i++)
				vertices.Add (_coords [i], _verts[i]);
		
		}

		#endregion
		
		#region Constructor 
		
		public DynamicMesh(Vector2 position)
		{
			this._position = position;
			this.vertices = new Dictionary<Vector2, Vertex>();
		}

		public DynamicMesh()
		{
			this._position = Vector2.zero;
			this.vertices = new Dictionary<Vector2, Vertex>();
		}

		#endregion
		
	}

}
