﻿using System.Collections.Generic;
using UnityEngine;
using BossMedia;

namespace SpaceForge {

	//A serializable grid that uses less data than a UnityExtensions.Grid, intended to be used with a limited range of Coords as keys
	public class SimpleGrid<T> {

		[SerializeField] List<T> values;
		[SerializeField] List<Coord> keys;

		public SimpleGrid () {
			values = new List<T> ();
			keys = new List<Coord> ();
		}

		internal virtual bool CoordValid(Coord coord)
		{
			return (System.Math.Abs (coord.x) <= 1 && System.Math.Abs (coord.y) <= 1);
		}

		void ValidateCoord(Coord coord)
		{
			if (!(CoordValid (coord)))
				throw new UnityException(this.GetType().Name + " coord is invalid: "+coord);
		}

		public void Add(Coord coord, T value)
		{
			ValidateCoord (coord);

			if (Has(coord))
				throw new UnityException("Already a value in position "+coord);

			values.Add (value);
			keys.Add (coord);
		}

		public void Remove(Coord coord)
		{
			ValidateCoord (coord);

			int index;
			if (!Has (coord, out index))
				throw new UnityException ("No value in position " + coord);

			values.RemoveAt (index);
			keys.RemoveAt (index);
		}

		public T Get(Coord coord)
		{
			ValidateCoord (coord);

			int index;
			if (Has (coord, out index))
				return values [index];
			else
				throw new UnityException ("No value in position " + coord);
		}

		public void Set(Coord coord, T value) {
			ValidateCoord (coord);

			int index;
			if (Has (coord, out index))
				values [index] = value;

			else {
				values.Add(value);
				keys.Add (coord);
			}
		}

		bool Has(Coord coord, out int i)
		{
			ValidateCoord (coord);

			for(i = 0; i < values.Count; i++)
				if (keys[i] == coord)
					return true;

			return false;
		}

		public bool Has(Coord coord)
		{
			int index;
			return Has (coord, out index);
		}

		public void ForEach(System.Action<Coord, T> action) {
			for(var i = 0; i < values.Count; i ++)
				action(keys[i], values[i]);
		}

		public int Count {
			get {
				return values.Count;
			}
		}

		public T GetRandom(out Coord coord) {
			var randomIndex = Random.Range (0, values.Count);
			
			coord = keys [randomIndex];
			return values [randomIndex];
			
		}
		
		public T GetRandom()
		{
			Coord coord;
			return GetRandom (out coord);
		}

		public T[] ToArray() 
		{
			return values.ToArray ();
		}
	}
}
