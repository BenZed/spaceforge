﻿using System.Collections;
using UnityEngine;
using BossMedia;
using System.Collections.Generic;

namespace SpaceForge.Structural {

	[System.Serializable]
	public class Node : DynamicMesh {

		#region Const && Nested

		internal const int Dimensions = 2; //3 tiles per node;

		public enum Type {
			Corner,
			HorizontalWall,
			VerticalWall,
			DiagonalWall,
		}

		#endregion

		#region Data

		[SerializeField] Coord _place;
		[SerializeField] Coord _origin;
		[SerializeField] Structure _structure;
		[SerializeField] Type _type;

		public Coord place {
			get {
				return _place;
			}
			private set {
				_place = value;
			}
		}

		public Coord origin {
			get {
				return _origin;
			}
			private set {
				_origin = value;
			}
		}

		public Coord coords {
			get {
				return _origin + _place;
			}
			private set {
				_place = value;
			}
		}

		private Coord direction {
			get {
				return type == Type.DiagonalWall ? tile.MissingCorner : place;
			}
		}

		public Structure structure {
			get {
				return _structure;
			}
			private set {
				_structure = value;
			}
		}

		public Tile tile {
			get {
				return structure.GetTile(Tile.CoordFromNodeSpace(origin));
			}
		}

		public Type type {
			get {
				return _type;
			}
			private set {
				_type = value;
			}
		}
	
		#endregion

		#region Constructor

		internal void SetTile(Tile tile, Coord placeCoord)
		{
			this.origin = Tile.CoordInNodeSpace (tile.coords);
			this.place = placeCoord;
			this.structure = tile.structure;
			this.position = GlobalPosition (tile, place);
			this.type = TypeFromPlacement (placeCoord);

			ClearVertices ();
			AddVertex (VertexKey(Coord.nw));
			AddVertex (VertexKey(Coord.ne));
			AddVertex (VertexKey(Coord.sw));
			AddVertex (VertexKey(Coord.se));
			
			if (this.type == Type.DiagonalWall) {
				AddVertex (VertexKey (Coord.n));
				AddVertex (VertexKey (Coord.s));
			}

			SetDefaultTrisAndColliders();

		}

		bool requiresNorthCornerExtraVert = false;
		bool requiresSouthCornerExtraVert = false; 

		void SetDefaultTrisAndColliders()
		{
			SetTriOrder (sw,nw,ne, se,sw,ne);
			SetColliderOrder (sw,nw, nw,ne, ne,se, se,sw);
		}

		void SetDiagonalCaseTrisAndColliders()
		{
			if (!requiresNorthCornerExtraVert && !requiresSouthCornerExtraVert)
				SetDefaultTrisAndColliders ();

			else if (requiresNorthCornerExtraVert && !requiresSouthCornerExtraVert) {
				SetTriOrder (sw,nw,n, 
				             n,se,sw,
				             n,ne,se);
				
				SetColliderOrder (sw,nw, nw,n, n,ne, ne,se, se,sw);


			} else if (!requiresNorthCornerExtraVert && requiresSouthCornerExtraVert) {
				SetTriOrder (s,sw,nw, nw,ne,s, ne,se,s);
				
				SetColliderOrder (s,sw, sw,nw, nw,ne, ne,se, se,s);


			} else if (requiresNorthCornerExtraVert && requiresSouthCornerExtraVert) {
				SetTriOrder (sw,nw,n,
				             n, s, sw,
				             s, n, ne,
				             ne,se,s);
				
				SetColliderOrder (sw,nw, nw,n, n,ne, ne,se, se,s, s,sw);

			}

		}

		internal Node (Tile tile, Coord placeCoord)
		{
			SetTile (tile, placeCoord);
		}

		#endregion

		#region Refresh

		internal void RefreshGeometry()
		{
			ResetOffsets ();

			if (type == Type.Corner)
				return;

			Rotate ( GetAngle() );

			requiresNorthCornerExtraVert = false;
			requiresSouthCornerExtraVert = false;

			HandleIntersection (true);
			HandleIntersection (false);

		}

		internal void DisableColliders()
		{
			nw.enableCollider = false;
			ne.enableCollider = false;
			se.enableCollider = false;
			sw.enableCollider = false;

			nw.enableMesh = false;
			ne.enableMesh = false;
			se.enableMesh = false;
			sw.enableMesh = false;

			if (type == Type.DiagonalWall) {
				s.enableCollider = false;
				s.enableCollider = false;

				n.enableMesh = false;
				n.enableMesh = false;
			}
		}

		float GetAngle() {
			if (type == Type.Corner)
				return 0f;

			Vector2 fromAsVector2 = new Vector2 (direction.x, direction.y);
			float angle = Geometry2D.AbsoluteAngle(fromAsVector2, Vector2.zero);

			return angle;

		}


		Coord GetIntersectionCoord(bool north) {
			
			int resolve = north ? -1 : 1;
			bool isDiagonal = type == Type.DiagonalWall;
			if (isDiagonal)
				resolve *= 2;
			
			return Coord.AroundResolve (direction, resolve);
		}

		void HandleIntersection (bool north) 
		{
			var cornerCoord = GetIntersectionCoord (north);
			var corner = tile.GetNode (cornerCoord);

			if (corner && type == Type.DiagonalWall)
				StickDiagonalWallToCorner (corner, north);

			else if (corner && type != Type.DiagonalWall)
				StickWallToCorner (corner, north);

			else {
				var walls = tile.GetWallsIntersectingCorner(cornerCoord);
			
				//if the number of walls to an intersection doesn't equal exactly 2,
				//it means we've created a wall and are running the gometry refresh before
				//corners have been properly created. if this is the case, we save some time
				//by skipping it.
				if (walls.Length != 2) 
					return; 

				var wall = walls[0] == this ? walls[1] : walls[0]; // Get the wall that isn't this wall
				StickWallToIntersectingWall(wall, cornerCoord, north);
			}

			if (type == Type.DiagonalWall)
				SetDiagonalCaseTrisAndColliders ();

		}

		void StickDiagonalWallToCorner(Node corner, bool north) {

			var facing = north ? direction : - direction;
			var centerVert = north ? n : s;

			//Coords on corner
			var centerCoord = Coord.AroundResolve (-facing, -2);
			var rightCoord = facing;
			var leftCoord = -facing;

			//Snap Center Vert
			var centerCornVert = corner.GetVertexAtKey (centerCoord);
			centerVert.position = centerCornVert.position;

			//Snap Left Vert
			var leftVertS = north ? nw : se;
			var leftVertE = north ? sw : ne;
			var leftWallCoord = Coord.AroundResolve (leftCoord, 1);
			var leftWall = tile.GetNode (leftWallCoord);
			var leftSnapVert = (leftWall) ? corner.GetVertexAtKey (rightCoord) : corner.GetVertexAtKey (leftCoord);
			leftVertS.SnapToIntersection (leftVertS, leftVertE, centerCornVert, leftSnapVert);

			//Snap Right Vert
			var rightVertS = north ? ne : sw;
			var rightVertE = north ? se : nw;
			var rightWallCoord = Coord.AroundResolve (rightCoord, -1);
			var rightWall = tile.GetNode (rightWallCoord);
			var rightSnapVert = (rightWall) ? corner.GetVertexAtKey (leftCoord) : corner.GetVertexAtKey (rightCoord);
			rightVertS.SnapToIntersection (rightVertS, rightVertE, centerCornVert, rightSnapVert);

			bool hasBoth = (leftWall != null && rightWall != null);
			bool hasNeither = (leftWall == null && rightWall == null);
			if (north)
				requiresNorthCornerExtraVert = hasBoth || hasNeither;
			else
				requiresSouthCornerExtraVert = hasBoth || hasNeither;

		}

		void StickWallToCorner(Node corner, bool north) {

			var leftVert = north ? nw : sw;
			var leftCoord = Coord.AroundResolve (-direction, north ? -1 : 1);
			leftVert.position = corner.GetVertexAtKey (leftCoord).position;
		
			var rightVert = north ? ne : se;
			var rightCoord = Coord.AroundResolve (direction, north ? 1 : -1);
			rightVert.position = corner.GetVertexAtKey (rightCoord).position;
		}

		Vector2 GetBisection(Vector2 wall_a, Vector2 wall_b, Vector2 corner_c) {
			
			Vector2 ac = wall_a - corner_c;
			Vector2 bc = wall_b - corner_c;
			Vector2 ba = wall_b - wall_a;
			
			//Bisection theorum, found it on the internet
			var ac_mag = ac.magnitude;
			Vector2 d = wall_a + ba * ac_mag / (ac_mag + bc.magnitude);
			
			Vector2 bisection = (d - corner_c).normalized;
			
			//If parallel
			if (bisection == Vector2.zero)
				bisection = Geometry2D.RotateVector(bc, 90f).normalized;
			
			return bisection;
			
		}

		void StickWallToIntersectingWall(Node wall, Coord cornerCoord, bool north) {

			var corner = GlobalPosition (tile, cornerCoord);
			var bisection = GetBisection (position, wall.position, corner) + corner;
			
			if ((tile.coords == new Coord (5, 7) && direction == Coord.n ) || (tile.coords == new Coord (5, 8) && direction == Coord.s))
				Geometry2D.DrawLine (corner, bisection, Color.red);

			var leftVertS = north ? nw : se;
			var leftVertE = north ? sw : ne;
			leftVertS.position = Geometry2D.Intersection (leftVertS.position, leftVertE.position, corner, bisection);

			var rightVertS = north ? ne : sw;
			var rightVertE = north ? se : nw;
			rightVertS.position = Geometry2D.Intersection (rightVertS.position, rightVertE.position, corner, bisection);
		
			if (type != Type.DiagonalWall)
				return;

			if (north) 
				n.SnapToIntersection (n, s, nw, ne);
			else
				s.SnapToIntersection (s, n, sw, se);
		
		}

		#endregion

		#region Helper 

		Vertex nw {
			get {
				return GetVertexAtKey(Coord.nw);
			}
		} 

		Vertex n {
			get {
				return GetVertexAtKey(Coord.n);
			}
		} 

		Vertex ne {
			get {
				return GetVertexAtKey(Coord.ne);
			}
		} 

		Vertex sw {
			get {
				return GetVertexAtKey(Coord.sw);
			}
		} 

		Vertex s {
			get {
				return GetVertexAtKey(Coord.s);
			}
		} 

		Vertex se {
			get {
				return GetVertexAtKey(Coord.se);
			}
		} 

		Vertex GetVertexAtKey(Coord coord) {
			return GetVertex (VertexKey (coord));
		}

		Vector2 VertexKey(Coord coord)
		{
			float width = structure.style.wallWidth;

			return new Vector2 ((float)coord.x * width, (float)coord.y * width);
		}

		internal static Type TypeFromPlacement(Coord place) 
		{
			if (place == Coord.zero)
				return Type.DiagonalWall;

			else if (place.x != 0 && place.y == 0)
				return Type.VerticalWall;

			else if (place.x == 0 && place.y != 0)
				return Type.HorizontalWall;
		
			return Type.Corner;
		}

		static Vector2 GlobalPosition(Tile tile, Coord placement)
		{
			return tile.position + Vector2.one * 0.5f + new Vector2 ((float)placement.x * 0.5f, (float)placement.y * 0.5f);
		}

		internal Node GetRelativeNode(Coord relativeCoord)
		{
			return structure.GetNode (coords + relativeCoord);
		}

		public static bool PlacementValid(Tile tile, Coord placeCoord, out string error)
		{
			error = "";
			if (!tile) {
				error = "Must place nodes on an existing tile.";
				return false;
			}

			if (tile.isDestroyed) {
				error = "Cannot place nodes on a destroyed tile.";
				return false;
			}

			var type = TypeFromPlacement (placeCoord);
			if (type == Type.DiagonalWall && !tile.IsPartial) {
				error = "Can't place diagonal walls on non-partial tiles.";
				return false;
			}

			if (tile.IsPartial && (placeCoord == Coord.NextAround(tile.MissingCorner) || placeCoord == Coord.PrevAround(tile.MissingCorner))) {
				error = "Can't place node on a missing quadrant.";
				return false;
			}

			var structure_coord = Tile.CoordInNodeSpace (tile.coords) + placeCoord;
			if (tile.structure.GetNode (structure_coord) != null) {
				error = "Can't place node if one already exists.";
				return false;
			}

			return true;
		}

		public override string ToString ()
		{
			return string.Format ("[Node: place={0}, tile={1}]", place, tile);
		}

		public override bool Equals (object obj)
		{
			Node node = obj as Node;
			if (node == null)
				return false;
			else
				return structure == node.structure && (origin + place) == (node.origin + place);
			
		}

		public override int GetHashCode ()
		{
			return structure.GetHashCode () ^ (origin + place).GetHashCode ();
		}

		public static implicit operator bool (Node node)
		{
			return node != null;
		}

		#endregion

	}

}