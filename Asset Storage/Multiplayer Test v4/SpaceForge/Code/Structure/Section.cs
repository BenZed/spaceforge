using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BossMedia;

namespace SpaceForge.Structural {

	[DisallowMultipleComponent]
	[ExecuteInEditMode]
	public sealed class Section : NetworkDummy {

		#region Constructor 
		
		internal static Section Create(Structure structure, Coord sectionCoord)
		{
			var section = new GameObject("Section " + sectionCoord.x + "," + sectionCoord.y, typeof(Section)).GetComponent<Section>();
			section.coord = sectionCoord;
			
			section.transform.parent = structure.transform;
			section.transform.localPosition = Application.isPlaying ? structure.center : Vector3.zero;
			section.transform.localRotation = Quaternion.identity;

			return section;
		}
		
		#endregion

		#region Nested

		[Serializable]
		internal struct Flags 
		{
			public Flag geometry;
			public Flag damageUV;
		}

		#endregion

		[SerializeField]
		internal Flags flags = new Flags 
		{
			geometry = true, 
			damageUV = false,
		};

		internal const int Dimensions = 10;

		/// <summary>
		/// Given a row or column of a tile, returns the row or column that tiles section would be in, in their respective parent structure.
		/// </summary>
		/// <returns>The row or column of a hypothetical section.</returns>
		/// <param name="rowOrColumn">Row or column.</param>
		internal static int AxisInStructure(int axis)
		{
			return axis >= 0
				? axis / Section.Dimensions 
				: ((axis + 1) / Section.Dimensions) - 1;
		}

		[SerializeField]Coord coord;
		public Coord coords{
			get {
				return coord;
			}
		}

		Structure _structure;
		public Structure structure {
			get {
				if (!_structure && (!transform.parent || !(_structure = transform.parent.GetComponent<Structure>())))
					throw new UnityException("Sections should be the children of GameObjects with a structure component.");
				
				return _structure;
			}
			
			private set {
				_structure = value;
			}
		}

		Level _roof;
		public Level roof { 
			get {
				return _roof ?? (_roof = CreateLevel (Level.Type.Roof, -0.5f, structure.gameObject.layer, true));
			}
		}

		Level _walls;
		public Level walls { 
			get {
				return _walls ?? (_walls = CreateLevel (Level.Type.Wall, -0.25f, Game.Constants.Layers.ShipObstacle, true));
			}
		}

		Level _floor;
		public Level floor { 
			get {
				return _floor ?? (_floor = CreateLevel (Level.Type.Floor, 0.25f, Game.Constants.Layers.Ship, true));
			}
		}

		Level _scaffolding;
		public Level scaffolding { 
			get {
				return _scaffolding ?? (_scaffolding = CreateLevel (Level.Type.Scaffolding, 0.5f, Game.Constants.Layers.Ship, false));
			}
		}

		internal void CheckFlags(List<Section> updated, List<Section> removed)
		{
			if (flags.geometry.SwitchOff ()) {
				structure.style.UpdateGeometryAndUV (this);
				structure.flags.center = true;
				if(Application.isPlaying)
					updated.Add (this);
				flags.damageUV = true;
			}

			if (flags.damageUV.SwitchOff ()) {
				structure.style.UpdateDamageUV (this);
				structure.flags.mass = true;
			}

			if (NumTiles > 0)
				return;

			else if (Application.isPlaying)
				removed.Add (this);

			else
				DestroyImmediate (gameObject);
		}

		#region Level Management

		Level CreateLevel(Level.Type type, float zOffset, int layer, bool hasPolygonCollider = false)
		{
			var name = type.ToString ();
			Transform existingLevelTansform = transform.Find (name);
			Level level = null;

			if (existingLevelTansform)
				level = existingLevelTansform.GetComponent<Level> () ?? existingLevelTansform.gameObject.AddComponent<Level> ();

			if (!level)
				level = (new GameObject (name, typeof(Level))).GetComponent<Level> ();

			level.transform.parent = transform;
			level.transform.localRotation = Quaternion.identity;
			level.transform.localPosition = Vector3.zero + Vector3.forward * zOffset;

			level.gameObject.layer = layer;
			level.type = type;
			level.EnsureCollider(hasPolygonCollider);
			level.SetMaterial (structure.style.material);

			return level;
		}

		#endregion

		#region Tile Management

		[Serializable]
		class GridOfTiles : Grid<Tile> {};

		[SerializeField] GridOfTiles tiles;

		public void AddTile(Tile tile) 
		{	
			if (tiles == null)
				tiles = new GridOfTiles();

			if (tile == null)
				throw new UnityException ("Tile attempting to add is null.");
			
			tile.structure = this.structure;
			tiles.Add(tile, Tile.CoordInSection(tile.coords));

			flags.geometry = true;

			structure.flags.center = true;
			structure.flags.mass = true;
			if (structure.events.onTileAdd != null)
				structure.events.onTileAdd.Invoke (tile);
		}

		public void RemoveTile(Tile tile) 
		{
			if (tiles == null)
				throw new UnityException ("Cannot remove tile. No tiles have been added to this section.");
			
			var coordInSection = Tile.CoordInSection(tile.coords);
			
			if (!tiles.Occupied (coordInSection))
				return;

			tiles.Remove (coordInSection);

			flags.geometry = true;

			structure.flags.center = true;
			structure.flags.mass = true;
			if (structure.events.onTileRemove != null)
				structure.events.onTileRemove.Invoke (tile);

		}

		public Tile GetTile(Coord coordInSection) 
		{
			if (tiles == null)
				tiles = new GridOfTiles();
			
			if (!tiles.Occupied(coordInSection))
				return null;
			else
				return tiles.Get(coordInSection);
		}

		public void ForEachTile(Action<Tile> action)
		{
			if (tiles != null)
				tiles.ForEach(tile => action (tile));
		}

		public int NumTiles {
			get {
				return tiles.Count;
			}
		}

		public float mass {
			get {
				float mass = 0f;

				tiles.ForEach(tile => mass += tile.mass);

				return mass;
			}
		}

		#endregion

	}
}