using UnityEngine;
using BossMedia;
using System;
using System.Collections.Generic;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SpaceForge.Structural {

	[DisallowMultipleComponent,
	 ExecuteInEditMode,
	 AddComponentMenu("SpaceForge/Properties/Structure")]
	public sealed class Structure : NetworkDummy , IUnitSerializedEvent {

		#region Nested

		[Serializable]
		internal struct Flags {

			public Flag mass;
			public Flag split;
			public Flag disinegrating;
			public Flag center;

		}

		#endregion

		#region Data

		/// <summary>
		/// The mass of all of the units and attachments on this structure,
		/// </summary>
		float loadedMass = 0f;

		Grid<Section> sections;

		Unit _unit;
		public Unit unit {
			get {
				return _unit;
		    }
		}

		[SerializeField] internal Style style;

		[SerializeField] internal Flags flags;

		#endregion

		#region Unity Callers

		void Awake()
		{
			CheckForUnit ();
			CreateSectionsFromAttached ();
		}
		/*
		void Start()
		{
			Debug.Log ("Structure Start();");
			CreateSectionsFromAttached ();
		}
			*/
		List<Section> updatedSections = new List<Section>();
		List<Section> removedSections = new List<Section>();
		void Update()
		{
			#if UNITY_EDITOR
 			EditorRefresh();
			#endif

			if (sections != null)
				sections.ForEach (section => section.CheckFlags (updatedSections, removedSections));

			if (!Application.isPlaying)
				return;

			if (flags.center.SwitchOff ()) {
				CenterSections ();
				RecalcBounds();
			}

			//SelfMass Flag
			if (flags.mass.SwitchOff() && _unit && _unit.physical) {
				float tileMass = 0;
				sections.ForEach(section => tileMass += section.mass);
				_unit.physical.body.mass = tileMass + loadedMass;
			}

			while (updatedSections.Count > 0) {
				var section = updatedSections[0];
				if (section)
					events.onSectionUpdate.Invoke(section);

				updatedSections.RemoveAt(0);
			}

			while (removedSections.Count > 0) {
				var section = updatedSections[0];
				if (section) {
					events.onSectionRemove.Invoke(section);
					sections.Remove(section.coords);
					Destroy(section.gameObject);
				}

				updatedSections.RemoveAt(0);
			}

		}

		void OnTransformChildrenChanged()
		{
			loadedMass = 0f;
			foreach (Transform child in transform) {
				var body = child.GetComponent<Rigidbody2D>();
				if (body)
					loadedMass += body.mass;
			}

			flags.mass = true;
		}

		#if UNITY_EDITOR

		bool tilesChecked = false;

		void EditorRefresh()
		{
			if (!Application.isEditor || Application.isPlaying)
				return;

			_center = Vector3.zero;
			bool invalid = false;

			if (!style)
				return;

			//Check for invalid sections. In case they were deleted in the editor.
			if (sections != null)
				sections.ForEach(section => {
					if (!section)
						invalid = true;
				});

			if (sections == null || invalid)
				CreateSectionsFromAttached();

			if (NumTiles == 0) {
				CreateTile(new Coord(4,4), true, Tile.Type.Regular);
				CreateTile(new Coord(4,5), true, Tile.Type.Regular);
				CreateTile(new Coord(5,5), true, Tile.Type.Regular);
				CreateTile(new Coord(5,4), true, Tile.Type.Regular);
			}

			if (tilesChecked)
				return;

			tilesChecked = true;
			ForEachTile (tile =>  tile.ResetToStyle());
		}

		#endif

		#endregion

		#region IUnitSerializedEvent implementation

		[Serializable]
		internal class SectionEvent : UnityEvent<Section> {}

		[Serializable]
		internal class TileEvent : UnityEvent<Tile> {}

		[Serializable]
		internal struct Events {
			[SerializeField] public SectionEvent onSectionAdd;
			[SerializeField] public SectionEvent onSectionRemove;
			[SerializeField] public SectionEvent onSectionUpdate;
			[SerializeField] public TileEvent onTileAdd;
			[SerializeField] public TileEvent onTileRemove;
		}

		[SerializeField] internal Events events;

		public void CreateEvents ()
		{
			CheckForUnit ();

			if (!_unit)
				return;

			events.onSectionAdd = new SectionEvent ();
			events.onSectionAdd.AddPersistentListener (_unit, "OnSectionAdd", typeof(Section));

			events.onSectionRemove = new SectionEvent ();
			events.onSectionRemove.AddPersistentListener (_unit, "OnSectionRemove", typeof(Section));

			events.onSectionUpdate = new SectionEvent ();
			events.onSectionUpdate.AddPersistentListener (_unit, "OnSectionUpdate", typeof(Section));

			events.onTileAdd = new TileEvent ();
			events.onTileAdd.AddPersistentListener (_unit, "OnTileAdd", typeof(Tile));
			events.onTileAdd.SetPersistentListenerState (0, UnityEventCallState.EditorAndRuntime);

			events.onTileRemove = new TileEvent ();
			events.onTileRemove.AddPersistentListener (_unit, "OnTileRemove", typeof(Tile));
			events.onTileRemove.SetPersistentListenerState (0, UnityEventCallState.EditorAndRuntime);

		}

		#endregion

		#region Helper

		void CheckForUnit()
		{
			_unit = GetComponent<Unit> ();
		}

		#endregion

		#region Section Management

		[SerializeField, HideInInspector]Vector3 _center = Vector3.zero;
		internal Vector3 center
		{
			get {
				return _center;
			}
		}

		void CalculateCenter()
		{
			_center = Vector3.zero;

			ForEachTile (tile => _center += (Vector3)tile.position);

			_center /= NumTiles;
			_center += new Vector3(0.5f,0.5f,0f); //add the offset of the center of a tile
		}

		/// <summary>
		/// Offsets the parent so that it is in the center of it's structure.
		/// </summary>
		void CenterSections()
		{
			Vector3 oldCenter = center;

			CalculateCenter();

			Vector3 offset = oldCenter - center;

			foreach (Transform child in transform) {
				child.transform.localPosition += offset;

				//I feel like this isn't a good spot for this. Should be in ship,
				//and the offset passed via an event, maybe? I dunno.
				var unit = child.GetComponent<Unit>();
				if (unit && unit.containable && unit.containable.containedBodyActive)
					unit.containable.containedBody.transform.localPosition += offset;

			}

			transform.position = transform.TransformPoint (-offset);
		}

		void CreateSectionsFromAttached()
		{
			if (sections == null)
				sections = new Grid<Section>();
			else
				sections.Clear ();

			var existingSections = GetComponentsInChildren<Section>();

			foreach (var section in existingSections) {
				AddSection(section, section.coords);
				section.flags.geometry = Application.isPlaying;
				section.roof.render.enabled = false;//Application.isPlaying;
			}
		}

		/// <summary>
		/// Gets a section matching the section coordinates in a strucutre.
		/// If no section exists, creates it.
		/// </summary>
		/// <returns>The found or created section.</returns>
		/// <param name="column">Section Column.</param>
		/// <param name="row">Section Row.</param>
		internal Section GetSection(Coord sectionCoords, bool ensure)
		{
			Section section = null;

			if (sections.Occupied(sectionCoords))
				section = sections.Get(sectionCoords);

			else if (ensure) {
				section = Section.Create(this, sectionCoords);
				AddSection(section, sectionCoords);
			}

			return section;
		}

		void AddSection(Section section, Coord sectionCoords)
		{
			sections.Add (section, sectionCoords);
			if (events.onSectionAdd != null && Application.isPlaying)
				events.onSectionAdd.Invoke (section);
		}

		internal void ForEachSection(Action<Section> action) {
			sections.ForEach (action);
		}

		public int NumSections {
			get {
				return sections == null ? 0 : sections.Count;
			}
		}

		#endregion

		#region Tile Management

		/// <summary>
		/// Creates a tile at given structural coordinates.
		/// </summary>
		/// <param name="col">Col.</param>
		/// <param name="row">Row.</param>
		public Tile CreateTile(Coord structureCoords, bool sealable, Tile.Type type, Coord? missingCorner = null)
		{
			string error;

			if (!Tile.PlacementValid(structureCoords, this, out error))
				throw new UnityException("Cannot Create Tile:\n\t"+error);

			Tile tile = new Tile (structureCoords, this, sealable, type, missingCorner);

			style.CreateUVCode(tile);
			GetSection(Tile.CoordOfSection(tile.coords), ensure: true).AddTile (tile);

			tile.RefreshQuadrantData (true);

			return tile;
		}

		public void RemoveTile(Coord coordInStructure)
		{
			var tile = GetTile (coordInStructure);
			if (!tile)
				throw new UnityException ("Cannot remove Tile, none exist at " + coordInStructure);

			Coord oldCoords = tile.coords;

			tile.TransferNodes ();
			tile.section.RemoveTile (tile);

			foreach (var coord in Coord.Around) {
				var adj = GetTile(oldCoords + coord);
				if (adj) {
					adj.RefreshQuadrantData(false);
					adj.RefreshNodeGeometry();
				}
			}
		}

		public Tile GetTile(Coord coordInStructure)
		{
			Section section = GetSection(Tile.CoordOfSection(coordInStructure), ensure: false);
			if (section)
				return section.GetTile(Tile.CoordInSection(coordInStructure));

			else
				return null;
		}

		Tile GetTileAt(Vector2 worldPosition, out Coord structureCoords, out Vector2 localPoint)
		{
			localPoint = transform.InverseTransformPoint (worldPosition) + center;
			int column = Mathf.FloorToInt(localPoint.x);
			int row = Mathf.FloorToInt (localPoint.y);

			structureCoords = new Coord(column, row);

			return GetTile(structureCoords);
		}

		public Tile GetTileAt(Vector2 worldPosition)
		{
			Coord coords;
			Vector2 localPoint;
			return GetTileAt (worldPosition, out coords, out localPoint);
		}

		//Not to toot my own horn, but this is a fucking brilliant piece of code.
		public Tile GetTileClosestTo(Vector3 worldPosition)
		{
			if (NumTiles == 0)
				return null;

			Coord coord;
			Vector2 localPoint;
			var exactTile = GetTileAt (worldPosition, out coord, out localPoint);

			if (exactTile)
				return exactTile;

			Tile closestTile = null;
			int numTilesChecked = 0;
			int searchIncrement = 1;

			while (closestTile == null) {

				var tilesInRing = new List<Tile>();
				bool ringComplete = false;
				int searchCol = -searchIncrement, searchRow = -searchIncrement;

				//we grab tiles in expanding rings around the initial row and col
				while (!ringComplete) {

					var searchTile = GetTile(coord + new Coord(searchCol, searchRow));
					if (searchTile) {
						tilesInRing.Add(searchTile);
						numTilesChecked++;
					}

					//this forces the search pointer to go around the tiles in a ring
					if (searchCol < searchIncrement && searchRow == -searchIncrement)
						searchCol ++;

					else if (searchCol == searchIncrement && searchRow < searchIncrement)
						searchRow ++;

					else if (searchCol > -searchIncrement && searchRow == searchIncrement)
						searchCol --;

					else if (searchCol == -searchIncrement && searchRow > -searchIncrement)
						searchRow --;

					//This condition will only ever happen on the last search.
					if (searchCol == -searchIncrement && searchRow == -searchIncrement)
						ringComplete = true;

				}

				foreach(var ringTile in tilesInRing)
					if (closestTile == null ||
					    ((Vector2)ringTile.position - localPoint).sqrMagnitude < ((Vector2)closestTile.position - localPoint).sqrMagnitude)
						closestTile = ringTile;

				searchIncrement ++;

			}

			return closestTile;
		}

		public Tile GetCentermostTile()
		{
			return GetTileClosestTo(transform.position);
		}

		internal void FindConnectedTiles(Tile tile, List<Tile> connected)
		{
			connected.Add(tile);
			tile.flags.splitCheck = true;

			tile.ForEachAdjacent ((adj, coord) => {

				if (!adj || adj.flags.splitCheck || (adj.isDestroyed && tile.isDestroyed))
					return;

				FindConnectedTiles(adj, connected);

			});
		}

		public Tile[] FindConnectedTiles(Tile tile)
		{
			var connected = new List<Tile>();
			FindConnectedTiles(tile, connected);

			return connected.ToArray();
		}

		public void ForEachTile(Action<Tile> action)
		{
			if (sections != null)
				sections.ForEach (section => section.ForEachTile (action));
		}

		public int NumTiles {
			get {
				int count = 0;
				if (sections == null)
					return count;

				sections.ForEach(section => count += section.NumTiles);

				return count;
			}
		}

		Bounds _bounds = new Bounds(Vector3.zero, Vector3.zero);
		public Bounds bounds {
			get {
				return _bounds;
			}
		}

		void RecalcBounds()
		{
			if (!_unit || !_unit.physical)
				return;

			var strucCols = GetComponentsInChildren<PolygonCollider2D>();
			_bounds = new Bounds (_unit.physical.body.position, Vector3.zero);

			foreach (var col in strucCols)
				if (col.gameObject.layer == Game.Constants.Layers.Ship)
					_bounds.Encapsulate (col.bounds);

		}

		#endregion

		#region Node Management

		static Coord[] cornerOffsets = 	new Coord[]{ Coord.nw, Coord.ne, Coord.se, Coord.sw };
		static Coord[] hWallOffsets = 	new Coord[]{ Coord.w, Coord.e };
		static Coord[] vWallOffsets = 	new Coord[]{ Coord.n, Coord.s };
		static Coord[] dWallOffsets = 	new Coord[]{ Coord.zero };

		Coord[] GetTileNodeOffsets(Coord nodeCoord)
		{
			//If x && y are even, we must be on a corner node.

			bool xEven = nodeCoord.x % 2 == 0;
			bool yEven = nodeCoord.y % 2 == 0;

			if (xEven && yEven)
				return cornerOffsets;

			else if (xEven && !yEven)
				return hWallOffsets;

			else if (!xEven && yEven)
				return vWallOffsets;

			else
				return dWallOffsets;

		}

		internal Node GetNode(Coord nodeCoord)
		{
			foreach (var placeCoord in GetTileNodeOffsets (nodeCoord)) {

				var tileCoord = Tile.CoordFromNodeSpace (nodeCoord - placeCoord);
				var tile = GetTile (tileCoord);

				if (!tile)
					continue;

				Node node = null;

				tile.ForEachNode((coord, otherNode) => {
					if (otherNode.place == placeCoord)
						node = otherNode;
				});

				if (node)
					return node;
			}

			return null;
		}

		internal Tile[] GetTilesSharedByNodePosition(Coord nodeCoord)
		{
			var tiles = new List<Tile> ();
			foreach (var placeCoord in GetTileNodeOffsets (nodeCoord)) {

				var tileCoord = Tile.CoordFromNodeSpace (nodeCoord - placeCoord);
				var tile = GetTile (tileCoord);

				if (tile)
					tiles.Add (tile);
			}
			return tiles.ToArray ();
		}

		/*
		internal Node CreateNode(Coord nodeCoord)
		{
			foreach (var placeCoord in GetTileNodeOffsets (nodeCoord))
			{
				var tileCoord = Tile.CoordFromNodeSpace(nodeCoord - placeCoord);
				var tile = GetTile(tileCoord);

				if (!Tile.Exists (tile))
					continue;

				return tile.CreateNode(placeCoord);
			}

			throw new UnityException ("Can't add node to that location, no tiles available.");
		}

		internal void RemoveNode(Coord nodeCoord)
		{
			var node = GetNode (nodeCoord);
			if (node) {
				var tile = GetTile (node.origin);
				tile.RemoveNode(node.place);
			}

			throw new UnityException ("Can't remove node from that location, none exists.");
		}*/

		#endregion

		#region Damage

//		[Server]
		internal void Damage (Damage dmg)
		{
			var damagedTiles = new List<Tile> ();
			var tile = GetTileClosestTo (dmg.center);
			var damagePoint = tile.section.transform.InverseTransformPoint((Vector3)dmg.center) - (Vector3)tile.position;

			DamageTile (tile, dmg.amount, damagedTiles, damagePoint);

			//Sync Damage
			/*foreach (var damagedTile in damagedTiles) {
				var coord = damagedTile.coords;
				var quads = damagedTile.quadrants;

				float northIntegrity = quads.Has(Coord.n) ? quads.Get (Coord.n).integrity.Current : 0f;
				float eastIntegrity =  quads.Has(Coord.e) ? quads.Get (Coord.e).integrity.Current : 0f;
				float southIntegrity = quads.Has(Coord.s) ? quads.Get (Coord.s).integrity.Current : 0f;
				float westIntegrity =  quads.Has(Coord.w) ? quads.Get (Coord.w).integrity.Current : 0f;

			//	RpcSyncTileIntegrity (coord.x, coord.y, northIntegrity, eastIntegrity, southIntegrity, westIntegrity);
			}*/

		}

		static void DamageTile(Tile tile, float amount, List<Tile> damagedTiles, Vector2? point = null)
		{
			tile.section.flags.damageUV = true;
			damagedTiles.Add (tile);

			var typeData = tile.structure.style.GetTileTypeData (tile.type);
			ReverbDamage (tile, damagedTiles, amount * typeData.damageReverbFactor, ref amount);

			if (tile.isDestroyed)
				return;

			AbsorptionQuadrantDamage (tile, point, ref amount);
			RandomQuadrantDamage (tile, ref amount);
			EvenQuadrantDamage (tile, ref amount);

			//If we still have a remaining amount, then the tile must be destroyed.
			if (amount <= 0f)
				return;

			var secondaryDamageTiles = new List<Tile> ();
			secondaryDamageTiles.Add (tile);
			ReverbDamage (tile, secondaryDamageTiles, amount, ref amount);
			foreach (var secondaryTile in secondaryDamageTiles)
				if (!damagedTiles.Contains (secondaryTile))
					damagedTiles.Add (secondaryTile);

			tile.Destroy();

		}

		static void ReverbDamage(Tile tile, List<Tile> reverberatedTiles, float total, ref float amount)
		{
			var tilesToReverb = new List<Tile> ();
			tile.quadrants.ForEach ((coord, quad) => {
				var adj = tile.GetRelativeTile(coord);
				if (!adj)
					return;

				if (!reverberatedTiles.Contains (adj) && adj.integrity >= tile.integrity)
					tilesToReverb.Add (adj);
			});

			if (tilesToReverb.Count == 0)
				return;

			float totalTiles = (float)tilesToReverb.Count;
			float reverbIndividual = total / totalTiles;
			if (reverbIndividual > amount * 0.5f)
				reverbIndividual = amount * 0.5f;

			if (reverbIndividual < Game.manager.constants.damagable.MinimumReverbDamage)
				return;

			foreach (var rTile in tilesToReverb)
				DamageTile(rTile, reverbIndividual, reverberatedTiles);

			amount -= reverbIndividual * totalTiles;
		}

		static void AbsorptionQuadrantDamage(Tile tile, Vector2? focusPoint, ref float amount)
		{
			if (!focusPoint.HasValue)
				return;

			Coord focusedCoord;
			tile.quadrants.GetRandom (out focusedCoord);
			tile.quadrants.ForEach ((place, quad) => {
				if (quad.integrity.isEmpty)
					return;

				var quadPosition = new Vector2(place.x, place.y);
				var focusQuadPosition = new Vector2(focusedCoord.x, focusedCoord.y);

				if ((quadPosition - focusPoint.Value).sqrMagnitude < (focusQuadPosition - focusPoint.Value).sqrMagnitude)
					focusedCoord = place;

			});

			var typeData = tile.structure.style.GetTileTypeData (tile.type);

			QuadrantDamage (tile.quadrants.Get (focusedCoord), amount * typeData.damageAbsorptionFactor, ref amount);
		}

		static void RandomQuadrantDamage(Tile tile, ref float amount)
		{
			//With remaining amount, damage the quadrants at Random.
			if (amount <= 0f)
				return;

			var typeData = tile.structure.style.GetTileTypeData (tile.type);

			foreach (var quad in tile.quadrants.ToArray()) {
				float randomDamage = UnityEngine.Random.Range(0f, amount) * typeData.damageAbsorptionFactor;
				QuadrantDamage(quad, randomDamage, ref amount);
			}
		}

		static void EvenQuadrantDamage(Tile tile, ref float amount)
		{
			//With remaining amount, damage the quadrants evenly.
			while (amount > 0f) {
				var damagableQuadrants = new List<Tile.QuadrantData>();
				tile.quadrants.ForEach((coord, quad) => {
					if (!quad.integrity.isEmpty)
						damagableQuadrants.Add (quad);
				});

				if (damagableQuadrants.Count == 0)
					return;

				float damage = amount / (float)damagableQuadrants.Count;
				foreach (var quad in tile.quadrants.ToArray())
					QuadrantDamage(quad, damage, ref amount);
			}
		}

		static void QuadrantDamage(Tile.QuadrantData quadant, float damage, ref float amount)
		{
			float overage = quadant.integrity.Deplete(damage);

			amount -= damage;
			amount += overage;
		}

		#endregion
	}

}
