﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace UnityExtensions {

	[CustomPropertyDrawer(typeof(Vital))]
	public class VitalDrawer : PropertyDrawer {

		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			//Validate Property
			var currentProp = property.FindPropertyRelative ("current");
			var maxProp = property.FindPropertyRelative ("max");
						
			if (maxProp.floatValue < 0f) {
				maxProp.floatValue = 0f;
				maxProp.serializedObject.ApplyModifiedProperties ();
			}

			if (currentProp.floatValue > maxProp.floatValue) {
				currentProp.floatValue = maxProp.floatValue;
				currentProp.serializedObject.ApplyModifiedProperties();

			} else if (currentProp.floatValue < 0f) {
				currentProp.floatValue = 0f;
				maxProp.serializedObject.ApplyModifiedProperties ();
			}

			//Draw Property;
			EditorGUI.BeginProperty (position, label, property);
			var indent = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;

			//Draw Vital Label
			position = EditorGUI.PrefixLabel (position, GUIUtility.GetControlID (FocusType.Passive), label);

			//Current Label
			EditorGUI.PrefixLabel (position, GUIUtility.GetControlID (FocusType.Passive), new GUIContent("Current"));

			//Current Property
			var currentRect = new Rect (position.x + 50, position.y, position.width * 0.2f, position.height);
			EditorGUI.PropertyField (currentRect, currentProp, GUIContent.none);

			//Max Label
			var maxLabel = new Rect (currentRect.x + currentRect.width + 5, currentRect.y, 25, currentRect.height);
			EditorGUI.PrefixLabel (maxLabel, GUIUtility.GetControlID (FocusType.Passive), new GUIContent("Max"));

			//Max Property
			var maxRect = new Rect (maxLabel.x + maxLabel.width + 5, position.y, currentRect.width, position.height);
			EditorGUI.PropertyField (maxRect, maxProp, GUIContent.none);

			//EndDraw Property
			EditorGUI.indentLevel = indent;
			EditorGUI.EndProperty ();

		}

	}

}