﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace UnityExtensions {

	[CustomPropertyDrawer(typeof(Timer))]
	public class TimerDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property,  GUIContent label)
		{
			string value = property.FindPropertyRelative ("setTime").floatValue.ToString ("0.00");
			EditorGUI.LabelField (position, label.text, value);
		}
	}
	
}