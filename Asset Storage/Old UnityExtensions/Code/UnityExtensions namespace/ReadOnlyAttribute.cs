﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace UnityExtensions {
	public class ReadOnlyAttribute : PropertyAttribute {}

#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
	public class ReadOnlyDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
		{
			bool supported = true;
			string supportedValue = string.Empty;

			switch (prop.propertyType)
			{
			case SerializedPropertyType.Integer:
				supportedValue = prop.intValue.ToString();
				break;
			case SerializedPropertyType.Boolean:
				supportedValue = prop.boolValue.ToString();
				break;
			case SerializedPropertyType.Float:
				supportedValue = prop.floatValue.ToString("0.00000");
				break;
			case SerializedPropertyType.String:
				supportedValue = prop.stringValue;
				break;
			case SerializedPropertyType.Vector2:
				var v2 = prop.vector2Value;
				supportedValue = "X  "+v2.x+"\tY  "+v2.y;
				break;
			case SerializedPropertyType.Vector3:
				var v3 = prop.vector3Value;
				supportedValue = "X  "+v3.x+"\tY  "+v3.y+"\tZ  "+v3.z;
				break;
			case SerializedPropertyType.Vector4:
				var v4 = prop.vector4Value;
				supportedValue = "X  "+v4.x+"\tY  "+v4.y+"\tZ  "+v4.z+"\tW  "+v4.w;
				break;
			case SerializedPropertyType.ObjectReference:
				try {
					supportedValue = prop.objectReferenceValue.ToString().Replace ("(UnityEngine."," (");
				} catch {
					supportedValue = "(null)";
				}
				break;
			default:
				supported = false;
				break;
			}

			if (supported)
				EditorGUI.LabelField (position, label.text, supportedValue);
			else {
				GUI.enabled = false;
				EditorGUI.PropertyField(position, prop, label, true);
				GUI.enabled = true;
			}
		}
	}
#endif
}
