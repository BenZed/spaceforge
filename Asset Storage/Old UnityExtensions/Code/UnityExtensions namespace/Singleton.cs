﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace UnityExtensions {
	
	public static class Singleton {
		
		struct RegisteredSingleton {
			
			public Func<MonoBehaviour> buildFunction;
			public bool dontDestroyOnLoad;
			
		}
		
		static Dictionary<Type, RegisteredSingleton> registrations = new Dictionary<Type, RegisteredSingleton>();
		static Dictionary<Type, MonoBehaviour> singletons = new Dictionary<Type, MonoBehaviour>();
		
		/// <summary>
		/// Returns a cached instance if one exists, otherwise it finds and creates a cached reference. If no instance exists,
		/// it looks for a registration method to build the singleton. If it cant find a method, or that method doesn't succeed
		/// in constructing an instance, it throws an error.
		/// </summary>
		/// <typeparam name="T">Reference to singleton by type.</typeparam>
		public static T Get<T>() where T : MonoBehaviour {
			
			Type type = typeof(T);
			MonoBehaviour value;
			T instance;
			
			//If the instance exists in the dictionary and hasn't been destroyed, we return it.
			if (singletons.TryGetValue(type, out value) && (instance = value as T))
				return instance;
			
			//Otherwise we try to find it in the scene.
			else if (!(instance = GameObject.FindObjectOfType<T>())) {
				
				//If we can't find it, we try to see if it's been registered so we can run the build method to construct it.
				RegisteredSingleton registration;
				if (registrations.TryGetValue(type, out registration) && registration.buildFunction != null) {
					
					instance = registration.buildFunction() as T;
					
					if (!instance)
						throw new UnityException("Singleton "+type.Name+" was registered with a build method, but it didn't return an instance!");
					
					else if (registration.dontDestroyOnLoad)
						MonoBehaviour.DontDestroyOnLoad(instance);
					
				} else
					throw new UnityException("No instance of "+type.Name+" exists. Place one in the editor.");
				
			}
			
			singletons[type] = instance;
			
			return instance;
			
		}
		
		/// <summary>
		/// Should be called in a singletons Awake() or Start(). Ensures only one instance of a singleton exists. Additionally,
		/// if the singleton's been registered, it will apply DontDestroyOnLoad() as marked.
		/// </summary>
		/// <typeparam name="T">Reference to singleton by Type.</typeparam>
		public static void Ensure<T>() where T : MonoBehaviour {
			
			T instance = Get<T>();
			var type = typeof(T);
			
			RegisteredSingleton registration;
			if (registrations.TryGetValue(type, out registration) && registration.dontDestroyOnLoad)
				MonoBehaviour.DontDestroyOnLoad(instance);
			
			var instances = GameObject.FindObjectsOfType<T>();
			if (instances.Length != 1) {
				
				Debug.LogError ("Only one instance of "+type.Name+" should ever exist. Removing extraneous.");
				
				foreach(var otherInstance in instances)
					if (otherInstance != instance)
						MonoBehaviour.Destroy (otherInstance);
				
			}
		}
		
		/// <summary>
		/// Should be called in a singletons static constructor. If a build method is supplied that returns a singleton of the supplied type, it will use that
		/// method to create that singleton should one not exist when attempting to reference it's static instance variable. Additionally, a singleton can be
		/// registered so that DontDestroyOnLoad() is automatically applied.
		/// </summary>
		/// <typeparam name="T">Reference to singleton by Type.</typeparam>
		public static void Register<T>(Func<MonoBehaviour> buildFunction, bool dontDestroyOnLoad = false) where T : MonoBehaviour {
			
			var type = typeof(T);
			
			if (registrations.ContainsKey(type))
				throw new UnityException("Cannot register the same singleton twice.");
			
			var registration = new RegisteredSingleton {
				buildFunction = buildFunction,
				dontDestroyOnLoad = dontDestroyOnLoad
			};
			
			registrations[type] = registration;
			
		}
	}  
}