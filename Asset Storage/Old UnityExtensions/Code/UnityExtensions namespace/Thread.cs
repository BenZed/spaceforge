using SysThread = System.Threading.Thread;
using SysThreadStart = System.Threading.ThreadStart;

namespace UnityExtensions {
	
	public abstract class Thread {
		
		SysThread thread;
		
		bool start = false;
		bool complete = false;
		bool expired = false;

		/// <summary>
		/// If true, this will OnExecute will fire in the main thread, rather than separate thread.
		/// Used for testing.
		/// </summary>
		protected bool executeInMainThread = false;

		public bool Expired {
			get {
				return (!start && expired && !Running);
			}
		}

		string name;
		public string Name {
			get {
				return name;
			}
		}

		/// <summary>
		/// Puts the thread in the manager queue if it isn't already, and queues it up to fire again if is. If it's already queued to fire again, nothing happens.
		/// </summary>
		public void Queue() {

			start = true;

			if (!Running)
				ThreadManager.instance.AddThread(this);

		}

		public void Abort() {

			thread.Abort ();
			complete = true;

		}
		
		/// <summary>
		/// Update should be called by the ThreadManager. 
		/// </summary>
		public void Update() {

			if (complete) {
				complete = false;
				expired = true;
				
				OnComplete();
				
			}

			if (ThreadManager.instance.numRunningThreads >= ThreadManager.instance.maxRunningThreads)
				return;
			
			if (start && (thread == null || !thread.IsAlive)) {
				start = false;
				expired = false;

				OnStart();

				if (executeInMainThread) {
					OnExecute();
					complete = true;
					return;
				}

				thread = new SysThread( new SysThreadStart(()=>{
					OnExecute();
					complete = true;
				}));
				thread.IsBackground = true;
				thread.Name = name;
				thread.Start ();
				
			}

		}

		public bool Running {

			get {
				return thread != null && thread.IsAlive;
			}

		}
		
		/// <summary>
		/// Called before the thread is executed. Use this to prepare data from the unity API for the thread to use.
		/// </summary>
		protected virtual void OnStart() {}
		
		/// <summary>
		/// Called after the thread is completed. Use this to use output from the thread action on the API. 
		/// </summary>
		protected virtual void OnComplete () {}
		
		/// <summary>
		/// Everything that happens inside the thread. No calls to the Unity API may be made here.
		/// </summary>
		protected abstract void OnExecute ();

		public Thread(string name = "Background Thread") {

			this.name = name;

		}
		
	}
	
}
