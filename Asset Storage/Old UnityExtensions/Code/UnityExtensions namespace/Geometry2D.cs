using UnityEngine;
using System.Collections;

namespace UnityExtensions {
	
	static class Geometry2D {

		/// <summary>
		/// Gets the angle between the two points, regardless of orientation.
		/// </summary>
		/// <returns>The angle.</returns>
		/// <param name="vector2">Vector2.</param>
		/// <param name="other">Other.</param>
		public static float AbsoluteAngle(this Vector2 vector2, Vector2 other) {
			return Mathf.Atan2(vector2.y - other.y, vector2.x - other.x) * Mathf.Rad2Deg;
		}

		public static Vector2 Rotate(this Vector2 v, float degrees) {
			float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
			float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);
			
			float tx = v.x;
			float ty = v.y;
			v.x = (cos * tx) - (sin * ty);
			v.y = (sin * tx) + (cos * ty);

			return v;
		}

		/// <summary>
		/// If this edge and another edge were both infinite lines, where would they intersect?
		/// </summary>
		/// <returns>The line intersection.</returns>
		/// <param name="otherEdge">Other edge.</param>
		public static Vector2 Intersection(Vector2 aStart, Vector2 aEnd, Vector2 bStart, Vector2 bEnd)
		{
			// Get A,B,C of this edge as a line
			float a1 = aEnd.y - aStart.y;
			float b1 = aStart.x - aEnd.x;
			float c1 = a1 * aStart.x + b1 * aStart.y;
			
			// Get A,B,C of other edge as a line
			float a2 = bEnd.y - bStart.y;
			float b2 = bStart.x - bEnd.x;
			float c2 = a2 *bStart.x + b2 * bStart.y;
			
			// Get delta and check if the lines are parallel
			float delta = a1 * b2 - a2 * b1;
			if (delta == 0f)
				throw new UnityException("Supplied Vector2s describe paralell lines.");
			
			// now return the Vector2 intersection point
			return new Vector2( (b2 * c1 - b1 * c2) / delta, (a1 * c2 - a2 * c1) / delta );
		}

		/// <summary>
		/// Gets the average of two angles.
		/// </summary>
		/// <returns>The angle average.</returns>
		/// <param name="angle1">The first angle.</param>
		/// <param name="angle2">The second angle.</param>
		public static float GetAverageAngle(float angle1, float angle2) 
		{

			angle1 = angle1 % 360f;
			angle2 = angle2 % 360f;
			
			float sum = angle1 + angle2;
			if (sum > 360f && sum < 540f)
				sum = sum % 180f;

			return sum / 2f;
		}

		/// <summary>
		/// Angle between vectors from -179 to 180.
		/// </summary>
		/// <returns>From -179 to 180.</returns>
		/// <param name="one">One.</param>
		/// <param name="two">Two.</param>
		public static float AngleBetweenVectors(Vector3 one, Vector3 two)
		{
			float a = one.sqrMagnitude;
			float b = two.sqrMagnitude;

			if (a > 0.0f && b > 0.0f) {
				float angle = Mathf.Acos(Vector2.Dot(one, two) / Mathf.Sqrt(a * b)) * Mathf.Rad2Deg;
				
				Vector3 cross = Vector3.Cross(one, two);
				float sign = cross.y;
				if (sign < 0.0f)
					return -angle;
				else
					return angle;
			}

			return 0.0f;
		}

		/// <summary>
		/// If there was a circle of radius surrounding this point, this returns a point offset by the given angle, on that imaginary circle.
		/// </summary>
		/// <returns>The on circle.</returns>
		/// <param name="origin">Origin.</param>
		/// <param name="radius">Radius.</param>
		/// <param name="angle">Angle.</param>
		public static Vector2 PointOnCircle(this Vector2 origin, float radius, float angle) {

			float x = radius * Mathf.Cos (angle * Mathf.Deg2Rad) + origin.x;
			float y = radius * Mathf.Sin (angle * Mathf.Deg2Rad) + origin.y;

			return new Vector2(x,y);

		}

		/// <summary>
		/// Returns the angular size of a circle of diameter at toPoint, compared to this point
		/// </summary>
		/// <returns>The size.</returns>
		/// <param name="fromPoint">From point.</param>
		/// <param name="toPoint">To point.</param>
		/// <param name="diameter">Diameter.</param>
		public static float AngularSize (this Vector2 fromPoint, Vector2 toPoint, float diameter) {
			float distance = Vector2.Distance(fromPoint, toPoint);
			
			return (diameter / distance) * Mathf.Rad2Deg;
		}

		public static void DrawCross(Vector3 position, Color color, float duration = 10f, bool depthTest = false)
		{
			Debug.DrawLine(position + Vector3.up * 0.1f, position + Vector3.down * 0.1f, color, duration, depthTest);
			Debug.DrawLine(position + Vector3.left * 0.1f, position + Vector3.right * 0.1f, color, duration, depthTest);
		}
		
		public static void DrawLine(Vector3 a, Vector3 b, Color color, float duration = 10f, bool depthTest = false)
		{
			Debug.DrawLine(a, b, color, duration, depthTest);
		}

	}

}