using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace UnityExtensions {
	
	public class ThreadManager : MonoBehaviour {

		#region API

		public static ThreadManager instance {
			get {		
				return Singleton.Get<ThreadManager>();
			}
		}
						
		List<Thread> threads;
		public void AddThread(Thread thread) {
		
			if (threads == null)
				threads = new List<Thread> ();

			if (!threads.Contains(thread))
				threads.Add(thread);
		}

		[Tooltip("Maximum number of simulatenously executing threads. Other threads will be Queued. Performance depends on system, but numbers higher than 10 will typically cause framerate issues.")]
		[Range(1,20)]
		public int maxRunningThreads = 7;

		int _runningThreads;
		public int numRunningThreads {
			get {
				return _runningThreads;
			}
		}

		public int numThreads {
			get {
				return (threads == null) ? 0 : threads.Count;
			}
		}

		#endregion

		#region Unity Callers 

		void Awake() {
		
			Singleton.Ensure<ThreadManager>();
		
		}

		void Update() {

			if (threads == null || threads.Count == 0 || threadsUpdating)
				return;

			StartCoroutine (UpdateThreads ());
	
		}

		#endregion

		#region Main

		bool threadsUpdating = false;
		IEnumerator UpdateThreads() {

			threadsUpdating = true;

			_runningThreads = 0;

			var removeIndexes = new List<int> ();
			for (int index = 0; index < threads.Count; index++) {

				var thread = threads [index];

				if (thread == null || thread.Expired) {
					removeIndexes.Add (index);
					continue;
				}

				thread.Update ();
				_runningThreads += (thread.Running) ? 1 : 0;

				if (ThreadMainChunker.Advocate ())
					yield return new WaitForEndOfFrame ();

			}

			for (int i = removeIndexes.Count - 1; i >= 0; i--)
				threads.RemoveAt (removeIndexes [i]);

			threadsUpdating = false;

			yield break;

		}

		static ThreadManager() {

			Singleton.Register<ThreadManager>(
				buildFunction: ()=>{
					return new GameObject("Thread Manager",typeof(ThreadManager)).GetComponent<ThreadManager>();
				},
				dontDestroyOnLoad: true);

		}

		#endregion

	}

}