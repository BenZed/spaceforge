using UnityEngine;

namespace UnityExtensions {
	
	/// <summary>
	/// Quick class for simple countdown timers.
	/// </summary>
	[System.Serializable]
	public class Timer {
		[SerializeField] float setTime;
		[SerializeField] float expireTime;
		[SerializeField] bool expireTrigger = false;
		
		public bool Expired {
			get {
				return (Time.time >= expireTime);
			}
		}
		
		public float Remaining {
			get {
				if (Expired)
					return 0f;
				else
					return expireTime - Time.time;
				
			}
		}
		
		/// <summary>
		/// Returns the amount the timer was set to.
		/// </summary>
		/// <value>TimeStamp.</value>
		public float SetTime {
			get {
				return setTime;
			}
		}
		
		public Timer (float amount) {
			Set(amount);
		}
		
		public void Set(float amount) {
			setTime = amount;
			expireTrigger = false;
			
			if (setTime <= 0f)
				throw new System.Exception("Timers cannot be set with negative amounts.");
			
			expireTime = Time.time + setTime;
		}
		
		public void Reset(){
			Set (setTime);
		}
		
		/// <summary>
		/// If the timer is expired when this function is called, it will return true and reset the timer.
		/// </summary>
		/// <returns><c>true</c>, if timer was expired and reset, <c>false</c> otherwise.</returns>
		public bool ExpireReset(){
			if (Expired) {
				Reset();
				return true;
			}
			return false;
		}
		
		/// <summary>
		/// If the timer is expired when this function is called, it will return true and set the timer to the given value.
		/// </summary>
		/// <returns><c>true</c>, if timer was expired and set, <c>false</c> otherwise.</returns>
		public bool ExpireSet(float amount){
			if (Expired) {
				Set (amount);
				return true;
			}
			return false;
		}
		
		/// <summary>
		/// If the timer is expired when this function is called, it will return true.
		/// Subsequent calls to this function will return false until the timer is set or reset.
		/// </summary>
		/// <returns><c>true</c>, if timer was expired and triggerd, <c>false</c> otherwise.</returns>
		public bool ExpireTrigger() {
			if (Expired && !expireTrigger) {
				expireTrigger = true;
				return true;
			}
			
			return false;            
			
		}
	}
}