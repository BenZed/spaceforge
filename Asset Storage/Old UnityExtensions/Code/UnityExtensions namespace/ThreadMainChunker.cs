﻿using System.Diagnostics;
using UnityEngine;

namespace UnityExtensions {

	public static class ThreadMainChunker {

		static int minimumFramesPerSecond;
		static float allowableMillisecondsPerFrame;
		static Stopwatch intraFrameStopWatch;
		static float timeStamp = 0f;

		static public bool Advocate(){

			//If if the timestamp doesn't match the current time, it means we haven't checked for a chunk yet this frame.
			if (timeStamp < Time.time) {
				timeStamp = Time.time;
				intraFrameStopWatch.Reset();
				intraFrameStopWatch.Start();

				return false;
			}

			//If the timestamp matches, then we've checked for a chunk at least twice this frame. If our timer blows our calculation budget,
			//we advocate a chunk.
			if (intraFrameStopWatch.ElapsedMilliseconds >= allowableMillisecondsPerFrame) {
				intraFrameStopWatch.Reset();
				intraFrameStopWatch.Start();
				timeStamp = Time.time;

				return true;
			}

			//If we've gotten here, it means we've checked twice at least twice this frame, but no chunk is needed.
			return false;
		}

		static ThreadMainChunker() {

			intraFrameStopWatch = new Stopwatch();
			intraFrameStopWatch.Start();

			minimumFramesPerSecond  = (Application.targetFrameRate < 2) ? 40 : Application.targetFrameRate - 1;
			allowableMillisecondsPerFrame = (1f / (float) minimumFramesPerSecond) * 1000f;
		}
	}
}

