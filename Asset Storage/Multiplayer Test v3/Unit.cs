//Handy thing that shows sever sync updates
//#define GHOST

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
//using UnityExtensions;

namespace SpaceForge {

	[DisallowMultipleComponent]
	[RequireComponent(typeof(NetworkIdentity))]
	public abstract class Unit : NetworkBehaviour {

		#region Component Links

		NetworkIdentity _identity;
		public NetworkIdentity identity {
			get {
				return _identity ?? (_identity = GetComponent<NetworkIdentity>());
			}
		}

		Rigidbody2D _body;
		public Rigidbody2D body {
			get {
				return _body ?? (_body = GetComponent<Rigidbody2D>());
			}
		}

		internal static Game.Constants constants {
			get {
				return Game.manager.constants;
			}
		}
		
		#endregion

		#region Unity Callers

		void OnCollisionStay2D(Collision2D other)
		{
			if (isServer)
				syncBodyManual = true;
		}
		
		void OnCollisionEnter2D(Collision2D collision)
		{
			if (isServer)
				syncBodyManual = true;
		}

		void LateUpdate()
		{
			if (isServer && RequiresSync ())
				SetDirtyBit (SyncBodyDirtyBit);

			if (isServer || !Application.isPlaying)
				return;

			if (syncBodyType != SyncBodyType.Transform)
				ApplySyncBody ();

			else
				ApplySyncTransform ();

		}

		void OnTransformParentChanged()
		{
			InferSyncBodyType ();
			SetParent (transform.parent);
		}

		#endregion
		
		#region Network Behaviour Overrides
		
		public override float GetNetworkSendInterval ()
		{
			if (syncBodyManual)
				return 0f;
			
			return syncFrequency == 0 ? 0f : 1f / (float)syncFrequency;
		}
		
		public override bool OnSerialize (NetworkWriter writer, bool initialState)
		{
			if (!initialState)
				writer.WritePackedUInt32(syncVarDirtyBits);

			if (!initialState && syncVarDirtyBits == 0u)
				return false;

			if (initialState || (syncVarDirtyBits & SyncBodyDirtyBit) != 0u) {
				syncBodyMsg.position = position;
				syncBodyMsg.rotation = rotation;
				syncBodyMsg.velocity = velocity;
				syncBodyMsg.angular = angular;

				syncBodyManual = false;
				syncBodyTimeStamp = Time.time;

				writer.Write(syncBodyMsg);
			}

			if (initialState || (syncVarDirtyBits & ParentDirtyBit) != 0u)
				writer.Write(transform.parent);

			return true;
		}

		protected uint receivedDirtyBits = 0u;
		public override void OnDeserialize (NetworkReader reader, bool initialState)
		{
			receivedDirtyBits = 0u;

			if (!initialState)
				receivedDirtyBits = reader.ReadPackedUInt32();

			if (initialState || (receivedDirtyBits & SyncBodyDirtyBit) != 0u) {
				syncBodyMsg = reader.ReadMessage<SyncBodyMessage> (); 
				syncBodyTimeStamp = Time.time;
			}

			if (initialState || (receivedDirtyBits & ParentDirtyBit) != 0u) {
				try {
					var parent = reader.ReadTransform();
					if (transform.parent != parent)
						transform.parent = parent;
				} catch {
					Debug.LogWarning (name+" couldn't deserialize transform parent, initial state: "+initialState);
				}
			}
		}
		
		public override void OnNetworkDestroy ()
		{
			base.OnNetworkDestroy ();
			Destroy (gameObject);
		}

		public override void OnStartServer()
		{
			setParent.RegisterForServer();
		}

		public override void OnStartClient()
		{
			gameObject.layer = Game.Constants.Layers.Exterior;
			InferSyncBodyType ();
		}
		
		#endregion

		#region Body Sync

		internal float z {
			get {
				return transform.position.z;
			}
			set {
				var pos = transform.position;
				pos.z = value;
				transform.position = pos;
			}
		}

		public Vector2 position {
			get {
				return (Vector2)transform.localPosition;
			}

			set {
				transform.localPosition = new Vector3(value.x, value.y, z);
			}
		}

		public float rotation {
			get {
				return transform.localEulerAngles.z;
			}
			
			set {
				transform.localEulerAngles = Vector3.forward * value;
			}
		}

		Vector2 transformVelocity;
		public Vector2 velocity {
			get {
				if (syncBodyType == SyncBodyType.Body)
					return body.velocity;

				else if (syncBodyType == SyncBodyType.ContainedBody)
					return Property<Containable>().containedBody.velocity;

				else
					return transformVelocity;
			}
			set {
				if (syncBodyType == SyncBodyType.Body)
					body.velocity = value;
				
				else if (syncBodyType == SyncBodyType.ContainedBody)
					Property<Containable>().containedBody.velocity = value;

				else
					transformVelocity = value;
			}
		}

		float transformAngular;
		public float angular {
			get {
				if (syncBodyType == SyncBodyType.Body)
					return body.angularVelocity;
				
				else if (syncBodyType == SyncBodyType.ContainedBody)
					return Property<Containable>().containedBody.angularVelocity;
				
				else
					return transformAngular;

			}
			set {
				if (syncBodyType == SyncBodyType.Body)
					body.angularVelocity = value;
				
				else if (syncBodyType == SyncBodyType.ContainedBody)
					Property<Containable>().containedBody.angularVelocity = value;

				else
					transformAngular = value;
			}
		}

		internal enum SyncBodyType {
			Body,
			Transform,
			ContainedBody
		}

		class SyncBodyMessage : MessageBase {
			
			public Vector2 position;
			public float rotation;
			
			public Vector2 velocity;
			public float angular;
			
		}

		[SerializeField]
		SyncBodyType _syncBodyType = SyncBodyType.Body;
		internal SyncBodyType syncBodyType {
			get {
				return _syncBodyType;
			}
		}
	
		[SerializeField, Range(0, 30)] 
		int syncFrequency = 4;
		
		float syncBodyTimeStamp = 0f;

		bool syncBodyManual = false;
		
		protected bool syncApplyRotationSkip = false;
		
		const float LowPosErrThreshold = 0.15f;
		const float HiPosErrThreshold = 5f;
		
		const float LowRotErrThreshold = 6f;
		const float HiRotErrThreshold = 180f;

		const uint SyncBodyDirtyBit = 1u;

		/// <summary>
		/// On the server, this is the object that is serialized to send, on the client, it's the one received
		/// </summary>
		SyncBodyMessage syncBodyMsg = new SyncBodyMessage();

		[Server]
		bool RequiresSync() {
			
			if (syncBodyManual) 
				return true;
			
			if (syncFrequency <= 0)
				return false;
			
			if ((position - syncBodyMsg.position).sqrMagnitude > LowPosErrThreshold * LowPosErrThreshold)
				return true;
			
			if (Mathf.Abs(rotation - syncBodyMsg.rotation) > LowRotErrThreshold)
				return true;

			return false;
		}

		[Client]
		void ApplySyncBody()
		{
			float syncDelta = Time.time - syncBodyTimeStamp;
					
			if (syncDelta <= Time.deltaTime)
				return;
		
			float syncInterval = syncFrequency == 0 ? syncDelta : 1f / (float)syncFrequency;
			float lerpDelta = Time.deltaTime / syncInterval;
			
			var sbm = syncBodyMsg;
			
			Vector2 expectedPos = sbm.position + (sbm.velocity * syncDelta);
			float posErr = (position - expectedPos).magnitude;
			
			float expectedRot = Mathf.LerpAngle(sbm.rotation, sbm.rotation + (sbm.angular * syncDelta), 1f);
			float rotationErr = Mathf.Abs(rotation - expectedRot);
			
			if (posErr > LowPosErrThreshold && posErr <= HiPosErrThreshold) {
				
				Vector2 projectedPos = sbm.position + sbm.velocity * syncInterval;
				position = Vector2.Lerp(position, projectedPos, lerpDelta);
				velocity = Vector2.Lerp(velocity, sbm.velocity, lerpDelta);
				
			} else if (posErr > HiPosErrThreshold) {
				
				position = expectedPos;
				velocity = sbm.velocity;
				
			}
			
			if (rotationErr > LowRotErrThreshold && rotationErr < HiRotErrThreshold && !syncApplyRotationSkip) {
				
				float projectedRot = Mathf.LerpAngle(sbm.rotation, sbm.rotation + sbm.angular * syncInterval, 1f);
				rotation = Mathf.LerpAngle(rotation, projectedRot, lerpDelta);
				angular = Mathf.LerpAngle(angular, sbm.angular, lerpDelta);
				
			} else if (rotationErr > HiRotErrThreshold && !syncApplyRotationSkip) {
				
				rotation = expectedRot;
				angular = sbm.angular;
				
			}
		}

		void InferSyncBodyType()
		{
			//If we've gotten here, we'll do a manual sync just to be safe.
			syncBodyManual = true;

			//Set Z based on parent
			z = transform.parent ? transform.parent.position.z + 0.1f : 0f;

			//Disable the physical property if we're parented to something.
			Property<Physical>(p => p.enabled = !parent);

			var containable = Property<Containable> ();
			var usingContainedBody = containable && containable.containedBody && containable.containedBody.gameObject.activeSelf;

			if (!body) 
				_syncBodyType = SyncBodyType.Transform;

			else if (!parent && body)
				_syncBodyType = SyncBodyType.Body;
			
			else if (parent && usingContainedBody)
				_syncBodyType = SyncBodyType.ContainedBody;
			
			else if (parent && !usingContainedBody)
				_syncBodyType = SyncBodyType.Transform;	

		}

			#if UNITY_EDITOR
		static Flag syncTransformWarning = false;
			#endif
		void ApplySyncTransform()
		{
			#if UNITY_EDITOR
			if (syncTransformWarning.SwitchOn())
				Debug.LogWarning("SyncBodyMode.Transform smoothing not yet complete.");
			#endif

			position = syncBodyMsg.position;
			rotation = syncBodyMsg.rotation;
		//	position += transformVelocity * Time.deltaTime;
		//	rotation += transformAngular * Time.deltaTime;
		}

		#endregion

		#region Parent

		class SetParentMessage : MessageBase {

			public GameObject childGameObject;
			public Transform parent;

			public SetParentMessage(){}

			public SetParentMessage(GameObject childGameObject, Transform parent)
			{
				this.childGameObject = childGameObject;
				this.parent = parent;
			}
		}

		static MessageHandler setParent;

		const uint ParentDirtyBit = 2u;

		public Transform parent {
			get {
				return transform.parent;
			}
			set {
				transform.parent = value;
			}
		}

		void SetParent(Transform parent)
		{
			//This ensures that this function gets called weather the transform.parent was set or the
			//message was received from client or server.
			if (transform.parent != parent) {
				transform.parent = parent;
				return;
			}

			if (!isServer) {
				var msg = new SetParentMessage(gameObject, parent);
				setParent.Send(msg);

				return;
			}

			SetDirtyBit(ParentDirtyBit);
		}

		[Server]
		static void SetParent(NetworkMessage msg)
		{
			var read = msg.ReadMessage<SetParentMessage>();
			var unit = read.childGameObject.GetComponent<Unit>();

			unit.SetParent(read.parent);
		}

		#endregion

		#region Convenience
		
		Collider2D[] colliders;
		internal void SetCollidersEnabled(bool value)
		{
			if (colliders == null)
				colliders = GetColliders();
			
			foreach(var collider in colliders)
				collider.enabled = value;
		}
		
		Collider2D[] GetColliders()
		{
			var colliderList = new List<Collider2D>();
			colliderList.AddRange(GetComponents<Collider2D>());
			colliderList.AddRange(GetComponentsInChildren<Collider2D>());
			
			return colliderList.ToArray();
		}

		public static Unit[] Find(Vector2 position, float radius)
		{
			return Find<Unit>(position, radius);
		}

		public static T[] Find<T>(Vector2 position, float radius) where T : Unit 
		{
			Collider2D[] colliders = Physics2D.OverlapCircleAll(position, radius);
			var units = new List<T>();
			
			foreach (var collider in colliders) {
				var unit = collider.GetComponent<T>();
				if (unit)
					units.Add(unit);
			}
			
			return units.ToArray();
		}

		Dictionary<Type, Property> cachedProperties = new Dictionary<Type, Property>();
		public T Property<T> (Action<T> action = null) where T : Property {
		
			var type = typeof(T);
			T cached = null;
			bool hasAction = action != null;

			bool contains = cachedProperties.ContainsKey (type);
			if (contains)
				cached = cachedProperties[type] as T;
			
			//if the dictionary has the component and the component hasn't since been removed, we're all done!
			if (contains && cached && hasAction)
				action (cached);
			
			if (contains && cached)
				return cached;
			
			T property = GetComponent<T>();

			if (property && hasAction)
				action(property);

			//If the component exists, we cache it into the dictionary
			if (property)
				cachedProperties [type] = property;

			//if the component doesn't exist, but is in the dictionary, it must have been previously added and since destroyed
			else if (contains)
				cachedProperties.Remove (type);

			return property;
		}

		#endregion

		#region Static Constructor

		static Unit()
		{
			setParent = new MessageHandler(SetParent, Game.Messages.SetParent);
		}

		#endregion

	}
}