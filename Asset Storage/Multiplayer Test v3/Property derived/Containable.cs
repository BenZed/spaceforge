﻿using UnityEngine;
using UnityEngine.Networking;
using System;

namespace SpaceForge {

	[DisallowMultipleComponent]
	[AddComponentMenu("SpaceForge/Properties/Containable")]
	[NetworkSettings(channel = 0, sendInterval = 0.325f)]
	public class Containable : Property {

		#region Links

		Rigidbody2D _containedBody;
		internal Rigidbody2D containedBody {
			get {
				return _containedBody;
			}
		}

		#endregion

		#region Inspector

		[SerializeField, Range(1,100)] int _size;
		public int size {
			get {
				return _size;
			}
		}

		#endregion

		Action<IContainer> onPickup;
		Action onDrop;

		#region Unity Callers
		
		protected virtual void Awake()
		{
			onPickup = CreateRuntimeCaller<Action<IContainer>>(unit, "OnPickup");
			onDrop = CreateRuntimeCaller<Action>(unit, "OnDrop");
		}

		void OnTransformParentChanged()
		{
			if (unit.parent && onPickup != null)
				onPickup (unit.parent.GetComponent<IContainer> ());

			else if (!unit.parent && onDrop != null)
				onDrop ();
		}

		void OnValidate()
		{
			if (_size < 1)
				_size = 1;
		}

		#endregion

		IContainer _container;
		public IContainer container {
			get {
				if (!unit.parent)
					return null;

				return _container ?? (_container = unit.parent.GetComponent<IContainer>());
			}
		}

		#region ContainedBody

		internal void CreateContainedBody()
		{
			_containedBody = new GameObject (name + " (Interior)", typeof(Rigidbody2D)).GetComponent<Rigidbody2D> ();
			_containedBody.gameObject.SetActive (false);
			_containedBody.transform.parent = transform;
		}

		internal void EnableContainedBody(IContainer container)
		{

		}

		internal void DisableContainedBody()
		{

		}

		#endregion

	}

}
