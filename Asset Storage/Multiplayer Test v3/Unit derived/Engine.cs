using UnityEngine;
using UnityExtensions;
using UnityEngine.Networking;
using System.Collections;

namespace SpaceForge {

	[AddComponentMenu("SpaceForge/Units/Jetpack")]
	[RequireComponent(typeof(Equippable))]
	public class Engine : Unit {


		#region Inspector

		[SerializeField, Range(0.1f,0.5f)] float thrust = 0.225f;
		[SerializeField] Vital _fuel;
		public Vital fuel {
			get {
				return _fuel;
			}
		}

		const uint FuelDirtyBit = 4u;

		#endregion

		#region Links

		Equippable _equippable;
		public Equippable equippable {
			get {
				return _equippable ?? (_equippable = GetComponent<Equippable>());
			}			 
		}

		#endregion

		#region Property Callers

		void OnUse() 
		{
			Sapien sapien = equippable.sapien;
			Vector2 move = sapien.movement;

			Vector2 force = sapien.transform.up * move.x + sapien.transform.right * -move.y;

			//No speed bonus for also moving diagonally
			if (force.sqrMagnitude > 1f)
				force = force.normalized;

			if (!fuel.isEmpty && isServer)
				SetDirtyBit (FuelDirtyBit);

			if (!fuel.isEmpty) {
				fuel.Deplete (force.magnitude * thrust * Time.fixedDeltaTime);
				sapien.body.AddForce (force * thrust);
			}
		}

		#endregion

		#region Network Behaviour Overrides
		
		public override bool OnSerialize (NetworkWriter writer, bool initialState)
		{
			if (!base.OnSerialize(writer, initialState))
				return false;
			
			if (initialState || (syncVarDirtyBits & FuelDirtyBit) != 0u)
				writer.Write(_fuel.Current);
			
			return true;
		}
		
		public override void OnDeserialize (NetworkReader reader, bool initialState)
		{
			base.OnDeserialize(reader, initialState);
			
			if (initialState || (receivedDirtyBits & FuelDirtyBit) != 0u) 
				_fuel.Set(reader.ReadSingle());
		}
		
		#endregion

	}
}