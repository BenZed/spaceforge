using UnityEngine;
using UnityExtensions;
using UnityEngine.Networking;
using System;
using System.Collections.Generic;

namespace SpaceForge {

	[AddComponentMenu("SpaceForge/Properties/Sapien")]
	[RequireComponent(typeof(DistanceJoint2D))]
	public class Sapien : Unit {

		const float UncontrollableAngularVelocityThreshold = 225f;

		#region Links

		Player _player;
		public Player player {
			get {
				if (_player && _player.transform.parent != transform)
					_player = null;

				return _player ?? (_player = GetComponentInChildren<Player>());					
			}
		}


		#endregion

		#region Links

		SpriteRenderer spriter;
		internal Sprite sprite {
			get {
				return spriter.sprite;
			}
			set {
				spriter.sprite = value;
			}
		}

		Head _head;
		public Head head {
			get {
				return _head ?? (_head = GetComponentInChildren<Head>());
			}
		}
		
		Hand _rightHand;
		public Hand rightHand {
			get {
				return _rightHand ?? (_rightHand = GetHand ());
			}
		}
		
		Hand _leftHand;
		public Hand leftHand {
			get {
				return _leftHand ?? (_leftHand = GetHand (left: true));
			}
		}

		#endregion

		#region Property Callers

		bool _isDead = false;
		public bool isDead {
			get {
				return _isDead;
			}
		}

		static Color DeathColor = new Color (0.45f, 0.45f, 0.525f, 0.95f);

		void OnKill(Damage overkill)
		{
			Debug.Log (name+" was killed.");
			_isDead = true;

		}

		#endregion

		#region Unity Callers

		void FixedUpdate()
		{
			Property<Physical>(ApplyMoveFacing);
		}

		void Update()
		{
			Property<Biological> (b => {

				if (b.atmosphere != null)
					return;

				var spaceSuit = Inventory<SpaceSuit>();
				if (spaceSuit && !spaceSuit.equipped)
					spaceSuit.Equip();

			});
		}

		protected override void Awake()
		{
			base.Awake ();
			grabJoint = GetComponent<DistanceJoint2D> ();
			spriter = GetComponent<SpriteRenderer> ();
		}

		void OnTransformChildrenChanged()
		{
			inventory = GetComponentsInChildren<Equippable> ();
		}

		#endregion

		#region Equipment and Inventory

		Equippable[] inventory = new Equippable[0];

		Equippable Inventory<T>(Predicate<Equippable> equipValidate, Predicate<T> unitValidate) where T : Unit {

			foreach (var equippable in inventory) {
				T unit = equippable.unit as T;
				
				if (!unit)
					continue;

				if (equipValidate != null && !equipValidate(equippable))
					continue;

				if (unitValidate != null && !unitValidate(unit))
					continue;

				return equippable;
			}

			return null;

		}

		internal Equippable Inventory<T>(Predicate<T> validate = null) where T : Unit {
			return Inventory<T> (null, validate);
		}

		public Equippable Worn<T>(Predicate<T> validate = null) where T : Unit {
			return Inventory<T> (e => e.equipped && e.equipType == Equippable.EquipType.Wearable, validate);
		}

		public Equippable Equipped<T>(Predicate<T> validate = null) where T : Unit {
			return Inventory<T> (e => e.equipped, validate);
		}

		internal void EachInventory(Predicate<Equippable> action)
		{
			foreach (var equippable in inventory) {
				if (action (equippable))
					break;
			}
		}

		internal void EachInventory(Action<Equippable> action)
		{
			foreach (var equippable in inventory)
				action (equippable);
		}

		#endregion

		#region Orders

		const uint MoveDirtyBit = 4u;
		const uint AimDirtyBit = 8u;
		const uint FaceDirtyBit = 16u;
		const uint GrabDirtyBit = 32u;

		Vector2 _movement = Vector2.zero;
		public Vector2 movement {
			get {
				return _movement;
			}
		}

		Vector2 _aim = Vector2.zero;
		internal Vector2 aim {
			get {
				return _aim;
			}
		}
		
		float _facing = 0f;
		internal float facing {
			get {
				return _facing;
			}
		}

		bool _grab = false;
		internal bool grab {
			get {
				return _grab;
			}
		}

		DistanceJoint2D grabJoint;
		internal DistanceJoint2D GrabJoint{
			get {
				return grabJoint;
			}
		}

		internal void Move(Vector2 input)
		{
			input.x = Mathf.Clamp(input.x, -1f, 1f);
			input.y = Mathf.Clamp(input.y, -1f, 1f);

			_movement = input;
			SetDirtyBit (MoveDirtyBit);
		}

		internal void Aim(Vector2 aim)
		{			
			this._aim = transform.InverseTransformPoint(aim);
			SetDirtyBit (AimDirtyBit);

		}

		internal void Face(Vector2 face)
		{
			Vector2 pos = transform.position;
			this._facing = pos.AbsoluteAngle(face);
			SetDirtyBit (FaceDirtyBit);

		}

		internal void Grab(bool grab)
		{
			_grab = grab;
			SetDirtyBit (GrabDirtyBit);

		}

		void ApplyMoveFacing(Physical physical)
		{
			Rigidbody2D body = physical.body;

			bool rotationControllable = Mathf.Abs (body.angularVelocity) < UncontrollableAngularVelocityThreshold;

			if (_isDead || !player) {
				syncApplyRotationSkip = false;
				return;

			} else
				syncApplyRotationSkip = rotationControllable;

			if (rotationControllable) {
				float angle 		 = Mathf.LerpAngle(_facing, _facing + body.angularVelocity, 1f);
				body.angularVelocity = Mathf.Lerp(body.angularVelocity, 0f, Time.fixedDeltaTime);
				body.rotation 		 = Mathf.LerpAngle(body.rotation, angle, Time.fixedDeltaTime * 10f);
			}
	
			if (parent)
				return;

			Equippable jetpack = Worn<Engine> ();

			if (jetpack && ((Engine)jetpack.unit).fuel.isEmpty && movement == Vector2.zero) {
				jetpack.UnEquip ();
				jetpack = null;
			}

			if (!jetpack)
				jetpack = Inventory<Engine> (jet => !jet.fuel.isEmpty);

			if (jetpack && !jetpack.equipped)
				jetpack.Equip ();

			if (!jetpack || !jetpack.equipped)
				return;

			jetpack.Use ();
		}

		#endregion

		#region Network Behaviour Overrides

		public override bool OnSerialize (NetworkWriter writer, bool initialState)
		{
			if (!base.OnSerialize (writer, initialState))
				return false;

			if (initialState || (syncVarDirtyBits & MoveDirtyBit) != 0u)
				writer.Write(_movement);

			if (initialState || (syncVarDirtyBits & AimDirtyBit) != 0u)
				writer.Write(_aim);

			if (initialState || (syncVarDirtyBits & FaceDirtyBit) != 0u)
				writer.Write(_facing);

			if (initialState || (syncVarDirtyBits & GrabDirtyBit) != 0u)
				writer.Write(_grab);

			return true;
		}

		public override void OnDeserialize (NetworkReader reader, bool initialState)
		{
			base.OnDeserialize (reader, initialState);

			if (initialState || (receivedDirtyBits & MoveDirtyBit) != 0u)
				_movement = reader.ReadVector2 ();

			if (initialState || (receivedDirtyBits & AimDirtyBit) != 0u)
				_aim = reader.ReadVector2 ();

			if (initialState || (receivedDirtyBits & FaceDirtyBit) != 0u)
				_facing = reader.ReadSingle ();

			if (initialState || (receivedDirtyBits & GrabDirtyBit) != 0u)
				_grab = reader.ReadBoolean ();
		}

		#endregion

		#region Helper

		Hand GetHand(bool left = false)
		{
			var hands = GetComponentsInChildren<Hand> ();
			foreach (var hand in hands) {
				if (hand.isLeft == left)
					return hand;
			}
			Debug.Log ("Couldn't find " + (left ? "Left" : "Right") + " hand.");
			return null;
		}

		#endregion

	}
}