﻿using UnityEngine;
using UnityExtensions;
using UnityEngine.Networking;
using System;

namespace SpaceForge {

	[AddComponentMenu("SpaceForge/Units/SpaceSuit")]
	public class SpaceSuit : Unit {

		#region Nested

		[Serializable]
		public class Sprites {

			[SerializeField] internal Sprite helmet;

			[SerializeField] internal Sprite suit;

			[SerializeField] internal Sprite rightGlove;
			[SerializeField] internal Sprite rightGloveGrab;

			[SerializeField] internal Sprite leftGlove;
			[SerializeField] internal Sprite leftGloveGrab;

			internal Sprite rightHand;
			internal Sprite rightHandGrab;

			internal Sprite body;
			
			internal Sprite leftHand;
			internal Sprite leftHandGrab;

		}

		#endregion

		#region Inspector

		[SerializeField] Sprites sprites;

		[SerializeField] Atmosphere _airTank;
		public Atmosphere airTank {
			get {
				return _airTank;
			}
		}

		SpriteRenderer helmet;

		Equippable _equippable;
		public Equippable equippable {
			get {
				return _equippable ?? (_equippable = GetComponent<Equippable>());
			}
		}

		#endregion

		#region Property Callers

		void OnEquip()
		{
			if (!helmet)
				CreateHelmet ();

			Sapien sapien = equippable.sapien;
			Transform head = equippable.sapien.head.transform;

			helmet.gameObject.SetActive (true);
			helmet.transform.parent = head;
			helmet.transform.localPosition = new Vector3 (0f, 0f, head.position.z - 0.1f);
			helmet.transform.localEulerAngles = Vector3.zero;

			sprites.rightHand = sapien.rightHand.closedSprite;
			sprites.rightHandGrab = sapien.rightHand.grabSprite;
			
			sprites.leftHand = sapien.leftHand.closedSprite;
			sprites.leftHandGrab = sapien.leftHand.grabSprite;

			sprites.body = sapien.sprite;
			sapien.sprite = sprites.suit;

			sapien.rightHand.grabSprite = sprites.rightGloveGrab;
			sapien.rightHand.closedSprite = sprites.rightGlove;

			sapien.leftHand.grabSprite = sprites.leftGloveGrab;
			sapien.leftHand.closedSprite = sprites.leftGlove;
			sapien.Property<Biological> (b => b.atmosphere = airTank);

			z = sapien.z - 0.1f;
		}

		void OnUnequip()
		{
			if (!helmet)
				CreateHelmet ();

			helmet.gameObject.SetActive (false);

			Sapien sapien = equippable.sapien;

			sapien.rightHand.SetDefaultSprite ();
			sapien.leftHand.SetDefaultSprite ();

			sapien.sprite = sprites.body;

			sapien.leftHand.grabSprite = sprites.leftHandGrab;
			sapien.leftHand.closedSprite = sprites.leftHand;

			sapien.Property<Biological> (b => b.atmosphere = null);

		}

		#endregion

		#region Helper

		void CreateHelmet()
		{
			helmet = new GameObject("Helmet", typeof(SpriteRenderer)).GetComponent<SpriteRenderer>();
			helmet.sprite = sprites.helmet;
			helmet.material = equippable.spriter.material;
			helmet.transform.parent = transform;
		}

		#endregion

	}
}