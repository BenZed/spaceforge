using UnityEngine;

namespace SpaceForge {

	[RequireComponent(typeof(SpriteRenderer))]
	[DisallowMultipleComponent]
	public abstract class BodyPart : MonoBehaviour {

		#region Links

		Sapien _sapien;
		protected Sapien sapien {
			get {
				return _sapien;
			}
		}

		CircleCollider2D _circle;
		protected CircleCollider2D circle {
			get {
				return _circle;
			}
		}

		SpriteRenderer spriter;
		internal Sprite sprite {
			get {
				return spriter.sprite;
			}
			set {
				spriter.sprite = value;
			}
		}

		#endregion 

		#region Limits 

		[SerializeField, Range(0f, 1f)]
		float _limitRadius = 0f;

		protected float limitRadius {
			get {
				return _limitRadius;
			}
		}

		[SerializeField, Range(0f, 360f)]
		float _limitRotation = 0f;
		
		protected float limitRotation {
			get {
				return _limitRotation;
			}
		}

		Vector2 _restingPos = Vector2.zero;
		protected Vector2 restingPos {
			get {
				return _restingPos;
			}
		}
		
		float _restingRot = 0f;
		protected float restingRot {
			get {
				return _restingRot;
			}
		}

		float z;

		protected Vector2 targetPosition;
		protected float targetRotation;

		#endregion

		#region Unity Callers
		 
		void Awake()
		{
			spriter = GetComponent<SpriteRenderer>();
			_sapien = GetComponentInParent<Sapien>();
			_circle = GetComponentInParent<CircleCollider2D> ();
			_restingPos = transform.localPosition;
			_restingRot = transform.localRotation.eulerAngles.z;

			z = transform.localPosition.z;

			if (!_sapien)
				throw new UnityException("Every bodypart must be parented to a sapien unit!");
		}
	
		void Update()
		{
			if (sapien.isDead) {
				enabled = false;
				return;
			}

			MoveTurn();
			LimitMoveTurn();
			Lerp ();
		}

		#endregion

		#region Abstract 

		protected abstract void MoveTurn();

		protected virtual void LimitMoveTurn()
		{
			Vector2 checkPos = targetPosition - restingPos;
			if (checkPos.sqrMagnitude > limitRadius * limitRadius)
				targetPosition = (checkPos.normalized * limitRadius) + restingPos;

			if (Mathf.Abs (targetRotation) > limitRotation)
				targetRotation = limitRotation * Mathf.Sign (targetRotation);
		}

		void Lerp()
		{
			Quaternion targetQuat = Quaternion.Euler (0f, 0f, targetRotation);
			Vector3 targetVec3 = new Vector3(targetPosition.x, targetPosition.y, z);

			transform.localPosition = Vector3.Lerp( transform.localPosition, targetVec3, Time.deltaTime * 5f);
			transform.localRotation = Quaternion.Slerp (transform.localRotation, targetQuat, Time.deltaTime * 5f);
		}

		#endregion

	}
}