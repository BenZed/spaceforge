﻿using UnityEngine;
using UnityEngine.Events;

namespace SpaceForge {

	[DisallowMultipleComponent]
	internal class Interpolator : MonoBehaviour {

		const float Threshold = 0.1f;

		#region Inspector

		[SerializeField]
		internal bool applyPosition = true;

		[SerializeField]
		internal bool applyRotation = true;

		[SerializeField]
		public bool applyScale = false;

		[Range(1f,100f), SerializeField]
		internal float speed = 8f;

		[SerializeField] 
		public Transform target;

		[SerializeField]
		internal UnityEvent onComplete;

		#endregion

		#region API

		internal bool atPosition {
			get  {
				if (!target)
					return false;

				var delta = transform.position - target.transform.position;

				return delta.sqrMagnitude < Threshold * Threshold;
			}
		}

		internal bool atRotation {
			get {
				if (!target)
					return false;

				return Quaternion.Angle(transform.rotation, target.transform.rotation) < Threshold;
			}
		}
		
		internal bool atScale {
			get {
				if (!target)
					return false;

				var delta = transform.localScale - target.transform.localScale;
				return delta.sqrMagnitude < Threshold * Threshold;
			}
		}

		#endregion

		#region Unity Callers

		void Update () {

			bool complete = true;

			if (!target) {
				enabled = false;
				return;
			}

			if (applyPosition) {
				transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime * speed);
				complete = complete ? atPosition : false;
			}

			if (applyRotation) {
				transform.rotation = Quaternion.Slerp(transform.rotation, target.rotation, Time.deltaTime * speed);
				complete = complete ? atRotation : false;
			}

			if (applyScale) {
				transform.localScale = Vector3.Lerp(transform.localScale, target.localScale, Time.deltaTime * speed);
				complete = complete ? atScale : false;
			}

			if (complete)
				enabled = false;

			if (complete && onComplete != null)
				onComplete.Invoke ();


		}

		#endregion
	}

	#region Interpolator2D Extensions
	internal static class InterpolatorTransformExtensions {
		
		internal static Interpolator Interpolate(this Transform transform, Transform target, bool applyPosition = true, bool applyRotation = true, bool applyScale = false, UnityAction onComplete = null)
		{
			var interpolator = transform.GetComponent<Interpolator> () ?? transform.gameObject.AddComponent<Interpolator> ();
			
			interpolator.applyPosition = applyPosition;
			interpolator.applyRotation = applyRotation;
			interpolator.applyScale = applyScale;
			
			interpolator.target = target;
			interpolator.enabled = true;

			if (interpolator.onComplete == null)
				interpolator.onComplete = new UnityEvent ();

			if (onComplete != null)
				interpolator.onComplete.AddListener (onComplete);
			
			return interpolator;
		}

		internal static bool IsInterpolating(this Transform transform)
		{
			var interpolator = transform.GetComponent<Interpolator> ();
			
			return interpolator && interpolator.enabled;
		}
		
	}
	#endregion
}