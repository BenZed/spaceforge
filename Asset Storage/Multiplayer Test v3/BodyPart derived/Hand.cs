using UnityEngine;
using BossMedia;

namespace SpaceForge {

	//[RequireComponent(typeof(HingeJoint2D))]
	public class Hand : BodyPart {

		const float DoubleHandYThresh = 0.3f;
		const float GrabRadius = 0.1f;
		const float BehindDistanceThresh = 0.1f;

		#region Links

		Hand otherHand;

		DistanceJoint2D joint {
			get {
				return sapien.GrabJoint;
			}
		}

		#endregion

		#region Unity Callers

		void Start()
		{
			otherHand = isLeft ? sapien.rightHand : sapien.leftHand;
		}

		#endregion

		#region BodyPart override

		protected override void MoveTurn ()
		{
			targetPosition = restingPos;
			targetRotation = restingRot;

			sprite = closed;

			if (sapien.grab && (ClosestHand (sapien.aim) || InDoubleGrabRange (sapien.aim))) {

				if (!joint.connectedBody)
					sprite = grab;
					
				targetPosition = sapien.aim;
				targetRotation = restingPos.AbsoluteAngle (targetPosition) + (_isLeft ? -90f : 90f);

			} else if (!sapien.grab && joint.connectedBody) {

				joint.enabled = false;
				joint.connectedBody = null;

			}
		}

		protected override void LimitMoveTurn ()
		{
			if (sapien.grab && !joint.connectedBody) {
				var units = Unit.Find (transform.position, GrabRadius);
				foreach (var found in units) {
					if (found == sapien)
						continue;
				
					Equippable equip = found.Property<Equippable>(e => {
						if (!e.sapien)
							e.unit.parent = sapien.transform;
					});

					if (equip)
						break;

					found.Property<Physical>(phys => {

						joint.enabled = true;
						joint.connectedBody = phys.body;
						joint.connectedAnchor = phys.transform.InverseTransformPoint(transform.position);
						joint.anchor = (Vector2)sapien.transform.InverseTransformPoint (transform.position);
						joint.distance = 0f;
						
					});

					if (joint.connectedBody)
						break;
				}
			}

			if (sapien.grab && joint.connectedBody) {
				targetPosition = joint.anchor;
				targetRotation = restingPos.AbsoluteAngle (joint.anchor) + (_isLeft ? -90f : 90f);

			} else {
				base.LimitMoveTurn ();
				if (targetPosition.x > BehindDistanceThresh)
					targetPosition.x = BehindDistanceThresh;
			}

		}

		#endregion

		bool ClosestHand(Vector2 point)
		{
			if (!otherHand || otherHand.joint.connectedBody)
				return true;

			float thisDist = (point - restingPos).sqrMagnitude;
			float otherDist = (point - otherHand.restingPos).sqrMagnitude;

			return thisDist < otherDist;
		}

		bool InDoubleGrabRange(Vector2 point)
		{
			return Mathf.Abs (point.y) <= DoubleHandYThresh;
		}


		[SerializeField] Sprite grab;
		internal Sprite grabSprite {
			get {
				return grab;
			}
			set {
				grab = value;
			}
		}

		[SerializeField] Sprite closed;
		internal Sprite closedSprite {
			get {
				return closed;
			}
			set {
				closed = value;
			}
		}

		[SerializeField] bool _isLeft;
		internal bool isLeft {
			get {
				return _isLeft;
			}
		}
		
	}
	
}