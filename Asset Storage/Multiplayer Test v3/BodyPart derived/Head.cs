using UnityEngine;
using UnityExtensions;

namespace SpaceForge {

	public class Head : BodyPart {

		#region BodyPart override
		
		protected override void MoveTurn ()
		{
			targetRotation = Vector2.zero.AbsoluteAngle(sapien.aim);
		}
		
		#endregion

	}

}
