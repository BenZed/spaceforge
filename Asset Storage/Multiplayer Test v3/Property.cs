﻿using UnityEngine;
using UnityEngine.Networking;
using System.Reflection;
using System;

namespace SpaceForge {

	[RequireComponent(typeof(Unit))]
	public abstract class Property : NetworkBehaviour {

		Unit _unit;
		public Unit unit {
			get {
				return _unit ?? (_unit = GetComponent<Unit>());
			}
		}

		#region Game Constants Link
		
		internal static Game.Constants constants {
			get {
				return Game.manager.constants;
			}
		}
		
		#endregion

		#region Runtime Caller Creator 
		
		internal static T CreateRuntimeCaller<T>(Unit unit, string methodName) where T : class {
			
			Type type = unit.GetType();
			MethodInfo method = type.GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
			
			if (method == null)
				return null;
			
			try {
				return Delegate.CreateDelegate(typeof(T), unit, method) as T;
			} catch {
				return null;
			}
			
		}
		
		#endregion
		 
	}
}