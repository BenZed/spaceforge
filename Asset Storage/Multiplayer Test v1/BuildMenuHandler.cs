﻿using UnityEngine;
using System.Collections;
using SpaceForge;
using SpaceForge.Structural;
using StructureCursor = SpaceForge.Structural.Cursor;

public class BuildMenuHandler : MonoBehaviour {

	[SerializeField] Structure buildingStructure;
	StructureCursor cursor;

	void Start()
	{
		var pos = CameraController.instance.transform.position;
		buildingStructure.transform.position = new Vector3 (pos.x, pos.y, 0f);

		cursor = StructureCursor.Create (buildingStructure);

	}

	void Update()
	{
		var mouse = CameraController.instance.cam.ScreenToWorldPoint (Input.mousePosition + Vector3.forward * 10f);

		cursor.Move (mouse);
		cursor.Apply(Input.GetMouseButton(0), Input.GetKey(KeyCode.LeftShift));

	}

	public void PaintLightTiles()
	{
		cursor.paintType = StructureCursor.PaintType.Floor;
		cursor.tileType = Tile.Type.Light; 
	}

	public void PaintRegularTiles()
	{
		cursor.paintType = StructureCursor.PaintType.Floor;
		cursor.tileType = Tile.Type.Regular; 
	}

	public void PaintArmoredTiles()
	{
		cursor.paintType = StructureCursor.PaintType.Floor;
		cursor.tileType = Tile.Type.Armoured; 
	}

	public void PaintWalls()
	{
		cursor.paintType = StructureCursor.PaintType.Wall;
	}
}
