using UnityEngine;
using System.Collections;
using SpaceForge;
using SpaceForge.Structural;

public class ContainLevelHandler : MonoBehaviour {

	[SerializeField] Ship containShip;

	void Start()
	{
		CameraController.instance.snapUnit = containShip;
	}

}
